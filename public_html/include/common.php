<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) V!M@L || vimal444@hotmail.com
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * 
 * Common functions 
 * 
 * Contains commonly used functions
 * 
 * @author		Vimal
 * @version		1
 * @date		2017 10 26
 * @copyright	<vimal> : vimal@asadhi.com
 * 
 */

/**
 * Aborts with a Fatal Error warning to user and mail alert to admin
 *
 * @param	string	Error description
 * @param	string|array	Error message. If array is provided, first value will be 
 * 							displayed to user and second will be mailed to admin
 * @param	string	Script that generated the error
 * @return	void
 */
function FatalError($message = '', $error = '', $title = '', $script = NULL, $arExtra = NULL) {
	$errorContext = FALSE;
	$skipTraceLevel = 3;
	if (is_array($arExtra)) extract($arExtra);
	if (is_array($error)) {
		$mailError = $error[1];
		$error = $error[0];
	} else {
		$mailError = $error;
	}
	if (!isset($error_logged) || !$error_logged) error_log("[Fatal Error] " . strip_tags($mailError));
	report(array('title' => $title, 'description' => $message, 'message' => "<b>Error</b> : $mailError", 'script' => $script, 'type' => 'alert', 'detailed' => TRUE, 'errorContext' => $errorContext, 'skipTraceLevel' => $skipTraceLevel));
	if (!headers_sent()) {
		header('HTTP/1.1 503 Service Temporarily Unavailable');
		header('Status: 503 Service Temporarily Unavailable');
	}
	include TEMPLATE_PATH . '/error.tpl.php';
	exit(1);
}

/**
 * Prints an array/variable as preformatted text for viewing in Browser
 *
 * @param	mixed	$var The array/variable to be printed
 * @return	void
 */
function printr($var) {
	$args = func_get_args();
	if (PHP_SAPI === 'cli') {
		$str = '';
		foreach ($args as $arg) {
			$type = gettype($arg);
			if ($type === 'boolean') {
				$str .= "\033[01;34m" . ($arg ? 'TRUE' : 'FALSE') . " \033[0m ";
			} elseif ($type === 'NULL') {
				$str .= "\033[01;34mNULL \033[0m ";
			} else {
				$str .= print_r($arg, true) . ' ';
			}
		}
		echo substr($str, 0, -1) . "\n";
	} else {
		echo '<pre>';
		$str = '';
		foreach ($args as $arg) {
			$type = gettype($arg);
			if ($type === 'boolean') {
				$str .= '<em style="color:blue;">' . ($arg ? 'TRUE' : 'FALSE') . '</em> ';
			} elseif ($type === 'NULL') {
				$str .= '<em style="color:blue;">NULL</em> ';
			} else {
				$str .= htmlspecialchars(print_r($arg, true), ENT_COMPAT|ENT_SUBSTITUTE|ENT_HTML5) . ' ';
			}
		}
		echo substr($str, 0, -1);
		echo '</pre>';
	}
}

function admin_printr() {
	if ($GLOBALS['userAuth']->isAdmin()) {
		call_user_func_array('printr', func_get_args());
	}
}

/**
 * Checks whether the current User Agent is a mobile browser by checking against various known mobile browsers
 * 
 * @date		$Date: 2010-07-17 11:19:00 +0530 (Sat, 17 Jul 2010	) $
 *
 * @param	void	
 * @return	boolean	TRUE if mobile browser, else FALSE
 */
function isMobileDevice() {
	$user_agent = strtolower(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
	$http_accept = strtolower(isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : '');
	$ar_user_agent = array(
		'acs-','alav','alca','amoi','audi','aste','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco',
		'eric','hipt','htc','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp',
		'mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','opwv','palm','pana','pant','pdxg','phil','play','pluc',
		'port','prox','qtek','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar',
		'sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr',
		'webc','winw','winw','xda-'
	);
	if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])
		|| (strpos($http_accept, 'text/vnd.wap.wml') !== FALSE) || (strpos($http_accept, 'application/vnd.wap.xhtml+xml') !== FALSE) 
		|| preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|vodafone|o2|pocket|mobile|pda|psp)/i', $user_agent)
		|| in_array(substr($user_agent, 0, 4), $ar_user_agent)) {
		return true;
	}
	return false;
}

/**
 * Add parameters to query string
 * 
 * @date		$Date: 2010-07-17 11:19:00 +0530 (Sat, 17 Jul 2010	) $
 *
 * @param	string	$qs		Original url / query string
 * @param	boolean	$ignoreEmpty	Whether or not to ignore empty parameters. This parameter 
 * 									can be omitted, in case of default value
 * @param	mixed	$param	Parameters to append as array or string. Any number of arrays can be passed.
 * 
 * @return	string	The new url / query string
 */
function qsAddParam($qs, $ignoreEmpty = TRUE, $param = array()) {
	$args = func_get_args();
	$cnt = count($args);
	$i = 2;
	if (!is_bool($args[1])) {
		$ignoreEmpty = TRUE;
		$i--;
	}
	if ($i < $cnt - 1) {
		$param = array();
		for (; $i < $cnt; $i++) {
			if (is_array($args[$i])) {
				$param = array_merge($param, $args[$i]);
			}
		}
	} else {
		$param = $args[$i];
	}
	if (is_array($param)) {
		$ar = array();
		if ($ignoreEmpty) {
			foreach ($param as $k => $v) {
				if (!empty($v)) $ar[] = $k . '=' . urlencode($v);
			}
		} else {
			foreach ($param as $k => $v) $ar[] = $k . '=' . urlencode($v);
		}
		$param = implode('&', $ar);
	}
	if (empty($param)) {
		return $qs;
	} else {
		return $qs .= ((strpos($qs, '?') === FALSE) ? "?$param" : "&$param");
	}
}

/**
 * Sends HTTP headers for requesting client side caching of a page
 *
 * @param	boolean	$cache Specifies whether the page has to be cached or not
 * @param	integer $maxAge Time in seconds to save the cache
 * @return	void
 */
function cache($cache = TRUE, $maxAge = 3600) {
	if ($cache === TRUE) {
		header("Cache-Control: max-age=$maxAge, must-revalidate");
		header('Expires: ' . gmdate('D, d M Y h:i:s T', TIME_NOW + $maxAge));
	} else {
		header('Cache-Control: no-cache');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
	}
}

#===========================================================================#
# * Function for creating the code for image verification                   #
# * @param  : [$code] The unique code Id                                    #
# * @return : The created code                                              #
#===========================================================================#
function verifyCode($code, $codeLen = 4, $advanced = true) {
	if (!defined('ADVANCED_CAPTCHA') && $advanced !== true) {
		$random = md5(md5($code));
		$code = '';
		for ($i = 1, $len = 0; $len < $codeLen && $i < 33; $i++) {
			if (is_numeric($random{$i})) {
				$code .= $random{$i};
				$len++;
			}
		}
	} else {
		$i = ord($code[0]) % 10;
		$salt = strtoupper(crypt(substr(md5($code), $i, $i + 10), CRYPT_KEY));
		$i = ord($salt[$i]) % 10;
		$code = '';
		for ($j = 0, $i = strlen($salt) - 1; $i >= 0; $i--) {
			$ord = ord($salt[$i]);
			if (($ord >= 65 && $ord <= 90) /* || ($ord > 48 && $ord <= 57) */) {
				$code .= $salt[$i];
				if (++$j == $codeLen)break;
			}
		}
	}
	return $code;
}

/**
 * Function for checking the validity of an email address
 * 
 * @date		$Date: 2010-06-30 19:42:00 +0530 (Wed, 30 Jun 2010	) $
 *
 * @param	string	Email address
 * @return	boolean	TRUE if valid, else FALSE
 */
function isValidEmail($email) {
	return (boolean)preg_match ("/^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}\$/i", $email);
}

#===========================================================================#
# * Function for getting the URL of the current page                        #
# * @param  : Void                       					                #
# * @return : The URL of the current page                                   #
#===========================================================================#
function getCurrentPageUrl() {
	static $pageURL = '';
	if (empty($pageURL)) {
		$pageURL = 'http';
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')$pageURL .= 's';
		$pageURL .= '://';
		if ($_SERVER['SERVER_PORT'] != '80')$pageURL .= $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		else $pageURL .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	}
	return $pageURL;
}

/**
 * Generates HTML code for a pagination menu
 * 
 * @date		$Date: 2010-06-30 20:14:00 +0530 (Wed, 30 Jun 2010	) $
 *
 * @param	integer	Current page number
 * @param	integer	Total number of items
 * @param	integer	Number of items per page
 * @param	string	File prefix / parameter name (if url rewriting is not enabled)
 * @param	boolean	Enable / Disable url rewriting
 * @return	string	HTML code for the pagination menu
 */

function genNavBar($nCurrentPage, $nTotalEntries, $nEntriesPerPage = 10, $linkName = null, $enableRewrite = false) {
	$nCurrentPage = (int)$nCurrentPage;
	$nCurrentPage = ($nCurrentPage >= 1) ? $nCurrentPage : 1;
	if ($nEntriesPerPage < 1)$nEntriesPerPage = 10;
	if (!$linkName) {
		$linkName = 'http';
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')$linkName .= 's';
		$linkName .= '://';
		if ($_SERVER['SERVER_PORT'] != '80')$linkName .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['PHP_SELF'];
		else $linkName .= $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
		$linkName .= '?page=';
	} elseif (is_assoc_array($linkName)) {
		if (isset($linkName['navBaseUrl'])) {
			$baseUrl = $linkName['navBaseUrl'];
		} else {
			$baseUrl = end(explode('/', $_SERVER['SCRIPT_FILENAME']));			
		}
		$baseUrl .= '?';
		$pageVar = isset($linkName['navPageVar']) ? $linkName['navPageVar'] : 'page';
		unset($linkName['navBaseUrl'], $linkName['navPageVar']);
		foreach ($linkName as $k => $v) {
			if (!empty($v))$baseUrl .= "$k=$v&amp;";
		}
		$linkName = "$baseUrl$pageVar=";
		
	}
	if ($enableRewrite) {
		if (substr($linkName, -1) != '-')$linkName .= '-';
		$linkExt = '.html';
	} else $linkExt = '';
	if ($nTotalEntries != 0) {
		$nTotalPages = ceil($nTotalEntries / $nEntriesPerPage);
		$nav_bar = "<table class='tblNavMenu' cellspacing='1' cellpadding='0'><tr class='Menu'><td class='Info'>Page $nCurrentPage of $nTotalPages</td>";
		if ($nCurrentPage > 1)$nav_bar .= "<td><a title='Previous Page' href='$linkName".($nCurrentPage - 1)."$linkExt'>&laquo; </a></td>";
		else $nav_bar .= '<td><a title="This is the first page">&laquo;</a></td>';
		if ($nTotalPages < 8) {
			for ($i = 1; $i <= $nTotalPages; $i++) {
				if ($i == $nCurrentPage)$nav_bar .= "<td class='CurPage'><a>$i</a></td>";
				else $nav_bar .= "<td><a href='$linkName$i$linkExt'>$i</a></td>";
			}
		} else {
			if ($nCurrentPage <= 4) {
				for ($i = 1; $i <= $nCurrentPage + 2; $i++) {
					if ($i == $nCurrentPage)$nav_bar .= "<td class='CurPage' title='Current Page'><a>$i</a></td>";
					else $nav_bar .= "<td><a title='Page $i' href='$linkName$i$linkExt'>$i</a></td>";
				}$nav_bar .= "<td><a>...</a></td><td><a title='Last Page' href='$linkName$nTotalPages$linkExt'>$nTotalPages</a></td>";
			} elseif ($nCurrentPage >= $nTotalPages - 3) {
				$nav_bar .= "<td><a title='First Page' href='$linkName" . "1$linkExt'>1</a></td><td><a>...</a></td>";
				for ($i = $nCurrentPage - 2; $i <= $nTotalPages; $i++) {
					if ($i == $nCurrentPage)$nav_bar .= "<td class='CurPage'><a title='Current Page'>$i</a></td>";
					else $nav_bar .= "<td><a title='Page $i' href='$linkName$i$linkExt'>$i</a></td>";
				}
			} else {
				$nav_bar .= "<td><a title='First Page' href='$linkName" . "1$linkExt'>1</a></td><td><a>...</a></td>";
				for ($i = $nCurrentPage - 2; $i <= $nCurrentPage + 2; $i++) {
					if ($i == $nCurrentPage)$nav_bar .= "<td class='CurPage'><a title='Current Page'>$i</a></td>";
					else $nav_bar .= "<td><a title='Page $i' href='$linkName$i$linkExt'>$i</a></td>";
				}$nav_bar .= "<td><a>...</a></td><td><a title='Last Page' href='$linkName$nTotalPages$linkExt'>$nTotalPages</a></td>";
			}
		}
		if ($nCurrentPage < $nTotalPages)$nav_bar .= "<td><a title='Next Page' href='$linkName".($nCurrentPage+1)."$linkExt'>&raquo;</a></td>";
		else $nav_bar .= '<td><a title="This is the last page">&raquo;</a></td>';
		return $nav_bar . '</tr></table>';
	} else {
		return false;
	}
}

#===========================================================================#
# * Function for checking whether a given array is associative or not       #
# * @param  : [$array] Array to be checked					                #
# * @return : TRUE if valid else FALSE		                                #
#===========================================================================#
function is_assoc_array($var) {
	if (!is_array($var))return 0;
	$keys = array_keys($var);
	return (array_keys($keys) !== $keys);
}
#===========================================================================#
# * Function for creating a unique/random phrase                            #
# * @param  : [$chars] No of characters to be returned                      #
# * @return : The unique/random phrase                                      #
#===========================================================================#
function uniqueID($chars, $saltType = 4) {
	$phrase = 0;
	$salt_salpha = 'abcdefghijklmnpqrstuvwxyz';
	$salt_calpha = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
	$salt_num = '0123456789';
	if ($saltType == 0)$salt = $salt_salpha . $salt_calpha . $salt_num;
	elseif ($saltType == 1)$salt = $salt_calpha;
	elseif ($saltType == 2)$salt = $salt_salpha . $salt_calpha;
	elseif ($saltType == 3)$salt = $salt_salpha . $salt_num;
	elseif ($saltType == 4)$salt = $salt_calpha . $salt_num;
	else $salt = $salt_num;
	srand((double)microtime()*1000000);
	$maxOff = strlen($salt) - 1;
	while ($phrase === 0) $phrase = $salt[mt_rand(0, $maxOff - 1)];
	for ($i = 1; $i < $chars; $i++)$phrase .= $salt[mt_rand(0, $maxOff)];
	return $phrase;
}
function makeSafeName($title, $len = NULL, $def = NULL, $separator = '-') {
	$title = preg_replace('/[^a-z0-9]+/i', $separator, strtolower(html_entity_decode($title, ENT_QUOTES, 'UTF-8')));
	$title = preg_replace("/\$separator+/", $separator, $title);
	$title = trim($title, $separator);
	if (!is_null($len) && (strlen($title) > $len)) {
		$pos = strrpos(substr($title, 0, $len), $separator);
		if ($pos)$title = substr($title, 0, $pos);
	}
	return !empty($title) ? $title : (is_null($def) ? 'page' : $def);
}
#===========================================================================#
# * Function for escaping a String                                          #
# * @param  : [$str] The required String                                    #
# * @return : The converted String                                          #
#===========================================================================#
function addSlash($str) {
	if (!get_magic_quotes_gpc()) {
		$str = addslashes($str);
	}
	return $str;
}
#===========================================================================#
# * Function for replacing new line character with break                    #
# * @param  : [$str] Original string                                        #
# * @return : Replaced string                                               #
#===========================================================================#
function nl2break($str) {
	return preg_replace('#\r?\n#', '<br />', $str);
}
function nl2para($str) {
	$str =  preg_replace(array('#(\r?\n) {2,}(\s+)?#', '#\r?\n#'), array("</p><p>", '<br/>'), $str);
	return !empty($str) ? "<p>$str</p>" : '';
}
#===========================================================================#
# * Function for replacing break with new line character                    #
# * @param  : [$str] Original string                                        #
# * @return : Replaced string                                               #
#===========================================================================#
function break2nl($str) {
	return  preg_replace('/\<br\s*\/?\>/i', "\n", $str);
}
function BBCodeFormat($str, $removeTags = FALSE, $linkReplace = false, $emailReplace = '') { 
	$str = $removeTags ? html2text($str) : htmlspecialchars($str); 
	$bbcode = array("<", ">",
		"[list]", "[*]", "[/list]", 
		"[img]", "[/img]", 
		"[b]", "[/b]", 
		"[u]", "[/u]", 
		"[i]", "[/i]",
		'[color="', "[/color]",
		"[size=\"", "[/size]",
		'[url="', "[/url]",
		"[mail=\"", "[/mail]",
		"[code]", "[/code]",
		"[quote]", "[/quote]",
		'"]'
	);
	$htmlcode = array("&lt;", "&gt;",
		"<ul>", "<li>", "</ul>", 
		"<img src=\"", "\">", 
		"<b>", "</b>", 
		"<u>", "</u>", 
		"<i>", "</i>",
		"<span style=\"color:", "</span>",
		"<span style=\"font-size:", "</span>",
		'<a href="', "</a>",
		"<a href=\"mailto:", "</a>",
		"<code>", "</code>",
		"<table width=100% bgcolor=lightgray><tr><td bgcolor=white>", "</td></tr></table>",
		'">'
	);
	$str = str_replace($bbcode, $htmlcode, $str);
	$str = nl2break($str);//second pass
	// Remove Links
	if ($linkReplace) {
		$str = is_string($linkReplace) ? removeLink($str, $linkReplace, $emailReplace) : removeLink($str);
	}
	// Replace Quotes
	$open = '<blockquote>'; 
    $close = '</blockquote>'; 
    preg_match_all ('/\[quote\]/i', $str, $matches); 
    $opentags = count($matches['0']); 
    preg_match_all ('/\[\/quote\]/i', $str, $matches); 
    $closetags = count($matches['0']); 
    $unclosed = $opentags - $closetags; 
    for ($i = 0; $i < $unclosed; $i++)$str .= '</blockquote>'; 
    $str = str_replace ('[quote]', $open, $str); 
    $str = str_replace ('[/' . 'quote]', $close, $str); 
    return $str; 
}
function BBCodeFormat1($str, $removeTags = FALSE, $linkReplace = false, $emailReplace = '') { 
	$str = $removeTags ? html2text($str) : htmlspecialchars($str); 
	$str = nl2br($str);
    $pattern = array( 
		'/\[b\](.*?)\[\/b\]/is',
		'/\[i\](.*?)\[\/i\]/is',
		'/\[u\](.*?)\[\/u\]/is',
		'/\[url\=(.*?)\](.*?)\[\/url\]/is',
		'/\[url\](.*?)\[\/url\]/is',
		'/\[align\=(left|center|right)\](.*?)\[\/align\]/is',
		'/\[img\](.*?)\[\/img\]/is',
		'/\[mail\=(.*?)\](.*?)\[\/mail\]/is',
		'/\[mail\](.*?)\[\/mail\]/is',
		'/\[font\=(.*?)\](.*?)\[\/font\]/is',
		'/\[size\=(.*?)\](.*?)\[\/size\]/is',
		'/\[color\=(.*?)\](.*?)\[\/color\]/is',
	);

    $replace = array( 
         '<strong>$1</strong>',
		'<em>$1</em>',
		'<u>$1</u>', 
		'<a href="$1">$2</a>', 
		'<a href="$1">$1</a>', 
		'<div style="text-align: $1;">$2</div>', 
		'<img src="$1" />', 
		'<a href="mailto:$1">$2</a>', 
		'<a href="mailto:$1">$1</a>', 
		'<span style="font-family: $1;">$2</span>', 
		'<span style="font-size: $1;">$2</span>', 
		'<span style="color: $1;">$2</span>', 
	); 
	// Replace Tags
    $str = preg_replace ($pattern, $replace, $str); 
	// Remove Links
	if ($linkReplace) {
		$str = is_string($linkReplace) ? removeLink($str, $linkReplace, $emailReplace) : removeLink($str);
	}
	// Replace Quotes
	$open = '<blockquote>'; 
    $close = '</blockquote>'; 
    preg_match_all ('/\[quote\]/i', $str, $matches); 
    $opentags = count($matches['0']); 
    preg_match_all ('/\[\/quote\]/i', $str, $matches); 
    $closetags = count($matches['0']); 
    $unclosed = $opentags - $closetags; 
    for ($i = 0; $i < $unclosed; $i++)$str .= '</blockquote>'; 
    $str = str_replace ('[quote]', $open, $str); 
    $str = str_replace ('[/' . 'quote]', $close, $str); 
    return $str; 
} 

function removeLink($str, $linkReplace = '', $emailReplace = '') {
	$str = link2url($str, '', FALSE);
	$patterns = array(
		"/([a-z0-9_\-.]+@(([a-z0-9_]|\\-)+\\.)+((net|org|com|tv)|co\.[a-z]{2}))/",
		"/(h?[ft]tp:[^\s*\"|:<>]+)/i",
		"/([a-z0-9]*[a-z0-9\-\.]+\.((net|org|com|tv)|co\.[a-z]{2})[^\s*\"|:<>]*)/i"
	);
	$replace  = array(
		$emailReplace,
		$linkReplace,
		$linkReplace
	);

	return preg_replace($patterns, $replace, $str);

}
#**********=================================================================#
# * Function for converting link to url                                     #
# * @param  : [$document]The html document to be converted                  #
# * @param  : [$basehref]The base url for local links                       #
# * @return : The converted text                                            #
#===========================================================================#
function link2url($document, $basehref='', $showLinks = true) {
	if (empty($basehref))$basehref = 'http://' . $_SERVER['HTTP_HOST'];
	$start   = "<a\s[^>]*href=";                                    // Start of the Anchor<a> tag
	$mail_q  = "[\'\"]mailto:([^\'\"]+)[\'\"]";                     // Quoted mailto 
	$mail_u  = "mailto:([^\s>]+)";                                  // Unquoted mailto 
	$link_q  = "[\'\"](h?[ft]tp:[^\'\"]+)[\'\"]";                   // Quoted http/ftp
	$link_u  = "(h?[ft]tp:[^\s>]+)";                                // Unquoted http/ftp
	$local_q = "[\'\"]([^\'\">]+)[\'\"]";                           // Quoted local links
	$local_u = "([^\s>]+)";                                         // Unquoted local links
	$end     = "[^>]*>(.+?)<\/a>";                                  // end of A link 

	$search  = array(
			   "'$start(?:$local_q|$local_u)$end'i",				// Local links
			   "'<a\s[^>]*>(.*)<\/a>'i");							// Others
	if ($showLinks) {
		$document = preg_replace_callback("'$start(?:$mail_q|$mail_u|$link_q|$link_u)$end'i", /* Quoted and Unquoted http/ftp/mailto*/
			create_function('$match', '$url = "$match[1]$match[2]$match[3]$match[4]";return $match[5] == $url ? $url : "$match[5] [$url]";'),
			$document);
		$replace = array('\3 [ '.$basehref.'/\1\2 ]','\1'); 
	} else {
		$document = preg_replace("'$start(?:$mail_q|$mail_u|$link_q|$link_u)$end'i", '\5', $document);
		$replace = array('\3','\1'); 
	}
	return preg_replace($search, $replace, $document); 
}


#===========================================================================#
# * Function for converting HTML string to plain text                       #
# * @param  : [$document ]The HTML string / file                            #
# * @param  : [$allowtags]Tags to be left unchanged                         #
# * @param  : [$basehref ]Tags to be left unchanged                         #
# * @param  : [$wordwrap ]Chars / line of $text                             #
# * @param  : [$isFile   ]Whether $document is a file or not                #
# * @return : The converted string                                          #
#===========================================================================#
function html2text($document, $allowtags = '', $basehref = '', $wordwrap = '70', $isFile = false) {
	if ($isFile && file_exists($document)) {
	    $handler = fopen($document, 'r');
	    $document = fread($handler, filesize($document));
	    fclose($handler);
	}
	if (empty($basehref))$basehref = 'http://' . $_SERVER['HTTP_HOST'];
	$document = link2url($document, $basehref);
	$search = array(
		"'\r'",											  // Carriage Return
		"'[\n\t]+'",									  // Newline and Tabs
        "'<script[^>]*>.*?<\/script>'si",				  // Script Tags
        "'<style[^>]*>.*?<\/style>'si",			    	  // Stylesheet Tags
        "'<!-- .* -->'",                                  // HTML Comments
        "'<p[^>]*>'i",									  // Paragraph <p>
        "'<br[^>]*>'i",									  // Break <br>
        "'<hr[^>]*>'i",                                   // Horizontal Rule<hr>
        "'<i[^>]*>(.+?)<\/i>'i",						  // Italics <i>
        "'(<ul[^>]*>|<\/ul>)'i",						  // Unordered List <ul>
        "'(<ol[^>]*>|<\/ol>)'i",                          // Ordered List <ol>
        "'<li[^>]*>'i",                                   // List <li>
        "'(<table[^>]*>|<\/table>)'i",                    // Table <table>
        "'(<tr[^>]*>|<\/tr>)'i",                          // Table Row <tr>
        "'<td[^>]*>(.+?)<\/td>'i",                        // Table Cell <td>
		"'&(quot|#34);'i",		                          // Replace HTML entities
		"'&(amp|#38);'i",
		"'&(lt|#60);'i",
		"'&(gt|#62);'i",
		"'&(nbsp|#160);'i",
		"'&(iexcl|#161);'i",
		"'&(cent|#162);'i",
		"'&(pound|#163);'i",
		"'&(copy|#169);'i",
		"'&(bull|#149);'i",
		"'&(trade|#8482);'i",
		"'&(reg|#174);'i",
		"'&(laquo|#171);'i",
		"'&(raquo|#187);'i",
		"'&(euro|#128);'i",
		"'&(deg|#176);'i",
	);
	$replace = array(
		"",												  // Carriage Return
		" ",											  // Newline and Tabs
        "",												  // Script Tags
        "",									              // Stylesheet Tags
        "",									              // HTML Comments
        "\n\n\t",										  // Paragraph <p>
        "\n",                                             // Break <br>
        "\n-------------------------------------------\n",// Horizontal Rule<hr>
        "_\\1_",                                          // Italics <i>
        "\n\n",                                           // Unordered List <ul>
        "\n\n",                                           // Ordered List <ol>
        "\t*",                                            // List <li>
        "\n\n",                                           // Table <table>
        "\n",                                             // Table Row <tr>
        "\t\t\\1\n",                                      // Table Cell <td>
		"\"",                                             // Replace HTML entities
		"&",
		"<",
		">",
		chr(128),
		chr(149),
		" ",
		chr(161),
		chr(162),
		chr(163),
		chr(169),
		chr(171),
		chr(174),
		chr(176),
		chr(187),
		chr(8482),
	);
	$text = preg_replace($search, $replace, $document);

	$text = preg_replace_callback("'<h[123][^>]*>(.+?)<\/h[123]>'i", function ($m) {return strtoupper("\n\n$m[1]\n\n");}, $text); // Headings <h1> - <h3>
	$text = preg_replace_callback("'<h[456][^>]*>(.+?)<\/h[456]>'i", function ($m) {return ucwords("\n\n$m[1]\n\n");}, $text); // Headings <h4> - <h6>
	$text = preg_replace_callback("'<b[^>]*>(.+?)<\/b>'i", function ($m) {return strtoupper($m[1]);}, $text); // Bold <b>
	$text = preg_replace_callback("'&#(\d+);'", function ($m) {return chr($m[1]);}, $text); // Evaluate as PHP
	$text = strip_tags($text, $allowtags);
	$text = preg_replace(array("/\n\s+\n/", "/[\n]{3,}/"), array("\n", "\n\n"), $text);

	if ($wordwrap)$text = wordwrap($text, $wordwrap);
	return $text;
}

/**
 * Encrypts a string. Use @strDecrypt for decryption
 *
 * @param	string	$str		String to encrypt
 * @param	string	$key		Key to use for encryption
 * @param	string	$algorithm	Algorithm to use for encryption @see http://in3.php.net/manual/en/mcrypt.ciphers.php
 * @param	string	$mode		Mode for encryption @see http://in3.php.net/manual/en/mcrypt.constants.php
 * @return	void
 */

function strEncrypt($str, $key = CRYPT_KEY, $algorithm = MCRYPT_CAST_128, $mode = MCRYPT_MODE_ECB) {
	if (is_null($key)) {
		$key = CRYPT_KEY;
	}
	$cipher = mcrypt_module_open($algorithm, '', $mode, '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($cipher), MCRYPT_RAND);
	$key = substr($key, 0, mcrypt_enc_get_key_size($cipher));
	mcrypt_generic_init($cipher, $key, $iv);            
	$str = mcrypt_generic($cipher, $str);
	mcrypt_generic_deinit($cipher);
	mcrypt_module_close($cipher);
	return base64_encode($str);
}

/**
 * Decrypts a string encrypted with @strEncrypt
 *
 * @param	string	$str		Encrypted string
 * @param	string	$key		Key used for encryption
 * @param	string	$algorithm	Algorithm used for encryption @see http://in3.php.net/manual/en/mcrypt.ciphers.php
 * @param	string	$mode		Mode for decryption @see http://in3.php.net/manual/en/mcrypt.constants.php
 * @return	void
 */

function strDecrypt($str, $key = CRYPT_KEY, $algorithm = MCRYPT_CAST_128, $mode = MCRYPT_MODE_ECB) {
	if (is_null($key)) {
		$key = CRYPT_KEY;
	}
	$cipher = mcrypt_module_open($algorithm, '', $mode, '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($cipher), MCRYPT_RAND);
	$key = substr($key, 0, mcrypt_enc_get_key_size($cipher));
	mcrypt_generic_init($cipher, $key, $iv);
	$str = mdecrypt_generic($cipher, base64_decode($str));
	mcrypt_generic_deinit($cipher);
	mcrypt_module_close($cipher);
	return $str;
}

#===========================================================================#
# * Function for encoding a string                                          #
# * @param  : [$val]The value to be encoded                                 #
# * @return : The encoded string                                            #
#===========================================================================#
function strEncode($val) {
	$e = serialize($val);
	$e = gzcompress($e);
	$e = base64_encode($e);
	$e = urlencode($e);
	return $e;
}

#===========================================================================#
# * Function for decoding a string                                          #
# * @param  : [$val]The value to be decoded                                 #
# * @return : The decoded string                                            #
#===========================================================================#
function strDecode($val) {
	$d = urldecode($val);
	$d = base64_decode($d);
	$d = gzuncompress($d);
	$d = unserialize($d);
	return $d;
}

#===========================================================================#
# * Function for saving a Cookie                                            #
# * @param  : [$key]The key for identifying the Cookie                      #
# * @param  : [$val]The value to be saved                                   #
# * @return : TRUE if successful else FALSE                                 #
#===========================================================================#
function saveCookie($key, $val, $time = FALSE, $dom = FALSE) {
	$z = strEncode($val);
	if (!$time) {
		// If Cookie Life is not specified, set to 30 days
		$time = defined('COOKIE_LIFE') ? COOKIE_LIFE : 259200;
	}
	if (!$dom) {
		// If Cookie Domain is not specified, set to default
		$dom = defined('COOKIE_DOMAIN') ? COOKIE_DOMAIN : null;
	}
	if (!headers_sent()) {
		return setcookie($key, $z, time() + $time, "/", $dom);
	} else {
		return FALSE;
	}
}

#===========================================================================#
# * Function for retrieving a stored a Cookie                               #
# * @param  : [$key]The key for identifying the Cookie                      #
# * @return : The Cookie if successful                                      #
#===========================================================================#
function getCookie($key, $default=NULL, $decrypt = TRUE) {
	if (isset($_COOKIE[$key])) {
		return $decrypt ? strDecode($_COOKIE[$key]) : $_COOKIE[$key];
	} else {
		return $default;
	}
}

#===========================================================================#
# * Function for deleting a stored a Cookie                                 #
# * @param  : [$key]The key for identifying the Cookie                      #
# * @return : TRUE if successful                                            #
#===========================================================================#
function deleteCookie($key, $dom = FALSE) {
	return saveCookie($key, FALSE, -64800, $dom);
}

/**
 * Sends an email report to Site Administrator
 * 
 * @date		$Date: 2010-12-01 11:57:00 +0530 (Wed, 01 Dec 2010	) $
 * 
 * @param	mixed	$arDataORSubject This parameter can be the email subject or an associative array
 * 						with one or more of subject, message, title, description, script & url
 * @param	string	$message Report message. This will override message value set via first parameter
 * @param	string	$tpl Template to be used
 * @param	string	$report Report type [report, alert]
 * @param	boolean	$detailed Include back trace in email
 * @return	boolean TRUE on success, else FALSE
 */
function report($arDataORSubject, $message = NULL, $tpl = NULL, $type = 'report', $detailed = FALSE) {
	static $tmpl = NULL, $tEmail, $fEmail, $defSubject, $url, $referer = FALSE, $skipTraceLevel = 2;
	if (is_null($tmpl)) {
		$fEmail = defined('MAILER_EMAIL') ? MAILER_EMAIL : ('report@' . SITE_DOMAIN);
		$tEmail = $type . '@' . SITE_DOMAIN;
		$defSubject = 'Report from ' . SITE_NAME;
		$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : FALSE;
		$url = (PHP_SAPI == 'cli') ? __FILE__ : '<a href="' . getCurrentPageUrl() . '">' . getCurrentPageUrl() . '</a>';
		@include_once(INCLUDE_PATH . '/PHPTemplate.php');
		$tmpl = new PHPTemplate();
	}
	if (!is_array($arDataORSubject)) {
		$subject = $arDataORSubject;
		$arMessage['message'] = $message;
		$vars['description'] = $arDataORSubject;
	} else {
		extract($arDataORSubject);
		if (is_array($message)) {
		 	$arMessage = $message;
		} else {
			$arMessage['message'] = !empty($message) ? $message : '';
		}
		$vars['description'] = !empty($description) ? $description : '';
	}
	if (empty($vars['title'])) {
		$arMessage['title'] = $defSubject;
	}
	if (empty($subject)) {
		$subject = '[' . SITE_NAME . '] ' . $title;
	}
	if (empty($vars['script'])) {
		$arMessage['script'] = defined('SCRIPT_NAME') ? SCRIPT_NAME : '';
	}
	if ($referer) $arMessage['Referer'] = $referer;
	$arMessage['url'] = $url;

	if (empty($type)) {
		$type = 'report';
	}
	if ($detailed) {
		$arMessage['$GLOBALS'] = '<pre>' . htmlentities(print_r(isset($errorContext) ? $errorContext : $GLOBALS, TRUE)) . '</pre>';
		$arMessage['trace'] = getBackTrace($skipTraceLevel);
	} else if ($type === 'alert') {
		if (!empty($_GET)) $arMessage['$_GET'] = '<pre>' . htmlentities(print_r($_GET, TRUE)) . '</pre>';
		if (!empty($_POST)) $arMessage['$_POST'] = '<pre>' . htmlentities(print_r($_POST, TRUE)) . '</pre>';
		if (!empty($_COOKIE)) $arMessage['$_COOKIE'] = '<pre>' . htmlentities(print_r($_COOKIE, TRUE)) . '</pre>';
	}
	$vars['message'] = $arMessage;
	//$vars['time'] = strftime('%a, %d %B %Y at %I:%M:%S %p', TIME_NOW);
	$message = $tmpl->render($tpl ? $tpl : 'mail_report', $vars, TRUE);
	
	$emailData = array(
		'toName' => SITE_NAME . ' Webmaster', 'toEmail' => $tEmail, 'fromName' => 'Report', 'fromEmail' => $fEmail, 
		'subject' => $subject, 'body' => $message, 'isHTML' => TRUE
	);
	return sendEmail($emailData);
}

/**
 * Sends an email using the specified template file
 * 
 * @date		$Date: 2011-03-01 15:03:00 +0530 (Tue, 01 Mar 2011	) $
 * 
 * @param	array	$data Email options for passing to sendEmail function
 * @param	string	$tpl Template to be used
 * @param	array	$var The array will extracted and passed to the template file
 * @return	boolean TRUE on success, else FALSE
 */
function sendTemplateMessage($data, $tplFile, $var = array()) {
	include_once (INCLUDE_PATH . '/PHPTemplate.php');
	$tpl = new PHPTemplate();
	$message = $tpl->render($tplFile, $var, TRUE);
	if (isset($data['isHTML']) && $data['isHTML']) {
		//$data['body'] = preg_replace('/([^>])\s*\r?\n/', '$1<br/>', $message);//nl2br($message);
		$data['body'] = preg_replace('/([^>\s])\s*(\r?\n)+/', '$1<br/>', $message);//nl2br($message);
		$data['altBody'] = html2text($message);
	} else {
		$data['isHTML'] = FALSE;
		$data['body'] = $message;
	}
	if (!isset($data['fromName'])) $data['fromName'] = MAILER_NAME;
	if (!isset($data['fromEmail'])) $data['fromEmail'] = MAILER_EMAIL;
	return sendEmail($data);
}

function getMailer() {
	// if (!class_exists('PHPMailer')) {
	// 	require_once(INCLUDE_PATH . '/PHPMailer/class.phpmailer.php');
	// }
	$mailer = new PHPMailer();
	// $mailer->SetLanguage('en', INCLUDE_PATH . '/PHPMailer/language/');
	//$mailer->SetCharSet('UTF-8');
	$mailer->CharSet  = 'UTF-8'; 
	$mailer->WordWrap = 70;                                 // Set word wrap to 70 characters
	// $mailer->XMailer = (defined('SITE_NAME') ? SITE_NAME : 'PHP') . ' Mailer';
	$mail_method = !isset($mail_method) ? $GLOBALS['mail_method'] : $mail_method;
	#**************************************************************************************************#
	#********************************         ADD SEND METHOD          ********************************#
	#**************************************************************************************************#
	switch ($mail_method) {
		case 0: 
			$mailer->IsMail();                                  // set the mailer to use mail()
			break;
		case 1: 
			$arConfig = array('smtp_port', 'smtp_host', 'smtp_user', 'smtp_pass', 'smtp_auth');
			foreach ($arConfig as $config) {
				if (!isset($$config)) {
					$$config = $GLOBALS[$config];
				}
			}
			$mailer->IsSMTP();                                  // set mailer to use SMTP
			$mailer->Port = $smtp_port;                         // set the SMTP server port
			$mailer->Host = $smtp_host  ;                       // set SMTP server to use
			$mailer->SMTPAuth = $smtp_auth;                     // turn on SMTP authentication
			$mailer->Username = $smtp_user;                     // set username for SMTP authentication
			$mailer->Password = $smtp_pass;                     // set password for SMTP authentication
			break;
		case 2:
			$mailer->isQmail();                                 // set mailer to use Qmail
			break;
		case 3:
			$sendmail_path = !isset($sendmail_path) ? $GLOBALS['sendmail_path'] : $sendmail_path;
			$mailer->IsSendmail();                              // set mailer to user Sendmail
			$mailer->Sendmail = $sendmail_path;                 // set the path to sendmail binary
			break;
		default:
			echo("<span style='color:red;'><b>Error :</b> Method for sending mail \$mail_method not filled.</span>");//Mail method not set
	}
	return $mailer;
}

#===========================================================================#
# * Function for sending email using phpMailer				                #
# * @param  : array the required arguments					                #
# * @return : TRUE if successful else the Error Data		                #
#===========================================================================#
function sendEmail($data) {
	static $mailer = null;
	extract($data);
	if (is_null($mailer)) {
		$mailer = getMailer();
	}
	#**************************************************************************************************#
	#********************************        ADD SENDER DETAILS        ********************************#
	#**************************************************************************************************#
	$mailer->From = $fromEmail;                                 // Set sender's email address
	$mailer->FromName = $fromName;                              // Set sender's name
	$mailer->Sender = $fromEmail;
	#**************************************************************************************************#
	#********************************          ADD RECIPIENTS          ********************************#
	#**************************************************************************************************#
	if (!empty($toEmail)) {
		if (is_array($toEmail)) {
			for ($i = 0; $i < count($toEmail); $i++)			
				$mailer->AddAddress($toEmail[$i], $toName[$i]);     // Add Recipient
		} else $mailer->AddAddress($toEmail, $toName);               // Add Recipient
	}
	#**************************************************************************************************#
	#********************************           ADD REPLY TO           ********************************#
	#**************************************************************************************************#
	if (!empty($reply)) {
		if (isset($reply['email'])) $reply = array($reply);
		$count = count($reply);
		for ($i = 0; $i < $count; $i++) {
			$mailer->AddReplyTo($reply[$i]['email'], $reply[$i]['name']); // Add Recipient
		}
	}
	#**************************************************************************************************#
	#********************************          ADD ATTACHMENTS         ********************************#
	#**************************************************************************************************#
	if (!empty($attachment)) {
		$count = count($attachment);
		for ($i = 0; $i < $count; $i++) {
			// Add the attachment to the email
			$mailer->AddAttachment($attachment[$i]['file'],$attachment[$i]['name'], $attachment[$i]['encoding'],$attachment[$i]['type']);
		}
	}
	#**************************************************************************************************#
	#********************************            EMBED IMAGE           ********************************#
	#**************************************************************************************************#
	if (!empty($embedImage)) {
		if (!isset($embedImage[0])) $embedImage = array($embedImage);
		$arDef = array('name' => '', 'encoding' => 'base64', 'type' => 'application/octet-stream');
		$count = count($embedImage);
		foreach ($embedImage as $img) {
			$img = array_merge($arDef, $img);
			// Embed the image to the email
			$mailer->AddEmbeddedImage($img['file'],$img['cid'],$img['name'],$img['encoding'],$img['type']); 
		}
	}
	#**************************************************************************************************#
	#********************************          Custom Headers          ********************************#
	#**************************************************************************************************#
	if (!empty($headers)) {
		foreach ($headers as $k => $v) {
				$mailer->AddCustomHeader("$k: $v");
		}
	}
	#**************************************************************************************************#
	#********************************          OTHER SETTINGS          ********************************#
	#**************************************************************************************************#
	if (isset($isHTML) && $isHTML) {
		$mailer->IsHTML(TRUE);								  // Set email format to HTML
		$mailer->AltBody = html2text($body);								  // Set the text-only body of the message.
		$mailer->Body    = $body;                               // Set email body
	} else {
		$mailer->IsHTML(FALSE);								  // Set email format to HTML
		//$mailer->AltBody = $body;								  // Set the text-only body of the message.
		$mailer->Body    = $body;                               // Set email body
	}
	$mailer->Subject = $subject;                                // Set email subject
	if (isset($priority))
		$mailer->Priority = $priority;                          // Set the priority of the message
	if (isset($confirm))
		$mailer->ConfirmReadingTo = $fromEmail;                 // Set email address for read reciept
	#**************************************************************************************************#
	#********************************             SEND MAIL            ********************************#
	#**************************************************************************************************#
	$mailer->SMTPDebug = 2;
	
	$sent = $mailer->Send();
	$mailer->ClearAddresses();
	$mailer->ClearAttachments();
	printr($mailer->ErrorInfo);
	exit;
	if (!$sent) {
		$GLOBALS['mailStatus'] = FALSE;
		$GLOBALS['mailErrorInfo'] = $mailer->ErrorInfo;
		return FALSE;							  // Error encountered
	} else {
		$GLOBALS['mailStatus'] = TRUE;
		$GLOBALS['mailErrorInfo'] = FALSE;
		return TRUE;                                          // Send successful
	}
}

function extractVars() {
	$args = func_get_args();
	foreach ($args as $arg) {
		$GLOBALS[$arg] = isset($_POST[$arg]) ? $_POST[$arg] : (isset($_GET[$arg]) ? $_GET[$arg] : '');
	}
}

function extractCleanVars() {
	$args = func_get_args();
	foreach ($args as $arg) {
		$GLOBALS[$arg] = getCleanVar($arg, NULL, NULL);
	}
}

function extractCleanPostVars() {
	$args = func_get_args();
	foreach ($args as $arg) {
		$GLOBALS[$arg] = getCleanPostVar($arg, NULL, NULL);
	}
}

/**
 * Sanitizes and returns a REQUEST parameter
 *
 * @param	string	$var The input parameter
 * @param	string	$default Default value if the parameter is not present
 * @param	string	$allowed_tags If specified, data will be stripped of tags other than $allowed_tags
 * @return	mixed The requested parameter
 */
function getCleanVar($var, $default = NULL, $strip_tags = NULL) {
	$returnVar = null;
	if (is_array($var)) {
		if (is_array($default)) {
			foreach ($var as $k => $v) {
				$returnVar[$k] = getCleanVar($v, isset($default[$k]) ? $default[$k] : null, $escape);
			}
		} else {
			foreach ($var as $k => $v) {
				$returnVar[$k] = getCleanVar($v, $default, $escape);
			}
		}
		return $returnVar;
	} else {
		$val = isset($_POST[$var]) ? $_POST[$var] : (isset($_GET[$var]) ? $_GET[$var] : $default);
		if (is_array($val)) {
			array_walk_recursive($val, 'sanitizeData', $strip_tags);
		} elseif (!is_null($val)) {
			sanitizeData($val, NULL, $strip_tags);
		}
		return $val;
	}
}

/**
 * Sanitizes and returns a POST parameter
 *
 * @param	string	$var The input parameter
 * @param	string	$default Default value if the parameter is not present
 * @param	string	$allowed_tags If specified, data will be stripped of tags other than $allowed_tags
 * @return	mixed The requested parameter
 */
function getCleanPostVar($var, $default = NULL, $strip_tags = NULL) {
	$returnVar = null;
	if (is_array($var)) {
		if (is_array($default)) {
			foreach ($var as $k => $v) {
				$returnVar[$k] = getCleanPostVar($v, isset($default[$k]) ? $default[$k] : null, $escape);
			}
		} else {
			foreach ($var as $k => $v) {
				$returnVar[$k] = getCleanPostVar($v, $default, $escape);
			}
		}
		return $returnVar;
	} else {
		$val = isset($_POST[$var]) ? $_POST[$var] : $default;
		if (is_array($val)) {
			array_walk_recursive($val, 'sanitizeData', $strip_tags);
		} elseif (!is_null($val)) {
			sanitizeData($val, NULL, $strip_tags);
		}
		return $val;
	}
}

/**
 * Sanitize a given input html code injection. 
 *
 * @param	string	&$var The input data
 * @param	string	$key Placeholder for array index, when invoked using array_walk_recursive
 * @param	string	$allowed_tags If specified, data will be stripped of tags other than $allowed_tags
 * @return	void
 */
function sanitizeData(&$var, $key, $allowed_tags = NULL) {
	$var = htmlspecialchars(is_null($allowed_tags) ? $var : strip_tags($var, $allowed_tags));
}

function htmlspecialchars_recursive(&$var) {
    if (is_array($var)) {
        return array_map('htmlspecialchars_recursive', $var);
    } else {
        return htmlspecialchars($var);
    }
}

function strtrim($input, $length, $ellipses = ' ...', $strip_html = TRUE, $break_word = FALSE) {
	if ($strip_html) $input = strip_tags($input);
	if (strlen($input) <= $length) return $input;
	$pos = $break_word ? $length : $pos = strrpos(substr($input, 0, $length), ' ');
	return substr($input, 0, $pos) . $ellipses;
}

function mb_strtrim ($input, $length, $ellipses = ' ...', $strip_html = TRUE, $break_word = FALSE) {
	if ($strip_html) $input = strip_tags($input);
	if (mb_strlen($input, 'UTF-8') > $length) {
		$input = mb_strimwidth($input, 0, $length, NULL, 'UTF-8');
		$pos = $break_word ? $length : mb_strrpos($input, ' ');
		return mb_substr($input, 0, $pos) . $ellipses;
	} else {
		return $input;
	}
}

/**
 * Increments cache value for a counter (views/downloads) in DB
 * 
 * @param	string	$key	Name of the table to update
 * @param	integer	$id		ID of the record to update
 * @param	integer	$incr	Increment value
 * @return	boolean	TRUE on Success
 */
function incrCounterCache($key, $id, $incr = 1) {
	$cache = \Cache::getInstance();
	if ($cache) {
		$arCounter = $cache->get($key . '_counter', SITE_NAME);
		if (!$arCounter) {
			$arCounter = array();
		}
		$arCounter[$id] = (isset($arCounter[$id])) ? $arCounter[$id] + 1 : 1;
		$cache->set($key . '_counter', $arCounter, 5400, SITE_NAME);
		return $arCounter[$id];
	}
	return FALSE;
}

function format_partial_date($date_string, $format_full = 'F d, Y', $format_month = 'F Y', $format_year = 'Y') {
	if (!empty($date_string)) {
		$date = array_map('intval', explode('-', preg_replace('/-9+\b/', '-00', $date_string)));
		if (empty($date[0]) || $date[0] == 9999) {
			return null;
		} elseif (empty($date[1])) {
			return $date[0];
		} elseif (empty($date[2])) {
			return date($format_month, mktime(0, 0, 0, $date[1], 1, $date[0]));
		} else {
			return date($format_full, mktime(0, 0, 0, $date[1], $date[2], $date[0]));
		}
	}
	return null;
}

function getFormattedDateTime($type, $input, $prefix = '') {
	if ($type === 'date') {
		if (empty($input["{$prefix}date"])) {
			$year = (int)$input["{$prefix}year"];
			$month = (int)$input["{$prefix}month"];
			$day = (int)$input["{$prefix}day"];
			return $year . '-' . ($month < 10 ? "0$month" : $month) . '-' . ($day < 10 ? "0$day" : $day);
		} else {
			return $input["{$prefix}date"];
		}
	} elseif ($type === 'time') {
		if (empty($input["{$prefix}time"])) {
			$hour = (int)$input["{$prefix}hour"];
			$min = (int)$input["{$prefix}min"];
			$apm = $input["{$prefix}apm"];
			if ($hour == 12) $hour = 0;
			if ($apm == 'pm') $hour += 12;
			return ($hour < 10 ? "0$hour" : $hour) . ':' . ($min < 10 ? "0$min" : $min);
		} else {
			return $input["{$prefix}time"];
		}
	} elseif ($type === 'datetime') {
		list($input['date'], $input['time']) = explode('T', $input['datetime']);
		return  getFormattedDateTime('date', $input, $prefix) . 'T' .  getFormattedDateTime('time', $input, $prefix);
	} elseif ($type === 'month') {
		if (empty($input["{$prefix}month"])) {
			$year = (int)$input["{$prefix}year"];
			$month = (int)$input["{$prefix}month"];
			return ($year . '-' . ($month < 10 ? "0$month" : $month));
		} else {
			return $input["{$prefix}month"];
		}
	}
}

function extractFormattedDateTime($type, $input, $prefix = '') {
	if ($type === 'date') {
		if (empty($input["{$prefix}date"])) {
			$year = (int)$input["{$prefix}year"];
			$month = (int)$input["{$prefix}month"];
			$day = (int)$input["{$prefix}day"];
			return [$year, $month, $day];
		} else {
			return array_map('intval', explode('-', $input["{$prefix}date"]));
		}
	} elseif ($type === 'time') {
		if (empty($input["{$prefix}time"])) {
			$hour = (int)$input["{$prefix}hour"];
			$min = (int)$input["{$prefix}min"];
			$apm = $input["{$prefix}apm"];
			if ($hour == 12) $hour = 0;
			if ($apm == 'pm') $hour += 12;
			return [$hour, $min];
		} else {
			return array_map('intval', explode(':', $input["{$prefix}time"]));
		}
	} elseif ($type === 'datetime') {
		list($input['date'], $input['time']) = explode('T', $input['datetime']);
		return  array_merge(extractFormattedDateTime('date', $input, $prefix),  extractFormattedDateTime('time', $input, $prefix));
	} elseif ($type === 'month') {
		if (empty($input["{$prefix}month"])) {
			$year = (int)$input["{$prefix}year"];
			$month = (int)$input["{$prefix}month"];
			return [$year, $month];
		} else {
			return array_map('intval', explode('-', $input["{$prefix}month"]));
		}
	}
}

/**
 * Astro format Date 
 */
function astroFormatDate($format = NULL, $date,  $tzOffset = NULL, $returnGhati = FALSE) {
	if (is_null($format)) {
		$format = 'd/m/y h:i A';
	}
	
	if ($date instanceof DateTime) {		
		if (!is_null($tzOffset)) {
			if ($date->getOffset() !== 0) {
				$date->setTimezone(new DateTimezone('UTC'));
			}
			$date->setTimestamp($date->getTimestamp() + ($tzOffset * 3600));
		}
		/*if ($returnGhati) {
			$ts = $date->getTimestamp();
			$elapsedSecs = $ts % 86400;
			// 1 gati = 24 mins; 1 vigati = 24 secs
			$gati = floor($elapsedSecs / 1440);
			$vigati = floor(($elapsedSecs % 1440) / 24);
			return "$gati:$vigati";
		} else {*/
			return $date->format($format);
		//}	
	} else {
		if (function_exists('adodb_date')) {
			return adodb_date($format, adodb_mktime($date[3], $date[4], $date[5], $date[1], $date[2], $date[0]));
		} else {
			return date($format, mktime($date[3], $date[4], $date[5], $date[1], $date[2], $date[0]));
		}
	}	
}