<?php
define('SECTION_NAME', 'Home');
define('SECTION_PATHNAME', '');
include 'include/prepend.php';
$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$tpl->setTemplate('contactus');

if (AJAX_REQUEST){
	extractCleanVars('name', 'surname', 'email', 'phone', 'message');
	$mailData = [
		'fromEmail' => 'webmaster@padiyaravallikattu-kudumbayogam.com',
		'fromName' => 'From Contact Page User : '.$name . ' '.$surname,
		'toEmail' => 'contact@padiyaravallikattu-kudumbayogam.com',
		'toName' => 'Admin',
		'subject' => 'Feedback',
		'body' => $message."\n\n".$name."({$phone})",
	];
	$status = sendEmail($mailData);
	if ($status) {
    	echo ['type' => 'success', 'message' => 'Success'];
	} else {
    	echo ['type' => 'danger', 'message' => 'Success'];
	}
	exit;
}

$tpl->generate();
