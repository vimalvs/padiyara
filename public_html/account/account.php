<?php
define('SCRIPT_NAME', '[PK] Account Management');
define('REQUIRE_DB', 1);
define('REQUIRE_TEMPLATE', 1);
define('ENABLE_MOBILE_REDIRECTION', 1);

use \Auth\UserAuth as UserAuth;

include '../include/prepend.php';
include '../include/password.inc.php';

header("X-Frame-Options: DENY");

$theme = Theme::load('Classic', 'Mobile', 'Basic');
$tpl = $theme->getTemplate();
$tpl->setTemplate('account');
$tpl->setOption('print_message', PHPTemplate::PRINT_MESSAGE_GENERAL);
$arProvider = array(UserAuth::AUTH_FACEBOOK => 'Facebook', UserAuth::AUTH_GOOGLE => 'Google', UserAuth::AUTH_YAHOO => 'Yahoo');

$mode = getCleanVar('mode', 'account');
$action = getCleanVar('action', 'dashboard');
$redirect = getCleanVar('redirect');

if (empty($redirect)) {
	$redirect = empty($_SERVER['HTTP_REFERER']) ? '/' : $_SERVER['HTTP_REFERER'];
}

$tpl_view = "{$mode}_{$action}";

$userAuth = \Auth\Session::getInstance();
SiteManager::assert($userAuth->isLoggedIn(), SITE_HOME . '/account/auth');

$user = \Auth\UserManager::loadById($userAuth->id);

$userData = $user->getData();
$arAuth = $user->getAuth();

extract($userData);
$user_email_unconfirmed = $user_verified ? (isset($user_meta_info['email_unconfirmed']) ? $user_meta_info['email_unconfirmed'] : NULL) : NULL;

if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
	$request_nonce = getCleanPostVar('nonce');
	if (!\Verification\Nonce::verify($request_nonce, $tpl_view)) {
		$tpl->addMessage('Failed to validate the request. Request session expired, retry.');
	}
}

if ($tpl->hasError()) {
	// Do Nothing
} elseif ($mode === 'account') {
	if ($action === 'dashboard') {
		$last_seen = (int)getCleanVar('last_seen');
		$activities = Logger::userActivity($user_id, NULL, NULL, 10, $last_seen);
		$tpl->data['activities'] = $activities;
	} elseif ($action === 'update') {
		if (getCleanPostVar('p')) {
			$arError = array();
			extractCleanPostVars('user_email', 'user_name', 'user_gender', 'user_birthday');
			$arUpdateData = compact('user_name', 'user_gender', 'user_birthday');
			$current_user_email = $user->email;
			$emailUpdated = $confirmEmail = ($user_email !== $current_user_email) && !(isset($user->meta_info['email_unconfirmed']) && ($user->meta_info['email_unconfirmed'] === $user_email));
			if ($emailUpdated) {
				$arUserEmail = array();
				foreach ($arAuth as $auth) {
					if ($auth['auth_type'] == UserAuth::AUTH_EMAIL) $arUserEmail[$auth['auth_identifier']] = $auth['auth_id'];
				}
				if (!$user_is_registered) {
					$arUpdateData['user_email'] = $user_email;
					$confirmEmail = $emailUpdated = FALSE;
				} elseif (isset($arUserEmail[$user_email])) {
					// Already validated email address, update
					$arUpdateData['user_email'] = $user_email;
					$confirmEmail = FALSE;
				} elseif (!UserAuth::loadByIdentifier(UserAuth::AUTH_EMAIL, $user_email)) {
					// Current email address is also unconfirmed, so update change immediately
					if (!$user_verified) $arUpdateData['user_email'] = $user_email;
					else $arUpdateData['user_meta_info']['email_unconfirmed'] = $user_email;
				} else {
					// Email already in use, show error
					$arError['user_email'] = 'Email has already been taken. An email can only be used on one account at a time.';
				}
			}
			if ($user->update($arUpdateData, $arError)) {
				\Auth\Session::refresh();
				if ($confirmEmail) {
					if (!$user->verified) {
						// Update login email address
						$auth = UserAuth::loadById($arUserEmail[$current_user_email], $user->id);
						$auth->update(array('auth_identifier' => $user_email, 'auth_comment' => $user_email));
					}
					// Send Confirmation email
					$status = \Account\Mailer::sendConfirmationEmail($user_email, $user, $msg);
					$tpl->addMessage($msg, $status ? 'info' : 'warning');
				} elseif ($emailUpdated) {
					// Replaced an unverified email address with a verified email address (most probably from a connected account).
					// Remove the unverified email address from the allowed login emails
					$auth = UserAuth::loadById($arUserEmail[$current_user_email], $user->id);
					if ($auth && $auth->delete()) {
						$user->update(array('user_verified' => 1));
					}
				}
				$tpl->addMessage('Profile updated successfully', 'success');
			} else {
				$tpl->addMessages($arError);
			}
		}
	} elseif ($action === 'resend-confirmation') {
		$tpl_view = 'account_update';
		if ($user_verified && empty($user_meta_info['email_unconfirmed'])) {
			$tpl->showMessage('Email Confirmed', 'Email address is already confirmed.', '/account/dashboard', 'success');
		} else {
			// Send Confirmation email
			$status = \Account\Mailer::sendConfirmationEmail($user_verified ? $user_meta_info['email_unconfirmed'] : $user_email, $user, $msg);
			$tpl->addMessage($msg, $status ? 'info' : 'warning');
		}
	} elseif ($action === 'confirm-email') {
		$token = getCleanVar('param');
		$email = getCleanVar('email');
		$token_verified = FALSE;
		if (preg_match('/(\d+)-([a-z0-9]+)-([a-z0-9]+)/i', $token, $match)) {
			$token_verified = ($user_id == $match[1]) && ($user->hash($email, $match[2]) === $match[3]);
		}
		$tpl_view = 'account_update';
		if ($token_verified) {
			// Email Ownership Verified
			$existingUser = \Auth\User::loadByEmail($email);
			if (!$existingUser || ($existingUser->id === $user_id)) {
				if (!$user->verified && ($user->email === $email)) {
					$status = $user->update(array('user_verified' => 1));
					if ($status) {
						\Auth\Session::refresh();
						$tpl->addMessage('Your account was confirmed successfully', 'success');
					}
				} elseif ($user_email_unconfirmed === $email) {
					$status = $user->update(array('user_email' => $email, 'user_meta_info' => array('email_unconfirmed' => NULL)));
					if ($status) {
						\Auth\Session::refresh();
						$status = $user->addAuth(UserAuth::AUTH_EMAIL, $email);
					}
					if ($status) $tpl->addMessage('Your email was updated successfully', 'success');
				} else {
					$tpl->addMessage("Email address <strong>$email</strong> can no longer be confirmed.");
					$status = TRUE;
				}
				if (!$status) $tpl->addMessage('Failed to update your email address. Please try again later.');
			} else {
				// User Exists
				$tpl->addMessage('Email has already been taken. An email can only be used on one account at a time.');
			}
		} else {
			$tpl->addMessage('Invalid verification token');
		}
	} elseif ($action === 'cancel-email-update') {
		$status = $user->update(array('user_meta_info' => array('email_unconfirmed' => NULL)));
		if ($status !== FALSE) {
			$user_email_unconfirmed = '';
			// $tpl->addMessage('Your pending email address change was canceled', 'info');
		} else {
			$tpl->addMessage('Failed to cancel your pending email address change');
		}
		$tpl_view = 'account_update';
	} elseif ($action === 'settings') {
		if (getCleanPostVar('p')) {
			extractCleanPostVars('auth_password', 'auth_confirm');
			if ($auth_password !== $auth_confirm) $tpl->addMessage('Passwords do not match');
			else {
				if ($user->setPassword($auth_password, $errorInfo)) {
					$redirect = empty($redirect) ? '/' : $redirect;
					$tpl->addMessage("Password changed successfully [<a href=\"$redirect\">Go Back</a>]", 'success');
				} else {
					$tpl->addMessage($errorInfo ? $errorInfo : 'Failed to update password. Please try again later.');
				}
			}
		}
	} elseif ($action === 'remove-auth') {
		$tpl_view = 'account_settings';
		\SiteManager::addOutputFormat('json');
		if (count($arAuth) > 1) {
			if (!getCleanVar('p')) {
				$tpl->addQuestion('Remove Identity', 'Are you sure you want to remove this identity?', array(array('Cancel', $redirect), array('Delete', qsAddParam($_SERVER['REQUEST_URI'], 'p=1'))));
				$tpl_view = 'message';
			} else {
				$auth = UserAuth::loadById(getCleanVar('param'), $user->id);
				$status = $auth && $auth->delete();
				if ($status) {
					$arAuth = $user->getAuth();
					$tpl->addMessage('Identity removed successfully', 'success');
				} else {
					$tpl->addMessage('Failed to remove identity');
				}
			}
		} else {
			$tpl->addmessage('You cannot remove your only login');
		}
	}
} elseif ($mode === 'session') {
	if (getCleanPostVar('p')) {
		$ids = getCleanPostVar('session_id');
		foreach ($ids as $id) {
			\Auth\Session::remove($user_id, $id);
		}
	}
	$tpl->data['sessions'] = \Auth\Session::search(['user_id' => $user_id]);
} else {
	include ROOT . '/404.php';
}

if ($tpl_view === 'account_settings') {
	$arUserAuth = $arUserEmail = array();
	foreach ($arAuth as $auth) {
		if ($auth['auth_type'] == UserAuth::AUTH_EMAIL) {
			$arUserEmail[$auth['auth_id']] = $auth['auth_identifier'];
		} else {
			$arUserAuth[$auth['auth_id']] = array($arProvider[$auth['auth_type']], $auth['auth_comment']);
		}
	}
	$tpl->data['valid_emails'] = $arUserEmail;
	$tpl->data['valid_credentials'] = $arUserAuth;
} elseif ($tpl_view === 'account_update') {
	$tpl->addData(compact('user_email', 'user_name', 'user_gender', 'user_birthday'));
}

if (!AJAX_REQUEST) {
	$tpl->data['user'] = $user->getData();
	$tpl->addData(compact('redirect', 'user_email_unconfirmed'));
}

$response_nonce = \Verification\Nonce::generate($tpl_view);
$tpl->addData(compact('mode', 'action', 'tpl_view', 'response_nonce'));

$tpl->generate();
