<?php
define('SCRIPT_NAME', '[LM] Account Dashboard');
define('REQUIRE_DB', 1);
define('REQUIRE_TEMPLATE', 1);

include '../include/prepend.php';

$tpl->setTemplatePath(ROOT . '/account/templates');
$tpl->setTemplate('dashboard');


$tpl->generate();