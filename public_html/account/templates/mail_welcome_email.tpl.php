Hi <?=$user_name?>,

Thank you for signing up with us. You are one step away from completing the sign up process.

To confirm your email address, please follow this link:
<?=SITE_HOME?>/account/confirm-email/<?=$user_id?>-<?=$token_salt?>-<?=$token_hash?>?email=<?=urlencode($user_email)?> 

<?=SITE_NAME?> Team