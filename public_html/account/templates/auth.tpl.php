<?php
$redirect_encoded = empty($redirect) ? '' : ('?redirect=' . urlencode($redirect));
if (empty($mode)) $mode = 'login';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Account :: <?=ucwords($mode)?></title>
<script src="<?=SITE_HOME?>/assets/js/jquery-1.8.js"></script>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no" />
<meta name="HandheldFriendly" content="True"/>
<style>
body {background: #f2f2f2 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAIAAABLixI0AAAAOElEQVQ4y2N4Qz3AAMSfP73DJCEIPxuNZBh112Bz14cPEISPjdVdg9Ws0fAaDa8hHF6j5dcQcxcA3DHODs/IAR8AAAAASUVORK5CYII=);}
body {margin: 0;};
@media (min-width: 320px) {
	body {margin: 8px;}
}
</style>
</head>
<body>
<div class="container">
<div class="content">
<?php $this->render(__DIR__ . '/auth-ajax.tpl.php', compact('redirect', 'generator'));?>
</div>
</div>
</body>
</html>