Hi <?=$user_name?>,

You have recently let us know that you need to reset your password.  Please follow this link: <?=SITE_HOME?>/account/auth/reset-password?user_email=<?=urlencode($user_email)?>&token=<?=$token?> 
(If clicking the link did not work, try copying and pasting it into your browser.)

If you did not request to reset your password, please disregard this message.  This link will automatically expire within 48 hours.

Thanks,
The <?=SITE_NAME?> Team
