<?php
$redirect_encoded = empty($redirect) ? '' : ('?redirect=' . urlencode($redirect));
TemplateRender::setFormDefaults(array('class' => 'input-xlarge'));
$user_verified = $user['user_verified'];
$user_is_registered = $user['user_is_registered'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ProKerala.com Account</title>
<link href="http://s0.nxstatic.com/assets/css/common.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="http://s0.nxstatic.com/assets/js/jquery-1.8.js"></script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style type="text/css">
h1 {margin-bottom:20px;}
#mainContent .input-xxlarge {width:430px;}
.page-header .thumbnail {margin-right:10px;}
.user-status {font-size:0.7em;}
.user-status-verified {color:green;}
</style>
<script>
_DRCB.push(function(){
	$('.auth-remove').click(function(e) {
		var $t = $(this), isEmail = $t.is('.auth-remove-email');
		if (confirm('Are you sure you want to remove this ' + (isEmail ? 'email address?' : 'identity?'))) {
			$.getJSON(this.href, {p : 1}, function (data) {
				if (data.messages.type === 'success') {
					$t.closest('li').remove();
					Util.notify(data.messages.message);
				} else Util.notify(data.messages.message);
			});
		}
		e.preventDefault();
	});
});
</script>
</head>
<body class="green">
<div id="wrapper">
<!--[if lte IE 6]><div class="ie iele7 ie6"><![endif]-->
<!--[if IE 7]><div class="ie iele7 ie7"><![endif]-->
<!--[if IE 8]><div class="ie ie8"><![endif]-->
<div id="header">
	<div id="siteLinks">
		<a href="http://www.prokerala.com/">Home</a>
		<a href="/news/">News</a>
		<a href="/astrology/" class="hover">Astrology</a>
		<a href="/movies/">Movies</a>
		<a href="http://www.greetings.prokerala.com/">Greetings</a>
		<a href="/health/">Health &amp; Beauty Tips</a>
		<a href="/automobile/">Automobile</a>
		<a href="/travel/">Travel &amp; Tourism</a>
		<a href="/banking/">Banking</a>
	</div>
	<a href="http://www.prokerala.com" id="logo"><img src="http://files.prokerala.com/imgs/common/logo_common.jpg" alt="ProKerala" width="271" height="73" /></a>
	<?php $this->render(TEMPLATE_PATH . '/header.tpl.php');?>
	<div class="clearer">&nbsp;</div>
	<div id="sectionLinks">
		<div class="center">
			<a href="http://www.prokerala.com/">Home</a><a href="/health/">Health &amp; Beauty</a><a href="/general/recipes/">Recipes</a><a href="/health/yoga/">Yoga</a><a href="/astrology/horoscope/">Horoscope</a><a href="/news/">News</a><a href="/astrology/">Astrology</a><a href="/kids/baby-gender/chinese-birth-chart.php">Pregnancy Calendar</a><a href="/general/calendar/chinesecalendar.php">Chinese Calendar</a><a href="http://www.greetings.prokerala.com/">Greetings</a><a href="/movies/">Movies</a><a href="/health/ayurveda/">Ayurveda</a><a href="/downloads/">Downloads</a>
		</div>
	</div>
</div>
<div id="body">
<div class="mainContent wide">
	<div id="account-action" class="section">
	<h1>Account <?php if (!empty($action)) echo ' :: ' . ucwords(strtr($action, '-', ' '));?></h1>
	<?php
		$arTabs = array('dashboard' => 'Dashboard', 'update' => 'Profile');
		if ($user_is_registered) $arTabs += ['settings' => 'Account', 'session/list' => 'Sessions'];
	?>
	<?php if (!$user_is_registered):?>
		
	<?php elseif (!$user_verified):?>
		<div class="alert alert-block alert-warning">
			<p>Confirm your email address for full access to the site. A confirmation message was sent to <strong><?=$this->user->email?></strong>.</p>
			<p><a class="btn" href="/account/resend-confirmation">Resend Confirmation</a> <a href="/account/update">Update email address</a></p>
		</div>
	<?php elseif ($user_email_unconfirmed && !in_array($action, array('resend-confirmation', 'cancel-email-update'))):?>
		<div class="alert alert-block alert-warning">
			<p>You have a pending email address change. A confirmation email was sent to <?=$user_email_unconfirmed?>.Your email will not be changed until you complete this step!</p>
			<p><a class="btn" href="/account/resend-confirmation">Resend Confirmation</a> <a href="/account/update">Cancel or update this email address</a></p>
		</div>
	<?php endif;?>
	<ul class="nav nav-tabs">
		<?php $tmpPath = ($mode === 'account') ? $action : "$mode/$action";?>
		<?php foreach ($arTabs as $k => $v):?>
			<li <?=($k === $tmpPath) ? 'class="active"' : ''?>><a href="/account/<?=$k?>"><?=$v?></a></li>
		<?php endforeach;?>
		<li class="pull-right"><a href="/account/auth/logout">Logout</a></li>
	</ul>
	
	<?php if ($tpl_view === 'account_dashboard'):?>
		<div class="page-header">
			<?php $this->printMessage();?>
			<h2 class="user-title">
				<a class="thumbnail pull-left" data-placement="right" title="Change your avatar at gravatar.com" href="http://gravatar.com/emails/"><img width="32" height="32" src="http://www.gravatar.com/avatar/<?=md5($user['user_email'])?>?s=32&amp;d=identicon&amp;r=PG"></a> 
				<?=$user['user_name']?>
				<?php if ($user_verified):?>
					<span title="Email address verified" class="user-status user-status-verified">✔ Verified</span>
				<?php endif;?>
			</h2>
			<?php if (!empty($activities)):?>
				<h3>Recent Activity</h3>
				<ul>
				<?php foreach ($activities as $v):?>
					<li><?=$v['log_message']?></li>
				<?php endforeach;?>
				</ul>
			<?php endif;?>
		</div>
		
	<?php elseif ($tpl_view === 'account_update'):?>
		<form class="form-horizontal" action="/account/update" method="post">
			<fieldset>
				<legend>Update Profile</legend>
				<?php $this->printMessage();?>
				<?php TemplateRender::formField('Display Name', 'user_name', ['attr' => ['required' => true, 'minlength' => 3, 'maxlength' => 20]], NULL, 'Your display name');?>
				<?php if ($user_is_registered && ($user_email_unconfirmed || !$user_verified)):?>
					<div class="controls muted">
						<?php if (!$user_verified):?>
							<p>Check your email (<strong><?=$user['user_email']?></strong>) to confirm.<br/>
							<a href="/account/resend-confirmation">Resend Confirmation</a></p>
						<?php else:?>
							<p>Check your email (<strong><?=$user_email_unconfirmed?></strong>) to confirm your new address. Until you confirm, notifications will continue to be sent to your current email address.<br/>
							<a href="/account/resend-confirmation">Resend Confirmation</a> &bull; <a href="/account/cancel-email-update">Cancel this change</a></p>
						<?php endif;?>
					</div>
				<?php endif;?>
				<?php TemplateRender::formField('Email Address', 'user_email', ['type' => 'email', 'attr' => ['required' => true, 'maxlength' => 60]], NULL, 'Your email address (requires verification)');?>
				<?php TemplateRender::formField('Gender', 'user_gender', ['type' => 'multi', 'multi' => ['Not Specified', 'Male', 'Female']], NULL);?>
				<?php TemplateRender::formField('Birth Day', 'user_birthday', ['type' => 'date', 'attr' => ['maxlength' => 10]], NULL, 'Birthday in YYYY-MM-DD Format');?>
				<div class="form-actions">
					<div class="row">
						<input type="hidden" name="p" value="1">
						<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
						<input type="hidden" name="nonce" value="<?=$response_nonce?>">
						<button class="btn btn-large btn-primary" type="submit">Update Profile</button>
					</div>
				</div>
			</fieldset>
		</form>
	<?php elseif ($tpl_view === 'account_settings'):?>
		<div>
			<?php if ($action === 'remove-auth') $this->printMessage();?>
			<p>These logins grant access to your account; add as many as you need. </p>
			<?php if (!empty($valid_emails)):?>
				<h4>Email</h4>
				<ul>
					<?php foreach ($valid_emails as $k => $v):?>
						<li>[ <a class="auth-remove auth-remove-email" href="/account/remove-auth/<?=$k?>">Remove</a> ] <?=$v?></li>
					<?php endforeach;?>
				</ul>
				<p class="muted">Sign in using any Google, Yahoo or Facebook account with the above email addresses</p>
			<?php endif;?>
			<?php if (!empty($valid_credentials)):?>
				<h4>Federated Identity</h4>
				<ul>
					<?php foreach ($valid_credentials as $k => $v):?>
						<li>[ <a class="auth-remove auth-remove-identity" href="/account/remove-auth/<?=$k?>">Remove</a> ] <?=$v[0]?> [ <?=$v[1]?> ]</li>
					<?php endforeach;?>
				</ul>
			<?php endif;?>
			<a target="_blank" class="btn" href="/account/auth/add?redirect=%2Faccount%2Fsettings"><i class="icon-plus"></i> Add another login</a>
		</div>
		<?php if (!empty($valid_emails)):?>
			<form class="form-horizontal" action="/account/settings" method="post">
				<fieldset>
					<legend>Update Password</legend>
					<?php TemplateRender::formField('Password', 'auth_password', array('value' => '', 'class' => '', 'type' => 'masked'));?>
					<?php TemplateRender::formField('Verify password', 'auth_confirm', array('value' => '', 'class' => '', 'type' => 'masked'));?>
					<div class="form-actions">
						<div class="row">
							<input type="hidden" name="p" value="1">
							<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
							<input type="hidden" name="nonce" value="<?=$response_nonce?>">
							<button class="btn btn-large btn-primary" type="submit">Update Password</button>
						</div>
					</div>
				</fieldset>
			</form>
		<?php endif;?>
	<?php elseif ($tpl_view === 'account_resend_confirmation'):?>
		<form class="form-horizontal" action="" method="post">
			<fieldset>
				<legend>Confirm Email Address</legend>
				<?php $this->printMessage();?>
				<?php TemplateRender::formField('Email Address', 'email', array('value' => $user['user_email'], 'attr' => array('readonly' => 'readonly')));?>
				<p>We will send you an email with a temporary link. Click the link within 48 hours to confirm your account.</p>
				<div class="form-actions">
					<div class="row">
						<input type="hidden" name="p" value="1">
						<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
						<input type="hidden" name="nonce" value="<?=$response_nonce?>">
						<button class="btn btn-large btn-primary" type="submit">Send Verification Email &raquo;</button>
					</div>
				</div>
			</fieldset>
		</form>
	<?php elseif ($tpl_view === 'session_list'):?>
		<form class="form-horizontal" action="" method="post">
			<?php TemplateRender::table($sessions, [
			'session_id' => T_CHECKBOX,
			'session_platform' => 'Platform',
			'session_time_last_seen' => 'Last Seen'
			]);?>
			<div class="form-actions">
				<input type="hidden" name="p" value="1">
				<input type="hidden" name="redirect" value="<?=htmlspecialchars($redirect)?>">
				<input type="hidden" name="nonce" value="<?=$response_nonce?>">
				<button class="btn btn-large btn-primary" type="submit">Delete Sessions</button>
			</div>
		</form>
	<?php else:?>
		<?php $this->printMessage();?>
	<?php endif;?>
	</div>
	</div>
	<div class="sideContent2">
		<?php \SiteManager::invokeHook(['template_content_area', $this], 'side_content');?>
	</div>
	<div class="clearer">&nbsp;</div>
</div>
<?php $this->render(TEMPLATE_PATH . '/page-footer.tpl.php');?>
</div>
<!--[if lte IE 8]></div><![endif]-->
</body>
</html>