<?php
$arAuthProvider = array(
	"base_url" => SITE_HOME . "/account/auth/connect",
	"providers" => array ( 
		// openid providers
		"yahoo" => array (
			"enabled" => true,
			"wrapper" => array( "path" => "Providers/Yahoo.php", "class" => "Hybrid_Providers_Yahoo" )
		),
		"google" => array ( 
			"enabled" => true,
			"wrapper" => array( "path" => "Providers/GoogleOpenID.php", "class" => "Hybrid_Providers_Google" ),
			// "keys"    => array ( "id" => "348396699333.apps.googleusercontent.com", "secret" => "9eQxN-ieA8vQvzSQQqkLX0kT" ),
			// "scope"   => ""
		),
		"facebook" => array ( 
			"enabled" => true,
			"keys"    => array ( "id" => "121476094536573", "secret" => "d24172afc3dcaa5652121b3b4b48776f" ),
			"wrapper" => array( "path" => "Providers/Facebook.php", "class" => "Hybrid_Providers_Facebook" ),
			// A comma-separated list of permissions you want to request from the user. See the Facebook docs for a full list of available permissions: http://developers.facebook.com/docs/reference/api/permissions.
			"scope"   => "", 
			// The display context to show the authentication page. Options are: page, popup, iframe, touch and wap. Read the Facebook docs for more details: http://developers.facebook.com/docs/reference/dialogs#display. Default: page
			"display" => "" 
		)
	),
	// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
	"debug_mode" => FALSE,
	"debug_file" => "",
);
