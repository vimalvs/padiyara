<?php
include 'include/prepend.php';

$theme = Theme::load('Classic', 'Classic', 'Classic');

$tpl = $theme->getTemplate();

$tpl->setTemplate('site_home');

$stmt = $pdbo->search("header_images", null, ['image_order', 'ASC']);
$arImage = $stmt ? $stmt->fetchAll() : [];

$tpl->addData(compact('arImage'));

$tpl->generate();
