$( document ).ready(function() {
    $('.nav-search').click(function() {
    	$(this).toggleClass('active');
    	if($(this).hasClass('fa-search')) {
    		$(this).removeClass('fa-search').addClass('fa-window-close');
    	} else {
    		$(this).removeClass('fa-window-close').addClass('fa-search');
    	}
    	$('.nav-search-box').toggleClass('active');
    });

    var timer;
	$(window).scroll(function(){
	    if ( timer ) clearTimeout(timer);
	    timer = setTimeout(function(){
       		var scrollTop = $(window).scrollTop();
		    if (scrollTop >= 90) {
				$('#d-header-upper').addClass('d-header-upper-inactive');
				$('#d-header-lower').removeClass('p-xl-2');
				$('#header > .navbar').addClass('nav-box-shadow');
		    } else if (scrollTop < 90) {
				$('#d-header-upper').removeClass('d-header-upper-inactive');
				$('#header > .navbar').removeClass('nav-box-shadow');
				$('#d-header-lower').addClass('p-xl-2');
	  		}
	  		console.log(scrollTop);
	    }, 50);
	});
});
