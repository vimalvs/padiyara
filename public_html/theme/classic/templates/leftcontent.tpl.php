<div id="leftContent">
	<div class="sections">
		<div class="cb">
			<span class="tb orange">&nbsp;</span>
			<span class="caption">Malayalam Scraps, Glitters</span>
			<div class="leftMenu">
				<a href="/">MalluBar Home</a>
				<?php foreach($this->data['glitter_cats'] as $k => $v):?>
					<a href="/glitters/<?=$k?>/"><?=$v?></a>
				<?php endforeach;?>
			</div>
		</div>
	</div>
	<div class="sections">
		<div class="cb">
			<span class="tb orange">&nbsp;</span>
			<span class="caption">Jokes</span>
			<div class="leftMenu">
				<a href="/jokes/">Jokes</a>
				<?php foreach($this->data['jokes_cats'] as $k => $v):?>
					<a href="/jokes/<?=$k?>/"><?=$v?></a>
				<?php endforeach;?>
			</div>
		</div>
	</div>
	<div class="sections">
		<div class="cb">
			<span class="tb orange">&nbsp;</span>
			<span class="caption">SMS</span>
			<div class="leftMenu">
				<a href="/sms/">SMS</a>
				<a href="/malayalam/sms/">Malayalam SMS</a>
				<?php foreach($this->data['sms_cat'] as $k => $v):?>
					<a href="/sms/<?=$k?>/"><?=$v?></a>
				<?php endforeach;?>
			</div>
		</div>
	</div>

	<!--div class="box">
	<span class="head">Mobile Utilities</span>
	</div-->
	<div class="sections">
		<div class="cb">
			<span class="tb blue">&nbsp;</span>
			<span class="caption">Glitter Generators &amp; Tools</span>
			<div class="leftMenu">
				<a href="/glitter-generators/">Glitter Generator</a>
				<a href="/glitter-generators/alphabets/">Alphabet Generator</a>
				<a href="/glitter-generators/glitter-animator/">Glitter Animator</a>
				<a href="/glitter-generators/text-animator/">Text Animator</a>
				<a href="/glitter-generators/teddy-messenger/">Teddy Messenger</a>
			</div>
		</div>
	</div>
	<div class="sections">
		<div class="cb">
			<span class="tb blue">&nbsp;</span>
			<span class="caption">Text Convertors</span>
			<div class="leftMenu">
				<a href="/text-convertors/">Text Convertors</a>
				<a href="/text-convertors/text-flip.php">Text Flipper</a>
				<a href="/text-convertors/circle-text.php">Circled Text</a>
				<a href="/text-convertors/fancy-text.php">Fancy Text</a>
			</div>
		</div>
	</div>
</div>