<footer>
	<div id="footer">
        <div class="footer-links container p-2">
            <ul class="footer-nav">
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Home</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">About Our Family</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Family History</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Who is Who </a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Family Geneology</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Photo Gallery</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="/contactus.php">Contact Us</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Family News Letter</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Committee Members</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Tributes</a></li>
                <li class="d-nav"><a class="sliding-middle-out nav-item" href="#">Memorial Pages</a></li>
            </ul>    
        </div>
        <div class="footer-info p-2">
            <p class="text-center">Copyright &copy; <?php echo date('Y')?><br>Padiyara Vallikattu Kudumbayogam<br>All   rights reserved.</p>
        </div>
  </div>
</footer>
<script type="text/javascript">
    if (!window.jQuery) {
        document.write('<' + 'script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/js/jquery.min.js"><'+'/script>');
    }
</script>
<script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/js/popper.min.js"></script>
<script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=THEME_ASSETS_PATH?>/src/js/common.js"></script>
