<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-blue">
	<table class="table table-header navbar-toggler table-no-pad">
		<tr>
			<td>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsSmall" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</td>
			<td>
				<a class="navbar-brand" href="/"><img src="<?=THEME_ASSETS_PATH?>/images/logo.png" /></a>
			</td>
			<td>
				<span class="fa fa-icon fa-search nav-search"></span>
			</td>
		</tr>
	</table>
	<div class="collapse navbar-collapse d-ld-none" id="navbarsSmall">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link fg-white" href="#">
					Fammily Geneology
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link fg-white" href="#">Photo Gallery</a>
			</li>
			<li class="nav-item">
				<a class="nav-link fg-white" href="/contactus.php">Contact Us</a>
			</li>
			<li class="nav-item">
				<a class="nav-link fg-white" href="#">Who is Who</a>
			</li>
			<li class="nav-item">
				<a class="nav-link fg-white" href="/history.php">Family History</a>
			</li>
			<li class="nav-item">
				<a class="nav-link fg-white" href="/aboutus.php">About Our Family</a>
			</li>
			<li class="nav-item">
				<span class="fa fa-eye fg-white"> View in : </span> &nbsp;  <a class="fg-white" href="?lang=en"> EN </a> <span class="fg-white">|</span> <a class="fg-white" href="?lang=ml"> ML </a>
			</li>
		</ul>
	</div>
	<div class="full-width nav-search-box d-md-none">
		<form class="pad">
			<table class="table table-search-box">
				<tr>
					<td>
						<div class="input-group">
							<input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Search">
						</div>
					</td>
					<td>
						<button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div id="navbarDesktop" class="d-none d-lg-block w-100">
		<div class="row">
			<div class="col-lg-4 m-auto">
				<a href="/"><img class="img-fluid" src="<?=THEME_ASSETS_PATH?>/images/logo.png" /></a>
			</div>
			<div class="col-lg-8">
				<div id="d-header-upper" class="text-right fg-white p-2 border border-top-0 border-left-0 border-right-0 border-info">
					<div class="row">
						<div class="d-nav col-lg-10">
							<a class="nav-item fg-white sliding-middle-out" href="#">Fammily Geneology</a>
							|
							<a class="nav-item fg-white sliding-middle-out" href="#">Photo Gallery</a>
							|
							<a class="nav-item fg-white sliding-middle-out" href="/contactus.php">Contact Us</a>
						</div>
						<div class="col-lg-2">
							<!-- <a href="/manage/" class="fg-white"><span class="fa fa-user-circle"></span></a>  <a class="fg-white" href="?lang=eng"> EN </a> | <a class="fg-white" href="?lang=ml"> ML </a> -->
						</div>
					</div>
				</div>
				<div id="d-header-lower" class="p-xl-2 ml-xl-5">
					<div class="row">
						<div class="d-nav col-lg-7 pt-3 fg-white">
							<a class="nav-item fg-white sliding-middle-out" href="/">Home</a>
							|
							<a class="nav-item fg-white sliding-middle-out" href="/aboutus.php">About Our Family</a>
							|
							<a class="nav-item fg-white sliding-middle-out" href="/history.php">Family History</a>
							|
							<a class="nav-item fg-white sliding-middle-out" href="#">Who is Who</a>
						</div>
						<div class="col-lg-3">
							<form class="pad">
								<table class="table table-search-box">
									<tr>
										<td>
											<div class="input-group search-box-wrapper pt-3">
												<input type="text" class="form-control search-box" id="search-box" placeholder="Search">
											</div>
										</td>
										<td class="align-middle pt-3">
											<span class="fa fa-search"></span>
										</td>
									</tr>
								</table>
							</form>
						</div>
						<div class="col-lg-2 pt-3">
							<a href="/manage/" class="fg-white"><span class="fa fa-user-circle"></span></a> &nbsp; <a class="fg-white" href="?lang=en"> EN </a> <span class="fg-white">|</span> <a class="fg-white" href="?lang=ml"> ML </a>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</nav>
