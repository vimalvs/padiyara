<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="<?=THEME_ASSETS_PATH?>/icons/favicon.png" type="image/png" />
<link rel="stylesheet" href="<?=THEME_ASSETS_PATH?>/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=THEME_ASSETS_PATH?>/css/style.css?20171030">
