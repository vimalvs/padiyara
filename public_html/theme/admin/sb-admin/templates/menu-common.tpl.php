<div class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
			<?php if (!empty($search_action)):?>
	            <li class="sidebar-search">
	                <div class="input-group custom-search-form">
	                    <input type="text" class="form-control" placeholder="Search...">
	                    <span class="input-group-btn">
	                    <button class="btn btn-default" type="button">
	                        <i class="icon icon-search"></i>
	                    </button>
	                </span>
	                </div>
	                <!-- /input-group -->
	            </li>
			<?php endif;?>
            <li>
                <a href="#"><i class="icon icon-files-o icon-fw"></i> Sections<span class="icon arrow"></span></a>
				 <ul class="nav nav-second-level">
					<li><a href="/manage/page.php">Pages</a></li>
					<li><a href="/manage/index.php">Families</a></li>
					<li><a href="/manage/index.php">Membors</a></li>
					<li><a href="/manage/index.php">Events</a></li>
					<li><a href="/manage/news.php">News &amp; Achivements</a></li>
					<li><a href="/manage/header-images.php">Header Images</a></li>
				</ul>
			</li>
            <li>
                <a href="#"><i class="icon icon-sitemap icon-fw"></i> Other<span class="icon arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="/manage/index.php">Profile</a></li>
					<li><a href="/manage/index.php">Users</a></li>
				</ul>
			</li>
        </ul>
    </div>
</div>