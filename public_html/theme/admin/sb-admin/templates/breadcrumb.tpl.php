<ul class="breadcrumb"><li class=""><a href="/manage/"><i class="icon icon-home"></i> Dashboard</a></li>
	<?php end($breadcrumb);$tmpActiveKey = key($breadcrumb);?>
	<?php foreach ($breadcrumb as $k => $v):?>
		<li <?=($tmpActiveKey === $k) ? ' class="active"' : ''?>><a <?=($tmpActiveKey === $k) ? ' class="active"' : ''?> href="<?=htmlspecialchars($v, ENT_QUOTES, 'utf-8')?>"><?=htmlspecialchars($k, ENT_QUOTES, 'utf-8')?></a></li>
	<?php endforeach;?>
</ul>