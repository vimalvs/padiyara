<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle sidebar-toggle" data-toggle="class" data-target=".sidebar-toggle,.navbar-static-side,#page-wrapper" data-class="sidebar-open">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon icon-bar"></span>
            <span class="icon icon-bar"></span>
            <span class="icon icon-bar"></span>
        </button>
		<?php if (!empty($admin_section)):?>
			<a class="navbar-brand" href="<?=isset($admin_section_url) ? $admin_section_url : '#'?>"><?=$admin_section?> <small>ACP</small></a>
		<?php endif;?>
    </div>
    <ul class="nav navbar-top-links navbar-right">
		<li><a href="<?=SITE_HOME?>/manage/"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-placement="bottom" title="Logged in as <?=$this->user->name?>"><i class="icon icon-user"></i> <span>Account</span></a>
			<ul class="dropdown-menu">
				<li><a href="<?=SITE_HOME?>/account/auth/logout"><i class="icon icon-sign-out"></i> Log out</a></li>
			</ul>
		</li>
	</ul>
	<?php $this->render(empty($admin_section_template) ? 'theme::menu-common' : $admin_section_template);?>
</nav>