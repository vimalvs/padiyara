<?php

\SiteManager::registerHook(['theme_init', $this], function () {
	$tpl = $this->getTemplate();
	$tpl->data['breadcrumb'] = [];
	$tpl->setOption('print_message', PHPTemplate::PRINT_MESSAGE_GENERAL);
	
	SiteManager::registerHook(['template_head', $tpl], function () {
		?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="HandheldFriendly" content="True"/>
		<script>
		var THEME_NAME = '<?=THEME?>', THEME_ASSETS_PATH = '<?=THEME_ASSETS_URL?>', THEME_BASE_PATH = THEME_ASSETS_PATH.slice(0, -7);
		Theme.init();
		</script>
		<?php
	});
});