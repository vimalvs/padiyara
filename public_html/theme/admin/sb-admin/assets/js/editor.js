/**
 * jQuery.editor - Converts normal textarea into Rich Text Editor / Code Editor
 * Copyright (c) VimalVS.com | http://www.ennexa.com/
 * Dual licensed under MIT and GPL.
 * Date: 02/01/2011
 * @author Joyce Babu
 * @version 1.0.0
 *
 */

(function($){
	
	var AbstractEditor = function (elem, options) {
		this.elem = elem;
		this.options = options;
	};
	$.extend(AbstractEditor, {
		load: function (cb) {
			cb.call();
		},
		prototype: {
			text: function () {},
			activate: function () {
				console.log('Editor Activate');
			},
			deactivate: function () {
				console.log('Editor Deactivate');
			},
			insert: function (text) {},
			focus: function () {
				console.log('Editor Focus');
			},
			setMode: function(mode) {
				console.log('Editor Set Mode');
			}
		}
	});
	
	var TinyMceEditor = function (elem, options) {
		var id = elem.id;
		this.editor = null;
		this.text = function (text) {
			this.editor.setContent(text);
		};
		this.activate = function () {
			tinyMCE.execCommand('mceAddEditor', false, id);
			this.editor = tinymce.get(id);
		};
		this.deactivate = function () {
			tinyMCE.execCommand('mceRemoveEditor', false, id);
		};
		this.insert = function (text) {
			this.editor.execCommand("mceInsertContent", false, text);
		}
		this.focus = function () {
			this.editor.execCommand('mceFocus', false);
		}
	};
	TinyMceEditor.prototype = new AbstractEditor();
	TinyMceEditor.load = function (cb) {
		$.lazyLoad('/assets/lib/tinymce/tinymce.gzip.php?js=1&plugins=table,searchreplace,contextmenu,paste,fullscreen,noneditable,nonbreaking,inlinepopups&themes=advanced&languages=en&diskcache=false&src=true', cb);
	};
	
	var TextAreaEditor = function (elem, options) {
		this.text = function (text) {
			elem.value = text;
		}
		this.insert = function (text) {
			if (document.selection) {
		      //For browsers like Internet Explorer
		      var sel = document.selection.createRange();
		      sel.text = text;
		    } else if (elem.selectionStart || elem.selectionStart == '0') {
		      //For browsers like Firefox and Webkit based
		      var startPos = elem.selectionStart;
		      var endPos = elem.selectionEnd;
		      var scrollTop = elem.scrollTop;
		      elem.value = elem.value.substring(0, startPos) + text + elem.value.substring(endPos,elem.value.length);
		      // this.focus();
		      elem.selectionStart = startPos + text.length;
		      elem.selectionEnd = startPos + text.length;
		      elem.scrollTop = scrollTop;
		    } else {
		      elem.value += text;
		    }
		};
		this.focus = function () {
			elem.focus();
		}
	};
	TextAreaEditor.prototype = new AbstractEditor();
	TextAreaEditor.load = function (cb) {
		cb.call();
	};
	
	var AceEditor = function (elem, options) {
		this.$elem = $(elem);
		var ace = this.aceEditor = window.__ace_shadowed__.transformTextarea(elem, __ace_loader__);
		ace.setOptions({
		    mode:               options.mode,
		    theme:              "textmate",
		    fontSize:           "14px",
		    softWrap:           "free",
		    showPrintMargin:    "false",
		    useSoftTabs:        "true",
		    showInvisibles:     "true"
		});
		this.$aceContainer = this.$elem.next();
		ace.getSession().on('change', function(){
			elem.value = ace.getSession().getValue();
			if (document.createEventObject) {
		        elem.fireEvent('onchange', document.createEventObject())
		    } else {
		        var evt = document.createEvent("HTMLEvents");
		        evt.initEvent('change', true, true ); // event type,bubbling,cancelable
		        elem.dispatchEvent(evt);
		    }
		});
		
		
		this.text = function (text) {
			this.editor.setContent(text);
		};
		this.activate = function () {
			this.aceEditor.getSession().setValue(elem.value);
			this.$aceContainer.show();
			this.$elem.hide();
		};
		this.deactivate = function () {
			this.$elem.show();
			// if (this.ace) {
				this.$aceContainer.hide();
			// 	this.style.display = 'block';
			// }
			// $(this).trigger('aceeditorinactive');
		};
		this.insert = function (text) {
			this.aceEditor.insert(text);
		}
		this.focus = function () {
			this.aceEditor.focus();
		}
		this.setMode = function (mode) {
			this.aceEditor.getSession().setMode("ace/mode/" + mode);
		}
	};
	AceEditor.prototype = new AbstractEditor();
	AceEditor.load = function (cb) {
		var baseUrl = "/assets/lib/ace/";
	    var load = window.__ace_loader__ = function(path, module, callback) {
	        var head = document.getElementsByTagName('head')[0];
	        var s = document.createElement('script');

	        s.src = baseUrl + path;
	        head.appendChild(s);

	        s.onload = function() {
				window.ace = window.__ace_shadowed__;
				ace.require.aceBaseUrl = baseUrl;
				// ace.options = {
				//     mode:               "javascript",
				//     theme:              "textmate",
				//     // gutter:             "true",
				//     fontSize:           "12px",
				//     softWrap:           "free",
				//     showPrintMargin:    "false",
				//     useSoftTabs:        "true",
				//     showInvisibles:     "true"
				// };
				ace.require([module], callback);
	        };
	    };
	    load('ace-bookmarklet.js', "ace/ext/textarea", function() {
			var ace = window.__ace_shadowed__;
			ace.require.aceBaseUrl = baseUrl;
			cb && cb();
	    });
	};
	
	var CodeMirrorEditor = function (elem, options) {
		var id = elem.id;
		this.editor = null;
		CodeMirror.modeURL = '/assets/lib/codemirror/mode/%N/%N.js';
        var t = this;
		this.text = function (text) {
			this.editor.setValue(text);
		};
		this.activate = function () {
			this.editor = CodeMirror.fromTextArea(elem, {
				lineWrapping: true,
			    lineNumbers: true,
			});
			this.setMode(options.mode);
            this.editor.on('blur', function (cm) {
                cm.save();
            });
			// CodeMirror.autoLoadMode(this.editor, this.editor.getOption('mode'));
		};
		this.deactivate = function () {
			// console.log('De-activate');
			if (this.editor) {
				this.editor.toTextArea();
			}
		};
		this.insert = function (text) {
			this.editor.replaceSelection(text);
		}
		this.focus = function () {
			this.editor.focus();
		}
		this.setMode = function (mode) {
			if (mode == 'html') mode = 'htmlmixed';
			this.editor.setOption('mode', mode);
			CodeMirror.autoLoadMode(this.editor, mode);
		}
	};
	CodeMirrorEditor.prototype = new AbstractEditor();
	CodeMirrorEditor.load = function (cb) {
		$.lazyLoad([['/assets/lib/codemirror/lib/codemirror.js', '/assets/lib/codemirror/lib/codemirror.css'], ['/assets/lib/codemirror/addon/mode/loadmode.js']], cb);
	};
	
	
	
	// var a = new RichTextEditor(document.getElementById('fin_unit_content'));
	// a.activate();

	
	// var RichTextEditor = function (elem) {
	// 	this.elem = elem;
	// };
	// $.extend(RichTextEditor, {
	// 	load: function (cb) {
	// 		$.lazyLoad('/assets/lib/tinymce/tinymce.gzip.php?js=1&plugins=table,searchreplace,contextmenu,paste,fullscreen,noneditable,nonbreaking,inlinepopups&themes=advanced&languages=en&diskcache=false&src=true', cb);
	// 	},
	// 	prototype: {
	// 		active: false,
	// 		text: function () {
	// 			this.editor.setContent(text);
	// 		},
	// 		activate: function () {
	// 			tinyMCE.execCommand('mceAddEditor', false, this.elem.id);
	// 		},
	// 		deactivate: function () {
	// 			tinyMCE.execCommand('mceRemoveEditor', false, this.elem.id);
	// 		},
	// 		insert: function () {
	//
	// 		},
	// 		focus: function () {
	//
	// 		}
	// 	}
	// });
	//
	// var TextEditor = function (elem) {
	// 	this.elem = elem;
	// };
	// $.extend(TextEditor, {
	// 	load: function (cb) {
	// 		cb.call();
	// 	},
	// 	prototype: {
	// 		active: false,
	// 		text: function () {
	// 			this.value = text;
	// 		},
	// 		activate: function () {
	//
	// 		},
	// 		deactivate: function () {
	//
	// 		},
	// 		insert: function (code) {
	// 			
	// 		},
	// 		focus: function () {
	//
	// 		}
	// 	}
	// });
	
	$(document).on('click', '.toggleEditor a', function(e) {
		var $t = $(this);
		var $editor = $(this).closest('tr').next().find('textarea.editor');
		$editor.editor('switch', $t.attr('rel'));
		e.stopPropagation();
		return false;
	});
	var Editor = function (elem, options) {
		this.elem = elem;
		this.__init(elem, options);
	};
	$.extend(Editor, {
		defaults : {
			editor : 'plain',
			mode: 'html'
		},
		count : 0,
		classes: {
			rte: TinyMceEditor,
			plain: TextAreaEditor,
			code: CodeMirrorEditor
		},
		prototype: {
			editors: null,
			editor: null,
			instance: null,
			__init: function (elem, options) {
				// var $me = $(this), opt, data;
				// if ((data = $me.data('editor')) && (typeof data === 'object')) return false;
				var $elem = $(this.elem).addClass('editor');
				this.editors = [];
				// options.editor = 'rte';
				this.options = options = $.extend(true, {}, $.editor.defaults, options);
				// $elem.data('editor', {editor : ''}).addClass('editor');
				$elem.wrap('<table class="editorWrapper"><tr><td></td></tr></table>').parent().parent().before('<tr><td class="tr toggleEditor"><a data-skip-ajax="true" href="#" rel="plain">Plain Text</a> | <a data-skip-ajax="true" rel="code" href="#">Code</a>' + (options.mode === 'html' ? ' | <a data-skip-ajax="true" rel="rte" href="#">Rich Text</a>' : '') +  '</td></tr>');
				this.idx = $.editor.count++;
				if (!this.id) this.id = 'txtEditorControl_' + this.idx;
				this.editor = this.options.editor;
				this.switch(this.options.editor);
			},
			__get: function (editor, cb) {
				if (!this.editors[editor]) {
					var cls = Editor.classes[editor];
					var t = this;
					cls.load($.proxy(function () {
						this.editors[editor] = new cls(this.elem[0], this.options);
						cb.call(this, this.editors[editor]);
					}, this))
				} else {
					cb.call(this, this.editors[editor]);
				}
			},
			switch: function (editor, cb) {
				this.__get(editor, $.proxy(function (instance) {
					this.editors[this.editor].deactivate();
					instance.activate();
					this.editor = editor;
					this.instance = instance;
					if (cb) cb.call();
				}, this));
				return this.editors[this.editor];
			},
			insert: function (text) {
				this.instance.insert(text);
			},
			update: function () {
			
			}
		}
	});
	
	var methods = {
		init : function(options) {
			var $me = $(this);
			if ($me.data('editor.api')) return;
			var editor = new $.editor($me, options);
			$me.data('editor.api', editor);
		},
		options : function() {
		
		},
		switch : function(editor) {
			// var $me = $(this);
		},
		insert: function () {
			
		},
		update : function() {
			
		}
	};
	$.editor = Editor;

	$.fn.editor = function (method) {
		var args = arguments;
		return this.each(function(){
			var $this = $(this), editor = $this.data('editor.api');
			if (!editor) {
				editor = new $.editor($this, (typeof method === 'object') ? method : null);
				$this.data('editor.api', editor);
			}
			if (editor && editor[method]) {
				return editor[method].apply(editor, Array.prototype.slice.call(args, 1));
			} else if (typeof method !== 'object') {
				$.error( 'Method ' +  method + ' does not exist on jQuery.editor' );
			}
		});
	}
})(jQuery);