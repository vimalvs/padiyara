/*!
* jQuery blockUI plugin
* Version 2.57.0-2013.02.17
* @requires jQuery v1.7 or later
*
* Examples at: http://malsup.com/jquery/block/
* Copyright (c) 2007-2013 M. Alsup
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
* Thanks to Amir-Hossein Sobhi for some excellent contributions!
*/
(function(){function n(b){function n(d,a){var e,g,h=d==window,f=a&&void 0!==a.message?a.message:void 0;a=b.extend({},b.blockUI.defaults,a||{});if(!a.ignoreIfBlocked||!b(d).data("blockUI.isBlocked")){a.overlayCSS=b.extend({},b.blockUI.defaults.overlayCSS,a.overlayCSS||{});e=b.extend({},b.blockUI.defaults.css,a.css||{});a.onOverlayClick&&(a.overlayCSS.cursor="pointer");g=b.extend({},b.blockUI.defaults.themedCSS,a.themedCSS||{});f=void 0===f?a.message:f;h&&l&&r(window,{fadeOut:0});if(f&&"string"!=typeof f&& (f.parentNode||f.jquery)){var j=f.jquery?f[0]:f,c={};b(d).data("blockUI.history",c);c.el=j;c.parent=j.parentNode;c.display=j.style.display;c.position=j.style.position;c.parent&&c.parent.removeChild(j)}b(d).data("blockUI.onUnblock",a.onUnblock);var c=a.baseZ,k;k=s||a.forceIframe?b('<iframe class="blockUI" style="z-index:'+c++ +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+a.iframeSrc+'"></iframe>'):b('<div class="blockUI" style="display:none"></div>'); j=a.theme?b('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+c++ +';display:none"></div>'):b('<div class="blockUI blockOverlay" style="z-index:'+c++ +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');a.theme&&h?(c='<div class="blockUI '+a.blockMsgClass+' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(c+10)+';display:none;position:fixed">',a.title&&(c+='<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+ (a.title||"&nbsp;")+"</div>"),c+='<div class="ui-widget-content ui-dialog-content"></div></div>'):a.theme?(c='<div class="blockUI '+a.blockMsgClass+' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(c+10)+';display:none;position:absolute">',a.title&&(c+='<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(a.title||"&nbsp;")+"</div>"),c+='<div class="ui-widget-content ui-dialog-content"></div>',c+="</div>"):c=h?'<div class="blockUI '+a.blockMsgClass+' blockPage" style="z-index:'+ (c+10)+';display:none;position:fixed"></div>':'<div class="blockUI '+a.blockMsgClass+' blockElement" style="z-index:'+(c+10)+';display:none;position:absolute"></div>';c=b(c);f&&(a.theme?(c.css(g),c.addClass("ui-widget-content")):c.css(e));a.theme||j.css(a.overlayCSS);j.css("position",h?"fixed":"absolute");(s||a.forceIframe)&&k.css("opacity",0);e=[k,j,c];var q=h?b("body"):b(d);b.each(e,function(){this.appendTo(q)});a.theme&&(a.draggable&&b.fn.draggable)&&c.draggable({handle:".ui-dialog-titlebar",cancel:"li"}); g=z&&(!b.support.boxModel||0<b("object,embed",h?null:d).length);if(u||g){h&&(a.allowBodyStretch&&b.support.boxModel)&&b("html,body").css("height","100%");if((u||!b.support.boxModel)&&!h){g=parseInt(b.css(d,"borderTopWidth"),10)||0;var p=parseInt(b.css(d,"borderLeftWidth"),10)||0,v=g?"(0 - "+g+")":0,w=p?"(0 - "+p+")":0}b.each(e,function(b,d){var c=d[0].style;c.position="absolute";if(2>b)h?c.setExpression("height","Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:"+ a.quirksmodeOffsetHack+') + "px"'):c.setExpression("height",'this.parentNode.offsetHeight + "px"'),h?c.setExpression("width",'jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"'):c.setExpression("width",'this.parentNode.offsetWidth + "px"'),w&&c.setExpression("left",w),v&&c.setExpression("top",v);else if(a.centerY)h&&c.setExpression("top",'(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"'), c.marginTop=0;else if(!a.centerY&&h){var e="((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "+(a.css&&a.css.top?parseInt(a.css.top,10):0)+') + "px"';c.setExpression("top",e)}})}f&&(a.theme?c.find(".ui-widget-content").append(f):c.append(f),(f.jquery||f.nodeType)&&b(f).show());(s||a.forceIframe)&&a.showOverlay&&k.show();if(a.fadeIn)e=a.onBlock?a.onBlock:t,k=a.showOverlay&&!f?e:t,e=f?e:t,a.showOverlay&&j._fadeIn(a.fadeIn,k),f&&c._fadeIn(a.fadeIn, e);else if(a.showOverlay&&j.show(),f&&c.show(),a.onBlock)a.onBlock();x(1,d,a);h?(l=c[0],m=b(":input:enabled:visible",l),a.focusInput&&setTimeout(y,20)):(e=c[0],f=a.centerX,j=a.centerY,g=e.parentNode,c=e.style,k=(g.offsetWidth-e.offsetWidth)/2-(parseInt(b.css(g,"borderLeftWidth"),10)||0),e=(g.offsetHeight-e.offsetHeight)/2-(parseInt(b.css(g,"borderTopWidth"),10)||0),f&&(c.left=0<k?k+"px":"0"),j&&(c.top=0<e?e+"px":"0"));a.timeout&&(f=setTimeout(function(){h?b.unblockUI(a):b(d).unblock(a)},a.timeout), b(d).data("blockUI.timeout",f))}}function r(d,a){var e=d==window,g=b(d),h=g.data("blockUI.history"),f=g.data("blockUI.timeout");f&&(clearTimeout(f),g.removeData("blockUI.timeout"));a=b.extend({},b.blockUI.defaults,a||{});x(0,d,a);null===a.onUnblock&&(a.onUnblock=g.data("blockUI.onUnblock"),g.removeData("blockUI.onUnblock"));var j;j=e?b("body").children().filter(".blockUI").add("body > .blockUI"):g.find(">.blockUI");a.cursorReset&&(1<j.length&&(j[1].style.cursor=a.cursorReset),2<j.length&&(j[2].style.cursor= a.cursorReset));e&&(l=m=null);a.fadeOut?(j.fadeOut(a.fadeOut),setTimeout(function(){q(j,h,a,d)},a.fadeOut)):q(j,h,a,d)}function q(d,a,e,g){var h=b(g);d.each(function(){this.parentNode&&this.parentNode.removeChild(this)});a&&a.el&&(a.el.style.display=a.display,a.el.style.position=a.position,a.parent&&a.parent.appendChild(a.el),h.removeData("blockUI.history"));h.data("blockUI.static")&&h.css("position","static");if("function"==typeof e.onUnblock)e.onUnblock(g,e);d=b(document.body);a=d.width();e=d[0].style.width; d.width(a-1).width(a);d[0].style.width=e}function x(d,a,e){var g=a==window;a=b(a);if(d||!(g&&!l||!g&&!a.data("blockUI.isBlocked")))a.data("blockUI.isBlocked",d),e.bindEvents&&(!d||e.showOverlay)&&(d?b(document).bind("mousedown mouseup keydown keypress keyup touchstart touchend touchmove",e,p):b(document).unbind("mousedown mouseup keydown keypress keyup touchstart touchend touchmove",p))}function p(d){if(d.keyCode&&9==d.keyCode&&l&&d.data.constrainTabKey){var a=m,e=d.shiftKey&&d.target===a[0];if(!d.shiftKey&& d.target===a[a.length-1]||e)return setTimeout(function(){y(e)},10),!1}a=d.data;d=b(d.target);if(d.hasClass("blockOverlay")&&a.onOverlayClick)a.onOverlayClick();return 0<d.parents("div."+a.blockMsgClass).length?!0:0===d.parents().children().filter("div.blockUI").length}function y(b){m&&(b=m[!0===b?m.length-1:0])&&b.focus()}b.fn._fadeIn=b.fn.fadeIn;var t=b.noop||function(){},s=/MSIE/.test(navigator.userAgent),u=/MSIE 6.0/.test(navigator.userAgent)&&!/MSIE 8.0/.test(navigator.userAgent),z=b.isFunction(document.createElement("div").style.setExpression); b.blockUI=function(b){n(window,b)};b.unblockUI=function(b){r(window,b)};b.growlUI=function(d,a,e,g){var h=b('<div class="growlUI"></div>');d&&h.append("<h1>"+d+"</h1>");a&&h.append("<h2>"+a+"</h2>");void 0===e&&(e=3E3);b.blockUI({message:h,fadeIn:700,fadeOut:1E3,centerY:!1,timeout:e,showOverlay:!1,onUnblock:g,css:b.blockUI.defaults.growlCSS})};b.fn.block=function(d){var a=b.extend({},b.blockUI.defaults,d||{});this.each(function(){var d=b(this);(!a.ignoreIfBlocked||!d.data("blockUI.isBlocked"))&&d.unblock({fadeOut:0})}); return this.each(function(){"static"==b.css(this,"position")&&(this.style.position="relative",b(this).data("blockUI.static",!0));this.style.zoom=1;n(this,d)})};b.fn.unblock=function(b){return this.each(function(){r(this,b)})};b.blockUI.version=2.57;b.blockUI.defaults={message:"<h1>Please wait...</h1>",title:null,draggable:!0,theme:!1,css:{padding:0,margin:0,width:"30%",top:"40%",left:"35%",textAlign:"center",color:"#000",border:"3px solid #aaa",backgroundColor:"#fff",cursor:"wait"},themedCSS:{width:"30%", top:"40%",left:"35%"},overlayCSS:{backgroundColor:"#000",opacity:0.6,cursor:"wait"},cursorReset:"default",growlCSS:{width:"350px",top:"10px",left:"",right:"10px",border:"none",padding:"5px",opacity:0.6,cursor:"default",color:"#fff",backgroundColor:"#000","-webkit-border-radius":"10px","-moz-border-radius":"10px","border-radius":"10px"},iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank",forceIframe:!1,baseZ:1E3,centerX:!0,centerY:!0,allowBodyStretch:!0,bindEvents:!0, constrainTabKey:!0,fadeIn:200,fadeOut:400,timeout:0,showOverlay:!0,focusInput:!0,onBlock:null,onUnblock:null,onOverlayClick:null,quirksmodeOffsetHack:4,blockMsgClass:"blockMsg",ignoreIfBlocked:!1};var l=null,m=[]}"function"===typeof define&&define.amd&&define.amd.jQuery?define(["jquery"],n):n(jQuery)})();

/**
 * Notification :: jQuery Plugin for Bootstrap v1.0.0
 * Copyright (c) VimalVS.com | http://www.ennexa.com/
 * Dual licensed under MIT and GPL.
 */
(function($){var $wrapper;$.notification=function(msg,opt){if(!$wrapper)$wrapper=$('<ul id="notification"></ul>').appendTo(document.body);if(typeof msg==="string")msg="<p>"+msg+"</p>";var options=this.settings=$.extend({},$.notification.defaults,opt);this.init(msg,options)};$.extend($.notification,{defaults:{"class":"alert-info",action:"append",timeout:5E3,close:'<a href="#" data-dismiss="alert" class="close">&times;</a>',click:new Function("e","this.close();e.stopPropagation();")},prototype:{elem:null, timer:null,settings:null,init:function(msg,options){var that=this;var close=$.proxy(this.close,this);this.__startTimer=function(){that.timer=setTimeout(close,options.timeout)};this.message=$(msg);var alert=$('<li class="alert '+options["class"]+' fade"/>').append($(options.close),this.message).click($.proxy(options.click,this)).data("notification",this);alert[0].offsetWidth;$wrapper[options.action](alert);alert.addClass("in");if(options.timeout){alert.hover(function(){clearTimeout(that.timer)}, function(){that.__startTimer()});this.__startTimer()}},close:function(){this.message.alert("close")}}});$.fn.notification=function(){var args=arguments;return this.each(function(){var $t=$(this);if(args[0]==="close")$t.data("notification").close();else $t.data("notification",new $.notification(this,args[0]||{}))})}})(jQuery);

/**
 * Animate Class Plugin (c) Ennexa Technolgoies (P) Ltd
 */
$.fn.animateClass = function (o) {
	o = o || {};
	return this.each(function () {
		var $t = $(this), times = o.animate||$t.data('animate') || 10, delay = o.delay||$t.data('animate-delay') || 500, hc = o.class||$t.data('animate-class')||'highlight';
		(function highlight () {
			if (times-- > 0) {
				$t.toggleClass(hc);
				setTimeout(highlight, delay);
			}
		})();
	});
};

/*
 * jQuery BBQ: Back Button & Query Library - v1.2.1 - 2/17/2010
 * http://benalman.com/projects/jquery-bbq-plugin/
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 */
(function($,p){var i,m=Array.prototype.slice,r=decodeURIComponent,a=$.param,c,l,v,b=$.bbq=$.bbq||{},q,u,j,e=$.event.special,d="hashchange",A="querystring",D="fragment",y="elemUrlAttr",g="location",k="href",t="src",x=/^.*\?|#.*$/g,w=/^.*\#/,h,C={};function E(F){return typeof F==="string"}function B(G){var F=m.call(arguments,1);return function(){return G.apply(this,F.concat(m.call(arguments)))}}function n(F){return F.replace(/^[^#]*#?(.*)$/,"$1")}function o(F){return F.replace(/(?:^[^?#]*\?([^#]*).*$)?.*/,"$1")}function f(H,M,F,I,G){var O,L,K,N,J;if(I!==i){K=F.match(H?/^([^#]*)\#?(.*)$/:/^([^#?]*)\??([^#]*)(#?.*)/);J=K[3]||"";if(G===2&&E(I)){L=I.replace(H?w:x,"")}else{N=l(K[2]);I=E(I)?l[H?D:A](I):I;L=G===2?I:G===1?$.extend({},I,N):$.extend({},N,I);L=a(L);if(H){L=L.replace(h,r)}}O=K[1]+(H?"#":L||!K[1]?"?":"")+L+J}else{O=M(F!==i?F:p[g][k])}return O}a[A]=B(f,0,o);a[D]=c=B(f,1,n);c.noEscape=function(G){G=G||"";var F=$.map(G.split(""),encodeURIComponent);h=new RegExp(F.join("|"),"g")};c.noEscape(",/");$.deparam=l=function(I,F){var H={},G={"true":!0,"false":!1,"null":null};$.each(I.replace(/\+/g," ").split("&"),function(L,Q){var K=Q.split("="),P=r(K[0]),J,O=H,M=0,R=P.split("]["),N=R.length-1;if(/\[/.test(R[0])&&/\]$/.test(R[N])){R[N]=R[N].replace(/\]$/,"");R=R.shift().split("[").concat(R);N=R.length-1}else{N=0}if(K.length===2){J=r(K[1]);if(F){J=J&&!isNaN(J)?+J:J==="undefined"?i:G[J]!==i?G[J]:J}if(N){for(;M<=N;M++){P=R[M]===""?O.length:R[M];O=O[P]=M<N?O[P]||(R[M+1]&&isNaN(R[M+1])?{}:[]):J}}else{if($.isArray(H[P])){H[P].push(J)}else{if(H[P]!==i){H[P]=[H[P],J]}else{H[P]=J}}}}else{if(P){H[P]=F?i:""}}});return H};function z(H,F,G){if(F===i||typeof F==="boolean"){G=F;F=a[H?D:A]()}else{F=E(F)?F.replace(H?w:x,""):F}return l(F,G)}l[A]=B(z,0);l[D]=v=B(z,1);$[y]||($[y]=function(F){return $.extend(C,F)})({a:k,base:k,iframe:t,img:t,input:t,form:"action",link:k,script:t});j=$[y];function s(I,G,H,F){if(!E(H)&&typeof H!=="object"){F=H;H=G;G=i}return this.each(function(){var L=$(this),J=G||j()[(this.nodeName||"").toLowerCase()]||"",K=J&&L.attr(J)||"";L.attr(J,a[I](K,H,F))})}$.fn[A]=B(s,A);$.fn[D]=B(s,D);b.pushState=q=function(I,F){if(E(I)&&/^#/.test(I)&&F===i){F=2}var H=I!==i,G=c(p[g][k],H?I:{},H?F:2);p[g][k]=G+(/#/.test(G)?"":"#")};b.getState=u=function(F,G){return F===i||typeof F==="boolean"?v(F):v(G)[F]};b.removeState=function(F){var G={};if(F!==i){G=u();$.each($.isArray(F)?F:arguments,function(I,H){delete G[H]})}q(G,2)};e[d]=$.extend(e[d],{add:function(F){var H;function G(J){var I=J[D]=c();J.getState=function(K,L){return K===i||typeof K==="boolean"?l(I,K):l(I,L)[K]};H.apply(this,arguments)}if($.isFunction(F)){H=F;return G}else{H=F.handler;F.handler=G}}})})(jQuery,this);

/**
 * jQuery serializeObject
 * @copyright 2014, macek <paulmacek@gmail.com>
 * @link https://github.com/macek/jquery-serialize-object
 * @license BSD
 * @version 2.5.0
 */
!function(e,i){if("function"==typeof define&&define.amd)define(["exports","jquery"],function(e,r){return i(e,r)});else if("undefined"!=typeof exports){var r=require("jquery");i(exports,r)}else i(e,e.jQuery||e.Zepto||e.ender||e.$)}(this,function(e,i){function r(e,r){function n(e,i,r){return e[i]=r,e}function a(e,i){for(var r,a=e.match(t.key);void 0!==(r=a.pop());)if(t.push.test(r)){var u=s(e.replace(/\[\]$/,""));i=n([],u,i)}else t.fixed.test(r)?i=n([],r,i):t.named.test(r)&&(i=n({},r,i));return i}function s(e){return void 0===h[e]&&(h[e]=0),h[e]++}function u(e){switch(i('[name="'+e.name+'"]',r).attr("type")){case"checkbox":return"on"===e.value?!0:e.value;default:return e.value}}function f(i){if(!t.validate.test(i.name))return this;var r=a(i.name,u(i));return l=e.extend(!0,l,r),this}function d(i){if(!e.isArray(i))throw new Error("formSerializer.addPairs expects an Array");for(var r=0,t=i.length;t>r;r++)this.addPair(i[r]);return this}function o(){return l}function c(){return JSON.stringify(o())}var l={},h={};this.addPair=f,this.addPairs=d,this.serialize=o,this.serializeJSON=c}var t={validate:/^[a-z_][a-z0-9_]*(?:\[(?:\d*|[a-z0-9_]+)\])*$/i,key:/[a-z0-9_]+|(?=\[\])/gi,push:/^$/,fixed:/^\d+$/,named:/^[a-z0-9_]+$/i};return r.patterns=t,r.serializeObject=function(){return new r(i,this).addPairs(this.serializeArray()).serialize()},r.serializeJSON=function(){return new r(i,this).addPairs(this.serializeArray()).serializeJSON()},"undefined"!=typeof i.fn&&(i.fn.serializeObject=r.serializeObject,i.fn.serializeJSON=r.serializeJSON),e.FormSerializer=r,r});
/*
 * jQuery Hotkeys Plugin
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Based upon the plugin by Tzury Bar Yochay:
 * http://github.com/tzuryby/hotkeys
 *
 * Original idea by:
 * Binny V A, http://www.openjs.com/scripts/events/keyboard_shortcuts/
*/
(function(d){function h(g){if("string"===typeof g.data){var h=g.handler,j=g.data.toLowerCase().split(" "),k="text password number email url range date month week time datetime datetime-local search color".split(" ");g.handler=function(a){if(!(this!==a.target&&(/textarea|select/i.test(a.target.nodeName)||-1<d.inArray(a.target.type,k)||"true"==$(a.target).prop("contenteditable")))){var c="keypress"!==a.type&&d.hotkeys.specialKeys[a.which],e=String.fromCharCode(a.which).toLowerCase(),b="",f={};a.altKey&&
"alt"!==c&&(b+="alt+");a.ctrlKey&&"ctrl"!==c&&(b+="ctrl+");a.metaKey&&(!a.ctrlKey&&"meta"!==c)&&(b+="meta+");a.shiftKey&&"shift"!==c&&(b+="shift+");c?f[b+c]=!0:(f[b+e]=!0,f[b+d.hotkeys.shiftNums[e]]=!0,"shift+"===b&&(f[d.hotkeys.shiftNums[e]]=!0));c=0;for(e=j.length;c<e;c++)if(f[j[c]])return h.apply(this,arguments)}}}}d.hotkeys={version:"0.8",specialKeys:{8:"backspace",9:"tab",13:"return",16:"shift",17:"ctrl",18:"alt",19:"pause",20:"capslock",27:"esc",32:"space",33:"pageup",34:"pagedown",35:"end",
36:"home",37:"left",38:"up",39:"right",40:"down",45:"insert",46:"del",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9",106:"*",107:"+",109:"-",110:".",111:"/",112:"f1",113:"f2",114:"f3",115:"f4",116:"f5",117:"f6",118:"f7",119:"f8",120:"f9",121:"f10",122:"f11",123:"f12",144:"numlock",145:"scroll",188:",",190:".",191:"/",224:"meta"},shiftNums:{"`":"~",1:"!",2:"@",3:"#",4:"$",5:"%",6:"^",7:"&",8:"*",9:"(","0":")","-":"_","=":"+",";":": ","'":'"',",":"<",".":">","/":"?","\\":"|"}};
d.each(["keydown","keyup","keypress"],function(){d.event.special[this]={add:h}})})(jQuery);

$.fn.animateClass = function (o) {
	o = o || {};
	return this.each(function () {
		var $t = $(this), times = o.animate||$t.data('animate') || 10, delay = o.delay||$t.data('animate-delay') || 500, hc = o.class||$t.data('animate-class')||'highlight';
		(function highlight () {
			if (times-- > 0) {
				$t.toggleClass(hc);
				setTimeout(highlight, delay);
			}
		})();
	});
};
$('[data-highlight]').animateClass();
/*
 * jQuery ajaxFileUpload
 * Depends on jQuery-File-Upload
*/
(function(c){function g(b,a){a=c.extend(!0,{},g.defaults,a);b=c(b);if(a.trigger){var d=this;b.one("click",function(){c(this).not(".fileupload-button").hide();d.init(b,a)})}else this.init(b,a)}g.defaults={target:"",callback:{success:null,error:null,complete:null},extra:{}};g.prototype={init:function(b,a){var d=this;this.options=a;this.element=c(b);var f=(THEME_ASSETS_PATH||"/assets")+"/lib/jquery-fileupload/js/";c.lazyLoad([[f+"vendor/jquery.ui.widget.js"],f+"jquery.fileupload.js"]).done(function(){var e; e=""===a.target?b:a.target||c('<div class="fileupload-dropzone"><span>Drag and Drop files here</span> <span class="label label-default">or</span> <button class="btn btn-default btn-xs fileupload-button">Browse</button></div>').appendTo(b);d.__target=e;var f=c('<input name="file" type="file" multiple="multiple" />').appendTo(e),g=e.parent().find(".fileupload-dropzone"),l=a.action||e.closest("form").attr("action"),h=c(".fileupload-button",e).click(function(){d.__browse();return!1});d.__browse=function(){this.__target.find("[name=file]").click()}; var k=h.is(".btn");e.show();e=c.extend({url:l,dataType:"json",formData:a.formData,dropZone:g},a.extra);f.fileupload(e).addClass("fileupload-input").bind("fileuploadalways",function(a,b){k&&h.button("reset");if("success"===b.textStatus){if(d.__trigger("complete",a,b)){var e=b.result.messages;"error"===e.type?d.__trigger("error",a,b)&&new c.notification(e.message[0].error,{"class":"alert-danger"}):d.__trigger("success",a,b)}}else d.__trigger("success",a,b)&&Util.alert("Error","Failed to upload. Try again later")}).bind("fileuploadsend", function(a,b){d.__trigger("start",a,b)&&k&&h.button("loading")});d.__trigger("init")})},browse:function(){this.__browse()},__trigger:function(b,a,d){var f,e=this.options[b];d=d||{};a=c.Event(a);a.type="ajaxupload"+b;a.target=this.element[0];if(b=a.originalEvent)for(f in b)f in a||(a[f]=b[f]);c(this.element).trigger(a,[this].concat(d));return!(c.isFunction(e)&&!1===e.apply(this.element,[a].concat(d))||a.isDefaultPrevented())}};c.fn.ajaxFileUploader=function(b){return this.each(function(){var a=c(this).data("ajaxFileUploader"); a||(a=new g(this,b),c(this).data("ajaxFileUploader",a));if(a[b])return a[b].apply(a,Array.prototype.slice.call(arguments,1))})}})(jQuery);

/**
 * jQuery.Util - Utility functions on top of jQuery
 * Copyright (c) VimalVS.com | http://www.ennexa.com/
 */
var Util = (function($) {
	var arLoginStatus = [], arReq = [];
	var dialogDefaults = {modal: '', callback : null, zIndex : 15000, autofocus : true};
	var ajaxProcessDefaults = {src:window, callback:{success:null, error:null, complete:null}, settings: {cache:false, type:'post', dataType: 'json', data: {}}};
	$.ROOT = '';
	$.blockUI.defaults.css = {};
	$.blockUI.defaults.overlayCSS = {};
	$.ajaxSetup({cache:true});

	Util = {
		d : $(document),
		template: {
			loaded: function(tpl) {
				return (typeof _tplPromise[tpl] != "undefined" && _tplPromise[tpl].state() === 'resolved');
			},
			load: function(tpl, callback) {
				console.warn('Util.template is deprecated, use Template instead');
				if (_tplPromise[tpl]) {
					if (callback) _tplPromise[tpl].done(callback);
				} else {
					var defer = new $.Deferred();
					_tplPromise[tpl] = defer;
					if (callback) defer.done(callback);
					Util.ajaxLoad(tpl, function(template) {
						var scripts = $(template).filter('script[type="text/javascript"]').each(function(i, j){
							var script   = document.createElement("script");
							script.type  = "text/javascript";
							if (j.src) script.src = j.src;
							else script.text  = j.innerHTML;
							document.body.appendChild(script);
						});
						$(template).appendTo('body').filter('script[type="text/x-jsrender"]').each(function(){
							$.templates(this.id, {markup : this, allowCode : true});
						});
						var defer = _tplPromise[tpl];
						defer.resolve();
					}, {settings: {cache : true, dataType: 'html', type: 'get'}});
				}
				return _tplPromise[tpl].promise();
			},
			render: function(tpl, data, callback, tplFile) {
				var defer = new $.Deferred();
				if (!tplFile && !$.render[tpl + 'Template']) {
					tplFile = THEME_BASE_PATH + '/templates/ajax/common.tpl.htm';
				}
				data = (typeof data =="string") ? Util.ajaxLoad(data) :  [data];
				$.when(data, tplFile ? this.load(tplFile) : null).done(function (data) {
					var node = $($.trim($.render[tpl + 'Template'](data[0])));
					callback.call(this, node);
					defer.resolve(node);
					$('#' + tpl + 'Template').trigger('template_render', [node]);
				});
				return defer.promise();
			}
		},
		ajax: {
			request: function() {
			
			},
			process: function() {
			
			}
		},
		ajaxProcess: function () {
			var url = false, callback = null, options = {}, process = true, o = {}, form = null;
			for (var i = 0, len = arguments.length; i < len; i++) {
				var type = typeof arguments[i];
				if (type == "object" && !$.isPlainObject(arguments[i])) form = arguments[i];
				else if (type == "object") options = arguments[i];
				else if (type == "function") callback = arguments[i];
				else if (type == "string" && !url) url = arguments[i];
				else process = arguments[i];
			}
			o = $.extend(true, {}, ajaxProcessDefaults, {url : url, form : form, callback: {success : callback}}, options);
			url = o.url;
			var data = '';
			if (o.form) {
				o.form = $(o.form);
				if (!url) url = o.url = o.form.attr('action');
				data = o.form.serialize();
			}
			var tmp = $.param($.extend(true, o.data, o.settings.data));
			data += (data ? '&' : '') + tmp;
			o.settings.data = data;
			if (o.block) {
				o.block = $(o.block);
				o.block = o.block.block({message: '<div class="loading-icon"></'+'div>'});
			}
			var root = Util.getRoot(url) || $.ROOT;
			if (arLoginStatus[root]) {
				if (!arReq[root]) {
					_ajaxLogin();  arReq[root] = {};
					$(window).bind('loginSuccess.queue', function(){
						arLoginStatus[root] = false;
						for (i in arReq[root]) {
							Util.ajaxProcess.apply(this, arReq[root][i]);
						}
					});
				}
				if(!arReq[root] || !arReq[root][url]) arReq[root][url] = [url, options, process];
				return false;
			}
			return _ajaxRequest(url, function(data){
				var callback = o.callback;
				var src = o.src;
				var type = !data ? 'error' : ((data.messages && data.messages.type) ? data.messages.type : '');
				if ((!type || type == "success") && callback.success) callback.success.call(src, data);
				else if(type == "error"){
					if (data && data.loginError) {
						arLoginStatus[root] = true;
						Util.ajaxProcess(url, options, process);
					}
					else if(callback.error) callback.error.call(src, data);
				}
				if (callback.complete) callback.complete.call(src, data);
				else if(data && data.messages) {
					var msgs = data.messages, code;
					if (msgs.message.length) {
						code = _getResponseMsgContent(msgs);
						if (msgs.title) code = '<h4>' + msgs.title + '</h4>' + code;
						new $.notification(code, {'class': 'alert-' + ((type === 'error') ? 'danger' : (type === 'success' ? 'success' : 'info'))});
					}
				}
				if (o.block) o.block.unblock();
			}, o.settings, process);
		},
		htmlspecialchars: function (text) {
			var div = document.createElement('div');
			var text = document.createTextNode(text);
			div.appendChild(text);
			return div.innerHTML
		},
		ucwords: function (str) {
			return str.replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();});
		},
		ajaxLoad: function (url, callback, opt) {
			return this.ajaxProcess(url, callback, opt, false);
		},
		getRoot: function(url) {
			if (url) {
				var pos = url.indexOf('://');
				return (pos == -1) ? false : (((pos = url.indexOf('/', 8)) == -1) ? url : url.substring(0, pos));
			} else {
				return $.ROOT || '';
			}
		},
		safeName: function(str) {
			return str.toLowerCase().replace(/[^a-z0-9]/g, '-').replace(/-+/g, '-').replace(/-*$/, '');
		},
		qsAddParam: function (url, ignoreEmpty, param) {
			var pos = url.indexOf('?');
			if (pos === -1) pos = url.length;
			var path = url.substring(0, pos), qs = url.substring(pos + 1);
			if (typeof ignoreEmpty !== "boolean") {
				param = ignoreEmpty;
				ignoreEmpty = true;
			}
			var param = $.extend({}, $.deparam(qs), param);
			if (ignoreEmpty) for (i in param) if (param[i] === "") delete param[i];
			var param = $.param(param);
			if (param !== "") path += '?' + param;
			return path;
		},
		notify: function(code, type) {
			type = type || 'error';
			new $.notification(code, {'class': 'alert-' + ((type === 'error') ? 'danger' : type)});
		},
		alert: function (title, message, callback, options) {
			var opt = getDialogOptions(arguments, {modal : false, buttons : ['#OK']});
			var cb = opt.callback;
			if (cb) {
				opt.callback = function(status) {return cb.call(this);}
			}
			this.showDialog(opt);
		},
		confirm: function (title, message, callback, options) {
			var opt = getDialogOptions(arguments, {modal : false, buttons : ['Cancel', {text: 'OK', priority: 'primary'}]});
			var cb = opt.callback;
			if (cb) {
				opt.callback = function(status) {return cb.call(this, Boolean(status === 1));}
			}
			this.showDialog(opt);
		},
		dialog: function (title, message, callback, options) {
			var opt = getDialogOptions(arguments, {});
			var cb = opt.callback;
			if (cb) {
				opt.callback = function(status) {return cb.call(this, status);}
			}
			this.showDialog(opt);
		},
		showDialog: function(options) {
			var dialog;
			var btns = [];
			var cb_orig = options.callback;
			var cb = function(e){
				var btn = $(e.target);
				var idx = btn.parent().children().index(btn);
				if (cb_orig && false === cb_orig.call(this, idx)) return false;
				$(this).popup('destroy').remove();
				e.stopPropagation();
			};
			var buttons = options.buttons;
			for (var i in buttons) {
				var button = buttons[i];
				if (typeof button === "string") {
					if (button[0] === '#') {
						button = {label : button.substr(1), action : cb, type: 'primary'};
					} else {
						button = {label : button, action : cb};
					}
				} else {
					button.label = button.text;
					button.action = button.click ? button.click : cb;
					button.type = (button.priority && button.priority == 'primary') ? 'primary' : '';
				}
				btns.push(button);
				if ((options.modal === '') && (['Cancel', 'Close'].indexOf(button.label) != -1)) {
					options.modal = false;
				}
			}
			if (options.modal === '') options.modal = true;
			if (options.modal) {
				$.extend(options, {backdrop: 'static', keyboard : false});
			} else {
				options.close = function(e){
					e.preventDefault();
					if (cb_orig && false === cb_orig.call(this, -1)) return false;
					$(this).popup('destroy').remove();
				};
			}
		
			options.buttons = btns;
			options.show = true;
			if (!options.title) options.title = ' ';
		
			var deferred = [Template.render('theme::dialog', options)];
			if (!$.fn.popup) deferred.push($.lazyLoad(THEME_ASSETS_PATH + '/js/popup.js'));
			$.when.apply($, deferred).then(function(node) {
				var $dialog = node.appendTo('body').show().popup(options).closest('.modal');
				if (options.autofocus) $dialog.find('.modal-footer .btn-primary').focus();
			});
		}
	};
	function getDialogOptions(args, defaults) {
		var ret = defaults;
		for (var i = 0, len = args.length; i < len; i++) {
			var type = typeof args[i];
			if (type == "string") {
				if (!ret.message) ret.message = args[i];
				else {
					ret.title = ret.message;
					ret.message = args[i];
				}
			} else if (type == "function") {
				ret.callback = args[i];
			} else if (type == "object") {
				ret = $.extend({}, args[i], ret);
			}
		}
		return $.extend({}, dialogDefaults, ret);
	}

	var frmLogin = $('#frmLogin').hide();
	var btnLogin = $('#btnLogin');
	function _ajaxLogin () {
		frmLogin.dialog({
			closeOnEscape : false, resizable : false, modal:true, width: 'auto',
			open:function(e, u){$(this).parent().children().children('.ui-dialog-titlebar-close').hide();}
		}).submit(function(){
			btnLogin.text('Verifying...').attr('disabled', '');
			_ajaxRequest('/manage/index.php?mode=login', function(data){
				btnLogin.text('Login').removeAttr('disabled');
				if (data && data.messages && data.messages.type == "success") {
					frmLogin.dialog('close');
					$(window).trigger('loginSuccess').unbind('loginSuccess.queue');
				}
			}, {data:$(this).serialize(), type:'post', dataType: 'json'}, false);
			return false;
		});
	}
	function _getResponseMsgContent(jmsgs) {
		var msg = jmsgs.message;
		var code = '', msgCount = 0;
		for (var i in msg) msgCount++;
		if ( msgCount > 1){
			for(i in msg) for (j in msg[i]) code += '<li class="'+j+'">' + msg[i][j] + '</li>';
			code = '<ul>' + code + '</ul>';
		} else {
			for (j in msg[0]) code = '<p>' + msg[0][j] + '</p>';
		}
		return code;
	}
	function _ajaxRequest (url, callback, settings, process) {
		var root;
		if (!(root = Util.getRoot(url))) url = $.ROOT + url;
		else if (root != Util.getRoot(document.location.href)) {
			url = url + (url.indexOf('?') != -1 ? '&jsonp_callback=?' : '?jsonp_callback=?');
		}

		return $.ajax($.extend(settings, {url:url, success:function(json){
			var jmsgs = json.messages;
			if (process && jmsgs && jmsgs.type && jmsgs.type != "success") {
				var buttons = [], actions = [];
				var btn = jmsgs.button;
				var code = _getResponseMsgContent(jmsgs);
				if (btn) {
					for (var i = 0, len = btn.length; i < len; i++){
						if (btn[i].value == 'Cancel') {
							buttons.push('Cancel');
							actions.push('#cancel');
						} else if(btn[i].value == 'Back') {
							buttons.push('Close');
							actions.push('#close');
						} else {
							buttons.push(btn[i].value);
							actions.push((btn[i].href.indexOf(':') != -1 || !root) ? btn[i].href : (root + btn[i].href))
						}
					}
				}
				if (!buttons.length) {
					buttons.push('Close');
					actions.push('#cancel');
				}
				Util.showDialog({
					title: jmsgs.title, 
					message: code, 
					buttons: buttons, 
					callback: function(idx){
						if (idx == -1 || actions[idx] == '#cancel') callback.call(this, null);
						else if(actions[idx] === '#close') callback.call(this, json);
						else _ajaxRequest(actions[idx], callback, settings, true);
					}
				});
			} else {
				callback.call(this, json);
			}
		}, error: function(){
			callback.call(this, null);
		}}));
	}
	return Util;
})(jQuery);

var Template = (function setupTemplate($, Util) {
	var _tplPromise = {}, 
		_tplCallback = {}, 
		_tplRenderCallback = {pre: {}, post: {}},
		_tplPath = {default: '/templates/ajax/common.tpl.htm'},
		_tplList = {},
		_tplEngine = {
			jsrender: {
				loaded: false,
				compile: function(id, tpl) {
					return $.templates(id, {markup : tpl, allowCode : true});
				},
				render: function (id, data) {
					return $.render[id](data);
				}
			},
			hogan: {
				loaded: false,
				compile: function (id, tpl) {
					return this.templates[id] = Hogan.compile(tpl.innerHTML);
				},
				render: function (id, data) {
					return this.templates[id].render(data);
				},
				templates: {}
			}
		
		};
	var resolved = new $.Deferred().resolve();
	function _invokeTemplateCallback(name, cb2, args) {
		var promise = _tplCallback[name] ? _tplCallback[name].apply(this, args) : resolved.promise();
		return promise.done(cb2);
	}
	function _prepareTemplate(tpl) {
		var id = tpl.id, data = tpl.dataset, type = tpl.type ? tpl.type.substr(7) : tpl.getAttribute('data-engine'), engine = _tplEngine[type];
		var queue = [
			_invokeTemplateCallback(data.preInit)
		];
		if (!engine.loaded) {
			queue.push($.lazyLoad(THEME_ASSETS_PATH + '/js/' + type + '.js').done(function () {
				engine.loaded = true;
			}));
		}
	
		var promise = $.when.apply($, queue).done(function () {
			$(document).triggerHandler('templateprecompile');
			_tplList[id] = {template: engine.compile(id, tpl), engine: engine, dependencies: (tpl.dataset.require||'').split(/\s*,\s*/)};
		})
		if (data.preRender) _tplRenderCallback.pre[id] = data.preRender;
		if (data.postRender) _tplRenderCallback.post[id] = data.postRender;
		return promise;
	}
	function _renderTemplate(id, data) {
		var tpl = _tplList[id], require = tpl.dependencies;
		var arPreRenderPromise = [
			_invokeTemplateCallback(_tplRenderCallback.pre[id], null, [data]) // Pre render callbacks
		];
		if (require.length) {
			// Load dependencies
			for (var i = 0, len = require.length; i < len; i++) {
				var group = require[i];
				if (group) arPreRenderPromise.push(Template.load(_tplPath[group]));
			}
		}
		return $.when.apply($, arPreRenderPromise).then(function () {
			return tpl.engine.render(id, data);
		});
	}
	Template = {
		loaded: function (tpl) {
			return (typeof _tplPromise[tpl] != "undefined" && _tplPromise[tpl].state() === 'resolved');
		},
		load: function (tpl, callback, isGroup) {
			if (isGroup) tpl = _tplPath[tpl];
			if (_tplPromise[tpl]) {
				if (callback) _tplPromise[tpl].done(callback);
			} else {
				var defer = new $.Deferred();
				_tplPromise[tpl] = defer;
				if (callback) defer.done(callback);
				Util.ajaxLoad(tpl, function(template) {
					var scripts = $(template).filter('script[type="text/javascript"],script:not([type])').each(function(i, j){
						var script   = document.createElement("script");
						script.type  = "text/javascript";
						if (j.src) script.src = j.src;
						else script.text  = j.innerHTML;
						document.body.appendChild(script);
					});
					var promises = [];
					$(template).filter('script[type="text/x-jsrender"],template').each(function(){
						promises.push(_prepareTemplate(this));
					});
					$.when.apply($, promises).then(function () {
						defer.resolve();
					});
				}, {settings: {cache : true, dataType: 'html', type: 'get'}});
			}
			return _tplPromise[tpl].promise();
		},
		render: function(tplName, data, tplFile, returnString) {
			var defer = new $.Deferred(), group = 'default', id;
			if (tplName.indexOf('::') !== -1) {
				var tmp = tplName.split('::');
				group = tmp[0];
				tplName = tmp[1];
			}
			id = tplName + 'Template';
			if (!$.render || !$.render[id]) {
				var elem = document.getElementById(id);
				if (elem) {
					tplFile = _prepareTemplate(elem);
				} else {
					tplFile = this.load(tplFile || _tplPath[group||'default']);
				}
			}

			data = (typeof data === "string") ? Util.ajaxLoad(data) :  [data || {}];
			$.when(data, tplFile ? tplFile : null)
			 .then(function (data, arg2) {
				data = data[0];
				$(document).triggerHandler('templateprerender.' + tplName, [data]);
			 
				_renderTemplate(id, data).then(function (node) {
					if (!returnString) node = $($.trim(node));
					defer.resolve(node).done(_invokeTemplateCallback(_tplRenderCallback.post[id], null, [node, data]));
					$(document).triggerHandler('templatepostrender.' + tplName, [node, data]);
				 })
			});
			return defer.promise();
		},
		callback: function (name, cb, run_once) {
			// Callbacks are useful when the execution has to be postponed till a non blocking action is completed (eg: ajax request)
			// for other purposes bind to the custom events
			_tplCallback[name] = function () {
				run_once = typeof run_once === "undefined" ? true : run_once;
				var defer = run_once ? new $.Deferred : null;
				var ran = false;
				return function () {
					var rval, deferred = defer || new $.Deferred();
					if (cb && !ran) {
						rval = cb.apply(this, [deferred].concat(Array.prototype.slice.call(arguments, 0)));
						if (run_once) ran = true;
						if (rval !== false)  deferred.resolve();
					}
					return deferred.promise();
				};
			}();
		},
		setTemplatePath: function (path, group, isDefault) {
			group = group || 'default';
			_tplPath[group] = path;
			if (isDefault) {
				_tplPath['default'] = path;
			}
		}
	}
	return Template;
})(jQuery, Util);

