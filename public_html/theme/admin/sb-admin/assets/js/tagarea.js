/**
 * jQuery.tagarea - 
 * Copyright (c) VimalVS.com | http://www.ennexa.com/
 * Dual licensed under MIT and GPL.
 * Date: 2011/12/06
 * @author Joyce Babu
 * @version 1.0.0
 *
 */
(function($) {
	var _orig_val = $.fn.val;
	$.fn.val = function() {
		if ($(this).is('[contenteditable]')) {
			return $.fn.text.apply(this, arguments);
		};
		return _orig_val.apply(this, arguments);
	};
	var TagArea = function(elem, options) {
		this.init(elem, options);
		
		// Handle Focus
		this.__inputSetFocus = $.proxy(function(e, data){
				if (!(data && data.originalEvent)) this.focus();
			}, this.__input);
		
		this.__inputTriggerEnterKey = function() {
			this.__input.trigger($.extend($.Event('keypress'), {keyCode : 13}));
		};
		this.__container.click(this.__inputSetFocus);
		this.__handle.focus(this.__inputSetFocus)
		this.parse();
	};
	$.extend(TagArea.prototype, {
		init : function(elem, options) {
			var tagarea = this, d = document, b = document.body;
			var options = this.options = options;
			var settings = this.settings = $.extend({}, {charCode: options.delimiter.charCodeAt(0), regex : new RegExp(options.delimiter + '(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))')});
			
			var handle = this.__handle = $(elem).addClass('tagarea');
			var container = this.__container = $('<div class="tagarea-container"><ul class="tagarea-tag-wrapper"/></div>')
				.insertAfter(handle)
				.height(options.height||handle.height())
				.on('click', 'a', function(){
					var $li = $(this).parent();
					var idx = container.find('li').index($li);
					tagarea.remove(idx);
					input.focus();
					return false;
				// }).on('click', 'li', function(e){
				// 	e.stopPropagation();
				}).on('dblclick', '.tagarea-tag', function(e){
					tagarea.edit(container.find('.tagarea-tag').index(this));
					e.stopPropagation();
				});
			var input = this.__input = $('<span contenteditable class="tagarea-input"/>');
			this.__inputWrapper = $('<li class="tagarea-input-wrapper"/>').append(input).appendTo(container.find('ul'));
			if (options.bindEvents) input.on(options.bindEvents, function(e, data){
				var d = {data : data, originalEvent : e};
				handle.triggerHandler(e.type, d, e);
			});
			
			var origText = '';
			if (!handle[0].value) input.addClass(options.placeholderClass).text(options.placeholder);
			input.focus(function(e){
				container.addClass('input-focus');
				if (input.hasClass(options.placeholderClass)) input.removeClass(options.placeholderClass).text('');
				origText = input.text();
				try {
					if (typeof window.getSelection != "undefined" && typeof d.createRange != "undefined") {
						var range = d.createRange();
						range.selectNodeContents(this);
						range.collapse(false);
						var sel = window.getSelection();
						sel.removeAllRanges();
						sel.addRange(range);
					} else if (typeof b.createTextRange != "undefined") {
						var textRange = b.createTextRange();
						textRange.moveToElementText(this);
						textRange.collapse(false);
						textRange.select();
					}
		        } catch (err) {}
			}).blur(function(){
				var text = input.text();
				if (origText != text) input.trigger('change');
				origText = text;
				container.removeClass('input-focus');
				if (options.placeholder && (text == handle[0].value) && (handle[0].value == '')) input.addClass(options.placeholderClass).text(options.placeholder);
			}).keypress(function(e){
				if (e.keyCode == 13 || e.which == settings.charCode) {
					var text = input.text();
					if (text.split('"').length % 2 === 0) return;
					var tags = text.split(settings.regex);
					if ((tags[0] === '"') && (tags[tags.length - 1] !== '"')) return;
					for (var i = 0; i < tags.length; i++) {
						tagarea.add(tags[i]);
					}
					input.text('');
                    e.preventDefault();
                } else if (e.keyCode === 8) {
					var text = input.text();
					if (!text) {
						if (tagarea.count) {
							tagarea.edit(tagarea.count - 1);
						}
						e.preventDefault();
						return false;
					}
				}
			});
			var events = ['beforeadd', 'afteradd', 'beforedit', 'afteredit'];
			for (var i = 0; i < 4; i++) {
				var event = events[i];
				if (options[event]) this.__container.on('tagarea' + event, options[event]);
			}

			input.val = function() {
				return this.text.apply(this, arguments);
			}

			handle.hide();
			$(elem.form).submit(function(e){
				if (input.text() && !input.hasClass(options.placeholderClass)) tagarea.__inputTriggerEnterKey();
			});
		},
		parse : function() {
			var handle = this.__handle;
			var tags = handle.val().split(this.settings.regex);
			handle.val('').hide();
			for (var i = 0; i < tags.length; i++) this.add(tags[i]);
			this.count = tags.length;
		},
		add : function(displaytag, tag) {
			var handle = this.__handle;
			displaytag = displaytag.replace(/\s/g, ' ');
			if (!tag) tag = displaytag;
			var options = this.options;
			var tagData = {tag:tag.replace(/\s/g, ' '), displaytag: displaytag};
			if ((this.__container.triggerHandler('tagareabeforeadd', tagData) !== false) && tagData.tag) {
				handle[0].value = (handle[0].value === '') ? tagData.tag : (handle[0].value + options.delimiter + tagData.tag);
				var element = $('<li class="tagarea-tag">'+tagData.displaytag+'<a class="close" href="#">x</a></li>').data('tagdata', tagData).insertBefore(this.__inputWrapper);
				this.__container.trigger('tagareaafteradd', {item : tagData, element : element});
				this.count++;
			}
		},
		remove : function(idx) {
			var handle = this.__handle;
			var tags = handle.val().split(this.settings.regex);
			tags.splice(idx, 1);
			handle.val(tags.join(this.options.delimiter));
 			this.__container.find('.tagarea-tag').eq(idx).remove();
			this.count--;
		},
		edit : function(idx) {
			var $li = this.__container.find('.tagarea-tag').eq(idx);
			var data = $li.data('tagdata');
			if ((this.__container.triggerHandler('tagareabeforeedit', data) !== false)) {
				this.__inputTriggerEnterKey();
				this.remove(idx);
				this.__input.text(data.tag).focus();
				this.__container.trigger('tagareaafteredit', {item : data});
			}
		}
	});

	$.fn.tagarea = function (method) {
		var args = arguments;
		return this.each(function(){
			var $this = $(this);
			var tagarea = $(this).data('tagarea');
			if (!tagarea) {
				var options = $.extend({}, $.fn.tagarea.defaults, {delimiter : $this.attr('data-tagarea-delimiter'), placeholder : $this.attr('placeholder')});
                if ($.isPlainObject(args[0])) {
                    options = $.extend(true, options, args[0]);
                }
				$(this).data('tagarea', new TagArea(this, options));
			} else if (tagarea[method]) {
				return tagarea[method].apply(tagarea, Array.prototype.slice.call(args, 1));
			} else if ( typeof method === 'object' || ! method ) {
				// return methods.init.apply( this, args );
			} else {
				$.error( 'Method ' +  method + ' does not exist on jQuery.tagarea' );
			}
		});
	}

	$.fn.tagarea.defaults = {
		height: 'auto',
		delimiter : ',',
		placeholder : '',
		placeholderClass : 'tagarea-input-placeholder',
		tagadd : null,
		tagremove : null,
		tagupdate : null,
		maxinputsize : 300,
		mininputsize : 30,
		bindEvents : 'change focus blur'
	}
})(jQuery);