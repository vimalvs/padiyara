var Theme = Theme || {};
var Util = Util || {};

(function ($, window, document, undefined) {

    var pluginName = "metisMenu",
        defaults = {
            toggle: true
        };
        
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {

            var $this = $(this.element),
                $toggle = this.settings.toggle;

            $this.find('li.active').has('ul').children('ul').addClass('collapse in');
            $this.find('li').not('.active').has('ul').children('ul').addClass('collapse');

            $this.find('li').has('ul').children('a').on('click', function (e) {
                e.preventDefault();

                $(this).parent('li').toggleClass('active').children('ul').collapse('toggle');

                if ($toggle) {
                    $(this).parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
                }
            });
        }
    };

    $.fn[ pluginName ] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);

(function($){
	Theme = $.extend(Theme, {
		init: function (parent) {
			parent = parent || document;
			window.tplCommon = THEME_BASE_PATH + '/templates/ajax/common.tpl.htm';
			Template.setTemplatePath(tplCommon, 'theme', false);
			$(function(){
				Theme.applyStyle(parent);
			});
			$(parent).on('domChange.apply-theme', function(e){
				Theme.applyStyle(e.target);
			}).on('change.apply-theme', '.input-color-replacement', function (e) {
				$(this).parent().find('.color-preview').css('background-color', this.value);
			}).on('focus.apply-theme', '.input-color-replacement', function () {
				this.select()
			}).on('dblclick.load-spectrum.apply-theme', '.input-color-replacement', function () {
				var $t = $(this);
				if (!$.fn.spectrum) {
					var base = THEME_ASSETS_PATH + '/js/spectrum';
					$.lazyLoad([base + '/spectrum.js', base + '/spectrum.css']).done(function () {
						$t.triggerHandler('dblclick.load-spectrum');;
					});
				} else {
					if (!$t.data('spectrum.id')) {
						$t.spectrum({
							showInitial: true,
							showInput: true,
							showPalette: true,
							showAlpha: true,
							allowEmpty: true,
							preferredFormat: "hex",
							replacerClassName: "spectrum-replacer",
							containerClassName: "spectrum-container",
							showSelectionPalette: true,
							localStorageKey: "spectrum.palette",
							"palette": [
								["#ffebee","#fce4ec","#f3e5f5","#ede7f6","#e8eaf6","#e3f2fd","#e1f5fe","#e0f7fa","#e0f2f1","#e8f5e9","#f1f8e9","#f9fbe7","#fffde7","#fff8e1","#fff3e0","#fbe9e7","#efebe9","#fafafa","#eceff1"],
								["#ffcdd2","#f8bbd0","#e1bee7","#d1c4e9","#c5cae9","#bbdefb","#b3e5fc","#b2ebf2","#b2dfdb","#c8e6c9","#dcedc8","#f0f4c3","#fff9c4","#ffecb3","#ffe0b2","#ffccbc","#d7ccc8","#f5f5f5","#cfd8dc"],
								["#ef9a9a","#f48fb1","#ce93d8","#b39ddb","#9fa8da","#90caf9","#81d4fa","#80deea","#80cbc4","#a5d6a7","#c5e1a5","#e6ee9c","#fff59d","#ffe082","#ffcc80","#ffab91","#bcaaa4","#eeeeee","#b0bec5"],
								["#e57373","#f06292","#ba68c8","#9575cd","#7986cb","#64b5f6","#4fc3f7","#4dd0e1","#4db6ac","#81c784","#aed581","#dce775","#fff176","#ffd54f","#ffb74d","#ff8a65","#a1887f","#e0e0e0","#90a4ae"],
								["#ef5350","#ec407a","#ab47bc","#7e57c2","#5c6bc0","#42a5f5","#29b6f6","#26c6da","#26a69a","#66bb6a","#9ccc65","#d4e157","#ffee58","#ffca28","#ffa726","#ff7043","#8d6e63","#bdbdbd","#78909c"],
								["#f44336","#e91e63","#9c27b0","#673ab7","#3f51b5","#2196f3","#03a9f4","#00bcd4","#009688","#4caf50","#8bc34a","#cddc39","#ffeb3b","#ffc107","#ff9800","#ff5722","#795548","#9e9e9e","#607d8b"],
								["#e53935","#d81b60","#8e24aa","#5e35b1","#3949ab","#1e88e5","#039be5","#00acc1","#00897b","#43a047","#7cb342","#c0ca33","#fdd835","#ffb300","#fb8c00","#f4511e","#6d4c41","#757575","#546e7a"],
								["#d32f2f","#c2185b","#7b1fa2","#512da8","#303f9f","#1976d2","#0288d1","#0097a7","#00796b","#388e3c","#689f38","#afb42b","#fbc02d","#ffa000","#f57c00","#e64a19","#5d4037","#616161","#455a64"],
								["#c62828","#ad1457","#6a1b9a","#4527a0","#283593","#1565c0","#0277bd","#00838f","#00695c","#2e7d32","#558b2f","#9e9d24","#f9a825","#ff8f00","#ef6c00","#d84315","#4e342e","#424242","#37474f"],
								["#b71c1c","#880e4f","#4a148c","#311b92","#1a237e","#0d47a1","#01579b","#006064","#004d40","#1b5e20","#33691e","#827717","#f57f17","#ff6f00","#e65100","#bf360c","#3e2723","#212121","#263238"],
								["#ff8a80","#ff80ab","#ea80fc","#b388ff","#8c9eff","#82b1ff","#80d8ff","#84ffff","#a7ffeb","#b9f6ca","#ccff90","#f4ff81","#ffff8d","#ffe57f","#ffd180","#ff9e80"],
								["#ff5252","#ff4081","#e040fb","#7c4dff","#536dfe","#448aff","#40c4ff","#18ffff","#64ffda","#69f0ae","#b2ff59","#eeff41","#ffff00","#ffd740","#ffab40","#ff6e40"],
								["#ff1744","#f50057","#d500f9","#651fff","#3d5afe","#2979ff","#00b0ff","#00e5ff","#1de9b6","#00e676","#76ff03","#c6ff00","#ffea00","#ffc400","#ff9100","#ff3d00"],
								["#d50000","#c51162","#aa00ff","#6200ea","#304ffe","#2962ff","#0091ea","#00b8d4","#00bfa5","#00c853","#64dd17","#aeea00","#ffd600","#ffab00","#ff6d00","#dd2c00"]
							],
							beforeShow: function(color) {
								$(this).spectrum("set", this.value);
							}
							
						}).on('change.color-update refresh-spectrum', function() {
							$(this).spectrum('set', this.value);
						}).spectrum("toggle").next().addClass('form-control').on('dblclick', function () {
							$(this).prev('.input-color-replacement').spectrum('destroy').focus();
						});
					}
				}
			});
			
			// var lazyPlugins = ['popup', 'editor', 'autosave', 'tagarea'];
			$.fn.lazy = function (plugin) {
				var deferred = null;
				var tmp = plugin.split('!');
				if (tmp.length === 2) {
					plugin = tmp[0];
					deferred = new $.Deferred();
				}
				args = Array.prototype.slice.call(arguments, 1)
				if ($.fn[plugin]) {
					var $t = $.fn[plugin].apply(this, args);
					return deferred ? deferred.resolve($t) : $t;
				} else {
					var t = this;
					$.lazyLoad(THEME_ASSETS_PATH + '/js/' + plugin + '.js', function () {
						t = $.fn[plugin].apply(t, args);
						return deferred ? deferred.resolve(t) : t;
					});
					return deferred;
				}
			};
			$(function() {
				var $body = $('body'), isFullWidth = $body.is('.full-width');
				$(document).on('click.toggle-class', '[data-toggle="class"]', function (e) {
					var data = $(this).data();
					$(data.target).toggleClass(data.class);
					e.preventDefault();
				});
				$(document).on('click', '.sidebar-toggle', function () {
					var isOpen = $(this).is('.sidebar-open') ? 1 : 0;
					$.cookie('sb-admin-sidebar-open',  isOpen);
					if (!isFullWidth) {
						if (isOpen) {
							var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
							if (width < 1670) $body.addClass('full-width');
						} else {
							$body.removeClass('full-width');
						}
					}
				});
				$(function () {
					if ($.cookie('sb-admin-sidebar-open') != 0) $('.sidebar-toggle').click();
				});
				if (!isFullWidth) {
					// $(window).bind("load resize", function() {
					// 				        var topOffset = 50, width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
					// 	console.log(width, $body);
					// 				        if (width < 1670) {
					// 		$body.addClass('full-width');
					// 				            // $('div.navbar-collapse').addClass('collapse')
					// 				            // topOffset = 100;
					// 				        } else {
					// 		$body.removeClass('full-width');
					// 				            // $('div.navbar-collapse').removeClass('collapse')
					// 				        }
					//
					// 				        // height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
					// 				        // height = height - topOffset;
					// 				        // if (height < 1) height = 1;
					// 				        // if (height > topOffset) {
					// 				        //     $("#page-wrapper").css("min-height", (height) + "px");
					// 				        // }
					// 				    });
				}
			});

			
			// var sidebarVisible = true;
			// $(document).on('click', '.sidebar-toggle', function () {
				// if (sidebarVisible) {
				// 	$('#page-wrapper').css({'margin-left': 0, transition: 'margin 0.2s'});
				// 	$('.navbar-static-side').css({'margin-left': -250, transition: 'margin 0.2s'});
				// } else {
				// 	$('#page-wrapper').css({'margin-left': 250, transition: 'margin 0.2s'});
				// 	$('.navbar-static-side').css({'margin-left': 0, transition: 'margin 0.2s'});
				// }
				// sidebarVisible = !sidebarVisible;
			// });
		},
		applyStyle : function(context) {
			$context = $(context || document.body);
			$context.trigger('themeapplystyle');
			
			$context.find('input[type="color"]').addClass('input-color-replacement').attr('type', 'text').each(function(){
				var $group = $(this).closest('.input-group');
				if (!$group.length) $group = $('<div class="input-group"/>').wrapInner(this);
				var $addon = $(this).prev('.input-group-addon');
				if (!$addon.length) $addon = $('<span class="input-group-addon"/>').insertBefore(this);
				$('<span class="color-preview"/>').css('background-color', this.value).appendTo($addon);
			}).on('change.color-update input.color-update paste.color-update', function () {
				$(this).parent().find('.color-preview').css('background-color', this.value);
			});
			
			$('#side-menu', $context).metisMenu();
			if ($('textarea[data-editor]', $context).length) {
				$.lazyLoad(THEME_ASSETS_PATH + '/js/editor.js', function(){
					$('textarea[data-editor]', $context).each(function(){
						$(this).editor($(this).data());
					});
				});
			}
			if ($(':text[data-datepicker]', $context).length) {
				var fn = function () {
					$(':text[data-datepicker]', $context).each(function(){
						var $t = $(this);
						$t.datepicker({dateFormat : ($t.attr('data-datepicker') || 'mm/dd/yy')});
					});
				}
				if ($.fn.datepicker) fn.call();
				else if (typeof jQuery.ui !== "undefined") new $.notification('jQuery UI loaded without Date Picker widget. Cannot convert input field.', {'class' : 'alert-error'});
				else $.lazyLoad([THEME_ASSETS_PATH + '/lib/ui-1.10/js/ui.js', THEME_ASSETS_PATH + '/lib/ui-1.10/css/ui.css'], fn);
			};
			$('a[data-process]', $context).click(function (e) {
				var $t = $(this);
				e.preventDefault();
				e.stopPropagation();
				Util.ajaxProcess(this.href, function () {
					var success = $t.data('process-success');
					if (success === 'remove') {
						$($t.data('process-item')).remove();
					}
				});
				return false;
			});
			$('[title]', $context).tooltip();
			$('[data-filter]', $context).click(function () {
				var $t = $(this), f, filter = $t.data('filter'), value = $t.data('filter-value') || $t.text(), action = $t.data('filter-action');
				var params = $.deparam.querystring(), dataFilter = params.query_filter || {};
				delete params.query_filter;
				delete params.page;
				var path = document.location.pathname + '?' + $.param(params);
				if (action === 'reset' && filter == '*') {
					f = {};
				} else if (action === 'remove') {
					if ($.isArray(dataFilter[filter])) {
						for (var i = dataFilter[filter].length; i >=0; i--) {
							if (dataFilter[filter][i] === value) {
								dataFilter[filter].splice(i, 1);
								break;
							}
						}
					} else {
						delete dataFilter[filter];
					}
					f = dataFilter;
				} else {
					f = (action === 'reset') ? {} : $.extend({}, dataFilter);
					f[filter] = value;
				}
				f = $.param({query_filter : f});
				if (f.length) path = path + ((path.indexOf('=') === -1) ? '' : '&') + f;
				document.location.href = path;
			}).attr('href', '#');
			if ($.fn.autosave) $('form[data-autosave]', $context).autosave({
				init : function(evt, autosave) {
					if (!autosave.check()) return ;
					var frm = $(this),
					msg = '<div class="alert alert-block alert-info fade in">';
					msg += '<a href="#" data-dismiss="alert" class="close">◊</a>';
					msg += '<h4 class="alert-heading">Auto saved version detected</h4>';
					msg += '<p>An automatically saved version of current form is available. Do you wish to load it?</p>';
					msg += '<p><a href="#" class="btn">Discard</a> <a href="#" class="btn">View</a> <a href="#" class="btn btn-info">Load Content</a></p>';
					msg += '</div>';
					var alert = $(msg).prependTo(frm).find('.btn').click(function(){
						if ($(this).text() === 'Discard') {
							autosave.discard();
						} else if ($(this).text() === 'View') {
							var data = autosave.data;
							var code = '';
							frm.find(autosave.options.selector).each(function () {
								if (typeof data[this.name] !== "undefined") {
									if (data[this.name] !== this.value) {
										var label = $('label[for="'+this.id+'"]', frm).html() || $(this).closest('label').html();
										code += '<tr><th style="max-width:200px;">' + (label || this.name) + '</th><td>' + diffString(this.value, data[this.name]) + '</td></tr>';
									}
								}
							});
							if (!code) new $.notification('No changes to display', {'class' : 'alert-error'});
							else {
								Util.template.render('modal', {title : 'Changes', body : '<table class="table table-striped table-autosave-diff">' + code + '</table>'}, function (code) {
									$(code).appendTo('body').modal();
								}, tplCommon);
							}
							return false;
						} else {
							autosave.load();
						}
						$(this).closest('.alert').remove();
						return false;
					});
				}
			});
			$('.chk-table-item:checked', $context).closest('tr').addClass('row-selected');
			var pagination = $('ul.pagination', $context);
			if (pagination.length) {
				var action = pagination.find('a[href]')[0].href;//.replace(/(?:\&|\?)page=[0-9]+/, '');
				var pages = pagination.children().eq(-2).text(), tmp = [5, 10, 25, 50, 100, 200];
				var code = '<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Jump To <b class="caret"></b></a><ul class="dropdown-menu">';
				var tmpUrl = Util.qsAddParam(Util.qsAddParam(action, {page : ''}), false, {page : ''});
				for (var i in tmp) {
					if (pages > tmp[i] ) code += '	<li><a href="' + tmpUrl + tmp[i] + '">Page ' + tmp[i] + '</a></li>'
				}
				if (pages > 5) code += '<li class="divider"/>';
				code += '<li><form class="form-inline" style="margin:0;" action="' + action + '" method="post"><label style="padding:5px;" class="control-label">Go To :</label><input name="page" type="text" class="form-control tc" style="width:50px;" placeholder="Page"> <input type="submit" value="Go" class="btn btn-info"></form></li>';
				code += '</ul></li>';
				var perpage = $.deparam.querystring(true).items_per_page || '';
				if (perpage) perpage = '<span class="label label-info">' + perpage + '</span>';
				code += '<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">' + perpage + ' Per Page <b class="caret"></b></a><ul class="dropdown-menu">';
				tmp = [10, 25, 50, 100];
				var tmpUrl = Util.qsAddParam(Util.qsAddParam(action, {items_per_page : '', page : ''}), false, {items_per_page : ''});
				for (var i in tmp) code += '<li><a href="' + tmpUrl + tmp[i] + '">' + tmp[i] + '</a></li>'
				code += '</ul></li>';
				pagination.append(code);
			}
			$('.chk-table-item', $context).closest('tr').addClass('chk-table-row');
			$context.on('click', '.chk-table-bulk-action', function() {
				var $t = $(this), $p = $t.parent(), idx = $p.parent().children().index($p);
				$t.closest('table').find('tbody td:nth-child(' + (idx + 1) + ') :checkbox').prop('checked', this.checked);
			}).on('click', '.chk-table-item', function (e) {
				e.stopPropagation();
				$(this).closest('tr')[this.checked ? 'addClass' : 'removeClass']('row-selected');
			}).on('click', '.chk-table-row', function () {
				var $t = $(this), $chk = $t.find('.chk-table-item'), checked = $chk.prop('checked');
				$chk.prop('checked', !checked);
				$t[!checked ? 'addClass' : 'removeClass']('row-selected');
			});
			$context.on('autosaveloaded', 'form[data-autosave]', function() {
				if ($.fn.editor) $('.editor', this).editor('update');
			});
			$(function(){
				$context.on('click', 'th.tablesorter-header', function(e){
					var a = $(e.target).children('a.tablesorter-header-inner');
					if (a.length) document.location.href = a[0].href;
					e.stopImmediatePropagation();
				});
			});
		}
	});
	
	(function(){
		/** Object.equals
		 * http://stackoverflow.com/questions/959670/generic-way-to-detect-if-html-form-is-edited#answer-6713782
		 */
		Object.equals=function(b,c){if(b===c)return!0;if(!(b instanceof Object)||!(c instanceof Object)||b.constructor!==c.constructor)return!1;for(var a in b)if(b.hasOwnProperty(a)&&(!c.hasOwnProperty(a)||b[a]!==c[a]&&("object"!==typeof b[a]||!Object.equals(b[a],c[a]))))return!1;for(a in c)if(c.hasOwnProperty(a)&&!b.hasOwnProperty(a))return!1;return!0};

		var methods = {
			init : function(options) {
				var $me = $(this);
				if ($me.data('dirty-form')) return;
				$me.data('dirty-form', $me.toJSON()).submit(function() {
					window.onbeforeunload = null;
				});
				window.onbeforeunload = function() {
					if (methods.check.apply($me)) return "You have made changes on this page that you have not yet confirmed. If you navigate away from this page you will lose your unsaved changes";
				}
			},
			check : function() {
				var $me = $(this);
				return !Object.equals($me.data('dirty-form'), $me.toJSON());
			}
		};
		$.fn.dirtyform = function(method) {
			var args = arguments;
			if (method === 'check') {
				return methods[method].apply(this);
			} else {
				return this.each(function(){
					if (this.tagName.toUpperCase() !== 'FORM') return;
					var dirtyForm = $(this).data('dirty-form');
					if ( methods[method] ) {
						return methods[method].apply( this, Array.prototype.slice.call( args, 1 ));
					} else if ( typeof method === 'object' || ! method ) {
						return methods.init.apply( this, args );
					} else {
						$.error( 'Method ' +  method + ' does not exist on jQuery.dirtyform' );
					}
				});
			}
		}
	})();
 })(jQuery);


/**
 * https://github.com/macek/jquery-to-json/
 */
(function(e){e.fn.toJSON=function(h){var h=e.extend({},h),f=this,g={},b={},i=/^[a-zA-Z][a-zA-Z0-9_-]*(?:\[(?:\d*|[a-zA-Z0-9_-]+)\])*$/,j=/[a-zA-Z0-9_-]+|(?=\[\])/g,k=/^$/,l=/^\d+$/,m=/^[a-zA-Z0-9_-]+$/;this.build=function(a,d,c){a[d]=c;return a};this.push_counter=function(a,d){void 0===b[a]&&(b[a]=0);if(void 0===d)return b[a]++;if(void 0!==d&&d>b[a])return b[a]=++d};e.each(e(this).serializeArray(),function(){if(i.test(this.name)){for(var a,d=this.name.match(j),c=this.value,b=this.name;void 0!==(a=d.pop());)b= b.replace(RegExp("\\["+a+"\\]$"),""),a.match(k)?c=f.build([],f.push_counter(b),c):a.match(l)?(f.push_counter(b,a),c=f.build([],a,c)):a.match(m)&&(c=f.build({},a,c));g=e.extend(!0,g,c)}});return g}})(jQuery);

/*
 * Javascript Diff Algorithm
 *  By John Resig (http://ejohn.org/)
 *  Modified by Chu Alan "sprite"
 *
 * Released under the MIT license.
 *
 * More Info:
 *  http://ejohn.org/projects/javascript-diff-algorithm/
 */
function htmlspecialchars(c){c=c.replace(/&/g,"&amp;");c=c.replace(/</g,"&lt;");c=c.replace(/>/g,"&gt;");return c=c.replace(/"/g,"&quot;")} function diffString(c,b){c=c.replace(/\s+$/,"");b=b.replace(/\s+$/,"");var d=diff(""==c?[]:c.split(/\s+/),""==b?[]:b.split(/\s+/)),e="",a=c.match(/\s+/g);null==a?a=["\n"]:a.push("\n");var h=b.match(/\s+/g);null==h?h=["\n"]:h.push("\n");if(0==d.n.length)for(var g=0;g<d.o.length;g++)e+="<del>"+htmlspecialchars(d.o[g])+a[g]+"</del>";else{if(null==d.n[0].text)for(b=0;b<d.o.length&&null==d.o[b].text;b++)e+="<del>"+htmlspecialchars(d.o[b])+a[b]+"</del>";for(g=0;g<d.n.length;g++)if(null==d.n[g].text)e+= "<ins>"+htmlspecialchars(d.n[g])+h[g]+"</ins>";else{var f="";for(b=d.n[g].row+1;b<d.o.length&&null==d.o[b].text;b++)f+="<del>"+htmlspecialchars(d.o[b])+a[b]+"</del>";e+=" "+d.n[g].text+h[g]+f}}return e}function randomColor(){return"rgb("+100*Math.random()+"%, "+100*Math.random()+"%, "+100*Math.random()+"%)"} function diffString2(c,b){c=c.replace(/\s+$/,"");b=b.replace(/\s+$/,"");var d=diff(""==c?[]:c.split(/\s+/),""==b?[]:b.split(/\s+/)),e=c.match(/\s+/g);null==e?e=["\n"]:e.push("\n");var a=b.match(/\s+/g);null==a?a=["\n"]:a.push("\n");for(var h="",g=[],f=0;f<d.o.length;f++)g[f]=randomColor(),h=null!=d.o[f].text?h+('<span style="background-color: '+g[f]+'">'+htmlspecialchars(d.o[f].text)+e[f]+"</span>"):h+("<del>"+htmlspecialchars(d.o[f])+e[f]+"</del>");e="";for(f=0;f<d.n.length;f++)e=null!=d.n[f].text? e+('<span style="background-color: '+g[d.n[f].row]+'">'+htmlspecialchars(d.n[f].text)+a[f]+"</span>"):e+("<ins>"+htmlspecialchars(d.n[f])+a[f]+"</ins>");return{o:h,n:e}} function diff(c,b){for(var d={},e={},a=0;a<b.length;a++)null==d[b[a]]&&(d[b[a]]={rows:[],o:null}),d[b[a]].rows.push(a);for(a=0;a<c.length;a++)null==e[c[a]]&&(e[c[a]]={rows:[],n:null}),e[c[a]].rows.push(a);for(a in d)1==d[a].rows.length&&"undefined"!=typeof e[a]&&1==e[a].rows.length&&(b[d[a].rows[0]]={text:b[d[a].rows[0]],row:e[a].rows[0]},c[e[a].rows[0]]={text:c[e[a].rows[0]],row:d[a].rows[0]});for(a=0;a<b.length-1;a++)null!=b[a].text&&null==b[a+1].text&&b[a].row+1<c.length&&null==c[b[a].row+1].text&& b[a+1]==c[b[a].row+1]&&(b[a+1]={text:b[a+1],row:b[a].row+1},c[b[a].row+1]={text:c[b[a].row+1],row:a+1});for(a=b.length-1;0<a;a--)null!=b[a].text&&null==b[a-1].text&&0<b[a].row&&null==c[b[a].row-1].text&&b[a-1]==c[b[a].row-1]&&(b[a-1]={text:b[a-1],row:b[a].row-1},c[b[a].row-1]={text:c[b[a].row-1],row:a-1});return{o:c,n:b}};
