/**
 * jQuery.popup - Modal popup library for Twitter Bootstrap
 * Copyright (c) VimalVS.com | http://www.ennexa.com/
 * Dual licensed under MIT and GPL.
 * Date: 2011/12/02
 * @author Joyce Babu
 * @version 1.0.0
 *
 */

(function($){
	var __HTML = "<div class='modal'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><h3 class='modal-title'></h3><span class='loader'></span><a href='#' class='close'></a></div><div class='modal-footer'></div></div></div></div>";
	$.popup = function(elem, options) {
		var handle = this.__handle;
		this.__parent = elem.parent();
		this.init(elem, options);
	};
	
	$.extend($.popup, {
		defaults: {
			title: '',
			buttons: [],
			show: true,
			autoDestroy: true
		},
		prototype: {
			isOpen: false,
			init: function(elem, options) {
				var popup = this;
				options = $.extend({}, $.popup.defaults, options);
				if (options.modal) {
					options.keyboard = false;
					options.backdrop = 'static';
				}
				this.settings = {open : false, title : ''};
				var modal = $(__HTML).hide().appendTo('body');
				this.__close = $.proxy(this, 'close');
				this.body = elem.addClass('modal-body').insertAfter(modal.find('.modal-header'));
				this.modal = this.body.closest('.modal');
				this.modal.on('hide.bs.modal.popup', this.__close).modal(options);
				this.options(options);
			},
			open: function () {
				// console.log('Dialog Open');
				if (!this.settings.open) {
					this.modal.modal('show');
					this.settings.open = true;
					this.body.trigger('popupopen');
				}
			},
			close: function () {
				// console.log('Dialog Close');
				var settings = this.settings;
				if (settings.open) {
					settings.open = false;
					this.modal.modal('hide');
					this.body.trigger('popupclose');
					if (settings.options.autoDestroy) this.destroy();
				}
			},
			destroy: function () {
				// console.log('Dialog Destroy');
				var settings = this.settings;
				var modal = this.modal;
				if (settings.open) {
					settings.open = false;
					this.modal.modal('hide');
				}
				this.body.appendTo($(this.__parent || 'body')).removeData('popup');
				modal.remove();
				this.body.trigger('popupdestroy');
			},
			options: function(options) {
				var body = this.body, modal = this.modal, settings = this.settings;
				if (!options) return settings.options;
				if (options.title) {
					$('.modal-header h3', modal).text(options.title);
				}
				if (options.buttons) {
					this.removeButtons();
					if ($.isPlainObject(options.buttons)) {
						for (var i in options.buttons) {
							this.addButton(i, options.buttons[i]);
						}
					} else {
						for (var i in options.buttons) {
							this.addButton(options.buttons[i]);
						}
					}
				}
				if (options.content) {
					body.empty().append(options.content);
				}
				var events = ['open', 'close', 'destroy', 'beforeajax', 'afterajax'];
				for (var i = events.length - 1; i >= 0; i--) {
					if (options[events[i]]) body.on('popup' + events[i] + '.popup', options[events[i]]);
				}
				if (options.show) this.open();
				else if (options.show === false) this.close();
				
				if (options.width) {
					var dialog = this.modal.children().removeClass('modal-lg modal-sm').css('width', '');
					if (isNaN(options.width)) {
						dialog.addClass(options.width);
					} else {
						dialog.css('width', options.width);
					}
				}
				
				settings.options = $.extend(settings.options || {}, options);
			},
			addButton: function(label, action) {
				var popup = this;
				var footer = this.body.next();
				var options = $.isPlainObject(label) ? label : {label : label, action : action};

				if (label[0] === '#') {
					$.extend(options, {label : label.substr(1), type : 'primary'});
				}

				if (typeof options.action === "string") {
					var url = options.action;
					if (url === '#popup-close') options.type = 'close';
					else options.action = function() {
						popup.load(url);
					};
				}
				var type = !options.type ? null : options.type;

				if (type === 'close') {
					$('<button class="btn btn-default">' + options.label + '</button>').click($.proxy(popup.close, popup)).appendTo(footer);
				} else {
					var cls = type ? 'btn btn-' + type : 'btn btn-default';
					$('<button class="' + cls + '">' + options.label + '</button>').click(function(e) {
						if (options.action) options.action.apply(popup.body, arguments);
					}).appendTo(footer);
				}
			},
			removeButtons: function() {
				var footer = this.body.next();
				$('.btn', footer).remove();
			}
		}
	});
	var methods = {
		init: function(options) {
			var $me = $(this);
			if ($me.data('popup')) return;
			var popup = new $.popup($me, options);
			$me.data('popup', popup);
		},
		options: function(options) {
			$(this).data('popup').options(options);
		},
		close: function() {
			$(this).data('popup').close();
		},
		destroy: function() {
			$(this).data('popup').destroy();
		}
	};
	$.fn.popup = function (method) {
		var args = arguments;
		return this.each(function(){
			var popup = $(this).data('popup');
			if ( methods[method] ) {
				return methods[method].apply( this, Array.prototype.slice.call( args, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
				return methods.init.apply( this, args );
			} else if (popup && popup[method]) {
				return popup[method](Array.prototype.slice.call(args, 1));
			} else {
				$.error( 'Method ' +  method + ' does not exist on jQuery.popup' );
			}
		});
	};
})(jQuery);
