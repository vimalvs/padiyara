<?php
include 'prepend.php';
$mode = getCleanVar('mode', 'dashboard');

$tpl->setTemplate("dashboard");

$mode = getCleanVar('mode', 'home');
extractCleanVars('action', 'p', 'redirect');
$pdbo = \SiteManager::getDataBase();
$tpl_view = "home";

$stmt = $pdbo->search("page_content");
$arPage = $stmt ? $stmt->fetchAll() : [];	

$tpl->addData(compact('mode', 'action', 'tpl_view', 'redirect', 'arPage'));

if (AJAX_REQUEST) {
	$tpl->generateJSON();
} else {
	$tpl->generate();
}
exit;