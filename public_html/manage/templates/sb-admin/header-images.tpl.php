<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Dashboard :: <?=SITE_NAME?></title>
<link href="<?=THEME_ASSETS_PATH?>/css/style.css" rel="stylesheet" data-skip-ajax="true">
<link href="<?=THEME_ASSETS_PATH?>/css/jquery-ui.min.css" rel="stylesheet" data-skip-ajax="true">
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-2.0.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/jquery-ui.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/notify.min.js" data-skip-ajax="true"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/util.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/theme.js"></script>
<script crossorigin="anonymous" src="<?=THEME_ASSETS_PATH?>/js/template.js"></script>
<script crossorigin="anonymous" src="/manage/assets/js/SimpleAjaxUploader.min.js"></script>
<script>
var Request = <?=json_encode(compact('mode', 'action', 'id', 'redirect'))?>;
</script>
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<style>
.border {
	border: 1px solid #ddd;
}
.progress {
    margin-bottom:0;
    margin-top:6px;
    margin-left:10px;
}
#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
#sortable li { margin: 0 3px 3px 3px; padding: 0.4em;}
</style>
</head>
<body>
<?php $this->render('theme::header', array('admin_section' => 'Dashboard', 'admin_section_url' => '/manage/'));?>
<div class="container admin-content-area" id="page-wrapper">

<?php $this->render('theme::breadcrumb');?>

<ul class="nav nav-tabs">
	<li class="nav-item <?=($tpl_view === 'home') ? 'active' : ''?>">
		<a class="nav-link" href="/manage/header-images.php">List</a>
	</li>
</ul>
<?php
?>

<?php if($this->messages->status === 'exit'):?>
	<?php $this->printMessage();?>
<?php elseif ($tpl_view === 'home'):?>
	<?php if(!empty($arImage)):?>
		<div class="pad border">
			<ul id="sortable">
				<?php foreach($arImage as $key => $image):?>
					<li id="<?=$image['image_order']?>" data-id="<?=$image['id']?>" class="ui-state-default">
						<div>
							<img src="<?=$image['thumbnail']?>" alt="<?=$image['thumbnail']?>">
						</div>
						<div class="pad tl">
							<button class="btn btn-primary btn-remove" data-id="<?=$image['id']?>">Remove</button>
						</div>
					</li>
				<?php endforeach;?>
			</ul>	
		</div>
	<?php endif;?>
	<div class="container pad-large">
		<p>Click here to choose image files</p>
		<div class="row">
			<div class="col-xs-4">
				<button id="uploadBtn" class="btn btn-large btn-primary">Choose File</button>
			</div>
			<div class="col-xs-8">
				<div id="progressOuter" class="progress progress-striped active" style="display:none;">
					<div id="progressBar" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
					</div>
				</div>
			</div>
			 <div  class="col-xs-12" id="msgBox">
		  </div>
		</div>
	</div>
<?php elseif ($tpl_view === 'form'):?>
	<div class="container">
		<div class="row">
			<div class="col-xs-2">
				<button id="uploadBtn" class="btn btn-large btn-primary">Choose File</button>
			</div>
			<div class="col-xs-8">
				<div id="progressOuter" class="progress progress-striped active" style="display:none;">
					<div id="progressBar" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
					</div>
				</div>
			</div>
			 <div  class="col-xs-2" id="msgBox">
		  </div>
		</div>
	</div>
<?php endif;?>
	
</div>
<?php $this->render('theme::footer');?>

<script>
	$(function() {
		$( "#sortable" ).sortable();
		$('#sortable').sortable({
		  update: function(event, ui) {
		  	var arOrder = [];
		  	$( "#sortable li").each(function(index, item) {
		  	  arOrder.push({'id':$(item).data('id'), 'index' : index});
		  	});
		    var postData = {
				order : arOrder,
				mode: 'images',
				action: 'update-order',
			}
		  	Util.ajaxProcess("/manage/header-images.php", {data: postData}).done(function(response) {
		  		console.log(response);
				if (response == 1) {
					$.notify("Order Changed Successfully", "success");
					// $t.closest('li').remove();	
				} else {
					$.notify("Order Changing failed", "error");
				}
			});	
		    var order = $(this).sortable('toArray');
		  }
		});


		$('.btn-remove').click(function() {
			var $t  = $(this);
			var image_id  = $t.data('id');
			var postData = {
				image_id : image_id,
				mode: 'image',
				action: 'remove',
			}
			Util.ajaxProcess("/manage/header-images.php", {data: postData}).done(function(response) {
				if (response == 1) {
					$.notify("Successfully Deleted", "success");
					$t.closest('li').remove();	
				} else {
					$.notify("Not Deleted", "error");
				}
			});	
		});
	});
function escapeTags( str ) {
	return String( str )
	.replace( /&/g, '&amp;' )
	.replace( /"/g, '&quot;' )
	.replace( /'/g, '&#39;' )
	.replace( /</g, '&lt;' )
	.replace( />/g, '&gt;' );
}

window.onload = function() {

	var btn = document.getElementById('uploadBtn'),
		progressBar = document.getElementById('progressBar'),
		progressOuter = document.getElementById('progressOuter'),
		msgBox = document.getElementById('msgBox');

	var uploader = new ss.SimpleUpload({
	    button: btn,
	    url: '/manage/header-images.php?mode=images&action=upload',
	    name: 'uploadfile',
	    multipart: true,
	    multipleSelect: true,
	    multiple: true,
	    hoverClass: 'hover',
	    focusClass: 'focus',
	    responseType: 'json',
	    startXHR: function() {
	        progressOuter.style.display = 'block'; // make progress bar visible
	        this.setProgressBar( progressBar );
	    },
	    onSubmit: function() {
	        msgBox.innerHTML = ''; // empty the message box
	        btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
	      },
	    onComplete: function( filename, response ) {
	    	console.log(response, filename);
	        btn.innerHTML = 'Choose Another File';
	        progressOuter.style.display = 'none'; // hide progress bar when upload is completed

	        if ( !response ) {
	            msgBox.innerHTML = 'Unable to upload file';
	            return;
	        }

	        if ( response.success === true ) {
	            msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';
	            location.reload();
	        } else {
	            if ( response.msg )  {
	                msgBox.innerHTML = escapeTags( response.msg );

	            } else {
	                msgBox.innerHTML = 'An error occurred and the upload failed.';
	            }
	        }
	      },
	    onError: function(filename, type, status, statusText, response, uploadBtn, size) {
			console.log(filename, type, status, statusText, response, uploadBtn, size);
		}
	});
};
</script>
</body>
</html>