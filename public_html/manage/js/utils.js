<!--
var w = window, d= document;

w.onload = function(){
	d.isLoaded = true;
	b = (d.compatMode && d.compatMode != "BackCompat")? d.documentElement : d.body? d.body : null;
	w.defaultStatus = '';
	if(typeof(onLoadHandler) == 'function')onLoadHandler();
};/* Execute on document load */
function getObject(id, pNode){
	Obj = null;
	if(isObj(id))Obj = id;
	else{
		if(!isObj(pNode))pNode=w.d;
		if(pNode.getElementById)Obj = pNode.getElementById(id);
		else if(pNode.all)Obj = pNode.all[id];
		else if(pNode.layers){
			Obj = getLayer(id, pNode);
		}else if(d.forms){
			for(var i=0; i<d.forms.length; i++)if(d.forms[i][id])Obj = d.forms[i][id];
		}
	}
	if(Obj && !Obj.style)Obj.style = Obj;
	return Obj;
}
function getLayer(id, pNode){
	for(i=0; i<pNode.d.layers.length; i++){
		cLayer = pNode.d.layers[i];
		if(cLayer.id == id)return cLayer;
		if(cLayer.d.layers.length)layer = getLayer(id, cLayer);
		if(layer)return layer;
	}
	return null;
}
function setContent(el, c){
	if(!(el=getObject(el)))return false;
	if(d.getElementById){el.innerHTML = '';el.innerHTML = c;}
	else if(d.all)el.innerHTML = c;
	else if(d.layers){c1 = '<p class='+id+'>'+c+'</p>';el.d.open();el.d.write(c1);el.d.close();}
}
function setProperty(el, p, v){
	if(!(el = getObject(el)))return false;
	eval('el.'+p+' = "'+v+'"');
}
function setCookie(n, v, e, p){
	d.cookie = n+'='+escape(v)+((!e)?'':('; expires='+e.toGMTString()))+'; path='+((!p)?'/':p);
}
function getCookie(n){
v = null, n = n + '=';
	if(d.cookie.length > 0){
		sPos = d.cookie.indexOf(n);
		if(sPos != -1){
			sPos += n.length;
			ePos = d.cookie.indexOf(';', sPos);
			if(ePos == -1)ePos = d.cookie.length;
			v = unescape(d.cookie.substring(sPos, ePos));
		}
	}
	return v;
}

function popUp(url, id, width, height, scroll, resize){
	var left = (screen.width - width) / 2;
	var top = (screen.height - height) / 2;
	winProp = 'width='+width+',height='+height+',left='+left+',top='+top+',scrollbars='+scroll+',resizable='+resize+'';
	win = w.open(url, id, winProp);
	win.window.focus();
}
function isFun(funX){return (typeof(funX) == 'function');}
function isObj(objX){return typeof(objX) == 'object';}
function Int2Hex(v){v=v.toString(16);return (v.length==1)?'0'+v:v;}
function Hex2Int(v){return parseInt(v,16);}
function go(url, target){
	if(!target)target = self;
	if(url.substring(0, 4) != 'http')url = homePage()+url;
	target.location.replace(url);
	return true;
}
function homePage(){
	home = location.hostname;
	if(home.substring(0, 4) != 'http')home = 'http://'+home+'/';
	return home;
}
function goBack(){
	if(history.length > 1)history.go(-1);
	else goHome();
	return false;
}
function searchbox(txt,action){
	if(action=='focus'){
		if(txt.value == 'Enter Search Term...')txt.value="";
		txt.select();
		txt.className = 'searchActive';
	}else{
		txt.className = 'searchInactive';
		if(txt.value == '')txt.value="Enter Search Term...";
	}
}
function cD(entry, lnk){
	if(typeof(window.opera) != 'undefined'){
		return true;
	}else{
		var confirmation = confirm("Are you sure you want to Delete? \n\""+entry+'"');
		if(confirmation){
			if(typeof(lnk.href) != 'undefined'){
				lnk.href += '&process=1';
			}else if(typeof(lnk.form) != 'undefined'){
				lnk.form.action	+= '?process=1';
			}
		}
		return confirmation;
	}
}
//-->