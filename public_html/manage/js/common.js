$.fn.tinymce = function(options)
{
    return this.each(function()
    {
        preString = "<div class='jqHTML_frame' style='width:"+$(this).css("width")+"px;height:" + ($(this).css("height")+20) + "px;'><div>";
        postString = "</div><div class='jqHTML_link' style='float:right' onclick=\"toogleEditorMode('" + this.id + "');\">HTML</div></div>";
        $(this).wrap(preString + postString);
    });
}

var tinyMCEmode = new Array();
function toogleEditorMode(sEditorID)
{
    if(tinyMCEmode[sEditorID])
    {
        try
        {
            tinyMCE.removeMCEControl(tinyMCE.getEditorId(sEditorID));
            tinyMCEmode[sEditorID] = false;
        }
        catch(e)
        {
            alert( "REMOVE:" + sEditorID + ':' + e.message);
        }
    }
    else
    {
        try
        {
            tinyMCE.addMCEControl(document.getElementById(sEditorID), sEditorID);
            tinyMCEmode[sEditorID] = true;
        }
        catch(e)
        {
            alert( "ADD:" + sEditorID + ':' + e.message);
        }
    }
}
function removeAllMCE()
{
    for (var i in tinyMCEmode)
    {
        if(tinyMCEmode[i])
        {
            tinyMCE.removeMCEControl(tinyMCE.getEditorId(i));
            tinyMCEmode[i] = false;
        }
    }
    initMCE()
}
function initMCE()
{
    tinyMCE.init({ mode : "none",
       theme : "advanced",
       plugins : "advhr,advlink,style",
       theme_advanced_layout_manager : "SimpleLayout",
       theme_advanced_disable: "hr,",
       theme_advanced_buttons1: "pasteword,justifyleft,justifycenter,justifyright,justifyfull,separator,removeformat,separator,charmap,advhr,separator,styleprops",
       theme_advanced_buttons2: "styleselect,bold,italic,underline,separator,link,separator,bullist,numlist,outdent,indent,",
       theme_advanced_buttons3: "",
       theme_advanced_toolbar_location : "top",
       theme_advanced_toolbar_align : "left",
       content_css : "css/content.css"});
}
//initMCE();

$(document).ready(function(){
	if($('.RTE').length > 0){
		if(typeof(tinyMCE_GZ) == 'undefined'){
			
			var e = document.createElement("script");
			e.src = '/js/tinymce/tiny_mce_gzip.js';
			e.type="text/javascript";
			document.getElementsByTagName("head")[0].appendChild(e); 
			tRTE = setInterval(checkRTEStatus, 250);
		}else initRTE;
	}
});
function checkRTEStatus(){
	if(typeof(tinyMCE_GZ) == 'undefined')return;
	else{
		clearTimeout(tRTE);
		tinyMCE_GZ.init({
				themes : "advanced",
				plugins : "media,fullscreen,inlinepopups,searchreplace,template,Archiv,o2k7",
				languages : "en",
				disk_cache : true
			},initRTE);
	}
}
function initRTE(){
	tinyMCE.init({
		mode : "textareas",
		theme : "simple",
		editor_selector : "RTE",
		theme : "advanced",
		skin : "o2k7",
		plugins : "media,fullscreen,inlinepopups,preview,searchreplace,template,Archiv",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		theme_advanced_buttons1 : "code,preview,fullscreen,|,cut,copy,paste,pastetext,pasteword,|,template,|,search,replace,|,undo,redo,cleanup,removeformat,|,|,help",
		theme_advanced_buttons2 : "bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,|,media,image,Archiv_files,Archiv_images",
		theme_advanced_buttons3 : '',
		dialog_type : "modal",
		plugin_preview_width : "500",
		plugin_preview_height : "600",		
		file_browser_callback : "tinyBrowser",
		relative_urls : false,
		content_css : '/news/style-editor.css',
		plugin_smimage_directory : "/news/pics/",
    	Archiv_settings_file : "config.php"
	});
	/*	
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		editor_selector : "mceAdvanced"
	});	

	$('.RTE').each(function(i){
		/*
		tinyMCE.init({
			mode: "exact",
			elements : this.name,
			theme : "advanced"					 
		});
		var oFCKeditor = new FCKeditor(this.name, '100%', '450') ;
		oFCKeditor.BasePath = "/js/fckeditor/" ;
		oFCKeditor.ToolbarSet = 'MyToolbar' ;
		oFCKeditor.height = '300';
		oFCKeditor.Config["CustomConfigurationsPath"] = "/news/js/myfckconfig.js?" + ( new Date() * 1 ) ;

		oFCKeditor.ReplaceTextarea() ;
		* /
		alert('');
		tinyMCE.init({
			mode: "exact",
			elements : this.name,

		});

	})
	*/
}