<?php
define('SKIP_WIDGET', 1);
if (!defined('ROOT')) {
	define('SKIP_TRACKING', true);
	include __DIR__ . '/../include/prepend.php';
}
if (!defined('THEME')) define('THEME', 'Admin\\SB_Admin');

$theme = Theme::getInstance(THEME);
$tpl = $theme->getTemplate();

\SiteManager::assert($userAuth->isAdmin() || (function_exists('isAuthorized') && isAuthorized()), ROOT . '/account/templates/classic/auth.tpl.php');

header('PageSpeedFilters: rewrite_domains,rewrite_javascript,extend_cache');

// $tpl->setTemplatePath(ROOT . '/manage/templates');


function makeSafeName2($title, $len = NULL, $def = NULL, $separator = '-') {
	$title = preg_replace_callback('/(?:(?<=^|\W)\w(?:\.| ) ?){2,}(?:\w )?/', function ($match) {
		return preg_replace('/[^\w]/', '', $match[0]) . '-';
	}, $title);
	$title = preg_replace('/(\w)\'s /i', '$1s-', $title);
	$title = preg_replace('/[^a-z0-9]+/i', $separator, strtolower(html_entity_decode($title, ENT_QUOTES, 'UTF-8')));
	$title = preg_replace("/\$separator+/", $separator, $title);
	$title = trim($title, $separator);
	if (!is_null($len) && (strlen($title) > $len)) {
		$pos = strrpos(substr($title, 0, $len), $separator);
		if ($pos)$title = substr($title, 0, $pos);
	}
	return !empty($title) ? $title : (is_null($def) ? 'page' : $def);
}
