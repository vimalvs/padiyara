<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            				  # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * PageManager Class
 * 
 * @package		Page
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2017-11-23 12:16:00
 */

namespace Page;

class PageManager extends Page {
	
	private $updateData = array();
	private $isNew = false;
	private static $tidyConfig = array('clean' => 'yes', "indent" => true, 'output-html' => true, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => true);
	protected static $db = null;
	
	public function __construct($data = null, $isNew = true) {
		$this->isNew = $isNew;
		if ($data) {
			if ($isNew) {
				$this->updateData = $data;
			} else {
				$this->data = $data;
			}
		}
	}

	public function delete(&$error = null) {
		$status = static::$db->deleteRecord('page_content', array('page_key' => $this->data['page_key']));
		if ($status) {
			\Logger::log("Deleted CMS Page : " . $this->data['page_content_title'], 'delete', 'page_content', $this->data['page_key']);
		}
		return $status;
	}
	
	public function update($data, &$arError = null) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($arError);
	}
	
	public function save(&$errorInfo = null) {
		// $isNew = empty($this->data['page_key']);
		$pdbo = \SiteManager::getDatabase();
		if (empty($this->updateData)) {
			return true;
		} elseif (!$this->isNew) {
			unset($page_key);
			if (!isset($data['page_pathname'])) $data['page_pathname'] = null;
		}
		extract($this->data);
		extract($this->updateData, EXTR_REFS);

		if (is_null($errorInfo)) $errorInfo = array();
		// $page_allow_comments = (bool)$page_allow_comments;
		
		$ext = pathinfo($page_pathname, PATHINFO_EXTENSION);
		if (empty($page_key)) $errorInfo['page_key'] = 'Page key cannot be empty';
		elseif ($this->isNew && Page::loadById($page_key)) $errorInfo['page_key'] = 'Page key already in use';
		
		if ($page_pathname && $pdbo->getRecord('page_content', $this->isNew ? compact('page_pathname') : "page_pathname = {$pdbo->quote($page_pathname)} AND page_key != '$page_key'")) {
			$errorInfo['page_pathname'] = 'The pathname already exists';
		}
		
		$config = array('clean' => 'yes', "indent" => true, 'output-html' => true, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => true);
		
		foreach (array('page_content_en', 'page_content_ml') as $c) {
			if (!isset($this->updateData[$c]) || preg_match('/^a:\d+:{/', $this->updateData[$c])) continue;
			$tidy = tidy_parse_string($this->updateData[$c], $config, 'utf8');
			if ($tidy->cleanRepair()) {
				$this->updateData[$c] = trim($tidy);
			} else {
				$errorInfo[$c] = 'Failed to clean input. The code is not valid HTML';
			}
		}
		
		$status = empty($errorInfo);
		if ($status) {
			/**
			 * Data Validated successfully
			 */
			$page_time_created = TIME_NOW;
			
			if (empty($page_pathname)) {
				$page_pathname = null;
			}
			
			if ($this->isNew) {
				$status = $pdbo->insertRecord('page_content', $this->updateData);
				if ($status) {
					$this->data['page_id'] = $pdbo->lastInsertId();
					\Logger::log("Added New CMS Page : $page_content_title", 'add' , 'page_content', $page_key);
				}
			} else {
				$status = $pdbo->updateRecord('page_content', array('page_key' => $page_key), $this->updateData);
				// Log event only if the entry was modified
				if ($status !== false) {
					\Logger::log("Updated CMS Page : $page_key", 'update' , 'page_content', $page_key);
				}
				// Return success even if data was not modified
				$status = ($status !== false);
			}
		}
		return $status;
	}
	
	public static function cleanHtml($html, &$error = null) {
		$tidy = tidy_parse_string($html, self::$tidyConfig, 'utf8');
		if ($tidy->cleanRepair()) {
			return trim($tidy);
		} else {
			$error = 'Failed to clean input. The code is not valid HTML';
			return false;
		}
	}

	public static function initialize() {
		static::$db = \SiteManager::getDatabase();
	}
}

PageManager::initialize();