<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Class
 * 
 * This class provides templates for common design blocks
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

class Theme {
	private static $instance = null;
	private static $basePath = TEMPLATE_PATH;

	private $themePath = TEMPLATE_PATH;
	private $themeName = null;
	private $template = null;
	
	public function __construct($themeName, $template = null) {
		$this->themeName = $themeName;
		$this->themePath = self::$basePath . '/' . strtr(strtolower($themeName), '\\_', '/-');
		include $this->themePath . '/config.php';
		
		if (!defined('THEME_ASSETS_PATH')) {
			define('THEME_ASSETS_PATH', str_replace(rtrim(ROOT, '/'), '', $this->themePath . "/assets"));
		}

		// \SiteManager::addNamespacePath("Theme\\$themeName", $this->themePath);
		if (!$template) {
			$template = \SiteManager::getTemplate("Theme\\$themeName\\Template");
		}
		$this->setTemplate($template);
		
		\SiteManager::invokeHook(['theme_init', $this]);
	}
	
	public function setTemplate($template) {
		$this->template = $template;
		$template->setTemplatePath($this->themePath . '/templates', 'theme', false);
		$pos = strrpos($this->themeName, '\\');
		$themeName = strtr(strtolower($pos ? substr($this->themeName, $pos + 1) : $this->themeName), '_', '-');
		$cwd = str_replace('\\', '/', getcwd());

		$arDirectory = explode('/', substr($cwd, strlen(DOCUMENT_ROOT) + 1, strrpos($cwd, '/')));
		$lastIdx = count($arDirectory) - 1;
		$path = '';

		foreach ($arDirectory as $idx => $dir) {
			$path .= "/$dir";
			$template->setTemplatePath(DOCUMENT_ROOT . "$path/templates/$themeName", $dir, $idx === $lastIdx);
		}
	}
	
	public function getName() {
		return $this->themeName;
	}
	
	public function getTemplate() {
		return $this->template;
	}
	
	public function getPath() {
		return $this->themePath;
	}
	
	public static function initialize($basePath = null) {
		if (!is_null($basePath)) {
			self::$basePath = realpath($basePath);
		} else {
			self::$basePath = ROOT . '/theme';
		}
	}
	
	public static function getInstance($themeName = null) {
		if (null === self::$instance) {
			if (!empty($themeName)) {
				// $dir = strtolower($theme);
				// if (file_exists(self::$basePath . '/' . $dir)) {
					$theme = new self($themeName);
					self::$instance = $theme;
				// } else {
				// 	FatalError('Theme not found', "Theme '$theme' not found in {self::$basePath}");
				// }
			} else {
				FatalError("Invalid Theme", "Theme name cannot be empty");
			}
		}
		return self::$instance;
	}
	
	public static function load($desktop, $mobile = null, $basic = null) {
		$theme = $desktop;
		if (MOBILE_EDITION) {
			$theme = ($_COOKIE['ua_type'][1] == 'A') ? $mobile : $basic;
		}
		return self::getInstance($theme);
	}
}

Theme::initialize();
