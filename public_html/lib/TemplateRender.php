<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * TemplateRender Class
 * 
 * This class provides templates for common design blocks
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

if (!defined('T_DATE')) {
    // define('T_STRING', 1);
    define('T_DATE', 2);
    define('T_TIME', 3);
    define('T_TEMPLATE', 4);
    define('T_INDEX', 5);
    define('T_HTML', 6);
    define('T_CHECKBOX', 7);
}

class TemplateRender {
	
	private static $arFormInputDefault = array('id' => '', 'class' => NULL, 'type' => 'string', 'value' => '', 'description' => '', 'attr' => '', 'size' => 0);
	
	public static function setFormDefaults($data) {
		static::$arFormInputDefault = array_merge(static::$arFormInputDefault, $data);
	}

	public static function pagination($nPages, $nCurrentPage = 1, $linkName = NULL, $enableRewrite = NULL, $addNoFollow = FALSE, $index = NULL) {
		$nCurrentPage = (int)$nCurrentPage;
		$nCurrentPage = ($nCurrentPage >= 1) ? $nCurrentPage : 1;
		$nPages = ceil($nPages);
		if (!$linkName) {
			// $linkName = 'http';
			// if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') $linkName .= 's';
			// $linkName .= '://';
			// if ($_SERVER['SERVER_PORT'] != '80') $linkName .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['PHP_SELF'];
			//else $linkName .= $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
			$linkName = rtrim(preg_replace('/(?<=[&?])page=[0-9]*(&|$)/', '', $_SERVER['REQUEST_URI']), '&');
			$linkName .= ((strpos($linkName, '?') === FALSE) ? '?' : '&') . 'page=';
			$enableRewrite = FALSE;
		} elseif (is_assoc_array($linkName)) {
			if (isset($linkName['navBaseUrl'])) {
				$baseUrl = $linkName['navBaseUrl'];
			} else {
				$baseUrl = end(explode('/', $_SERVER['SCRIPT_FILENAME']));			
			}
			$baseUrl .= '?';
			$pageVar = isset($linkName['navPageVar']) ? $linkName['navPageVar'] : 'page';
			unset($linkName['navBaseUrl'], $linkName['navPageVar']);
			foreach ($linkName as $k => $v) {
				if (!empty($v))$baseUrl .= "$k=$v&amp;";
			}
			$linkName = "$baseUrl$pageVar=";
		
		}
		if (is_null($enableRewrite)) $enableRewrite = (substr($linkName, -1) !== '=');
		if (is_null($index)) $index = $enableRewrite ? './' : $linkName . 1;
		if ($enableRewrite) {
			if (substr($linkName, -1) != '-') $linkName .= '-';
			$linkExt = '.html';
		} else {
			$linkExt = '';	
		}
		$i = ($nCurrentPage !== 1) ? 2 : 1;
		$rel = $addNoFollow ? 'rel="nofollow"' : '';
		if ($nPages <= 1) return FALSE;
		ob_start();
		?>
		<div class="pagination">
		<ul>
		<?php if ($nCurrentPage === 2):?> 
			<li class="prev"><a <?=$rel?> title='Previous Page' href='<?=$index?>'>&laquo; </a></li>
		<?php elseif ($nCurrentPage > 2):?>
			<li class="prev"><a <?=$rel?> title='Previous Page' href='<?php echo $linkName, ($nCurrentPage - 1), $linkExt?>'>&laquo; </a></li>
		<?php else:?>
			<li class="prev disabled"><a title="This is the first page">&laquo;</a></li>
		<?php endif;?>
	
		<?php if ($nPages < 8):?>
			<?php if ($i === 2):?>
				<li><a <?=$rel?> title="First Page" href='<?=$index?>'>1</a></li>
			<?php endif;?>
			<?php for (; $i <= $nPages; $i++):?>
				<?php if ($i == $nCurrentPage):?><li class="current active"><a title="Current Page"><?=$i?></a></li>
				<?php else:?><li><a title="Page <?=$i?>" <?=$rel?> href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
			<?php endfor;?>
		<?php else:?>
			<?php if ($nCurrentPage <= 4):?>
				<?php if ($i === 2):?><li><a <?=$rel?> title="Page <?=$i?>" href='<?=$index?>'>1</a></li><?php endif;?>
				<?php for(; $i <= $nCurrentPage + 2; $i++):?>
					<?php if ($i == $nCurrentPage):?><li class="current active" title="Current Page"><a><?=$i?></a></li>
					<?php else:?><li><a <?=$rel?> title='Page <?=$i?>' href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
				<?php endfor;?>
				<li class="disabled"><a>...</a></li><li><a <?=$rel?> title='Last Page' href='<?=$linkName, $nPages, $linkExt?>'><?=$nPages?></a></li>
			<?php elseif ($nCurrentPage >= $nPages - 3):?>
				<li><a <?=$rel?> title='First Page' href='<?=$index?>'>1</a></li><li class="disabled"><a>...</a></li>
				<?php for ($i = $nCurrentPage - 2; $i <= $nPages; $i++):?>
					<?php if ($i == $nCurrentPage):?><li class="current active"><a <?=$rel?> title="Current Page"><?=$i?></a></li>
					<?php else:?><li><a <?=$rel?> title='Page <?=$i?>' href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
				<?php endfor;?>
			<?php else:?>
				<li><a <?=$rel?> title='First Page' href='<?=$index?>'>1</a></li><li class="disabled"><a>...</a></li>
				<?php for ($i = $nCurrentPage - 2; $i <= $nCurrentPage + 2; $i++):?>
					<?php if ($i == $nCurrentPage):?><li class="current active"><a <?=$rel?> title="Current Page"><?=$i?></a></li>
					<?php else:?><li><a <?=$rel?> title='Page <?=$i?>' href='<?=$linkName, $i, $linkExt?>'><?=$i?></a></li><?php endif;?>
				<?php endfor;?>
				<li class="disabled"><a>...</a></li><li><a <?=$rel?> title="Last Page" href='<?=$linkName, $nPages, $linkExt?>'><?=$nPages?></a></li>
			<?php endif;?>
		<?php endif;?>
		<?php if ($nCurrentPage < $nPages):?><li class="next"><a class="next" <?=$rel?> title='Next Page' href='<?=$linkName, ($nCurrentPage + 1), $linkExt?>'>&raquo;</a></li>
		<?php else:?><li class="next disabled"><a title="This is the last page">&raquo;</a></li><?php endif;?>
		</ul>
		</div>
		<?php return ob_get_clean();
	}

	public static function table($arData, $header, $options = array()) {
		$defaultOption = array('startIndex' => 1, 'placeholder' => 'There is no data to display', 'class' => 'table table-striped', 'rowAttrs' => '', 'sort' => array('fields' => array(), 'field' => '', 'direction' => 'asc'));
		if (isset($options['sort'][0])) $options['sort'] = array('fields' => $options['sort']);
		if (!empty($options['sort']['fields'])) {
			$sort =& $options['sort'];
			if (empty($sort['href'])) {
				$sort['href'] = isset($_SERVER['SCRIPT_URL']) ? $_SERVER['SCRIPT_URL'] : (($pos = strpos($_SERVER['REQUEST_URI'], '?')) ? substr($_SERVER['REQUEST_URI'], 0, $pos) : $_SERVER['REQUEST_URI']);
				parse_str($_SERVER['QUERY_STRING'], $arTmp);
				if (!empty($arTmp['sort_field'])) {
					$sort['field'] = $arTmp['sort_field'];
					unset($arTmp['sort_field']);
				}
				if (!empty($arTmp['sort_direction'])) {
					$sort['direction'] = $arTmp['sort_direction'];
					unset($arTmp['sort_direction']);
				}
				unset($arTmp['page']);
				if (!empty($arTmp)) $sort['href'] .= '?' . http_build_query($arTmp);
			}
			$sort['href'] .= (strpos($sort['href'], '?') !== FALSE) ? '&' : '?';
			if (!isset($sort['field'])) $sort['field'] = '';
			if (!isset($sort['direction'])) $sort['direction'] = 'asc';
		}
		extract(array_merge($defaultOption, $options));
		
		$noData = empty($arData) || !$arData;
		$rowAttrsCallable = is_callable($rowAttrs);
		$row_attrs = $rowAttrsCallable ? '' : $rowAttrs;
	?>
		<table<?=empty($options['id']) ? '' : " id=\"{$options['id']}\""?> class="<?=$class?>">
		<thead>
		<tr>
			<?php foreach ($header as $k => $v):
				if (is_int($v)) $v = $header[$k] = array(ucwords(str_replace('_', ' ', $k)), $v);
				if (!is_array($v) || (empty($v[1]) && ($v = $v[0]))):
					if (!$noData && array_key_exists($k, $arData[0])) $v = array($v, T_STRING);
					else $v = array($v, T_TEMPLATE, $v);
					$header[$k] = $v;
				elseif ($v[1] === T_CHECKBOX):
					$name = empty($v[2]) ? $k : $v[2];
					$v[0] = '<input type="checkbox" class="chk-table-bulk-action"/>';
					$header[$k] = array(NULL, T_TEMPLATE, "<input class='chk-table-item' type='checkbox' value='{{$k}}' name='{$name}[]' />");
				elseif (count($v) === 2):
					if ($v[1] === T_DATE && empty($v[2])):
						$header[$k][2] = 'Y-m-d';
					elseif ($v[1] === T_TIME && empty($v[2])):
						$header[$k][2] = 'Y-m-d g:i a';
					elseif (is_callable($v[1])):
						$header[$k] = array($v[0], T_FUNCTION, $v[1]);
					elseif (!is_int($v[1])):
						$header[$k] = array($v[0], T_TEMPLATE, $v[1]);
					endif;
				endif;
				if ($header[$k][1] === T_TEMPLATE && !$noData):
					if (empty($header[$k][2])) trigger_error("Template string cannot be empty for column <em>{$v[0]}</em>", E_USER_WARNING);
					$arTemplateKey = array_keys($arData[0]);
					foreach ($arTemplateKey as $k1 => $v2):
					 	$arTemplateKey[$k1] = '{' . $v2 . '}';
					endforeach;
				endif;
				?>
				<?php if (in_array($k, $sort['fields'])):?>
					<?php if ($sort['field'] === $k):?>
						<th class="tablesorter-header tablesorter-headerSort<?=($sort['direction'] === 'desc' ? 'Up' : 'Down')?>"><a class="tablesorter-header-inner" href="<?=htmlspecialchars($sort['href'])?>sort_field=<?=urlencode($k)?><?=($sort['direction'] !== 'desc' ? '&amp;sort_direction=desc' : '')?>"><?=$v[0]?></a></th>
					<?php else:?>
						<th class="tablesorter-header"><a class="tablesorter-header-inner" href="<?=htmlspecialchars($sort['href'])?>sort_field=<?=urlencode($k)?>"><?=$v[0]?></a></th>
					<?php endif;?>
				<?php else:?>
					<th><?=$v[0]?></th>
				<?php endif;?>
			<?php endforeach;?>
		</tr>
		</thead>
		<tbody>
		<?php if ($noData):?>
			<tr><td class="tc" colspan="<?=count($header) + 1?>"><em><?=$placeholder?></em></td></tr>
		<?php else:?>
			<?php foreach ($arData as $idx => $data):
				$data['$INDEX'] = $startIndex;
				if ($rowAttrsCallable) $row_attrs = $rowAttrs($data, $idx);
			?>
			<tr <?=$row_attrs?>>
				<?php foreach ($header as $k => $v):?>
					<?php if ($v[1] === T_STRING):
						if (empty($data[$k])) {
							$val = is_null($data[$k]) ? 'NULL' : 'EMPTY';
							echo "<td><span class='label'>$val</span></td>";
						} else {
							echo  '<td>', htmlspecialchars($data[$k]), '</td>';
						}
					?>
					<?php elseif ($v[1] === T_INDEX):?>
						<td class="tindex"><?=$startIndex++?></td>
					<?php elseif ($v[1] === T_DATE || $v[1] === T_TIME):?>
						<td><?=date($v[2], $data[$k]);?></td>
					<?php elseif ($v[1] === T_TEMPLATE):
						$INDEX = $idx;
						$FIELD = $k;
					?>
						<td><?=preg_replace_callback('/\{(\$?[a-z0-9_]+)\}/i', function($match) use($data, $v){
							if (empty($data[$match[1]])) {
								$val = is_null($data[$match[1]]) ? 'NULL' : 'EMPTY';
								return  ($v[2] === $match[0]) ? "<span class='label'>$val</span>" : "{{$val}}";
							} else {
								return $data[$match[1]];
							}
						}, $v[2]);?></td>
					<?php elseif ($v[1] === T_FUNCTION):?>
						<?php call_user_func($v[2], $data);?>
					<?php else:?>
						<td><?=$data[$k]?></td>
					<?php endif;?>
				<?php endforeach;?>
				</tr>
			<?php endforeach;?>
		<?php endif;?>
		</tbody>
		</table>
	<?php	
	}
	
	public static function formField($label, $field, $opt = NULL, $tpl = NULL, $placeholder = FALSE, $hint = NULL) {
		if (null === $opt) $opt = array();
		if (null === $tpl) $tpl= \SiteManager::getTemplate();
		if (empty($opt['id'])) $opt['id'] = "fin_" . preg_replace('/[^a-z0-9-_]/i', '__', $field);
		$hasError = ($tpl->getOption('print_message') === \PHPTemplate::PRINT_MESSAGE_GENERAL && !empty($tpl->messages->message[$field]['error']));
		?>
		<div class="control-group <?=$hasError ? 'error' : ''?>">
		    <label class="control-label" for="<?=$opt['id']?>"><?=$label?></label>
		    <div class="controls">
				<?php self::formInput($field, $opt, $tpl, $placeholder, array('hasError' => $hasError))?>
				<?php if ($hint):?><p class="help-block"><?=$hint?></p><?php endif;?>
			</div>
		</div>
		<?php
	}
	
	public static function formInput($field, $opt = NULL, $tpl = NULL, $placeholder = FALSE, $fromFormField = FALSE) {
		if (null === $opt) $opt = array();
		if (null === $tpl) $tpl= \SiteManager::getTemplate();
		
		$opt['attributes'] = '';
		if (!$fromFormField) {
			if (empty($opt['id'])) $opt['id'] = "fin_" . preg_replace('/[^a-z0-9-_]/i', '__', $field);
			$fromFormField['hasError'] = ($tpl->options['print_message'] === \PHPTemplate::PRINT_MESSAGE_GENERAL && !empty($tpl->messages->message[$field]['error']));
		}
		$opt = array_merge(self::$arFormInputDefault, $opt);
		extract($fromFormField);
		extract($opt, EXTR_REFS);
		
		$field_name = htmlspecialchars($field);
		if ($type === 'uneditable') $class .= ' uneditable-input';
		if ($hasError) $class .= " error";
		if ($placeholder) $attr['placeholder'] = $placeholder;
		
		$attributes .= "id=\"$id\" ";
		if ($type !== 'boolean') $attributes .= "class=\"$class\" ";
		if (!empty($attr)) {
			if (isset($attr['minlength']) && empty($attr['title'])) {
				$attr['title'] = "Minimum {$attr['minlength']} characters required";
				$attr['pattern'] = '.{3,}';
			}
			foreach ($attr as $k => $v) {
				$attributes .= ($k . '="' . $v . '" ');
			}
		}
		if (empty($value)) {
			$fname = $field;
			if ($type === 'multi_select') $fname = substr($fname, 0, -2);
			if (isset($tpl->data[$fname])) $value = $tpl->data[$fname];
			else {
				if (preg_match('/([a-z0-9_]+)(?:\[([^]]+)\])(?:\[([^]]+)\])?/i', $fname, $matches) && !empty($tpl->data[$matches[1]])) {
					$value = $tpl->data[$matches[1]];
					for ($i = 2, $cnt = count($matches); $i < $cnt; $i++) {
						if (isset($value[$matches[$i]])) $value = $value[$matches[$i]];
						else {
							$value = NULL;
							break;
						}
					}
				}
			}
		}
		if ($type === 'string'):
			echo "<input name=\"$field_name\" $attributes value=\"$value\" type=\"text\" />";
		elseif ($type === 'text'):
	    	echo "<textarea name=\"$field_name\" $attributes rows='5' cols='22'>$value</textarea>";
		elseif ($type === 'code'):
	    	echo "<span class='$class input-editor input-field input-textarea'><textarea name=\"$field_name\" data-editor='code' $attributes rows='5' cols='22'>" . htmlspecialchars($value, ENT_COMPAT, 'UTF-8') . '</textarea></span>';
		elseif ($type === 'boolean'):
			echo "<label class='checkbox $class'><input type='checkbox' name=\"$field_name\" value='1' " . (!empty($value) ? 'checked="checked"' : '') . " $attributes/> $description</label>";
		elseif ($type === 'multi'):?>
			<?php if ($size !== -1):?>
				<select name="<?=$field_name?>" <?=$attributes?>>
				<?php foreach ($multi as $k => $v):?>
					<?php if (is_array($v)):?>
						<optgroup label="<?=$k?>">
							<?php foreach ($v as $k1 => $v1):?>
								<option value="<?=$k1?>"  <?=(($value == $k1) ? 'selected="selected"' : '')?>><?=$v1?></option>
							<?php endforeach;?>
						</optgroup>
					<?php else:?>
						<option value="<?=$k?>"  <?=(($value == $k) ? 'selected="selected"' : '')?>><?=$v?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			<?php else:?>
				<fieldset class="form-list-wrapper">
				<?php foreach ($multi as $k => $v):?>
					<?php if (is_array($v)):?>
						<fieldset>
							<legend><?=$k?></legend>
							<?php foreach ($v as $k1 => $v1):?>
								<div class="radio"><label><input type="radio" name="<?=$field_name?>" value="<?=$k?>" <?=(($value == $k1) ? 'checked="checked"' : '')?>> <?=$v1?></label></div>
							<?php endforeach;?>
						</fieldset>
					<?php else:?>
						<div class="radio"><label><input type="radio" name="<?=$field_name?>" value="<?=$k?>" <?=(($value == $k) ? 'checked="checked"' : '')?>> <?=$v?></label></div>
					<?php endif;?>
				<?php endforeach;?>
				</fieldset>
			<?php endif;?>
		<?php elseif ($type === 'multi_select'):
			$value = (array)$value;
			?>
			<?php if ($size !== -1):?>
				<select name="<?=$field_name?>" multiple="multiple" <?=$attributes?>>
				<?php foreach ($multi as $k => $v):?>
					<?php if (is_array($v)):?>
						<optgroup label="<?=$k?>">
							<?php foreach ($v as $k1 => $v1):?>
								<option value="<?=$k1?>" <?=(in_array($k1, $value) ? 'selected="selected"' : '')?>><?=$v1?></option>
							<?php endforeach;?>
						</optgroup>
					<?php else:?>
						<option value="<?=$k?>" <?=(in_array($k, $value) ? 'selected="selected"' : '')?>><?=$v?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			<?php else:?>
				<fieldset class="form-list-wrapper">
				<?php foreach ($multi as $k => $v):?>
					<?php if (is_array($v)):?>
						<fieldset>
							<legend><?=$k?></legend>
							<?php foreach ($v as $k1 => $v1):?>
								<div class="checkbox"><label><input type="checkbox" name="<?=$field_name?>" value="<?=$k?>" <?=(in_array($k1, $value) ? 'checked="checked"' : '')?>> <?=$v1?></label></div>
							<?php endforeach;?>
						</fieldset>
					<?php else:?>
						<div class="checkbox"><label><input type="checkbox" name="<?=$field_name?>" value="<?=$k?>" <?=(in_array($k, $value) ? 'checked="checked"' : '')?>> <?=$v?></label></div>
					<?php endif;?>
				<?php endforeach;?>
				</fieldset>
			<?php endif;?>
		<?php elseif($type === 'toggle_single'):
			?>
			<div data-toggle="buttons-radio" class="btn-group" style="display:inline-block;">
				<?php foreach ($multi as $k => $v):?>
					<label class="btn <?=(($value == $k) ? 'active' : '')?>" for="<?=($tmpId = makeSafeName($id . '-' . $k))?>"><input type="radio" id="<?=$tmpId?>" name="<?=$field_name?>" value="<?=$k?>" <?=(($value == $k) ? 'checked="checked"' : '')?>> <?=$v?></label>
				<?php endforeach;?>
			</div>
		<?php elseif ($type === 'captcha'):?>
			<?=\ReCaptcha::getHTML()?>
		<?php elseif (!in_array($type, array('range', 'date', 'file')) && is_callable($type)):?>
			<?php call_user_func($type, $field, $opt, $tpl, $placeholder, $fromFormField)?>
		<?php elseif ($type === 'uneditable'):?>
			<span <?=$attributes?>><?=$value?></span>
			<input type="hidden" name="<?=$field_name?>" value="<?=$value?>" />
		<?php elseif ($type === 'masked'):?>
			<input name="<?=$field_name?>" type="password" <?=$attributes?> value="<?=$value?>" />
		<?php elseif ($type === 'label'):?>
			<span class="input-label"><?=$value?></span>
		<?php else:?>
			<input name="<?=$field_name?>" type="<?=$type?>" <?=$attributes?> value="<?=$value?>">
		<?php endif;?>
	    <?php if ($hasError):?>
			<span class="help-inline"><?=$tpl->messages->message[$field]['error'];?></span>
		<?php endif;
	}
	
	public static function listArray($data, $arField = NULL, $ignoreEmpty = TRUE) {
		$indexToKey = FALSE;
		if (is_null($arField)) {
			$arField = array_keys($data);
			$indexToKey = TRUE;
		}
		?>
		<dl>
			<?php foreach($arField as $k => $key):
				$val = $data[$indexToKey ? $key : $k];
				if ($ignoreEmpty && empty($val)):
					continue;
				elseif (is_array($val)):
					self::listArray($val);
					continue;
				endif;
				if (!$indexToKey) {
					$key = ucwords(str_replace('_', ' ', $k));
				}
			?>
				<dt class="label"><?=$key?></dt>
				<dd><pre><?=$val?></pre></dd>
			<?php endforeach;?>
		</dl>
		<?php
	}
	
	public static function status() {
		
	}
	
	public static function initialize() {
		self::$arFormInputDefault['class'] = defined('MOBILE_EDITION') && MOBILE_EDITION ? '' : 'input-xxlarge';
	}
	
}
TemplateRender::initialize();