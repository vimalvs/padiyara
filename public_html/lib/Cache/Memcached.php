<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Cache;

/**
 * Memcached Class
 * 
 * This class implements an Memcache module for @class Cache
 * Use Cache::getInstance to use the module
 * $cache = Cache::getInstance(instance_name, 'redis')
 * 
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @package		Cache
 * @version		1.0.0
 * @date		$Date: 2010-07-03 21:47:00 +0530 (Fri, 03 Jul 2010	) $
 */
class Memcached {
	
	private $cacheGroup = '_cache';
	private $cacheTime = 900;
	private $backend = null;
	
	private static $arBucket = [];
	
	public function __construct($cacheTime = null, $cacheGroup = null) {
		$ts = microtime(true);

		if (!class_exists('\\Memcached')) {
			throw new \Exception("Memcached extension not loaded");
		}

		if (!is_null($cacheTime)) $this->cacheTime = $cacheTime;
		if (!is_null($cacheGroup)) $this->cacheGroup = $cacheGroup;
		
		$this->backend = new \Memcached;
		$this->backend->addServer('127.0.0.1', 11211);
		$this->backend->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);
		$this->backend->setOption(\Memcached::OPT_TCP_NODELAY, true);
		$this->backend->setOption(\Memcached::OPT_COMPRESSION, false);
		$this->backend->setOption(\Memcached::OPT_PREFIX_KEY, SITE_CODE);
		\Feedback\Stats::timing("timing.cache.memcached.init", (microtime(true) - $ts) * 1000000);
	}
	
	public function setCacheGroup($cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup =  'cache';
		$this->cacheGroup = $cacheGroup;
	}
	
	public function setCacheTime($cacheTime = null) {
		if (!is_null($cacheTime)) {
			$this->cacheTime = $cacheTime;
		}
	}
	
	public function __get($key) {
		return $this->get($key);
	}
	
	public function get($key, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		$data = $this->backend->get($this->getVersionedKey($cacheGroup . '.' . $key));
		\Feedback\Stats::timing("timing.cache.memcached.get", (microtime(true) - $ts) * 1000000);
		return $data !== false ? $data : null;
	}
	
	public function __set($key, $value) {
		$this->set($key, $value);
	}
	
	public function set($key, $value, $cacheTime = null, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		
		if (ENV_DEVELOPMENT) {
			if (preg_match('/[^.]{45,}\./', $key, $match)) {
				trigger_error("Unique bucket name should be ideally less than 45 characters - $key", E_USER_NOTICE);
			} elseif (substr_count($key, '.') > 2) {
				trigger_error("A cache key must ideally contain at most one bucket - $key", E_USER_NOTICE);
			} elseif (preg_match('/\.(jpg|png|gif|webp|mp3|mp4|flv|ogg|webm|gz|zip|htm|html|php)$/', $key)) {
				trigger_error("Using filename in cache key leads to an unnecessary bucket creation - $key", E_USER_WARNING);
			}
		}

		$versionedKey = $this->getVersionedKey($cacheGroup . '.' . $key);

		if (!$cacheTime) {
			$this->backend->delete($versionedKey);
			return true;
		}
		
		$status = $this->backend->set($this->getVersionedKey($cacheGroup . '.' . $key), $value, (int)$cacheTime);
		\Feedback\Stats::timing("timing.cache.memcached.set", (microtime(true) - $ts) * 1000000);
		return $status;
	}
	
	public function __isset($key){
		return $this->has($key);
	}
	
	public function has($key, $cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->get($this->getVersionedKey($cacheGroup . '.' . $key)) !== false;
	}
	
	public function __unset($key){
		return $this->delete($key);
	}
	
	public function delete($key, $cacheGroup = null) {
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->delete($this->getVersionedKey($cacheGroup . '.' . $key));
	}
	
	public function clear($prefix = null, $cacheGroup = null) {
		$ts = microtime(true);
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		if (is_null($prefix)) $prefix = '';
		
		if ($cacheGroup) {
			/*
			 * Partial Purge
			 * Increment bucket version and let Memcached handle purging of expired keys
			 */
			list($bucket, $last) = $this->getVersionedKey($prefix ? $cacheGroup . '.' . $prefix : $cacheGroup, true);
			$this->backend->increment("$bucket.$last.__ver", 1, 1, 86400);
			$arBucketName = explode('.', $bucket);
			$bucket =& self::$arBucket;
			foreach ($arBucketName as $bucketName) {
				$bucketName = substr($bucketName, 0, strrpos($bucketName, '#'));
				$bucket =& $bucket['buckets'][$bucketName];
				$last = '';
			}
			if (isset($bucket['buckets'][$last])) {
				$bucket['buckets'][$last]['version']++;
				$bucket['buckets'][$last]['buckets'] = [];
			}
		} else {
			/*
			 * Full Purge
			 * Iterate throught keys and clear everything. Not doing this on every clear request as this is a costly operation
			 */
			$len = strlen(SITE_CODE);
			$arKey = [];
			$count = 0;
			$this->iterateKeys(function ($key) use ($len, &$count, &$arKey) {
				if (substr($key, 0, $len) === SITE_CODE) {
					$arKey[$count++] = rtrim(substr($key, $len));
				}
				if ($count === 1000) {
					$count = 0;
					$arKey = [];
					$this->backend->deleteMulti($arKey, 0);
				}
			});
			if ($count) {
				$this->backend->deleteMulti($arKey, 0);
			}
			self::$arBucket = [];
		}
		\Feedback\Stats::timing("timing.cache.memcached.clear", (microtime(true) - $ts) * 1000000);
		return true;
	}
	
	public function inc($key, $value = 1, $cacheTime = null, $cacheGroup = null) {
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->increment($this->getVersionedKey($cacheGroup . '.' . $key), $value, $value, $cacheTime);
	}
	
	public function dec($key, $value = 1, $cacheTime = null, $cacheGroup = null) {
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		if (is_null($cacheGroup)) $cacheGroup = $this->cacheGroup;
		return $this->backend->decrement($this->getVersionedKey($cacheGroup . '.' . $key), $value, $value, $cacheTime);
	}
	
	public function getBackend() {
		return $this->backend;
	}
	
	public function getVersionedKey($key, $returnArray = false) {
		$bucket =& self::$arBucket;
		$versionedKey = '';
		$arBucket = explode('.', ".$key");
		$itemKey = array_pop($arBucket);

		foreach ($arBucket as $bucketName) {
			if (!isset($bucket['buckets'][$bucketName])) {
				// Load bucket current version from redis
				$version = $this->backend->get($versionedKey . $bucketName . '.__ver');
				if ($version === false) {
					$version = 1;
					$this->backend->increment($versionedKey . $bucketName . '.__ver', 1, 1, 292000);
				}
				$bucket['buckets'][$bucketName] = ['version' => $version, 'buckets' => []];
			}
			
			$bucket =& $bucket['buckets'][$bucketName];
			$versionedKey .= "$bucketName#{$bucket['version']}.";
		}
		if ($returnArray) {
			return [rtrim($versionedKey, '.'), $itemKey];
		} else {
			assert('strpos($key, "#") === false', new \AssertionError("[Memcached] Invalid key $key. Cache keys should not have # character in it"));
			assert('$this->backend->get($versionedKey . "." . $itemKey . ".__ver") === false', new \AssertionError("[Memcached] A bucket exists with the name $key. Versioned keys cannot have items sharing a bucket name."));
			return $versionedKey . $itemKey;
		}
	}
	
	private function iterateKeys($callback) {
		$proc = popen('/usr/local/bin/memdump --servers=localhost', 'r');
		if ($proc) {
			while (($key = fgets($proc)) !== false) {
				$callback($key);
			}
			return true;
		}
		return false;
	}
}

