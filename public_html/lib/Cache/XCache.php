<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Cache;

/**
 * XCache Class
 * 
 * This class implements an XCache module for @class Cache
 * Use Cache::getInstance to use the module
 * $cache = Cache::getInstance(instance_name, 'xcache')
 * 
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @package		Cache
 * @version		1.0.0
 * @date		$Date: 2010-07-03 21:47:00 +0530 (Fri, 03 Jul 2010	) $
 */
class XCache{
	
	private $cacheGroup = null;
	private $cacheTime = 900;
	
	public function __construct($cacheTime = null, $cacheGroup = null){
		
		if (!is_callable('xcache_get')) {
			throw new \Exception("XCache extension not loaded");
		}
		
		if (!is_null($cacheTime)) $this->cacheTime = $cacheTime;
		if (!is_null($cacheGroup)) $this->cacheGroup = SITE_CODE . '.' . $cacheGroup;
		else $this->cacheGroup = SITE_CODE . '.cache';
	}
	public function setCacheGroup($cacheGroup = null){
		if (is_null($cacheGroup)) $cacheGroup =  'cache';
		$this->cacheGroup = SITE_CODE . '.' . $cacheGroup;
	}
	public function setCacheTime($cacheTime = null){
		if (!is_null($cacheTime)) {
			$this->cacheTime = $cacheTime;
		}
	}
	public function __get($key){
		return $this->get($key);
	}
	public function get($key, $cacheGroup = null){
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);

		if (ENV_DEVELOPMENT) {
			if (preg_match('/[^.]{45,}\./', $key, $match)) {
				trigger_error("Unique bucket name should be ideally less than 45 characters - $key", E_USER_NOTICE);
			} elseif (substr_count($key, '.') > 2) {
				trigger_error("A cache key must ideally contain at most one bucket - $key", E_USER_NOTICE);
			} elseif (preg_match('/\.(jpg|png|gif|webp|mp3|mp4|flv|ogg|webm|gz|zip|htm|html|php)$/', $key)) {
				trigger_error("Using filename in cache key leads to an unnecessary bucket creation - $key", E_USER_WARNING);
			}
		}

		assert('strpos($cacheGroup . $key, "#") === false', new \AssertionError("[XCache] Invalid key $cacheGroup.$key. Cache keys should not have # character in it"));
		return xcache_get($cacheGroup . '.' . $key);
	}
	public function __set($key, $value){
		$this->set($key, $value);
	}
	public function set($key, $value, $cacheTime = null, $cacheGroup = null){
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return 	xcache_set($cacheGroup . '.' . $key, $value, (int)$cacheTime);
	}
	public function __isset($key){
		return $this->has($key);
	}
	public function has($key, $cacheGroup = null){
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return xcache_isset($cacheGroup . '.' . $key);
	}
	public function __unset($key){
		return $this->delete($key);
	}
	public function delete($key, $cacheGroup = null){
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return xcache_unset($cacheGroup . '.' . $key);
	}
	public function clear($prefix = null, $cacheGroup = null){
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		if (is_null($prefix)) $prefix = '';
		xcache_unset_by_prefix("$cacheGroup.$prefix.");
		return true;
	}
	public function inc($key, $value = 1,  $cacheTime = null, $cacheGroup = null){
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return xcache_inc($cacheGroup . '.' . $key, $value, $cacheTime);
	}
	public function dec($key, $value = 1,  $cacheTime = null, $cacheGroup = null){
		if (is_null($cacheTime)) $cacheTime = $this->cacheTime;
		$cacheGroup = is_null($cacheGroup) ? $this->cacheGroup : (SITE_CODE . '.' . $cacheGroup);
		return xcache_dec($cacheGroup . '.' . $key, $value, $cacheTime);
	}
}

