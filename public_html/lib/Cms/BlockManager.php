<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * BlockManager Class
 * 
 * Class for managing cms blocks
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

namespace Cms;
class BlockManager extends Block {
	
	private $updateData = array();
	private static $db = NULL;
	
	public static function initialize() {
		static::$db = \SiteManager::getDatabase();
	}
	public function __construct($data = NULL) {
		if ($data) {
			if (isset($data['block_id'])) {
				$this->data = $data;
			} else {
				$this->updateData = $data;
			}
		}
	}

	public function delete(&$error = NULL) {
		$status = \SiteManager::getDatabase()->deleteRecord('cms_block', array('block_key' => $this->data['block_key']));
		if ($status) {
			\Logger::log("Deleted CMS Block : " . $this->data['block_title'], 'delete', 'cms_block', $this->data['block_key']);
		}
		return $status;
	}
	
	public function update($data, &$arError = NULL) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($arError);
	}
	
	public function save(&$errorInfo = NULL) {
		$isNew = empty($this->data['block_id']);
		$pdbo = \SiteManager::getDatabase();
		
		unset($this->updateData['block_id']);
		// Unset id from update requests
		if (!$isNew) unset($this->updateData['block_key']);
		
		if (empty($this->updateData)) return TRUE;
		
		extract($this->data);
		extract($this->updateData, EXTR_REFS);

		if (is_null($errorInfo)) $errorInfo = array();

		if (empty($block_title)) $errorInfo['block_title'] = 'Title is mandatory';
		if ($isNew) {
		    if (empty($block_key)) $block_key = makeSafeName($block_title);
		    if ($pdbo->getRecord('cms_block', compact('block_key'))) $errorInfo['block_key'] = 'Block already exists';
		}
		$config = array('clean' => 'yes', "indent" => TRUE, 'output-html' => TRUE, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => TRUE);
		$tidy = tidy_parse_string($block_content, $config, 'utf8');
		if ($tidy->cleanRepair()) {
			$block_content = (string)$tidy;
			$block_content = preg_replace('/ _tmplitem="[0-9]+"/', '', $block_content);
		} else {
			$errorInfo['block_content'] = 'Failed to clean input. The code is not valid HTML';
		}
		$status = empty($errorInfo);
		if ($status) {
			/**
			 * Data Validated successfully
			 */
			
			$block_created = TIME_NOW;
			if ($isNew) {
				$status = $pdbo->insertRecord('cms_block', $this->updateData);
				if ($status) {
					\Logger::log("Added New CMS Block : $block_title", 'add' , 'cms_block', $block_key);
				}
			} else {
				$status = $pdbo->updateRecord('cms_block', array('block_key' => $block_key), $this->updateData);
				// Log event only if the entry was modified
				if ($status !== FALSE) {
					\Logger::log("Updated CMS Block : $block_title", 'update' , 'cms_block', $block_key);
				}
				// Return success even if data was not modified
				$status = ($status !== FALSE);
			}
		}
		return $status;
	}
}