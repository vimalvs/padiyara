<?php
namespace Cms;
class TemplateGenerator {
	
	private $tpl = null;
	private $dryRun = false;
	private $article = null;
	private $arAdSlot = array();
	private $code = null;
	private $data = null;
	private $hookIndex = null;
	private $cacheDir = null;
	
	private static $tmpDir = null;
	// private static $instance = null;
	
	public static function _autoInitialize() {
		self::setTempDir(sys_get_temp_dir());
	}
	
	public static function setTempDir($tmpDir) {
		self::$tmpDir = $tmpDir;
	}
	// 
	// public static function getInstance() {
	// 	return self::$instance;
	// }
	
	public function __construct($article, $dryRun = false, $theme = null) {
		$this->article = $article;
		$this->dryRun = $dryRun;
		
		if (!$theme) {
			$theme = new \Theme('Classic', new \PHPTemplate);
		}
		
		$themeName = $theme->getName();
		$this->tpl = $theme->getTemplate();
		
		$this->tpl->setTemplatePath(ROOT . '/res/cms/templates/' . strtolower($themeName));
		$this->tpl->setTemplate('cms-article');
		
		$cls = "Cms\\Theme\\" . $themeName;
		if (class_exists($cls)) {
			new $cls($theme);
		}
		$this->setCacheDir(ROOT . '/../.cache/cms/' . strtolower($themeName));

		// self::$instance = $this;
		
		\SiteManager::registerHook('cms_template_begin', array($this, 'templateBeginHook'));
		
		$this->parse();
	}
	
	public function __get($key){
		return isset($this->data['article_' . $key]) ? $this->data['article_' . $key] : NULL;
	}
	
	public function setCacheDir($dir) {
		$this->cacheDir = $dir;
	}
	
	public function getData($key = NULL) {
		return is_null($key) ? $this->data : $this->data[$key];
	}
	
	public function destroy() {
		// $this->arAdSlot = array();
		\SiteManager::unregisterHook('cms_template_begin', array($this, 'templateBeginHook'));
	}
	
	public function addAdSlot($adSlotName) {
		$this->arAdSlot[] = $adSlotName;
	}
	
	public function getTemplate() {
		return $this->tpl;
	}
	
	public function save(&$output = null) {
		if (!$this->dryRun || ($status = $this->validate(TRUE, $output))) {
			$tplFileCache = ($this->cacheDir . $this->data['article_pathname'] . (empty($this->data['article_require_trailing_slash']) ? '' : '/index.php'));
			$tplPath = substr($tplFileCache, 0, strrpos($tplFileCache, '/'));
			if (!file_exists($tplPath)) {
				mkdir($tplPath, 0777, TRUE);
			}
			return $this->dryRun ? (!file_exists($tplFileCache) || unlink($tplFileCache)) : (file_put_contents($tplFileCache, $this->code, LOCK_EX) !== FALSE);
		} else {
			error_log("[Cms\ArticleManager] PHP " . end($output));
		}
		return FALSE;
	}
	
	public function validate($execute = FALSE, &$output = NULL) {
		$tmpFile = self::$tmpDir . '/' . md5($this->code) . '.tmp.php';
		
		if (file_put_contents($tmpFile, $this->code, LOCK_EX)) {
			$return = FALSE;
			exec("php -l $tmpFile", $output, $rval);
			$a = (strpos($output[0], 'No syntax errors detected') !== FALSE);
			if ($rval && (strpos($output[0], 'No syntax errors detected') !== FALSE)) {
				$rval = 0;
				$output = '';
			}
			if (!$rval && $execute) {
				$syntaxChecker = ROOT . '/res/cms/syntax-check.php';
				exec("/usr/local/bin/php -f $syntaxChecker $tmpFile", $output, $rval);
				if (!$rval) {
					$output = NULL;
				} elseif (empty($output)) {
					$output = array('Failed to execute php file.');
				}
			} elseif (empty($output)) {
				$output = array("Syntax check failed");
			}
			unlink($tmpFile);
			return ($rval === 0);
		} else {
			error_log("[Cms\ArticleManager] Error : Failed to create temporary file $tmpFile for syntax checking.");
			return false;
		}
	}
	
	private function parse() {
		$data = $this->dryRun ? $this->article->getUpdateData() : $this->article->getData();
		$this->data =& $data;
		$data['article_content_parsed'] = preg_replace_callback('@<code[^>]+>(.*?)</code>@S', array($this, 'parseCallback'), $data['article_content']);
		$this->tpl->addData($data);

		// Section Info
		$sectionInfo = \SiteManager::getConfig("cms_section_{$data['article_section']}");
		$this->tpl->addData($sectionInfo);

		$this->tpl->data['section_menu'] = Block::get($sectionInfo['menu'], '');

		if (!$sectionInfo['wide_layout']) {
			$this->tpl->data['midcontent'] = preg_replace_callback('@<code[^>]+>(.*?)</code>@S', array($this, 'parseCallback'), Block::get($sectionInfo['midcontent']));
		}
		// if ($multipart !== false) {
		// 	if (preg_match_all('#<h[12][^>]*>(.*?)</h[12]>(.*?)(?=<h[12]|$)#is', $article_content_parsed, $matches)) {
		// 		$multipart = true;
		// 		$arPart = $matches[0];
		// 		$arPartHead = $matches[1];
		// 		$arPartContent = $matches[2];
		// 		$page = getCleanVar('page', 0);
		// 	}
		// } else {
			\SiteManager::invokeHook('cms_template_before_generation');
			$this->code = $this->tpl->render('cms-article', NULL, true);
		// }
	}
	
	private function parseCallback($match) {
		// $type = strtoupper($match[1]);
		$data = array();
		$sxml = simplexml_load_string($match[0]);
		if ($sxml && ($attr = $sxml->attributes())) {
			foreach ($attr as $key => $val) {
				$tmp = explode('-', $key);
				if ($tmp[0] === 'data') {
					unset($tmp[0]);
					$data[implode('_', $tmp)] = (string)$val;
				}
			}
			$type = strtoupper($data['type']);
			if ($type === 'AD') {
				$adSlotName = htmlspecialchars(trim($match[1]));
				$filterArgs = ['unit_name' => $adSlotName, 'data' => $data];
				$filterArgs = \SiteManager::invokeFilter('cms_template_ad_unit', $filterArgs);
				// printr($filterArgs);
				if ($filterArgs['unit_name']) {
					return "<ins class=\"adm-unit {$filterArgs['data']['align']}\"><?php AdManager::printAdslot('{$filterArgs['unit_name']}');?></ins>";
					if (MOBILE_EDITION) {
						static $MOBILE_AD_COUNT = 0;
						$arPosition = array('main', 'middle', 'bottom');
						if ($MOBILE_AD_COUNT < 3) {
							return "<ins class=\"adm-unit\"><?php \$this->render(TEMPLATE_PATH . '/m/advertisement-{$arPosition[$MOBILE_AD_COUNT]}.tpl.php');?></ins>";
						}
					} else {
						$this->arAdSlot[] = $adSlotName;
						$align = empty($data['align']) ? '' : htmlspecialchars($data['align']);
						return "<ins class=\"adm-unit $align\"><?php AdManager::printAdslot('$adSlotName');?></ins>";
					}
				}
			} elseif ($type === 'PHP') {
				$tmp = trim($match[1]);
				if (preg_match('/((?:[a-z_]+::)?[a-z0-9_]+)\s*(?:\((.*)\))?/i', $tmp, $fnmatch)) {
					$fn = NULL;
					if (strpos($fnmatch[1], '::') !== FALSE) {
						$tmp = explode('::', $fnmatch[1]);
						if ($tmp[0] === 'Cms' && method_exists("\Cms\\$tmp[0]", $tmp[1])) {
							$fn = '\Cms\\' . $fnmatch[1];
							unset($data['type']);
						}
					} elseif (in_array($fnmatch[1], self::$arFunctionWhitelist) && function_exists($fnmatch[1])) {
						$fn = $fnmatch[1];
						// if (preg_match_all('/\[([^\]+])\]/', $fnmatch[2], $tags)) {
						// 	foreach ($tags as $tag) {
						// 		$tag[1] = strtolower($tag[1]);
						// 		if (isset(self::$article[$tag[1]])) $fnmatch[2] = str_replace($tag[0], htmlentities(self::$article[$tag[1]]), $fnmatch[2]);
						// 	}
						// }
					}
					if ($fn) {
						$args = (array)json_decode("[$fnmatch[2]]");
						array_unshift($args, $this, $data);
						
						$content = call_user_func_array($fn, $args);
						return $content;
					}
				}
			} elseif ($type === 'BLOCK') {
				return $this->tpl->render('content-block', $data + array('name' => trim($match[1])), TRUE);
			}
		}
	}
	
	public function templateBeginHook() {
		foreach ($this->arAdSlot as $slot) {
			echo "<?php AdManager::createAdSlot('$slot');?>\n";
		}
	}
}

TemplateGenerator::_autoInitialize();