<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Article Class
 * 
 * Class for cms article related actions
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */

namespace Cms;
class Article {
	protected $data = array('article_allow_comments' => FALSE);
	protected static $db = NULL;
	
	const STATUS_DRAFT = 0;
	const STATUS_PUBLISHED = 1;
	
	public static function initialize() {
		static::$db = \SiteManager::getDatabase();
	}
	private static function load($condition) {
		$article = static::$db->getRecord('cms_article', $condition);
		if ($article) {
			// Any updations here should be performed in ProfileManager::save too
			if ($article['article_require_trailing_slash']) $article['article_pathname'] .= '/';
			$article['article_alt_title'] = unserialize($article['article_alt_title']);
			$article['article_related_links'] = unserialize($article['article_related_links']);
			$article['article_options'] = unserialize($article['article_options']);
			return new static($article);
		}
		return FALSE;	
	}
	public static function loadById($id) {
		return static::load(array('article_id' => $id));
	}

	public static function loadByPathname($pathname, $published = 1) {
		$condition = array('article_pathname' =>  $pathname);
		if ($published) $condition['article_is_published'] = 1;
		return static::load($condition);
	}
	
	public function __construct($data) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	public function __get($key){
		return isset($this->data['article_' . $key]) ? $this->data['article_' . $key] : NULL;
	}
	
	public function getData($key = NULL, $prefix = 'article_') {
		return is_null($key) ? $this->data : $this->data[$prefix . $key];
	}
	
	public static function search($condition = NULL, $status = NULL, $order = NULL, $limit = NULL, &$count = FALSE) {
		$arItem = array();

		if ($order && !is_array($order)) $order = array($order, 'ASC');
		if ($limit && !is_array($limit)) $limit = array(0, $limit);

		if (is_string($condition)) {
			$whereSql = 'MATCH(`article_content_tags`) AGAINST (' . static::$db->quote($condition) . ' IN BOOLEAN MODE)';
		} else {
			$whereSql = empty($condition) ? 1 : static::$db->genQuery($condition);
		}
		if (is_null($status)) $status = Article::STATUS_PUBLISHED;
		if ($status !== FALSE) $whereSql .= ' AND ' . static::$db->genQuery(array('article_status' => $status));
		if ($count !== FALSE) {
			$count = static::$db->getCount('cms_article', $whereSql);
		}

		$stmt = static::$db->search('cms_article', $whereSql, $order, $limit);
		$arItem = $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : FALSE;
		return $arItem;
	}
	

}
Article::initialize();