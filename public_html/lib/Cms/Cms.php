<?php
/**
 * 
 * Content Management System 
 * 
 * @version		1.0.0
 * @date		$Date: 2011-09-17 17:55:00 +0530 (Thu, 17 Sep 2011	) $
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * 
 */
namespace Cms;

class Cms {
	
	private static $arFunctionWhitelist = array('date');
	private static $article = NULL;
	private static $related = array();
	private static $relatedDefaults = array('ad_unit' => NULL, 'title' => 'Related Content', 'align' => 'right', 'layout' => 'vertical', 'count' => 5, 'fq' => null);
	
	public static function showRelatedContent($item_uid, $options, $tplFile = null, $tpl = null) {
		if (!$tplFile) $tplFile = 'related';
		if (!$tpl) $tpl = \SiteManager::getTemplate();
		// $serializedResult = 
		// extract($options, EXTR_REFS);
		// if (!in_array($options['section'], array('c', 'n', 'v'))) $options['section'] = 'c';
		if (!in_array($options['layout'], array('vertical', 'horizontal'))) $options['layout'] = 'vertical';
		$arRelated = static::getRelatedContent($item_uid, $options);
		return $tpl->render($tplFile, array('related' => $arRelated) + $options);
	}
	
	public static function getRelatedContent($item_uid, $options = []) {
		$arRelated = array();
		$options = array_merge(self::$relatedDefaults, $options);
		if (empty(self::$related[$item_uid]['content'])) {
			$fq = $options['fq'];
			if (!empty($options['section'])) {
				$fq = ['item_section' => is_array($options['section']) ? implode(' ', $options['section']) : $options['section']];
			}
			$content = \Solr::similar(strtolower(SITE_CODE) . '-related-content', "item_uid:$item_uid", 15, ['fl' => 'item_*,score', 'fq' => $fq, 'mlt.fl' => 'item_tags^10,item_title^2,item_content', 'mlt.boost' => 'true']);

			$tmp = array();
			if ($content) {
				$minScore = $content['response']['maxScore'] * 0.2;
				foreach ($content['response']['docs'] as $doc) {
					if ($doc['score'] > $minScore) {
						$tmp[] = $doc;
					}
				}
			}
			$tmp = array_merge($tmp, $tmp);
			self::$related[$item_uid] = array('content' => $tmp, 'counter' => 0);
		}
		$related =& self::$related[$item_uid];
		$tmp = min(count($related['content']), $related['counter'] + $options['count']);
		// if ($options['section'] === 'c') {
		// 	for ($i = $related['counter']; $i < $tmp; $i++) {
		// 		$doc = $related['content'][$i];
		// 		$tmp_title =  isset($doc['multi_alt_title']) ? $doc['multi_alt_title'][array_rand($doc['multi_alt_title'])] : $doc['title'];
		// 		$arRelated[] = array('pathname' => $doc['path'], 'title' => $tmp_title);
		// 	}
		// 	$related['counter'] = $tmp;
		// } else {
			$arRelated = array_slice($related['content'], $related['counter'], $options['count']);
		// }
		return $arRelated;
	}
	
	public static function related($generator, $options, $count = 5, $tplFile = 'related') {
		$article_id = (int)$generator->getData('article_id');
		$count = (int)$count;
		$tpl = class_exists('\Cms\ArticleManager', FALSE) ? $generator->getTemplate() : \SiteManager::getTemplate();
		if (!empty($options['ad_unit'])) {
			$adSlotName = htmlspecialchars(trim($options['ad_unit']));
			$generator->addAdSlot($adSlotName);
		}
		return "<?php \Cms\Cms::showRelatedContent('article_$article_id', " . var_export($options + compact('article_id', 'count'), TRUE) .  ");?>";
	}
	
	public static function clearCache() {
		if ($pdbo = \SiteManager::getDatabase()) {
			$stmt = $pdbo->search('SELECT article_pathname, article_require_trailing_slash FROM cms_article');
			if ($stmt && $arPath = $pdbo->fetchAll(\PDO::FETCH_NUM)) {
				foreach ($arPath as $path) {
					if (!self::clearCacheByPath($path[0], $path[1])) {
						return FALSE;
					}
				}
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public static function clearCacheByPath($path, $require_trailing_slash = FALSE) {
		if (empty($path) && !$require_trailing_slash) return FALSE;
		
		$path = CMS_CACHE_PATH . $path . ($require_trailing_slash ? '/index.php' : '');
		if (file_exists($path)) {
			return unlink($path);
		} else {
			return TRUE;
		}
	}
	
	public static function clearCacheBySection($section) {
		if ($pdbo = \SiteManager::getDatabase()) {
			$stmt = $pdbo->query('SELECT article_pathname, article_require_trailing_slash FROM cms_article WHERE article_section LIKE ' . $pdbo->quote($section));
			if ($stmt && $arPath = $stmt->fetchAll(\PDO::FETCH_NUM)) {
				foreach ($arPath as $path) {
					if (empty($path[0]) && !$path[1]) continue;
					if (!self::clearCacheByPath($path[0], $path[1])) {
						return FALSE;
					}
				}
				return TRUE;
			}
		}
		return FALSE;
	}
}