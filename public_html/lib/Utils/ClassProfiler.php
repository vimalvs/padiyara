<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Utils;

class ClassProfiler {
	private $origClassName = null;
	private $className = null;
	private $ns = null;
	private $modifyOriginal = false;
	
	public function __construct($className, $destClass = null) {
		$className = trim($className, '\\');
		$this->origClassName = $className;
		
		if (!ENV_DEVELOPMENT) {
			trigger_error("Utils\\ClassProfiler is intended for use in development environment", E_USER_WARNING);
			$this->className = $className;
		} else if ($destClass === false) {
			if  (!extension_loaded('runkit')) {
				throw new \RuntimeException("Utils\\ClassProfiler requires `runkit` extension");
			}
			$this->className = $className;
			$this->modifyClass();
		} else {
			$this->createSubClass($destClass);
		}
		
	}
	
	private function getMethodSignature($method) {
		$parameters = $method->getParameters();
		$signature = '';
		foreach ($parameters as $p) {
			$class = $p->getClass();
			if ($class) {
				$signature .= '\\' . $class->name . ' ';
			}
			$signature .= "\${$p->name}";
			if ($p->isOptional()) {
				try {
					$val = $p->getDefaultValue();
				} catch (\Exception $e) {
					$val = null;
				}
				if (is_bool($val)) $val = $val ? 'true' : 'false';
				else if (is_null($val)) $val = 'null';
				$signature .= '= ' . $val;
			}
			$signature .= ', ';
		}
		return rtrim($signature, ', ');
	}
	
	private function modifyClass() {
		$className = $this->className;
		$reflector = new \ReflectionClass($className);
		$methods = $reflector->getMethods();
		runkit_method_add($className, '__callOrig', '$method, $args', "\$m = \"$className::\$method\";
		if (isset(\$this->arProfilerStat[\$m])) {
			\$this->arProfilerStat[\$m]['counter']++;
		} else {
			\$this->arProfilerStat[\$m] = ['counter' => 0, 'time' => 0];
		}
		\$time = microtime(true);
		\$result = call_user_func_array(\"parent::\$method\", \$args);
		\$this->arProfilerStat[\$m]['time'] += microtime(true) - \$time;
		return \$result;", RUNKIT_ACC_PRIVATE);
		foreach ($methods as $method) {
			$name = $method->name;
			$signature = $this->getMethodSignature($method);
			if ($name !== '__construct') {
				runkit_method_rename($className, $method->name, "__p_{$method->name}");
				runkit_method_add($className, $name, $signature, "return false;", RUNKIT_ACC_PUBLIC);
			}
		}
		
	}
	
	private function createSubClass($destClass) {
		$className = $this->origClassName;
		$reflector = new \ReflectionClass($className);
		$methods = $reflector->getMethods();
		
		$this->className = $destClass ?: ("\\Utils\\ClassProfiler\\{$className}_" . uniqid());
		$pos = strrpos($this->className, '\\');
		$class = substr($this->className, $pos + 1);
		$ns = substr($this->className, 1, $pos - 1);
		$code = "namespace $ns;\nclass $class extends \\{$className} {\n\tuse \\Utils\\ClassProfilerTrait;\n\tconst CLASS_NAME = '$className';\n";

		foreach ($methods as $method) {
			// if ($method->name !== '__construct') {
				$static = $method->isStatic();
				$methodCode = "\tpublic " . ($static ? 'static ' : '') . "function {$method->name}(";
				$methodCode .= $this->getMethodSignature($method);
				// $parameters = $reflector->getMethod($method->name)->getParameters();
				// foreach ($parameters as $parameter) {
				// 	$methodCode .= "\${$parameter->name}" . ($parameter->isOptional() ? '=null' : '') . ", ";
				// }
				// $methodCode = rtrim($methodCode, ', ');
				$methodCode .= ") {\n\t\treturn " . ($static ? 'self::callStatic' : '$this->call') . "('{$method->name}', func_get_args());\n\t}\n";
				$code .= $methodCode;
			// }
			// printr($parameters);
		}
		$code .= "}\n";
		// printr($code);
		eval($code);
	}
	
	public function getClass() {
		return $this->className;
	}
	
}