<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Utils;

trait ClassProfilerTrait {
	protected $arProfilerStats = [];
	protected static $arProfilerStaticStats = [];
	
	public function call($method, $args) {
		$m = '[I] ' . self::CLASS_NAME . "::$method";
		file_put_contents('/tmp/test.log', $m . PHP_EOL, FILE_APPEND);
		if (isset($this->arProfilerStats[$m])) {
			$this->arProfilerStats[$m]['counter']++;
		} else {
			$this->arProfilerStats[$m] = ['counter' => 0, 'time' => 0];
		}
		$time = microtime(true);
		$result = call_user_func_array("parent::$method", $args);
		$this->arProfilerStats[$m]['time'] += microtime(true) - $time;
		file_put_contents('/tmp/test.log', $m . print_r($result, true) . PHP_EOL, FILE_APPEND);
		return $result;
	}
	
	public static function callStatic($method, $args) {
		$m = '[S] ' . self::CLASS_NAME . "::$method";
		file_put_contents('/tmp/test.log', $m . PHP_EOL, FILE_APPEND);
		if (isset(self::$arProfilerStaticStats[$m])) {
			self::$arProfilerStaticStats[$m]['counter']++;
		} else {
			self::$arProfilerStaticStats[$m] = ['counter' => 0, 'time' => 0];
		}
		$time = microtime(true);
		$result = call_user_func_array("parent::$method", $args);
		self::$arProfilerStaticStats[$m]['time'] += microtime(true) - $time;
		return $result;
	}
	
	public function getProfilerStats() {
		return $this->arProfilerStats;
	}

	public static function getProfilerStaticStats() {
		return self::$arProfilerStaticStats;
	}
}