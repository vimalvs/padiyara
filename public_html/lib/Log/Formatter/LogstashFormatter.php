<?php

namespace Log\Formatter;

use Monolog\Formatter\NormalizerFormatter;

class LogstashFormatter extends NormalizerFormatter {
	protected $systemName;
	protected $applicationName;

	public function __construct($applicationName, $systemName = null) {
		// logstash requires a ISO 8601 format date with optional millisecond precision.
		parent::__construct('Y-m-d\TH:i:s.uP');

		$this->systemName = $systemName ?: gethostname();
		$this->applicationName = $applicationName;
	}
	
	public static function flatten($array, $prefix = '') {
		$result = array();
		foreach($array as $key=>$value) {
			if(is_array($value)) {
				$result = $result + self::flatten($value, $prefix . $key . '_');
			}
			else {
				$result[$prefix . $key] = $value;
			}
		}
		return $result;
	}

	public function format(array $record) {
		$record = parent::format($record);

		if (empty($record['datetime'])) {
			$record['datetime'] = gmdate('c');
		}
		$message = array(
			'@timestamp' => $record['datetime'],
			'@version' => 1,
			'domain' => $this->systemName,
			'client_ip' => $_SERVER['REMOTE_ADDR']
		);

		try {
			$theme = \Theme\Theme::getInstance();
			if ($theme) {
				$message['theme_name'] = $theme->getName();
			}
		} catch (\Theme\Exception\ThemeNotFoundException $e) {
			// Do Nothing
		}
		
		if (isset($record['message'])) {
			$message['message'] = $record['message'];
		}
		if (isset($record['channel'])) {
			$message['type'] = $record['channel'];
			$message['channel'] = $record['channel'];
		}
		if (isset($record['level_name'])) {
			$message['log_level'] = $record['level_name'];
		}
		if ($this->applicationName) {
			$message['type'] = $this->applicationName;
		}
		if (!empty($record['extra'])) {
			$message['app_log_extra'] = self::flatten($record['extra']);
		}
		if (!empty($record['context'])) {
			$message['app_log_context'] = self::flatten($record['context']);
		}
		return $this->toJson($message) . "\n";
	}
}
