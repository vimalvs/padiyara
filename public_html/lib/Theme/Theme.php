<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Class
 * 
 * This class provides templates for common design blocks
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-12-14 14:43:00 +0530 (Wed, 14 Dec 2011	) $
 */
namespace Theme;

class Theme {
	protected static $instance = null;
	protected static $basePath = TEMPLATE_PATH;

	protected $themePath = TEMPLATE_PATH;
	protected $themeName = null;
	protected $template = null;
	protected $layout = null;
	
	private static $themeSelector = null;
	
	public function __construct($themeName, $template = null) {
		$this->themePath = self::$basePath . '/' . strtr(strtolower($themeName), '\\_', '/-');
		$status = include $this->themePath . '/config.php';
		
		if ($status === false) {
			throw new Exception\ThemeNotFoundException();
		}
		
		$this->themeName = $themeName;
		
		// \SiteManager::addNamespacePath("Theme\\$themeName", $this->themePath);
		// if (is_null($template)) {
		// }
		if (!$template) {
			$templateClass = "Theme\\$themeName\\Template";
			$template = new $templateClass;
		}
		$this->setTemplate($template);
	}
	
	public function setTemplate($template) {
		$this->template = $template;
		\SiteManager::registerHook('template_head', [$template, 'printHeader']);
		// \SiteManager::registerHook('template_foot', array(self::$tpl, 'printFooter'));
		
		$template->setTemplatePath($this->themePath . '/templates', 'theme', false);
		$pos = strrpos($this->themeName, '\\');
		$themeName = strtr(strtolower($pos ? substr($this->themeName, $pos + 1) : $this->themeName), '_', '-');
		$cwd = str_replace('\\', '/', getcwd());

		$arDirectory = explode('/', substr($cwd, strlen(DOCUMENT_ROOT) + 1, strrpos($cwd, '/')));
		$lastIdx = count($arDirectory) - 1;
		$path = '';

		foreach ($arDirectory as $idx => $dir) {
			$path .= "/$dir";
			$template->setTemplatePath(DOCUMENT_ROOT . "$path/templates/$themeName", $dir, $idx === $lastIdx);
		}
	}
	
	public function getLayout() {
		return $this->layout;
	}
	
	public function setLayout($layoutName) {
		$this->layout = new Layout($this, $layoutName);
	}
	
	public function getName() {
		return $this->themeName;
	}
	
	public function getTemplate() {
		return $this->template;
	}
	
	public function getPath() {
		return $this->themePath;
	}
	
	public static function initialize($themeSelector = '\\Theme\\Theme::__defaultLoadHandler', $basePath = null) {
		if (!is_null($basePath)) {
			self::$basePath = realpath($basePath);
		} else {
			self::$basePath = ROOT . '/theme';
		}
		
		if (is_callable($themeSelector)) {
			self::$themeSelector = $themeSelector;
		} else {
			throw new Exception("Theme selector should be a valid callback");
		}
	}
	
	public static function getInstance($themeName = null) {
		if (null === self::$instance) {
			if (empty($themeName)) {
				throw new Exception\ThemeNotFoundException("Theme name cannot be empty");
			}
			// $dir = strtolower($theme);
			// if (file_exists(self::$basePath . '/' . $dir)) {
				list($themeName, $layout) = strpos($themeName, '::') ? explode('::', $themeName) : [$themeName, 'default'];
				$theme = new self($themeName);
				// $theme->setTemplate(\SiteManager::getTemplate());
				$theme->setLayout($layout);
				define('THEME_ASSETS_PATH', str_replace(rtrim(ROOT, '/'), '', $theme->getPath() . "/assets"));
				define('THEME_ASSETS_URL', SITE_HOME . THEME_ASSETS_PATH);
				self::$instance = $theme;
				\SiteManager::invokeHook(['theme_init', $theme], [$theme]);

			// } else {
			// 	FatalError('Theme not found', "Theme '$theme' not found in {self::$basePath}");
			// }
		}
		return self::$instance;
	}
	
	public static function load() {
		return self::getInstance(call_user_func_array(self::$themeSelector, func_get_args()));
	}
	
	private static function __defaultLoadHandler($desktop, $mobile = null, $basic = null) {
		$theme = $desktop;
		$ua_type = \RequestManager::getUserAgentType();
		if ($ua_type[0] === 'M') {
			$theme = $ua_type[1] === 'A' ? $mobile : $basic;
		}
		return $theme ?: $desktop;
	}
}

Theme::initialize();
