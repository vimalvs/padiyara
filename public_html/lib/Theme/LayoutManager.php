<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Theme Layout Class
 * 
 * Support for multiple layouts in themes
 * 
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2015-07-04 21:20:00 +0530 (Sat, 21 Jul 2015	) $
 */
namespace Theme;

class LayoutManager extends Layout {
	private $arContentArea = [];
	
	public function getContentAreas() {
		if (empty($this->arContentArea)) {
			foreach ($this->layoutInfo['content_areas'] as $contentArea) {
				$this->arContentArea[$contentArea] = new ContentAreaManager($this, $contentArea);
			}
		}
		return $this->arContentArea;
	}
	
	public function getContentArea($contentArea) {
		return new ContentAreaManager($this, $contentArea);
	}
	
	public function activate($contentArea = null) {
		$layoutInfo = $this->reload();

		foreach ($layoutInfo['content_areas'] as $contentAreaName => $contentDetails) {
			if ($contentArea && $contentArea !== $contentAreaName) continue;
			if (!in_array($contentAreaName, $this->layoutInfo['content_areas'])) {
				// Activate new content area
				$area = new ContentAreaManager($this, $contentAreaName);
				$area->activate();
				$this->layoutInfo['content_areas'][] = $contentAreaName;
			}
		}
		
		return \SiteManager::setConfig("theme_layout_{$this->theme->getName()}::{$this->layoutName}", $this->layoutInfo);
	}
	
	public function deactivate($contentArea = null) {
		foreach ($this->layoutInfo['content_areas'] as $idx => $contentAreaName) {
			if ($contentArea && $contentArea !== $contentAreaName) continue;
			// Deactivate content area
			$area = new ContentAreaManager($this, $contentAreaName);
			$area->deactivate();
			
			unset($this->layoutInfo['content_areas'][$idx]);
		}
		return \SiteManager::setConfig("theme_layout_{$this->theme->getName()}::{$this->layoutName}", $contentArea ? $this->layoutInfo : null);
	}
	
}