<?php
/**
 * 
 * Content Area
 * 
 * Class for managing content area for a Layout
 * 
 * @author		Joyce Babu <joyce@ennexa.com>
 * @version		1.0.0
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * 
 */
namespace Theme\Widget;

class View {
	private $theme = null;
	private $type = null;
	
	public static function getList($theme) {
		return ['list', 'thumbnail-list', 'thumbnails'];
	}
	
	public function __construct($type = null, $theme = null) {
		if (!$theme) {
			$theme = \Theme\Theme::getInstance();
		}
		$this->type = $type;
		$this->theme = $theme;
	}
	
	public function render($data) {
		return $this->theme->getTemplate()->render("theme::widgets/view/{$this->type}", $data, true);
	}
	
}