<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Widgets Class
 * 
 * This class provides methods for managing themes
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2015-07-04 21:20:00 +0530 (Sat, 21 Jul 2015	) $
 */
namespace Theme;

class Widgets {
	private $contentArea = null;
	private $section = null;
	private $widgets = null;
	
	public function __construct($contentArea, $section) {
		$this->contentArea = $contentArea;
		$this->section = strtr($section, '.', '_');
		$this->namespace = $contentArea->getNamespace();
		$this->widgets = $this->load($section, true);
	}
	
	public function activate($id, &$errorInfo = null) {
		if (isset($this->widgets['inactive'][$id])) {
			$widget = $this->widgets['inactive'][$id];
			if ($widget = $this->add($widget['class'], $widget['options'], $errorInfo)) {
				$this->remove($id, false);
				return $widget;
			}
		} else {
			throw new Exception\WidgetException('Invalid widget');
		}
		return false;
	}

	public function deactivate($id, &$errorInfo = null) {
		$widget = $this->widgets['active'][$id];
		if ($this->remove($id, true, $errorInfo)) {
			$this->widgets['inactive'][$id] = $widget;
			return \SiteManager::setConfig("wdgt-inactive-{$this->namespace}{$this->section}", $this->widgets['inactive']);
		}
		return false;
	}
	
	public function load($section, $loadInactive = false) {
		if (!$this->widgets) {
			$cache = \Cache::getInstance();
			$arWidgetInfo =  null;
		
			if (is_null($section)) $section = $this->section;
			
			$section = strtr($section, '.', '_');
			$sectionBucket = preg_replace(['#/#', '#\.$#'], ['.', '.home'], $section, 2, $count);
			if ($count <= 1) {
				$sectionBucket .= '.default';
			}
			do {
				$widgets = \SiteManager::getConfig("wdgt-{$this->namespace}{$section}");
				if (!is_null($widgets)) {
					$arWidgetInfo = ['active' => $widgets, 'inherited_from' => str_ireplace(['_php', '_htm', '_html'], ['.php', '.htm', '.html'], $section)];
					$cache->set("section-{$sectionBucket}", $arWidgetInfo, 259200, 'wdgt');
				} else {
					if (!empty($section)) {
						// Try with parent section
						$section = substr($section, 0, strrpos($section, '/'));
						$sectionBucket = preg_replace(['#/#', '#\.$#'], ['.', '.home'], $section, 2, $count);
						if ($count <= 1) {
							$sectionBucket .= '.default';
						}
						continue;
					}
				}
				break;
			} while (true);
			if (!$arWidgetInfo) {
				$arWidgetInfo = ['active' => [], 'inherited_from' => ''];
			}
			
			$this->widgets = $arWidgetInfo;
		}
		if ($loadInactive && !isset($this->widgets['inactive'])) {
			
			$this->widgets['inactive'] = \SiteManager::getConfig("wdgt-inactive-{$this->namespace}{$section}", []);
		}
		
		return $this->widgets;
	}
	

	public function add($widgetClass, $options = [], &$errorInfo = null) {
		$tmp = explode('\\', $widgetClass);
		$widgetName = array_pop($tmp);
		$widgetNamespace = implode('\\', $tmp);
		
		$widgetManager = new \WidgetManager($widgetNamespace);
		$widgets = $widgetManager->getDetails();
		
		if (!isset($widgets[$widgetName])) {
			throw new Exception\WidgetException("Invalid widget : $widgetClass");
		}
		
		$contentAreaDetails = $this->contentArea->getDetails();

		$limit = 999;
		// Get Default Options for the widget
		$widgetDefaultOptions = [];

		foreach ($contentAreaDetails['widgets'] as $widget) {
			if ($widget['namespace'] === $widgetNamespace && in_array($widget['name'], [$widgetName, '*'])) {
				$widgetDefaultOptions = isset($widget['options']) ? $widget['options'] : [];
				if (isset($widget['limit'])) {
					$limit = $widget['limit'];
				}
				break;
			}
		}

		$settings = $widgetClass::getSetting();
		if (!$settings['multi']) $limit = 1;
	
		foreach ($this->widgets['active']  as $tmp) {
			if ($tmp['class'] === $widgetClass) {
				$limit--;
			}
		}
		
		if ($limit <= 0) {
			throw new Exception\WidgetException("$widgetName already has max allowed number of instances.");
		}
		
		// do {
		// 	$id = md5(uniqid($this->namespace, true));
		// } while (isset($this->widgets['active'][$id]) || isset($this->widgets['inactive'][$id]));
		
		$id = md5(uniqid($this->namespace, true));
		
		$widget = new $widgetClass($id, $options, $this->contentArea->getLayout()->getTheme());
		$widget->options($widgetDefaultOptions);
		
		$this->contentArea->getWidgetHelper()->add($widget);
		\SiteManager::invokeHook(['widget_add', $this->contentArea], $widget);
		
		$this->widgets['active'][$id] = [
			'id' => $id, 
			'class' => $widgetClass, 
			'options' => $widget->options()
		];

		if ($this->save()) {
			return $widget;
		}
		
		return false;
	}

	public function update($id, &$errorInfo = null) {
		if (!isset($this->widgets['active'][$id])) {
			throw new Exception\WidgetException('Invalid Widget');
		}
		
		$widget = $this->widgets['active'][$id];
		$clsName = $widget['class'];
		$widget = new $clsName($id, $widget['options'], $this->contentArea->getLayout()->getTheme());
		
		$this->contentArea->getWidgetHelper()->update($widget, $errorInfo);
		
		if ($widget->update($errorInfo)) {
			$options = $widget->options();
			$this->widgets['active'][$id]['options'] = $options;
			$this->save();
			return $widget;
		}
		return false;
	}

	public function import($id, $options, &$errorInfo = null) {
		if (isset($this->widgets['active'][$id])) {
			$widget = $this->widgets['active'][$id];
			$clsName = $widget['class'];
			$widget = new $clsName($id, $options, $this->contentArea->getLayout()->getTheme());
			$this->widgets['active'][$id]['options'] = $options;
			
			$this->save();
			return $widget;
		} else {
			$errorInfo = 'Invalid Widget';
		}
		return false;
	}
	
	public function remove($id, $status, &$errorInfo = null) {
		if ($status) {
			if (!isset($this->widgets['active'][$id])) {
				throw new Exception\WidgetException('Invalid Widget');
			}
			unset($this->widgets['active'][$id]);
			return $this->save();
		} else {
			if (!isset($this->widgets['inactive'][$id])) {
				throw new Exception\WidgetException('Invalid Widget');
			}
			unset($this->widgets['inactive'][$id]);
			return \SiteManager::setConfig("wdgt-inactive-{$this->namespace}{$this->section}", empty($this->widgets['inactive']) ? null : $this->widgets['inactive']);
		}
		return false;
	}

	public function reset() {
		if ($this->section) {
			$updated = \SiteManager::setConfig("wdgt-{$this->namespace}{$this->section}", null);
			if ($updated !== false) {
				$this->clearCache();
				return true;
			}
		} else {
			throw new Exception\WidgetException("Already at default set. Cannot reset further.");
		}
		return false;
	}

	public function reorder($order, &$errorInfo = null) {
		if (count($order) === count($this->widgets['active'])) {
			foreach ($order as $c) {
				if (!isset($this->widgets['active'][$c])) {
					Exception\WidgetException("Invalid widget : $widgetClass");
				}
				$arNewWidgets[$c] = $this->widgets['active'][$c];
			}
			if (empty($errorInfo)) {
				$this->widgets['active'] = $arNewWidgets;
				// $updated = \SiteManager::setConfig($fieldKey, $arNewWidgets);
				if ($this->save()) {
					return true;
				} else {
					throw new Exception\WidgetException("Database Error : Failed to update database.");
				}
			}
		} else {
			throw new Exception\WidgetException('New widget count does not match with current value in database');
		}
		return false;
	}

	public function save() {		
		$status = \SiteManager::setConfig("wdgt-{$this->namespace}{$this->section}", $this->widgets['active']);
		if (!$this->section) {
			// Cache default widget set on Filesystem
			file_put_contents(ROOT . '/../.cache/widgets/' . preg_replace('/[^a-z0-9_]+/i', '_', $this->namespace) . '.json', json_encode($this->widgets['active']));
		}
		$this->clearCache();
		return $status;
	}
	
	public static function autocomplete($term, $layout) {
		$term = strtr($term, '.', '_');
		list($theme_name, $layout_name) = explode('::', $layout);
		$pdbo = \SiteManager::getDatabase();
		$stmt = $pdbo->query("SELECT DISTINCT(SUBSTRING(name, LOCATE('/', name))) FROM config WHERE name LIKE " . $pdbo->quote("wdgt-$theme_name%$term%") . " ORDER BY name desc LIMIT 15");
		$result = [];
		while ($rec = $stmt->fetchColumn()) {
			$result[] = str_ireplace(['_php', '_html', '_htm'], ['.php', '.html', '.htm'], $rec);
		}
		return $result;
	}
	
	public function clearCache() {
		$cache = \Cache::getInstance();
		$cache->delete("section-{$this->section}", 'wdgt');
		$this->contentArea->clear($this->section);
	}
}