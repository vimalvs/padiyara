<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * ThemeManager Class
 * 
 * This class provides methods for managing themes
 * @package		Theme
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2015-07-04 21:20:00 +0530 (Sat, 21 Jul 2015	) $
 */
namespace Theme;

class ThemeManager extends Theme {
	private static $arTheme = [];
	
	private $themeInfo = null;
	
	public function __construct($theme_name) {
		parent::__construct($theme_name);
		$this->themeInfo = json_decode(file_get_contents($this->themePath . '/theme-info.json'), true);
	}
	
	public function getLayouts() {
		return $this->themeInfo['layouts'];
	}
	
	public function getInfo($key = null) {
		if (!is_null($key)) {
			return isset($this->themeInfo[$key]) ? $this->themeInfo[$key] : null;
		} else {
			return $this->themeInfo;
		}
	}
	
	public function getLayoutByName($layout_name = 'default') {
		return new LayoutManager($this, $layout_name);
	}
	
	public function activate($errorInfo = null) {
		self::$arTheme[$this->themeName] = $this->themeInfo;
		foreach ($this->themeInfo['layouts'] as $layoutName) {
			try {
				$layout = new LayoutManager($this, $layoutName);
				$layout->activate();
			} catch (\Exception $e) {
				$errorInfo = "Failed to activate layout $layoutName - {$e->getMessage()}";
				return false;
			}
		}
		$status = \SiteManager::setConfig('theme_list', self::$arTheme);
		
		if ($status) {
			return true;
		}
		
		$errorInfo = 'Failed to activate theme';
		return false;
	}
	
	public function deactivate($errorInfo = null) {
		foreach ($this->themeInfo['layouts'] as $layoutName) {
			try {
				$layout = new LayoutManager($this, $layoutName);
				$layout->deactivate();
			} catch (\Exception $e) {
				$errorInfo = "Failed to activate layout $layoutName - {$e->getMessage()}";
				return false;
			}
		}

		unset(self::$arTheme[$this->themeInfo['theme_name']]);
		$status = \SiteManager::setConfig('theme_list', self::$arTheme);
		
		if ($status) {
			return true;
		}
		
		$errorInfo = 'Failed to activate theme';
		return false;
	}
	
	public static function initialize($themeSelector = '\\Theme\\Theme::__defaultLoadHandler', $basePath = null) {
		self::$arTheme = \SiteManager::getConfig('theme_list', []);
		Theme::initialize($themeSelector, $basePath);
	}
	
	public static function getList() {
		return self::$arTheme;
	}
	
	public static function reload() {
		$dirIterator = new \RecursiveDirectoryIterator(self::$basePath);
		$fileIterator = new \Filesystem\FilenameFilterIterator($dirIterator, "/theme-info\.json/");
		$iterator = new \RecursiveIteratorIterator($fileIterator);
		$iterator->setMaxDepth(2);
		
		$arTheme = [];
		
		foreach ($iterator as $file) {
			$name = basename(dirname($file));
			$themeInfo = json_decode(file_get_contents($file), true);
			
			if ($themeInfo && !empty($themeInfo['theme_name'])) {
				$theme = new self($themeInfo['theme_name']);
				if ($theme) {
					$themeInfo['status'] = 0;
				} else {
					$themeInfo = ['theme_name' => $name, 'theme_pathname' => $name, 'status' => 3, 'message' => 'Failed to instantiate Theme'];
				}
			} else {
				$themeInfo = ['title' => $name, 'theme_pathname' => $name, 'status' => 3, 'message' => 'Invalid JSON'];
			}
			$arTheme[$themeInfo['theme_name']] = $themeInfo;
		}

		foreach (self::$arTheme as $theme) {
			$name = $theme['theme_name'];
			if (isset($arTheme[$name])) {
				$arTheme[$name]['status'] = 1;
			} else {
				$theme['status'] = 2;
				$arTheme[$name]['status'] = $theme;
			}
		}
		return $arTheme;
	}
}

ThemeManager::initialize();