<?php
/**
 * 
 * Content Area
 * 
 * Class for managing content area for a Layout
 * 
 * @author		Joyce Babu <joyce@ennexa.com>
 * @version		1.0.0
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * 
 */
namespace Theme;

use \Widget;

class ContentArea {
	
	private $cache = null;
	protected $section = '/';
	protected $layout = null;
	protected $name = null;
	protected $namespace = null;
	
	private static $wprefix = null;
	private static $wpath = null;
	
	public static function setPath($path = null, $prefix = null) {
		if (!is_null($path)) self::$wpath = $path;
		if (!is_null($prefix)) self::$wprefix = $prefix;
	}
	
	public static function getPath() {
		if (defined('WIDGET_PATH')) {
			return WIDGET_PATH;
		} else {
			if (is_null(self::$wpath)) {
				$path = !empty($_SERVER['SCRIPT_URL']) ? $_SERVER['SCRIPT_URL'] : (($pos = strpos($_SERVER['REQUEST_URI'], '?')) ? substr($_SERVER['REQUEST_URI'], 0, $pos) : $_SERVER['REQUEST_URI']);
				if (preg_match("#/page-\d+.html?#", $path, $match)) $path = substr($path, 0, -strlen($match[0])) . '/{PAGE}';
			} else {
				$path = self::$wpath;
			}
			return (empty(self::$wprefix) ? '' : ('/' . self::$wprefix)) . $path;
		}
	}
	
	public function __construct($layout, $contentAreaName) {
		$this->layout = $layout;
		$this->name = $contentAreaName;
		$this->namespace = $layout->getTheme()->getName() . "::$contentAreaName";
		$widgetHelper = "\\Theme\\{$this->layout->getTheme()->getName()}\\WidgetHelper";
		$this->widgetHelper = new $widgetHelper($this);
		$this->cache = \Cache::getInstance();
	}
	
	private function prepareWidgets($widgets, &$cacheable = null) {
		$arWidgetDetails = [];
		$theme = $this->layout->getTheme();
		$cacheable = true;
		foreach ($widgets as $id => $widgetInfo) {
			if (class_exists($widgetInfo['class'])) {
				try {
					$widget = new $widgetInfo['class']($id, $widgetInfo['options'], $theme);
					$cache = $widget->getCacheType();
					switch ($cache) {
						case Widget::CACHE_DENY:
							$arWidgetDetails[$id] = ['cache' => $cache, 'widgetInfo' => $widgetInfo, 'expires' => 0];
							$cacheable = false;
							break;
						case Widget::CACHE_ALLOW:
							$code = $this->widgetHelper->format($widget);
							if ($code) {
								$arWidgetDetails[$id] = ['cache' => $cache, 'code' => $code];
							}
							break;
						case Widget::CACHE_TTL:
							$cacheable = false;
							$code = $this->widgetHelper->format($widget);
							$expires = $widget->getExpiry();
							$arWidgetDetails[$id] = ['cache' => $cache, 'widgetInfo' => $widgetInfo, 'code' => $code, 'expires' => $expires];
							break;
					}
				} catch (\Exception $e) {
					\SiteManager::getLogger()->addError("Widget Error: Failed to render widget in '$section'- {$e->getMessage()}", $widgetInfo);
				}
			} else {
				\SiteManager::getLogger()->addWarning("Invalid Widget - {$widgetInfo['class']}", $widgetInfo);
			}
		}
		return $arWidgetDetails;
	}
	
	private function getBucketName($section) {
		$sectionBucket = preg_replace(['#/#', '#\.$#'], ['.', '.home'], $section, 2, $count);
	
		if ($count <= 1) {
			$sectionBucket .= '.default';
		}
		
		return $sectionBucket;
	}
	
	private function getWidgetDetails($section) {
		
		$sectionBucket = $this->getBucketName($section);
		
		$widgetsInfo = $this->cache->get("{$this->namespace}{$sectionBucket}", 'c_area');
		if ($widgetsInfo === null) {
			$widgets = \SiteManager::getConfig("wdgt-{$this->namespace}{$section}");
			if (!is_null($widgets)) {
				$arWidgetDetails = $this->prepareWidgets($widgets, $cacheable);
				$widgetsInfo = ['widgets' => $arWidgetDetails, 'inherited_from' => str_ireplace(['_php', '_htm', '_html'], ['.php', '.htm', '.html'], $section), 'cacheable' => $cacheable];

				$this->cache->set("{$this->namespace}{$sectionBucket}", $widgetsInfo, 259200, 'c_area');
			} else {
				if (!empty($section)) {
					$tmp = explode('/', $section);
					unset($tmp[count($tmp) - 1]);
					$tmp = implode('/', $tmp);
					$widgetsInfo = $this->getWidgetDetails($tmp);
					if ($widgetsInfo) {
						$inheritedBucket =  preg_replace(['#/#', '#\.$#'], ['.', '.home'], strtr($widgetsInfo['inherited_from'], '.', '_'), 2, $count);		
						if ($count <= 1) {
							$inheritedBucket .= '.default';
						}
						$this->cache->set("{$this->namespace}{$sectionBucket}", $inheritedBucket, 259200, 'c_area');
					}
				} else {
					$widgets = json_decode(file_get_contents(ROOT . '/../.cache/widgets/' . preg_replace('/[^a-z0-9_]+/i', '_', $this->namespace) . '.json'), true);
					$arWidgetDetails = $this->prepareWidgets($widgets);
					$widgetsInfo = ['widgets' => $arWidgetDetails, 'inherited_from' => '', 'cacheable' => false];
				}
			}
			if (!$widgetsInfo) {
				$widgetsInfo = ['widgets' => [], 'inherited_from' => '', 'cacheable' => true];
			}
		} elseif (is_string($widgetsInfo)) {
			$widgetsInfo = $this->getWidgetDetails(str_ireplace(['.default', '.'], ['', '/'], $widgetsInfo));
		}
		return $widgetsInfo;
	}
	
	public function load($section) {
		if (is_null($section)) {
			$section = $this->section;
		}
		$section = strtr($section, '.', '_');
		$arWidgetDetails = $this->getWidgetDetails($section);
		
		$arWidgets = [];
		$updated = false;
	
		if (!isset($theme)) $theme = $this->layout->getTheme();
		
		foreach ($arWidgetDetails['widgets'] as $id => $widgetDetails) {
			if ($widgetDetails['cache'] === Widget::CACHE_ALLOW || $widgetDetails['expires'] > TIME_NOW) {
				$arWidgets[] = $widgetDetails['code'];
			} else {
				$widgetInfo = $widgetDetails['widgetInfo'];
				if (class_exists($widgetInfo['class'])) {
					try {
						$widget = new $widgetInfo['class']($id, $widgetInfo['options'], $theme);
						$code = $this->widgetHelper->format($widget);
			
						if ($code !== false) {
							$arWidgets[] = $code;
						}
						if ($widgetDetails['cache'] === Widget::CACHE_TTL) {
							$widgetDetails['code'] = $code;
							$widgetDetails['expires'] = $widget->getExpiry();
				
							$arWidgetDetails['widgets'][$id] = $widgetDetails;
							$updated = true;
						}
					} catch (\Exception $e) {
						\SiteManager::getLogger()->addError("Widget Error: Failed to render widget in '$section'- {$e->getMessage()}", $widgetInfo);
					}
				} else {
					\SiteManager::getLogger()->addError("Invalid Widget in used in '$section'- {$widgetInfo['class']}", $widgetInfo);
				}
			}
		}
		
		if ($updated) {
			$sectionBucket = $this->getBucketName($section);
			$this->cache->set("{$this->namespace}{$sectionBucket}", $arWidgetDetails, 259200, 'c_area');
		}
		return $arWidgets;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getLayout() {
		return $this->layout;
	}
	
	public function getNamespace() {
		return $this->namespace;
	}
	
	public function getWidgetHelper() {
		return $this->widgetHelper;
	}
	
	
}
