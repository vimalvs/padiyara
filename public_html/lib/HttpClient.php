<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

class HttpClient {
	
	private $ch = NULL;
	private $arHeader = array (
		'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language: en-us,en;q=0.5',
		'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
		'Keep-Alive: 115',
		'Connection: keep-alive',
		'Cache-Control: max-age=0'
	);
	private $lastRequestUrl = null;
	private $lastRequestData = null;
	
	private static $_instance = NULL;
	
	public function __construct() {
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($this->ch, CURLOPT_TIMEOUT, 3); 
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($this->ch, CURLOPT_ENCODING, '');
		curl_setopt($this->ch, CURLOPT_COOKIESESSION, TRUE);
		//curl_setopt($this->ch, CURLOPT_FAILONERROR, TRUE);
		
		// curl_setopt($this->ch, CURLOPT_HEADER, 1);
		// curl_setopt($this->ch, CURLINFO_HEADER_OUT, 1);
		
		$this->setUserAgent(0);
		
	}
	
	private function _request($url, $arHeader = array()) {
		$arHeader = is_array($arHeader) ? array_merge($arHeader, $this->arHeader) : $this->arHeader;
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $arHeader);
		// if ($userAgent) curl_setopt($this->ch, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($this->ch, CURLOPT_URL, $url);
		$this->lastRequestUrl = $url;
		$data = curl_exec($this->ch);
		return $data;
	}
	
	public function doRequest($url, $method = 'GET', $data = null, $arHeader = array()) {
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, is_array($data) ? http_build_query($data) : $data);
		return $this->_request($url, $arHeader);
	}
	
	public function doGet($url, $arParam = array(), $arHeader = array()) {
		if (!empty($arParam)) {
			$url = qsAddParam($url, $arParam);
		}
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($this->ch, CURLOPT_HTTPGET, true);
		curl_setopt($this->ch, CURLOPT_POST, false);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, null);
		return $this->_request($url, $arHeader);
	}
	
	public function doPost($url, $data = null, $arHeader = array()) {
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($this->ch, CURLOPT_HTTPGET, false);
		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, is_array($data) ? http_build_query($data) : $data);
		return $this->_request($url, $arHeader);
	}
	
	public function doPostMultipart($url, $data = null, $arHeader = array()) {
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($this->ch, CURLOPT_HTTPGET, false);
		curl_setopt($this->ch, CURLOPT_POST, true);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
		return $this->_request($url, $arHeader);
	}
	
	public function enableCookie($file = '/tmp/cookie.txt') {
		curl_setopt($this->ch, CURLOPT_COOKIEJAR, $file);
		curl_setopt($this->ch, CURLOPT_COOKIEFILE, $file);
	}
	
	public function disableCookie() {
		curl_setopt($this->ch, CURLOPT_COOKIEJAR, FALSE);
		curl_setopt($this->ch, CURLOPT_COOKIEFILE, FALSE);
	}
	
	public function setHeader($header, $value = null, $overwrite = null) {
		if (is_array($header)) {
			$overwrite = $value;
			if (isset($header[0])) {
				foreach ($header as $h) {
					list($h, $v) = preg_split('/\s*:\s*/', $h);
					$this->setHeader($h, $v, $overwrite);
				}
			} else {
				foreach ($header as $h => $v) {
					$this->setHeader($h, $v, $overwrite);
				}
			}
		} else {
			if ($overwrite !== false) {
				foreach ($this->arHeader as $idx => $h) {
					list($h, $v) = explode(': ', $h);
					if ($h === $header) {
						if (is_null($value)) {
							unset($this->arHeader[$idx]);
						} else {
							$this->arHeader[$idx] = "$header: $value";
							return;
						}
					}
				}
				// Not Found - Add New Key
				if (!is_null($value)) {
					$this->arHeader[] = "$header: $value";
				}
			} else {
				$this->arHeader[] = "$header: $value";
			}
		}
		// $this->arHeader = array_merge($this->arHeader, $arHeader);
	}
	
	public function clearHeader($key = null) {
		if ($key) {
			$this->setHeader($key, null);
		} else {
			$this->arHeader = [];
		}
	}
	
	public function setUserAgent($userAgentType) {
		if ($userAgentType === 0) {
			$userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1';
		} elseif ($userAgentType === 1) {
			$userAgent = $_SERVER['HTTP_USER_AGENT'];
		} else {
			$userAgent = $userAgentType;
		}
		curl_setopt($this->ch, CURLOPT_USERAGENT, $userAgent);
	}
	
	public function setReferer($referer) {
		curl_setopt($this->ch, CURLOPT_REFERER, $referer);
	}
	
	public function getInfo($opt = NULL) {
		return curl_getinfo($this->ch, $opt);
	}
	
	public function getLastError() {
		return curl_error($this->ch);
	}
	
	public function setOpt($opt, $value) {
		return curl_setopt($this->ch, $opt, $value);
	}
	
	public function getPageContent($url, $arHeader = array(), $userAgentType = 0) {
		$arHeader = empty($arHeader) ? $this->arHeader : array_merge($this->arHeader, $arHeader);
		return self::_request($url, $arHeader, $userAgent);
	}
	
	public function getMetaInfo($content, &$arData = array()) {
		//parsing begins here:
		$doc = new DOMDocument();
		$tidy = new tidy();
		@$doc->loadHTML($tidy->repairString($content));
		$nodes = $doc->getElementsByTagName('title');
		//get and display what you need:
		$arData['title'] = strTrim((string)$nodes->item(0)->nodeValue, 150);

		$metas = $doc->getElementsByTagName('meta');

		for ($i = 0; $i < $metas->length; $i++) {
		    $meta = $metas->item($i);
		    if ($meta->getAttribute('name') == 'description') {
				$arData['description'] = strTrim((string)$meta->getAttribute('content'), 256);
			}
		    if ($meta->getAttribute('name') == 'keywords') {
				$keywords = strTrim((string)$meta->getAttribute('content'), 150, '');
				$tmp = preg_split('/,/', $keywords, -1, PREG_SPLIT_NO_EMPTY);
				$arData['keywords'] = implode(', ', $tmp);
			}
		}
		// $content = self::getPageContent($page);
		// $hash = md5($page);
		// if ($content && file_put_contents("/tmp/$hash", $content)) {
		// 	$tidy = new tidy();
		// 	$content = $tidy->repairString($content);
		// 	if (!empty($tmp['title'])) {
		// 		$arData['title'] = strTrim($tmp['75'], 256);
		// 	}
		// 	if (!empty($tmp['description'])) {
		// 		$arData['description'] = strTrim($tmp['description'], 256);
		// 	}
		// }
		return $arData;
	}
	public function getLastRequestDetails() {
		return [
			'url' => $this->lastRequestUrl
		];
	}
	
	public static function request($url, $arParam = array(), $arHeader = array(), $doPost = FALSE) {
		if (is_null(self::$_instance)) self::$_instance = new self;
		return $doPost ? self::$_instance->doPost($url, $arParam, $arHeader) : $_instance->doGet($url, $arParam, $arHeader);
		
	}
}
