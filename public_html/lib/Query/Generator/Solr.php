<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Query;

class Generator_Solr {
	private $qc = NULL;
	private static $arRelation = array(
		Condition::LT => '<',
		Condition::GT => '>',
		Condition::LTE => '<=',
		Condition::GTE => '>='
	);
	
	public static function escape($string) {
		$match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ');
		$replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ');
		$string = str_replace($match, $replace, $string);
			// return urlencode($string);
		return $string;
	}
	
	public function __construct() {
		
	}
	
	public function condition($qc) {
		return $this->_generateCondition($qc);
	}
	
	private function _generateCondition($qc) {
		$conditions = array();
		foreach ($qc as $k => $v) {
			if (is_array($v)) {
				if (is_array($v[0])) {
					
				} else {
					$code = $v[0] & 127; // 128 => 1000 0000, 127 => 0111 1111
					$flgNegate = $v[0] & 128;
					$escapedVal = is_scalar($v[2]) ? self::escape($v[2]) : NULL;
					switch ($code) {
						// -fieldName:[* TO *]
						case Condition::EQUAL:
						case Condition::LIKE:
							if (is_array($v[2])) {
								$tmp = array();
								foreach ($v[2] as $v2) {
									// [WARNING] null and empty are same in solr
									if (is_null($v2) || $v[2] === '') {
										$tmp[] = ($flgNegate ? '' : '-') . "$v[1]:[* TO *]";
									} else {
										$tmp[] = ($flgNegate ? '-' : '') . "$v[1]:" . str_replace('%', '*', self::escape($v2));
									}
									
								}
								$conditions[] = '(' . implode(' OR ', $tmp) . ')';
							} else {
								// [WARNING] null and empty are same in solr
								if (is_null($v[2]) || $v[2] === '') {
									$conditions[] = ($flgNegate ? '' : '-') . "$v[1]:[* TO *]";
								} else {
									$conditions[] = ($flgNegate ? '-' : '') . "$v[1]:" . str_replace('%', '*', $escapedVal);
								}
							}
							break;
						case Condition::FULLTEXT:
							$conditions[] = ($flgNegate ? '-' : '') . "$v[1]:(" . $v[2] . ')';
							break;
						case Condition::RANGE:
							if (is_array($v[2][0])) {
								$tmp = array();
								foreach ($v[2] as $v2) {
									$tmp[] = ($flgNegate ? '-' : '') . "$v[1]:[$v2[0] TO $v2[1]]";
								}
								$conditions[] = '(' . implode(' OR ', $tmp) . ')';
							} else {
								$conditions[] = ($flgNegate ? '-' : '') . "$v[1]: [{$v[2][0]} TO {$v[2][1]}]";
							}
							break;
						case Condition::GT:
							trigger_error('[SolrQueryGenerator] Greater than is not supported. Try GTE', E_USER_WARNING);
							break;
						case Condition::LT:
							trigger_error('[SolrQueryGenerator] Less than is not supported. Try LTE', E_USER_WARNING);
							break;
						case Condition::GTE:
							$conditions[] = ($flgNegate ? '-' : '') . "$v[1]:[$v[2] TO *]";
							break;
						case Condition::LTE:
							$conditions[] = ($flgNegate ? '-' : '') . "$v[1]:[* TO $v[2]]";
							break;
						default:
							trigger_error("[SolrQueryGenerator] Invalid condition type", E_USER_WARNING);
					}
				}
			} elseif ($v instanceof Condition) {
				$conditions[] = $this->_generateCondition($v);
			} else {
				$conditions[] = $qc;
			}
		}
		return empty($conditions) ? 1 : ('(' . implode(($qc->type() === Condition::ANY) ? ' OR ' : ' AND ', $conditions) . ')');
	}
}