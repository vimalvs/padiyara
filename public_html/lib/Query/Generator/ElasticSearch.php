<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Query;

class Generator_ElasticSearch {
	private $qc = NULL;
	private static $arRelation = array(
		Condition::LT => '<',
		Condition::GT => '>',
		Condition::LTE => '<=',
		Condition::GTE => '>='
	);
	
	public static function escape($string) {
		$match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ');
		$replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ');
		$string = str_replace($match, $replace, $string);
			// return urlencode($string);
		return $string;
	}
	
	public function __construct() {
		
	}
	
	public function condition($qc) {
		return $this->_generateCondition($qc);
	}
	
	private function _generateCondition($qc) {
		$conditions = array();
		$conjunction = $qc->type() === Condition::ANY ? 'or' : 'and';
		
		foreach ($qc as $k => $v) {
			if (is_array($v)) {
				if (is_array($v[0])) {
					
				} else {
					$code = $v[0] & 127; // 128 => 1000 0000, 127 => 0111 1111
					$flgNegate = $v[0] & 128;
					//$escapedVal = is_scalar($v[2]) ? self::escape($v[2]) : NULL;
					switch ($code) {
						case Condition::EQUAL:
						case Condition::LIKE:
						case Condition::FULLTEXT:
							if (is_array($v[2])) {
								$tmp = array('terms' => array($v[1] => $v[2]));
							} else {
								$tmp = array('term' => array($v[1] => $v[2]));
							}
							if ($flgNegate) {
								$conditions[] = array('not' => $tmp);
							} else {
								$conditions[] = $tmp;
							}
							break;
							
						case Condition::RANGE:
							$relation = $flgNegate ? 'NOT BETWEEN' : 'BETWEEN';
							if (is_array($v[2][0])) {
								$tmp = array();
								foreach ($v[2] as $v2) {
									$tmp[] = "$v[1] $relation " . $this->db->quote($v2[0]) . ' AND ' . $this->db->quote($v2[1]);
								}
								$conditions[] = '(' . implode(' OR ', $tmp) . ')';
							} else {
								$conditions[] = "$v[1] $relation " . $this->db->quote($v[2][0]). " AND " . $this->db->quote($v[2][1]);
							}
							break;
						case Condition::GT:
						case Condition::LT:
						case Condition::GTE:
						case Condition::LTE:
							$relation = self::$arRelation[$code];
							$conditions[] = "$v[1] $relation " . (is_numeric($v[2]) ? $v[2] : $this->db->quote($v[2]));
							break;
						default:
							trigger_error("Invalid condition type", E_USER_WARNING);
					}
				}
			} elseif ($v instanceof Condition) {
				$conditions[] = $this->_generateCondition($v);
			} else {
				$conditions[] = $qc;
			}
		}
		if (!empty($conditions)) {
			if (count($conditions) === 1) {
				return $conditions[0];
			} else {
				return ($qc->type() === Condition::ANY) ? array('or' => $conditions) : array('and' => $conditions);
			}
		} else {
			return 1;
		}
	}
}