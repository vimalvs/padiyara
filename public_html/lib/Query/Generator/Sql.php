<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

namespace Query;

class Generator_Sql {
	private $qc = NULL;
	private $db = NULL;
	private static $arRelation = array(
		Condition::LT => '<',
		Condition::GT => '>',
		Condition::LTE => '<=',
		Condition::GTE => '>='
	);
	public function __construct($conn = NULL) {
		if ($conn) $this->setConnection($conn);
	}
	
	public function setConnection($db) {
		$this->db = $db;
	}
	
	public function condition($qc) {
		return $this->_generateCondition($qc);
		return FALSE;
	}
	
	private function _generateCondition($qc) {
		$conditions = array();
		foreach ($qc as $k => $v) {
			if (is_array($v)) {
				if (is_array($v[0])) {
					
				} else {
					$code = $v[0] & 127; // 128 => 1000 0000, 127 => 0111 1111
					$flgNegate = $v[0] & 128;
					switch ($code) {
						case Condition::EQUAL:
						case Condition::LIKE:
							if ($flgNegate) {
								$relation = ($code === Condition::EQUAL) ? '!=' : 'NOT LIKE';
							} else {
								$relation = ($code === Condition::EQUAL) ? '=' : 'LIKE';
							}
							if (is_array($v[2])) {
								if ($code === Condition::EQUAL) {
									$quotedValue = array_filter($v[2], 'is_numeric') ? $v[2] : array_map([$this->db, 'quote'], $v[2]);
									$conditions[] = $v[1] . ($flgNegate ? ' NOT' : '') .' IN (' . implode(', ', $quotedValue) . ')';
								} else {
									$tmp = array();
									foreach ($v[2] as $v2) {
										$tmp[] = $v[1] . (is_null($v2) ? ($flgNegate ? ' IS NOT ' : ' IS ') : " $relation ") . $this->db->quote($v2);
									}
									$conditions[] = '(' . implode(' OR ', $tmp) . ')';
								}
							} else {
								$conditions[] = $v[1] . (is_null($v[2]) ? ($flgNegate ? ' IS NOT ' : ' IS ') : " $relation ") . $this->db->quote($v[2]);
							}
							break;
						case Condition::FULLTEXT:
							$conditions[] = ($flgNegate ? 'NOT ' : '') . "MATCH($v[1]) AGAINST(" . $this->db->quote($v[2]) . (!empty($v[3]) ? ' IN BOOLEAN MODE' : '') . ")";
							break;
						case Condition::RANGE:
							$relation = $flgNegate ? 'NOT BETWEEN' : 'BETWEEN';
							if (is_array($v[2][0])) {
								$tmp = array();
								foreach ($v[2] as $v2) {
									$tmp[] = "$v[1] $relation " . $this->db->quote($v2[0]) . ' AND ' . $this->db->quote($v2[1]);
								}
								$conditions[] = '(' . implode(' OR ', $tmp) . ')';
							} else {
								$conditions[] = "$v[1] $relation " . $this->db->quote($v[2][0]). " AND " . $this->db->quote($v[2][1]);
							}
							break;
						case Condition::GT:
						case Condition::LT:
						case Condition::GTE:
						case Condition::LTE:
							$relation = self::$arRelation[$code];
							$conditions[] = "$v[1] $relation " . (is_numeric($v[2]) ? $v[2] : $this->db->quote($v[2]));
							break;
						default:
							trigger_error("Invalid condition type", E_USER_WARNING);
					}
				}
			} elseif ($v instanceof Condition) {
				$conditions[] = $this->_generateCondition($v);
			} else {
				$conditions[] = $qc;
			}
		}
		return empty($conditions) ? 1 : ('(' . implode(($qc->type() === Condition::ANY || $qc->type() === Condition::ANY) ? ' OR ' : ' AND ', $conditions) . ')');
	}
}