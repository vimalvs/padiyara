<?php
/**
 * Error Handler
 * 
 * PHP Error Handler
 * @package		Utilities
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		1.0.0
 * @date		$Date: 2010-07-02 17:47:00 +0530 (Fri, 02 Jul 2010	) $
 */
class ErrorHandler {
	
	const DEBUG = 0;
	const NOTICE = 1;
	const WARNING = 2;
	const ERROR = 4;
	
	private static $enabled = FALSE;
	private static $count = 0;
	
	public static function initialize() {
		set_error_handler(array(__CLASS__, 'handler'));
		self::$enabled = TRUE;
	}
	
	public static function uninitialize() {
		restore_error_handler();
		self::$enabled = FALSE;
	}
	
	private static function _errorInfoFromCode($code) {
 		$arError = array(
			E_ERROR => array('Error', ErrorHandler::ERROR),
			E_PARSE => array('Parse error', ErrorHandler::ERROR),
			E_CORE_ERROR => array('Core error', ErrorHandler::ERROR),
			E_COMPILE_ERROR => array('Compile error', ErrorHandler::ERROR),
			E_USER_ERROR => array('User error', ErrorHandler::ERROR),
			E_RECOVERABLE_ERROR => array('Recoverable fatal error', ErrorHandler::WARNING),
			E_CORE_WARNING => array('Core warning', ErrorHandler::WARNING),
			E_COMPILE_WARNING => array('Compile warning', ErrorHandler::WARNING),
			E_USER_WARNING => array('User warning', ErrorHandler::WARNING),
			E_WARNING => array('Warning', ErrorHandler::WARNING),
			E_NOTICE => array('Notice', ErrorHandler::NOTICE),
			E_USER_NOTICE => array('User notice', ErrorHandler::NOTICE),
			E_STRICT => array('Strict warning', ErrorHandler::DEBUG)
		);
		if (defined('E_DEPRECATED')) {
			$arError[E_DEPRECATED] = array('Deprecated function', ErrorHandler::DEBUG);
			$arError[E_USER_DEPRECATED] = array('User deprecated function', ErrorHandler::DEBUG);
		}
		return $arError[$code];
	}
	
	public static function handler($errorCode, $errorString, $errorFile, $errorLine, $errorContext) {
		if ($errorCode & error_reporting()) {
			$arErrorInfo = self::_errorInfoFromCode($errorCode);
			$errorMsg = "<pre id=eh-alert-" . self::$count++ . " class=eh-alert-type-" . makeSafeName($arErrorInfo[0]) . "><b>{$arErrorInfo[0]}</b>: $errorString in <b>$errorFile</b> on line <b>$errorLine</b><br /></pre>\n";
			switch ($arErrorInfo[1]) {
				case ErrorHandler::ERROR:
					$arErrorMsg = array(
						'<p>Unfortunately, an unrecoverable error has occurred. Our <em>Webmaster</em> have been automatically notified about this error. </p><p>We are extremely sorry for the inconvenience caused, and promise to be back as soon as possible.  Please try again at a later time.</p>',
						$errorMsg
					);
					FatalError('A fatal error encountered while generating the requested web page.', $arErrorMsg, "PHP {$arErrorInfo[0]}", NULL, array('errorContext' => $errorContext, 'skipTraceLevel' => 4));
					break;
				default:
					if (ini_get('display_errors') == '1') echo $errorMsg;
					break;
			}
			error_log("PHP {$arErrorInfo[0]}:  $errorString in $errorFile on line $errorLine");
			if (!ENV_DEVELOPMENT) {
				Feedback\Stats::increment('error.php.' . strtolower($arErrorInfo[0]));
			}
		}
		return TRUE;
    }
}
