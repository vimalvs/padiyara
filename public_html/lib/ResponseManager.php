<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * ResponseManager Class
 * 
 * HTTP Response Handler
 * @package		Utilities
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2011-05-18 14:43:00 +0530 (Wed, 18 May 2011	) $
 */

class ResponseManager {
	const DB_RESOURCE_NOT_FOUND = 1;
	public static $message = NULL;
	
	public function getMessage() {
		if (is_null(self::$message)) {
			self::$message = new \Message;
		}
		return self::$message;
	}
	
	public static function error($status = 503, $options = array(), $cms = false) {
		if ($status === ResponseManager::DB_RESOURCE_NOT_FOUND) {
			$status = DB_DEFINED ? 404 : 503;
		}
		if ($status === 404 && $cms) {
			include ROOT . '/res/cms/cms.php';
		}
		if (!in_array($status, array(404, 503))) $status = 404;
		include ROOT . "/$status.php";
		exit;
	}
	
	
	public static function redirect($url, $params = array(), $session = FALSE, $status = 301, $method = 'GET') {
		if ($method === 'GET') {
			$url = qsAddParam($url, $params);
			header('Location: ' . $url, TRUE, $status);
		} else {
			$tpl = new PHPTemplate;
			$tpl->render(TEMPLATE_PATH . '/http-redirect.tpl.php', array('action' => $url, 'params' => $params));
		}
		exit;
	}
	
	public static function setHeaderByCode($status) {
		if ($status === ResponseManager::DB_RESOURCE_NOT_FOUND) {
			$status = DB_DEFINED ? 404 : 503;
		}
		
		if ($status == 404) {
			header('HTTP/1.1 404 Not Found');
		} elseif ($status == 503) {
			header('HTTP/1.1 503 Service Temporarily Unavailable');
		} elseif ($status == 200) {
			header('HTTP/1.1 200 OK');
		}
	}
	
	/**
	 * Sends HTTP headers for requesting client side caching of a page
	 *
	 * @param	boolean	$cache Specifies whether the page has to be cached or not
	 * @param	integer $maxAge Time in seconds to save the cache
	 * @return	void
	 */
	function cache($maxAge = 3600) {
		if ($maxAge > 0) {
			header("Cache-Control: max-age=$maxAge");
			header('Expires: ' . gmdate('D, d M Y h:i:s T', TIME_NOW + $maxAge));
		} else {
			header('Cache-Control: no-cache');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		}
	}
}

//HttpResponse::initialize();
