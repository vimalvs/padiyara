<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

use \Log\Formatter\LogstashFormatter;
use \Monolog\Handler\RedisHandler;
use \Monolog\Handler\StreamHandler;
use \Monolog\Logger;

/**
 * SiteManager
 * 
 * Provides functions for handling current request
 * @package		Utilities
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		1.0.0
 * @date		$Date: 2010-07-02 17:47:00 +0530 (Fri, 02 Jul 2010	) $
 */

final class SiteManager {
	
	private static $cache = null;
	private static $db = null;
	private static $mongo = null;
	private static $logger = [];
	private static $tpl = null;
	private static $hooks = array();
	private static $filters = array();
	private static $user = array();
	private static $arAllowedOutputFormat = array('html');
	private static $arNameSpacePath = array();
	private static $arClassAlias = array();
	private static $configTable = 'config';
	
	public static function initialize() {
		static $inited = false;
		
		if ($inited) return;
		// Class Auto Loader
		spl_autoload_register(array(__CLASS__, '_autoload'));
		self::getCache();
		
		// RequestManager::initialize($baseUrl, $mobileBaseUrl);
		// self::addNamespacePath('', ROOT . '/lib');

		include ROOT . '/lib/ErrorHandler.php';
		\ErrorHandler::initialize();
		
		if (defined('REQUIRE_DB')) {
			include ROOT . '/lib/PDODatabase.php';
			$GLOBALS['pdbo'] = self::getDatabase();
			if (!$GLOBALS['pdbo'] && defined('DB_REQUIRED')) {
				FatalError('Database Error', '<p>Unfortunately, we are unable to connect to our database server at the moment. The <em>Webmaster</em> have been automatically notified about this error. </p><p>We are extremely sorry for the inconvenience caused, and promise to be back as soon as possible.</p>', 'Internal Error');
			}
		}
		
		register_shutdown_function(function() {
			\SiteManager::invokeHook(['shutdown', null]);
		});
		
		$inited = true;
	}
	
	public static function setConfigTable($table = 'config', $db = null) {
		self::$configTable = $db ? "`$db`.`$table`" : $table;
	}
	
	/**
	 * Retrives data from config table and caches it for later use
	 * @param string $key Key to obtain data
	 * @param integer $cacheTime Time to cache the value. Negative value will reload the value.
	 * 
	 * @return mixed Requested data
	 */
	public static function getConfig($name, $default = null) {
		if (!self::$cache) self::getCache();
		$data = self::$cache->get($name, 'cfg');
		if (is_null($data)) {
			try {
				if (self::$db || self::getDatabase()) {
					$stmt = self::$db->query("SELECT value FROM " . self::$configTable . " WHERE name=" . self::$db->quote($name) . " LIMIT 1");
					if ($stmt) {
						$record = $stmt->fetchColumn();
						$data = $record ? unserialize($record) : $default;
						self::$cache->set($name, $data, 3600, 'cfg');
					}
				}
			} catch (\PDOException $e) {
				// Do nothing. Returns null
			}
		}
		return $data;
	}
	
	/**
	 * Retrieves data from config table and caches it for later use
	 * @param string $key Key to obtain data
	 * @param integer $cacheTime Time to cache the value. Negative value will reload the value.
	 * 
	 * @return mixed Requested data
	 */
	public static function setConfig($name, $data, $cacheTime = null, $cacheGroup = 'cfg') {
		if (!self::$cache) self::getCache();
		if (!self::$db) self::getDatabase();
		if (self::$db) {
			$name_quoted = self::$db->quote($name);
			if (is_null($data)) {
				$status = self::$db->exec("DELETE FROM " . self::$configTable . " WHERE name=$name_quoted LIMIT 1");
			} else {
				$value = self::$db->quote(serialize($data));
				$status = self::$db->exec("REPLACE INTO " . self::$configTable . " (name, value) VALUES ($name_quoted, $value)");
			}

			if ($status !== false) {
				$status = self::$cache->set($name, $data, $cacheTime, $cacheGroup);
				return true;
			}
		}
		return false;
	}
	
	public static function shutdown() {
		
	}
	
	public static function getJSONPCallBack() {
		static $jsonpCallback = null;
		if (is_null($jsonpCallback)) {
			$jsonpCallback = !empty($_POST['jsonp_callback']) ? $_POST['jsonp_callback'] : (!empty($_GET['jsonp_callback']) ? $_GET['jsonp_callback'] : null);
		}
		return $jsonpCallback;
	}
	
	public static function addOutputFormat($ar, $clear = null) {
		if (!empty($ar)) {
			if (!is_array($ar)) $ar = array($ar);
			self::$arAllowedOutputFormat = $clear ? $ar : array_merge(self::$arAllowedOutputFormat, $ar);
		}
	}
	
	public static function getOutputFormat() {
		if (\RequestManager::isAjax()) {
			if (!empty($_SERVER['HTTP_ACCEPT']) && (strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'json') !== false) && in_array('json', self::$arAllowedOutputFormat)) {
				return 'json';
			} elseif (in_array('jsonp', self::$arAllowedOutputFormat) && !is_null(self::getJSONPCallBack())) {
				return 'jsonp';
			}
		}
		return 'html';
	}
	
	public static function getLogger($name = 'app_log', $arHandler = null) {
		if (!isset(self::$logger[$name])) {
			self::$logger[$name] = new Logger("{$name}_php");
			if (is_null($arHandler)) {
				$arHandler = ['file' => Logger::DEBUG, 'logstash' => Logger::DEBUG];
			}
			
			if (isset($arHandler['file'])) {
				self::$logger[$name]->pushHandler(new StreamHandler(HOME."/log/applog/" . SITE_CODE . "_$name.log", $arHandler['file']));
			}
			if (isset($arHandler['logstash'])) {
				try {
					// Prevent error, if Redis extension is not available
					// $redis = new \Redis();
					// $status = $redis->connect('127.0.0.1', 6379, 0.2);
					// if ($status) {
					// 	$redisHandler = new RedisHandler($redis, 'logstash', $arHandler['logstash']);
					// 	$formatter = new LogstashFormatter('app_logs', $_SERVER['HTTP_HOST']);
					// 	$redisHandler->setFormatter($formatter);
				
					// 	// Create a Logger instance with the RedisHandler
					// 	self::$logger[$name]->pushHandler($redisHandler);
					// } else {
					// 	trigger_error("Failed to connect to Redis server for logging", E_USER_WARNING);
					// }
				} catch (\Exception $e) {
					// Do nothing
					trigger_error("Failed to connect to Redis server for logging", E_USER_WARNING);
				} 
			}

		}
		return self::$logger[$name];
	}
	
	public static function getCache() {
		if (!self::$cache) {
			self::$cache = Cache::getInstance();
		}
		return self::$cache;
	}
	
	public static function getDatabase($dbms = 'mysql') {
		$dbms = strtolower($dbms);
		if ($dbms === 'mysql') {
			if (is_null(self::$db)) {
				try {
				    self::$db = new \PDODatabase(DB_TYPE . ':host=' . DB_HOST . ';port=' . DB_PORT  . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
					if (self::$db) {
						define('DB_DEFINED', 1);
						self::$db->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
						self::$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
						self::$db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
						self::$db->enableCaching();
					}
				} catch(\PDOException $e) {
					report('DB Error', "MySQL Connection via PDO failed.<br/><pre>{$e->getMessage()}</pre>");
					self::$db = false;
					if (ENV_DEVELOPMENT) {
						throw $e;
					}
				}
			}
			return self::$db;
		} elseif ($dbms === 'mongodb') {
			if (is_null(self::$mongo)) {
				try {
				   	self::$mongo = new \MongoDB\Client('mongodb://localhost:27017');
				} catch(\MongoConnectionException $e) {
				    report('DB Error', "MongoDB Connection PDO failed.<br/><pre>{$e->getMessage()}</pre>");
					self::$mongo = false;
					if (ENV_DEVELOPMENT) {
						throw $e;
					}
				}
			}
			return self::$mongo;
		}

	}
	
	public static function getTemplate() {
		if (!self::$tpl) {
			try {
				self::$tpl = \Theme::getInstance()->getTemplate();
			} catch (\Theme\Exception\ThemeNotFoundException $e) {
				self::$tpl = new \PHPTemplate;
			}
		}
		return self::$tpl;
	}
	
	public static function registerHook($hook, $callback, $multiTrigger = false, $priority = 1, $persistent = false) {
		list($hook_name, $hook_target) = is_array($hook) ? $hook : [$hook, null];
		$hookInfo = [$callback, $multiTrigger, $hook_target];
		
		if ($persistent) {
			// This is a persistent hook. Save to database
			if (is_object($hook_target)) {
				throw new \RuntimeException("Hook target cannot be an object for persistent hooks");
			}
			$arHooks = \SiteManager::getConfig("hook_$hook_name", [[], []]);
			if (!in_array($hookInfo, $arHooks[$priority])) {
				$arHooks[$priority][] = $hookInfo;
				ksort($arHooks[$priority]);
				\SiteManager::setConfig("hook_$hook_name", $arHooks);
			} else {
				trigger_error("Callback already registered for the hook '$hook_name'", E_USER_WARNING);
			}
		}
		
		if (!isset(self::$hooks[$hook_name])) {
			self::$hooks[$hook_name] = \SiteManager::getConfig("hook_$hook_name", [[], []]) ?: [];
		}
		
		if (!isset(self::$hooks[$hook_name][$priority])) {
			self::$hooks[$hook_name][$priority] = [$hookInfo];
			ksort(self::$hooks[$hook_name]);
		} else {
			self::$hooks[$hook_name][$priority][] = $hookInfo;
		}
	}
	
	public static function deregisterHook($hook, $callback, $multiTrigger = false, $persistent = false) {
		list($hook_name, $hook_target) = is_array($hook) ? $hook : [$hook, null];
		
		if (!isset(self::$hooks[$hook_name])) {
			self::$hooks[$hook_name] = \SiteManager::getConfig("hook_$hook_name", [[], []]) ?: [];
		}
		
		$found = false;
		foreach (self::$hooks[$hook_name] as $priority => $hooks) { 
			foreach ($hooks as $idx => $val) {
				if ([$callback, $multiTrigger, $hook_target] === $val) {
					// printr(array($callback, $multiTrigger, $hook_target), $val, array($callback, $multiTrigger, $hook_target) === $val);
					$found = true;
					unset(self::$hooks[$hook_name][$priority][$idx]);
				}
			}
		}
		if ($persistent && $found) {
			// Update database for persistent hooks
			$arHooks = \SiteManager::getConfig("hook_$hook_name", [[], []]);
			foreach ($arHooks as $priority => $hooks) { 
				foreach ($hooks as $idx => $val) {
					$a = [$callback, $multiTrigger, $hook_target];
					if ([$callback, $multiTrigger, $hook_target] === $val) {
						unset($arHooks[$priority][$idx]);
					}
				}
			}
			\SiteManager::setConfig("hook_$hook_name", $arHooks ?: null);
		}
	}

	public static function invokeHook($hook, $param = [], $persistent = false) {
		list($hook_name, $hook_target) = is_array($hook) ? $hook : [$hook, null];

		if (!isset(self::$hooks[$hook_name])) {
			self::$hooks[$hook_name] = \SiteManager::getConfig("hook_$hook_name", [[], []]) ?: [];
		}

		if (!empty(self::$hooks[$hook_name])) {
			// printr($hook_name);
			foreach (self::$hooks[$hook_name] as $priority => $hooks) {
				// Foreach copies arrays before traversal. Hence self::$hooks[$hook_name][$priority] is used instead of $hooks 
				// to detect and load new low priority hooks added by higher priority hooks
				foreach (self::$hooks[$hook_name][$priority] as $idx => $func) {
					if (!is_null($func[2]) && $func[2] !== $hook_target) {
						// Bound target different from invoked target
						continue;
					}
					if (is_callable($func[0])) {
						call_user_func_array($func[0], array_slice(func_get_args(), 1));
						if ($func[1] && !$persistent) {
							// Remove single use non-persistent hooks
							unset(self::$hooks[$hook_name][$priority][$idx]);
						}
					} else {
						$func_name = '';
						if (is_array($func[0])) {
							$func_name = is_object($func[0][0]) ? (get_class($func[0][0]) . "::{$func[0][1]}") : implode('::', $func[0]);
						} else {
							$func_name = $func[0];
						}
						trigger_error("Failed to invoke " . ($persistent ? 'persistent ' : '') . "hook $hook_name. '$func_name' does not exist.", E_USER_WARNING);
					}
				}
			}
		}
	}
	
	public static function registerFilter($filter, $callback) {
		list($filter_name, $filter_target) = is_array($filter) ? $filter : [$filter, null];
		if (!isset(self::$hooks[$filter_name])) self::$hooks[$filter_name] = [];
		self::$filters[$filter_name][] = array($callback, false, $filter_target);
	}
	
	public static function invokeFilter($filter, $content) {
		list($filter_name, $filter_target) = is_array($filter) ? $filter : [$filter, null];
		if (isset(self::$filters[$filter_name])) {
			$orig = $content;
			foreach (self::$filters[$filter_name] as $idx => $func) {
				if (!is_null($func[2]) && $func[2] !== $filter_target) {
					// Bound target different from invoked target
					continue;
				}
				if (is_callable($func[0])){
					$content = call_user_func($func[0], $content, $orig);
					if ($func[1]) {
						unset(self::$filters[$filter_name][$idx]);
					}
				} else {
					$func_name = '';
					if (is_array($func[0])) {
						$func_name = is_object($func[0][0]) ? (get_class($func[0][0]) . "::{$func[0][1]}") : implode('::', $func[0]);
					} else {
						$func_name = $func[0];
					}
					trigger_error("Failed to invoke hook. '$func_name' does not exist.", E_USER_WARNING);
				}
			}
		}
		return $content;
	}
	
	public static function addNamespacePath($ns, $path, $prefix = '', $suffix = '.php') {
		self::$arNameSpacePath[strtolower(trim($ns, ' \\'))] = array(rtrim($path, '/'), $prefix, $suffix);
	}

	public static function addClassAlias($original, $alias) {
		self::$arClassAlias[$alias] = $original;
	}
	
	private static function _autoload($class) {
		if ($class === 'Cache') {
			include ROOT . '/lib/Cache.php';
			return;
		} else {
			if (isset(self::$arClassAlias[$class])) $class = self::$arClassAlias[$class];
			
		    $baseNS = ($baseNssPos = strpos($class, '\\')) ? substr($class, 0, $baseNssPos) : '';
			// $cache = self::getCache();
			$arClassFile = self::$cache->get("NS_\\$baseNS", 'Class_AutoLoader');
			if (empty($arClassFile[$class])) {
				$ns = '';
				$className = $class;
				$pos = $len = strlen($class);
				$i = 0;
				$nsInfo = false;
				while ($pos = strrpos($class, '\\', $pos - $len - 1)) {
					$className = substr($class, $pos + 1);
					$ns = strtolower(substr($class, 0, $pos));
					if (isset(self::$arNameSpacePath[$ns])) {
						// $className = strtr($className, '\\', '/');
						$nsInfo = self::$arNameSpacePath[$ns];
						break;
					}
				}
				$arIncludePath = array();
				if ($nsInfo) {
				    $filePath = '';
					if ($pos = strrpos($className, '\\')) {
    					$ns = substr($className, 0, $pos);
    					$className = substr($className, $pos + 1);
    					$filePath = strtr($ns, '\\', '/') . '/';
					}
					$fileName = $nsInfo[1] . str_replace('_', '/', $className) . $nsInfo[2];
					// PSR-0 Format Section_Name/Class_Name => Section_Name/Class_Name.php
   				    $arIncludePath[] = $nsInfo[0] . '/' . $filePath . $fileName;
					// Custom Section Class Section_Name/Class_Name => section-name/Class/Name.php
					$arIncludePath[] = $nsInfo[0] . '/' .  strtolower(strtr($filePath, '_', '-')) . 'lib/' . $fileName;
				} else {
				    $filePath = '';
					if ($pos = strrpos($class, '\\')) {
    					$ns = substr($class, 0, $pos);
    					$className = substr($class, $pos + 1);
    					$filePath = str_replace('\\', '/', $ns) . '/';
					}
                    // $file .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
					$fileName = str_replace('_', '/', $className) . '.php';
					// Custom Section Class Section_Name/Class_Name => section-name/Class/Name.php
                    $arIncludePath[] = ROOT . '/' . strtolower(strtr($filePath, '_', '-')) . 'lib/' . $fileName;
   					// PSR-0 Format Section_Name/Class_Name => Section_Name/Class_Name.php
   				    $arIncludePath[] = ROOT . '/lib/' . $filePath . $fileName;
					if ($baseNssPos) {
						$arIncludePath[] = ROOT . '/' . strtolower(strtr($baseNS, '_', '-')) . '/lib' . strtr(substr($filePath, $baseNssPos), '_', '-') . $fileName;
					}
					$tmp = explode('\\', $class);
					$path = ROOT;
					while ($ns = array_shift($tmp)) {
						if ($tmp) {
							$dir = strtolower(strtr($ns, '_', '-'));
							if (is_dir("$path/$dir")) {
								$path .= "/$dir";
							} else {
								$arIncludePath[] = $path . "/lib/$ns/" . implode('/', $tmp) . '.php';
								break;
							}
						}
					}
				}
				foreach ($arIncludePath as $file) {
					if (file_exists($file)) {
						include $file;
						break;
					}
				}
				
				$autoLoaded = class_exists($class, false) || interface_exists($class, false) || trait_exists($class, false);
				if (!$autoLoaded) {
					// Try to autoload from Composer
					$composer = require ROOT . '/../composer/autoload.php';
					if ($composer) {
						$composer->unregister();
						$file = $composer->findFile($class);
						if ($file) {
							include $file;
							$autoLoaded = class_exists($class, false) || interface_exists($class, false);
						}
					}
				}
				
				if ($autoLoaded) {
					// Cache the result
					$arClassFile = self::$cache->get("NS_\\$baseNS", 'Class_AutoLoader');
					$arClassFile[$class] = $file;
					self::$cache->set("NS_\\$baseNS", $arClassFile, 3600, 'Class_AutoLoader');
				}
			} else {
				include $arClassFile[$class];
			}
		}
	}
	
	public static function assert($condition, $action, $extra = null) {
		if (is_callable($condition)) $condition = call_user_func($condition);
		if (!$condition) {
			$exit = true;
			if (is_string($action)) {
				$redirect = \RequestManager::getStateUrl();
				if (strpos($action, '://') !== false) {
					\ResponseManager::redirect($action, array('redirect' => $redirect), null, 302);
				} else {
					$tpl = \Theme\Theme::getInstance()->getTemplate();
					$data = (array)$extra;
					$data['generator'] = 'assert';
					$data['redirect'] = $redirect;
					$tpl->addData($data)->render($action);
				}
			} else {
				$exit = (true !== call_user_func($action, $extra));
			}
			if ($exit) exit;
		}
	}

}
