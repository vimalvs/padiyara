<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * PHPTemplate Class
 * 
 * This class implements a basic Templating Engine
 * @package		Utilities
 * @author 		Joyce Babu <joyce@ennexa.com>
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.4
 */

const T_DATE = 2;
const T_TIME = 3;
const T_TEMPLATE = 4;
const T_INDEX = 5;
const T_HTML = 6;
const T_CHECKBOX = 7;

const JS_BACK = 'javascript:history.back();';
define('ERROR_TPL', ROOT . '/templates/error.tpl.php');

class PHPTemplate {
	
	const OUTPUT_NORMAL = 1;
	const OUTPUT_STRIP_WHITESPACE = 2;
	const PRINT_MESSAGE_ALL = 1;
	const PRINT_MESSAGE_GENERAL = 2;

	public $data = array();
	public $user = null;
	public $head = array();
	protected $options = array();
	private $tplFile = null;
	private $tplPath = null;
	private $arIncludeFile = array();
	private $arHelper = [];
	private $counter = 0;
	
	// private $compactOutput = false;
	private $inlineStyle = '';
	
	private function _getTemplateFile($tplFile) {
		if ($tplFile[0] === '/' || $tplFile[0] === '.' || $tplFile[1] === ':') {
			return $tplFile;
		} else {
			$tmp = explode('::', $tplFile);
			if (isset($tmp[1])) {
				if (isset($this->tplPath[$tmp[0]])) {
					return $this->tplPath[$tmp[0]] . '/' . $tmp[1] . '.tpl.php';
				} else {
					throw new \Exception("Template group <i>$tmp[0]</i> not found.");
				}
			} else {
				return $this->tplPath['default'] . '/' . $tplFile . '.tpl.php';
			}
		}
	}

	public function __construct($tpl = null) {
		$this->messages = new ArrayObject(array(), ArrayObject::STD_PROP_LIST);
		$this->messages->status = null;
		$this->messages->type = '';
		$this->messages->message = array();
		$this->tplFile = $tpl;
		$this->setTemplatePath(TEMPLATE_PATH, 'base', true);
		$this->options = array('output' => self::OUTPUT_NORMAL, 'print_message' => self::PRINT_MESSAGE_ALL);
	}
	
	public function __get($key) {
		return isset($this->data[$key]) ? $this->data[$key] : null;
	}
	
	public function __set($key, $value) {
		$this->data[$key] = $value;
	}
	
	public function getNextCounter() {
		return ++$this->counter;
	}
	
	public function setOption($option, $value) {
		if (isset($this->options[$option])) {
			$this->options[$option] = $value;
			return true;
		}
		trigger_error("Invalid option '$option'.", E_USER_WARNING);
		return false;
	}
	
	public function getOption($option) {
		if (isset($this->options[$option])) {
			return $this->options[$option];
		}
		trigger_error("Invalid option '$option'.", E_USER_WARNING);
		return false;
	}

	public function setTemplate($tpl, $tplPath = null) {
		if (!is_null($tplPath)) {
			$this->setTemplatePath($tplPath);
		}
		$this->tplFile = $this->_getTemplateFile($tpl);
	}
	public function setTemplatePath($tpl, $group = 'default', $default = false) {
		if ($default) {
			$this->tplPath['default'] = $tpl;
		}
		$this->tplPath[$group] = $tpl;
	}
	
	public function setShareInfo($share_key, array $shareInfo = []) {
		// if (is_null($share_key) && defined('SHARE_KEY')) $share_key = SHARE_KEY;
		
		if ($share_key) {
			$share = \Share\Image\Image::loadByPathname($share_key);
			if ($share) {
				$shareInfo += $share->getData();
			} else {
				trigger_error("[Share Image] Failed to load '$share_key'", E_USER_WARNING);
				return;
			}
		}
		if (!empty($shareInfo)) {
			if (empty($shareInfo['share_title'])) {
				trigger_error('[Share Image] Share title cannot be empty');
			}
			if (empty($shareInfo['share_uri'])) {
				$shareInfo['share_uri'] = defined('SHARE_PATH') ? 'SHARE_PATH' : \RequestManager::getShareUri();
			}
			$this->data['__shareInfo'] = $shareInfo + ['share_type' => 'article'];
		}
	}
	
	public function printShareHeader() {
		if (defined('SHARE_KEY')) {
			$this->setShareInfo(SHARE_KEY);
		}
		if (empty($this->data['__shareInfo'])) return;
		
		$data = $this->data['__shareInfo'];
		if (!defined('SHARE_PATH')) define('SHARE_PATH', $data['share_uri']);
		if (empty($data['share_image'])) $data['share_image'] = "http://files.prokerala.com/res/share/{$data['share_pathname']}.jpg";
		elseif ($data['share_image'][0] === '/') $data['share_image'] = "http://files.prokerala.com{$data['share_image']}";
		?>
		<meta property="og:type" content="<?=$data['share_type'] ?: 'article'?>" />
		<meta property="og:title" content="<?=htmlspecialchars($data['share_title'], null, null, false)?>" />
		<?php if (!empty($data['share_description'])):?><meta property="og:description" content="<?=htmlspecialchars($data['share_description'], null, null, false)?>" /><?php endif;?> 
		<meta property="og:url" content="<?=\RequestManager::getBaseUrl(), htmlspecialchars(SHARE_PATH)?>" />
		<meta property="og:image" content="<?=$data['share_image']?>" />
		<?php
	}
	
	public function includeFile($tpl) {
		$this->arIncludeFile[] = $this->_getTemplateFile($tpl);
	}
	
	public function addScript($script) {
		if (is_array($script) || ($script[0] == '/' && $script[1] != '*') || (strpos($script, 'http') === 0)) {
			if (!is_string($script) && isset($script[0])) {
				foreach ($script as $s)$this->addScript($s);
			} else {
				$script = is_array($script) ? $script['src'] : $script;
				$this->head['script'][] = array('src' => $script);
			}
		} else {
			$this->head['script'][] = $script;
		}
	}

	public function addStyle($style) {
		if (is_array($style) || ($style[0] == '/' && $style[1] != '*') || (strpos($style, 'http') === 0)) {
			if (!is_string($style) && isset($style[0])) {
				foreach ($style as $s)$this->addStyle($s);
			} else {
				$style = is_array($style) ? $style['href'] : $style;
				$this->addLink($style, 'stylesheet', 'text/css');
			}
		} else {
			$this->inlineStyle .= "$style\n\n";
		}
	}

	public function addLink($href, $rel, $type) {
		$this->head['link'][] = array('href' => $href, 'type' => $type, 'rel' => $rel);
	}

	public function addMeta($name, $content = null) {
		if (is_array($name)) {
			foreach ($name as $n => $c) $this->head['meta'][] = array('name' => $n, 'content' => $c);
		} else {
			$this->head['meta'][] = array('name' => $name, 'content' => $content);
		}
	}
	public function addMessageData($title = null, $message, $arBtn = array(), $type = 'error', $status = null, $class = null) {
		if (!is_null($title)) $this->messages->title = $title;
		if (!is_null($class)) $this->messages->class = $class;
		if (!is_null($status)) $this->messages->status = $status;
		$this->messages->type = $type;
		if ($message) $this->messages->message[] = array($type => $message);
		if (is_array($arBtn)) {
			if (is_assoc_array($arBtn)) {
				foreach ($arBtn as $label => $href) {
					$this->messages->button[] = array('href' => $href, 'value' => $label);
				}
			} else {
				if (!is_array($arBtn[0]))$arBtn = array($arBtn);
				foreach ($arBtn as $btn) {
					$ar = array('href' => $btn[1], 'value' => $btn[0]);
					if (isset($btn[2]))$ar['name'] = $btn[2];
					$this->messages->button[] = $ar;
				}
			}
		}
	}

	public function addMessageButton($btn) {
		$ar = array('href' => $btn[1], 'value' => $btn[0]);
		if (isset($btn[2]))$ar['name'] = $btn[2];
		$this->messages->button[] = $ar;
	}

	public function addMessages($messages, $type = 'error') {
		$this->messages->type = $type;
		if ($type === 'error') {
			$this->messages->message[] = array('error' => 'Please correct the errors below to continue');
		}
		if (is_string($messages)) {
			$this->messages->message[] = array($type => $messages);
		} else {
			foreach ($messages as $k => $v) {
				if (is_numeric($k)) $this->messages->message[] = array($type => $v);
				else $this->messages->message[$k] = array($type => $v);
			}
		}
	
	}

	public function addMessage($message, $type = 'error') {
		$this->messages->type = $type;
		$this->messages->message[] = array($type => $message);
	}
	
	public function addNamedMessage($field, $message, $type = 'error') {
		$this->messages->type = $type;
		$this->messages->message[$field] = array($type => $message);
	}
	
	public function hasError($field = null) {
		if (is_null($field)) {
			return ($this->messages->type === 'error');
		} else {
			return !empty($this->messages->message[$field]['error']);
		}
	}

	public function showError($title, $description, $tplFile = null, $header = null) {
		if ($header) ResponseManager::setHeaderByCode($header);
		if ($tplFile) $this->setTemplate($tplFile);
		$this->showMessage($title, $description, JS_BACK);
		$this->generate();
		exit;
	}
	
	public function showMessage($title = '&nbsp;', $message, $arBtn = null, $type = 'error', $status = 'exit') {
		$this->messages->type = $type;
		if (is_null($arBtn)) {
			$this->messages->title = $title;
			if ($message) $this->messages->message[] = array($type => $message);
		} else {
			if (is_string($arBtn))$arBtn = array('Back', $arBtn);
 			$this->addMessageData($title, $message, $arBtn, $type);
		}
		if ($status)$this->messages->status = $status;
	}

	public function addQuestion($data, $message = '&nbsp;', $arBtn = array()) {
		if (is_array($data)) extract($data);
		else $title = $data;
		if (empty($title)) $title = '&nbsp;';
		$this->addMessageData($title, $message, $arBtn, 'info', 'exit');
	}

	public function addData($data, $value = null) {
		if (is_array($data)) {
			foreach ($data as $key => $value)$this->data[$key] = $value;
		} else {
			$this->data[$data] = $value;
		}
		return $this;
	}
	
	public function generateJSON() {
		header('Content-Type: application/json');
		echo json_encode($this->data);
	}
	
	public function generateJSONP($callback = null) {
		header('Content-Type: application/javascript');
		if (is_null($callback)) $callback = $_GET['jsonp_callback'];
		echo $callback . '(' . json_encode($this->data) . ')';
	}
	
	public function generate($ar = array()) {
		if (($outputFormat = SiteManager::getOutputFormat()) === 'html') {
			SiteManager::invokeHook(['template_before_generation', $this]);
			// $compact = ($this->options['output'] === self::OUTPUT_STRIP_WHITESPACE) && empty($_GET['compact']);
			extract($this->data, EXTR_REFS | EXTR_SKIP);
			// if ($compact) ob_start();
			// foreach ($this->arIncludeFile as $includeFile) {
			// 	include $includeFile;
			// }
			$status = include $this->tplFile;
			if ($status === false && !file_exists($this->tplFile)) {
				throw new \Exception("Template File <i>$this->tplFile</i> not found.");
			}
			SiteManager::invokeHook(['template_after_generation', $this]);
			// if ($compact) {
			// 	echo preg_replace(array('/(\s+)?\n+(\s+)?/', '/[ \t\r]+/'), array("\n", ' '), ob_get_clean());
			// }
		} elseif ($outputFormat === 'json') {
			$this->generateJSON();
		} elseif ($outputFormat === 'jsonp') {
			$this->generateJSONP(SiteManager::getJSONPCallBack());
		}
	}
	
	public function render($tplFile, $var = array(), $return = false) {
		$tplFile = $this->_getTemplateFile($tplFile);
		if (is_array($var)) {
			extract($var, EXTR_REFS);
		}
		extract($this->data, EXTR_REFS | EXTR_SKIP);
		if ($return === true) {
			ob_start();
			$status = include $tplFile;
			$return = ob_get_clean();
		} else {
			$status = include $tplFile;
		}
		if ($status === false && !file_exists($tplFile)) {
			throw new \Exception("Template File <i>$tplFile</i> not found.");
		}
		if ($return !== false) return $return;
	}
	
	public function addHelpers($tplFile, $namespace = null) {
		if (is_null($namespace)) {
			// $tmp = explode('::', $tplFile);
			// if (count($tmp) === 2) {
			// 	$namespace = $tmp[2];
			// } else {
			// 	throw new \InvalidArgumentException("Namespace not specified for helper - $tplFile");
			// }
			// $namespace = $tplFile;
		}
		$tplFile = $this->_getTemplateFile($tplFile);
		$arHelper = include $tplFile;
		if (is_array($arHelper)) {
			$this->arHelper = array_merge($this->arHelper, $arHelper);
		} else {
			throw new \InvalidArgumentException("Invalid helper - $tplFile");
		}
		$this->arHelper = array_merge($this->arHelper, $arHelper);
		// printr($status);
		//
		// if ($status === false && !file_exists($tplFile)) {
		// 	FatalError('A fatal error was encountered.', "Template Helper <i>$tplFile</i> not found.", 'Invalid Template');
		// }
	}
	
	public function getHelper($helper) {
		if (isset($this->arHelper[$helper])) {
			return $this->arHelper[$helper];
		} else {
			trigger_error('[PHPTemplate] Invalid helper - ' . $help);
		}
		return null;
	}
	
	public function help($helper) {
		if (isset($this->arHelper[$helper])) {
			// printr(func_get_args(), array_slice(func_get_args(), 1));
			return call_user_func_array($this->arHelper[$helper], array_slice(func_get_args(), 1));
		} else {
			trigger_error('[PHPTemplate] Invalid helper - ' . $helper);
		}
		return null;
	}
	
	public function printHeader() {
		if (empty($this->head)) {
			return;
		}
		$head = $this->head;
		//header('Content-Type: text/html, charset=utf-8');
		/** /
		echo "<?xml version='1.0' encoding='utf-8'?>\n";
		/**/
		if (!empty($head['title'])) echo '<title>', $head['title'], "</title>\n";
		if (isset($head['meta'])) {
			foreach ($head['meta'] as $config_name => $config_content) {
				if (!empty($config_content)) echo '<meta name="', $config_name, '" content="', $config_content, "\" />\n";
			}
		}
		if (isset($head['link']) && is_array($head['link'])) {
			foreach ($head['link'] as $link) {
				echo '<link ';
				foreach ($link as $k => $v)echo $k, '="', $v, '" ';
				echo "/>\n";
			}
		}
		$tmpStyle = '';
		if (isset($head['style'])) {
			foreach ($head['style'] as $style) {
				$s = substr($style, 0, 2);
				if (($s[0] == '/' && $s[1] != '*' && $s[1] != '/') || $s == './') {
					echo "<link type='text/css' rel='stylesheet' href='", $style, "' />\n";
				} else {
					$tmpStyle .= "$style\n";
				}
			}
		}
		if (!empty($this->inlineStyle)) $tmpStyle .= "{$this->inlineStyle}";
		if (!empty($tmpStyle)) echo "<style type=\"text/css\">\n", trim($tmpStyle), "\n</style>\n";
		if (isset($head['script']) && is_array($head['script'])) {
			foreach ($head['script'] as $script) {
				$s = substr($script, 0, 2);
				if (($s[0] == '/' && $s[1] != '*' && $s[1] != '/') || $s == './' || (substr($script, 0, 4) === 'http')) {
					echo "<script type='text/javascript' src='", $script, "'></script>\n";
				} else {
					echo "<script type='text/javascript'>\n", $script, "\n</script>\n";
				}
			}
		}
	}
	
	public function printFooter() {
		
	}

	public function printMessage($fields = []) {
		if (!empty($this->messages->message)) {
			$messages = array();
			$bPrintGeneral = ($this->options['print_message'] === self::PRINT_MESSAGE_GENERAL);
			if ($bPrintGeneral) {
				$flag = empty($fields);
				foreach ($this->messages->message as $idx => $message) {
					if (is_int($idx) || !($flag || in_array($fields))) $messages[] = $message;
				}
			} else {
				$messages = $this->messages->message;
			}
		} else {
			return ;
		}
		if (empty($this->messages->title) && empty($this->messages->button) && count($messages) < 5 && $bPrintGeneral):?>
			<?php foreach ($messages as $key => $message):?>
				<div class="alert alert-<?=key($message)?>"><?=current($message)?></div>
			<?php endforeach;?>
		<?php elseif (!empty($messages)):?>
			<div class="msgBox <?=$this->messages->type?>Msg alert alert-block alert-<?=$this->messages->type?>">
			<?php if (!empty($this->messages->title)):?>
				<h4 class='msgTitle alert-heading'><span><?=$this->messages->title?></span></h4>
			<?php endif;?>
				<div class="msgContent">
				<?php if (count($messages) > 1):?>
					<ul class="msgWrapper">
					<?php foreach ($messages as $key => $message):?>
						<li class="<?=key($message)?>"><?=current($message)?></li>
					<?php endforeach;?>
					</ul>
				<?php else:?>
					<div class="msgWrapper"><p><?=current(reset($messages))?></p></div>
				<?php endif;?>
				</div>
			<?php if (!empty($this->messages->button)):?>
				<div class="msgButton"><span>
				<?php foreach ($this->messages->button as $button):?>
					<a class="btn" href="<?=$button['href']?>"><?=$button['value']?></a>
				<?php endforeach;?>
				</span></div>
			<?php endif;?>
			</div>
		<?php endif;
	}
	
	public function compact($bValue = true) {
		// $this->setOption('output', $bValue ? self::OUTPUT_STRIP_WHITESPACE : self::OUTPUT_NORMAL);
		//$this->compactOutput = (bool)$bValue;
	}

}
