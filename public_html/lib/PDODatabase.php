<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * 
 * Database Wrapper Class 
 * 
 * This class extends PDO database wrapper class and adds features like caching, error reporting etc
 * 
 * @author		Joyce Babu
 * @version		1.0.6
 * @date		$Date: 2011-02-10 11:57:00 +0530 (Thu, 10 Feb 2011	) $
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * 
 */

const MYSQL_QUERY_CACHING = 1;

class PDODatabase extends PDO {
	private $fetchMode = PDO::FETCH_ASSOC;
	private $arQuery = array();
	private $dsn = NULL;
	private $cache = false;
	private $cacheType = '';
    private $transactionLevel = 0;
	private $transactionNestable = false;
	private $exception = false;
	
	const ERROR_REPORT_QUERY_NONE = 0;
	const ERROR_REPORT_QUERY_LAST = 1;
	const ERROR_REPORT_QUERY_ALL = 2;
	
	public function enableNestedTransaction($status = true) {
		if ($status) {
			if (!$this->transactionNestable) {
				$this->transactionNestable = in_array($this->getAttribute(PDO::ATTR_DRIVER_NAME), array('pgsql', 'mysql'));
				if ($this->inTransaction()) $this->transactionLevel++;
			}
			return $this->transactionNestable;
		} elseif ($this->transactionLevel === 0) {
			$this->transactionNestable = false;
			return true;
		}
		return false;
	}
	
	public function beginTransaction() {
		$nested = $this->transactionNestable && $this->transactionLevel++;
		if ($nested) {
			return $this->exec("SAVEPOINT LEVEL{$this->transactionLevel}") !== FALSE;
		} else {
			return parent::beginTransaction();
		}
	}
	
	public function endTransaction($status) {
		if ($status) {
			$this->commit();
		} else {
			$this->rollBack();
		}
	}

	public function commit() {
		$level = $this->transactionLevel;
		$nested = $this->transactionNestable && --$this->transactionLevel;
		if ($nested) {
			return $this->exec("RELEASE SAVEPOINT LEVEL{$level}") !== FALSE;
		} else {
			return parent::commit();
		}
    }

	public function rollBack() {
		$level = $this->transactionLevel;
		$nested = $this->transactionNestable && --$this->transactionLevel;
		if ($nested) {
			return $this->exec("ROLLBACK TO SAVEPOINT LEVEL{$level}") !== FALSE;
		} else {
			return parent::rollback();
		}
	}
	
	// public function getDatabaseName() {
	// 	$result = preg_match('#dbname=([^;]*)(?:;|$)#', $this->dsn, $match);
	// 	if ($result) {
	// 		return $match[1];
	// 	}
	// 	return false;
	// }
	
	public function __construct($dsn, $user = NULL, $pass = NULL, $options = []) {
		$this->dsn = $dsn;
		parent::__construct($dsn, $user, $pass, $options);
		$this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}
	
	public function exec($statement) {
		if ($statement) {
			$queryType = strtok($statement, ' ');
			Feedback\Stats::increment("php.pdo.{$queryType}.query", 1);
			$this->arQuery[] = $statement;
			try {
				$mysql_exec_start = microtime(true);
				$result = parent::exec($statement);
				$mysql_response_time = (microtime(true) - $mysql_exec_start) * 1000000;
				
				\Feedback\Stats::timing("php.pdo.execution_time.{$queryType}", $mysql_response_time);
				if ($result === FALSE) {
					Feedback\Stats::increment("php.pdo.{$queryType}.false", 1);
					$this->triggerError();
				}
				return $result;
			} catch (PDOException $e) {
				Feedback\Stats::increment("php.pdo.{$queryType}.error", 1);
				$this->triggerError($e);
			}
		}
		return FALSE;
	}
	
	public function query($statement, $fetchMode = PDO::FETCH_ASSOC, $objORClassnameORColNo = NULL, $ctorargs = array()) {
		if ($statement) {
			$queryType = strtok($statement, ' ');
			if ($this->cache) {
				$pos = stripos($statement, 'SELECT');
				if ($pos !== FALSE) {
					$statement = substr_replace($statement, 'SELECT SQL_CACHE', $pos, 6);
				}
				//$statement = str_replace('SELECT', 'SELECT SQL_CACHE', $statement);
			}
			assert('strtoupper(strtok($statement, " \n\t")) === strtok($statement, " \n\t")', 'SQL Keywords should be in uppercase');
			assert('!in_array(preg_replace("/[^A-Z]/", "", strtoupper(strtok($statement, " \n\t"))), ["INSERT", "UPDATE", "DELETE", "REPLACE"])', 'PDO::query should be used only for SELECT/SHOW queries. Use PDO::exec instead.');
			
			Feedback\Stats::increment("php.pdo.{$queryType}.query", 1);
			try {
				$this->arQuery[] = $statement;
				$mysql_exec_start = microtime(true);
				switch($fetchMode) {
					case PDO::FETCH_COLUMN:
					case PDO::FETCH_INTO:
						$stmt = parent::query($statement, $fetchMode, $objORClassnameORColNo);
						break;
					case PDO::FETCH_CLASS:
						$stmt = parent::query($statement, $fetchMode, $objORClassnameORColNo, $ctorargs);
						break;
					default:
						$stmt = parent::query($statement, $fetchMode);
				}
				$mysql_response_time = (microtime(true) - $mysql_exec_start) * 1000000;
				\Feedback\Stats::timing("php.pdo.execution_time.{$queryType}", $mysql_response_time);
				//$stmt = parent::query($statement, $fetchMode, $objORClassnameORColNo, $ctorargs);
				if ($stmt === FALSE) {
					Feedback\Stats::increment("php.pdo.{$queryType}.false", 1);
					$this->triggerError();
				}
				return $stmt;
			} catch (\PDOException $e) {
				Feedback\Stats::increment("php.pdo.{$queryType}.error", 1);
				$this->triggerError($e);
			}
		}
		return FALSE;
	}
	
	public function enableCaching() {
		$this->cache = TRUE;
	}

	public function setExceptionOnError($status) {
		$this->exception = $status;
	}
	
	public function disableCaching() {
		$this->cache = FALSE;
	}
	
	public function getQuery($idx = NULL) {
		if (empty($this->arQuery)) return false;
		if ($idx === -1) {
			return $this->arQuery;
		} elseif (is_null($idx)) {
			$idx = count($this->arQuery) - 1;
		}
		return $this->arQuery[$idx];
	}
	
	public function purgeHistory() {
		$this->arQuery = array();
	}
	
	public function setFetchMode($fetch_mode) {
		$this->fetchMode = $fetch_mode;
	}

	/**
	 * Returns the number of records matching $condition
	 *
	 * @param	string 	$tblName	Table to search in 
	 * @param	mixed 	$condition	Condition to filter resultset as array or string
	 * @param	mixed	$selFields	Fields to be selected from the resultset
	 * @param	mixed	$count		Index value for the count field. Rarely used.
	 * @param	mixed	$relation	Relation to be used for $condition array
	 * @return	void
	 */
	public function getCount($tblName, $conditions = FALSE, $selFields = NULL, $count = NULL, $relation = '=') {
		if ($selFields) {
			$selFieldSql = is_array($selFields) ? implode(', ', $selFields) . ',' : "$selFields,";
		} else {
			$selFieldSql = '';
		}
		$whereSql = $this->genQuery($conditions, NULL, $relation);
		if (is_null($count)) {
			$expr = '*';
			$alias = 'count';
		} elseif (is_array($count)) {
			list($expr, $alias) = $count;
		} else {
			$expr = $count;
			$alias = 'count';
		}
		$tblName = is_array($tblName) ? implode(', ', $tblName) : $tblName;
		$sql = "SELECT $selFieldSql COUNT($expr) AS $alias FROM $tblName WHERE $whereSql" . ($selFields ? " GROUP BY $selFields[0]" : '');
		$stmt = $this->query($sql);
		if ($stmt) {
			return $selFields ? $stmt : $stmt->fetchColumn(0);
		}
	}
	public function search($tblName, $conditions = FALSE, $order = FALSE, $limit = NULL, $selFields = FALSE, $group = FALSE, $relation = '=') {
		$selFieldSql = $orderBySql = $whereSql = $groupBySql = '';
		if ($selFields) {
			$selFieldSql = is_array($selFields) ? implode(', ', $selFields) : $selFields;
		} else $selFieldSql = '*';
		$whereSql = $this->genQuery($conditions, NULL, $relation);
		$orderBySql = '';
		if ($order) {
			if (is_array($order)) {
				if (is_array($order[0])) {
					$tmpArSort = array();
					foreach ($order as $sort) {
						if (!$sort[1]) $sort[1] = 'ASC';
						$tmpArSort[] = $sort[0] . ' ' . $sort[1];
					}
					$orderBySql .= 'ORDER BY ' . implode(', ', $tmpArSort) . ' ';
				} else $orderBySql .= 'ORDER BY ' . $order[0] . ' ' . $order[1] . ' ';
			} else $orderBySql .= 'ORDER BY ' . $order . ' ASC ';
		}
		if ($group) {
			if (is_array($group)) $groupBySql =  'GROUP BY ' . implode(', ', $group) . '';
			else $groupBySql = 'GROUP BY ' . $group . '';
		} else $groupBySql = '';
		if (!is_null($limit)) {
			$limitSql = is_array($limit) ? " LIMIT $limit[0], $limit[1]" : " LIMIT $limit ";
		} else {
			$limitSql = '';
		}
		$stmt = $this->query("SELECT $selFieldSql FROM $tblName WHERE $whereSql $groupBySql $orderBySql $limitSql");
		//var_dump($stmt);
		return $stmt;
	}
	public function getRecord($tblName, $conditions, $limit = NULL, $selFields = NULL, $relation = '=') {
		if ($selFields) {
			$selFieldSql = is_array($selFields) ?  implode(', ', $selFields) : $selFields;
		} else $selFieldSql = '*';
		if (is_null($limit)) $limit = 1;
		$whereSql = $this->genQuery($conditions, $limit, $relation);
		$tblName = is_array($tblName) ? implode(', ', $tblName) : $tblName;
		$stmt = $this->query("SELECT $selFieldSql FROM $tblName WHERE $whereSql");
		$lmt = is_array($limit) ? $limit[1] : $limit;
		if ($stmt && $lmt == 1) {
			return $stmt->fetch(PDO::FETCH_ASSOC);
		} else {
			return $stmt;
		}
	}
	public function quote($string, $parameter_type = PDO::PARAM_STR) {
		return is_null($string) ? ' NULL ' : parent::quote($string, $parameter_type);
	}
	#===========================================================================#
	# * Function for inserting a new Record into a Table                        #
	# * @param  : [$tblName] Name of the Table                                  #
	# * @param  : [$query  ] The query to execute                               #
	# * @return : TRUE on Success / FALSE on Failure                            #
	#===========================================================================#
	public function insertRecord($tblName, $arValues, $upsert = false) {
		if (is_assoc_array($arValues)) {
			$arValues = array($arValues);
		}
		$keys = array_keys($arValues[0]);
		$fields = '(' . implode(', ', $keys) . ')';
		foreach ($arValues as $values) {
			$arVal[] = '(' . implode(', ', array_map(array($this, 'quote'), $values)) . ")";
		}
		$values = implode(', ', $arVal);
		
		if ($upsert) {
			$keys = is_array($upsert) ? $upsert : $keys;
			$arUpdateField = [];
			foreach ($keys as $key) {
				$arUpdateField[] = "$key = VALUES($key)";
			}
			$suffix = ' ON DUPLICATE KEY UPDATE ' . implode(', ', $arUpdateField);
		} else {
			$suffix = '';
		}
		return $this->exec("INSERT INTO $tblName $fields VALUES {$values}{$suffix}");
	}
	#===========================================================================#
	# * Function for Updating a Table                                           #
	# * @param  : [$tblName] Name of the Table                                  #
	# * @param  : [$query  ] The query to execute                               #
	# * @return : TRUE on Success / FALSE on Failure                            #
	#===========================================================================#
	public function updateRecord($tblName, $conditions, $values, $limit = 1) {
		if ($values) {
			$arUpdateField = array();
			foreach ($values as $field => $value) {
				$value = $this->quote($value);
				$arUpdateField[] = "$field = $value";
			}
			$fields = implode(', ', $arUpdateField);
		}
		$whereSql = $this->genQuery($conditions, $limit);
		return $this->exec("UPDATE $tblName SET $fields WHERE $whereSql");
	}
	
	public function replaceRecord($tblName, $arValues) {
		if (is_assoc_array($arValues)) {
			$arValues = array($arValues);
		}
		$keys = array_keys($arValues[0]);
		$fields = '(' . implode(', ', $keys) . ')';
		foreach ($arValues as $values) {
			$arVal[] = '(' . implode(', ', array_map(array($this, 'quote'), $values)) . ")";
		}
		$values = implode(', ', $arVal);
		return $this->exec("REPLACE INTO $tblName $fields VALUES $values");
	}
	#===========================================================================#
	# * Function for Deleting a Record                                          #
	# * @param  : [$tblName] Name of the Table                                  #
	# * @param  : [$conditions] The condition for deleting                      #
	# * @param  : [$query  ] The query to execute                               #
	# * @return : TRUE on Success / FALSE on Failure                            #
	#===========================================================================#
	public function deleteRecord($tblName, $conditions, $limit = 1) {
		$whereSql = $this->genQuery($conditions, $limit);
		return $this->exec("DELETE FROM $tblName WHERE $whereSql");
	}
	public function getNumRows($tblName, $condition) {
		$condition = $this->genQuery($condition);
		$result = $this->query("SELECT COUNT(*) FROM $tblName WHERE $condition");
		return $result ? $result->fetchColumn() : 0;
	}
	public function getLastInsertID() {
		$result = $this->query('SELECT LAST_INSERT_ID()');
		return $result ? $result->fetchColumn() : false;
	}
	#===========================================================================#
	# * Function for generating a query from the conditions provided            #
	# * @param  : [$name] Name of the Database, Table, Field                    #
	# * @return : The backquoted name                                           #
	#===========================================================================#
	public function genQuery($conditions, $limit = NULL, $relation = '=') {
		$arCondition = array();
		$sql = '';
		if (is_array($conditions) && !empty($conditions)) {
			if (is_assoc_array($conditions)) {
				$logic = (isset($conditions[0]) && $conditions[0]) ? ' OR ' : ' AND ';
				unset($conditions[0]);
				foreach ($conditions as $field => $value) {
					if (is_array($value)) {
						if ($relation === '=') {
							$quotedValue = array_filter($value, 'is_numeric') ? $value : array_map([$this, 'quote'], $value);
							$arCondition[] = "$field IN (" . implode(', ', $quotedValue) . ")";
						} else {
							foreach ($value as $option) {
								$arOption[] = "$field $relation " . $this->quote($option);
							}
							$arCondition[] = ' ( ' . implode(' OR ', $arOption) . ' ) ';
						}
					} else {
						$arCondition[] = "$field $relation " . $this->quote($value);
					}
				}
				$sql = implode($logic, $arCondition);
			} else {
				$cnt = count($conditions);
				if (is_array($lElm = $conditions[$cnt - 1]) || !is_string($lElm)) $logic = ' AND ';
				else {
					$logic = $lElm ? ' OR ' : ' AND ';
					$cnt--;
				}
				for ($i = 0; $i < $cnt; $i++) {
					$arCondition[] = $this->genQuery($conditions[$i], NULL, $relation);
				}
				$sql = implode($logic, $arCondition);
			}
		} elseif ($conditions instanceof \Query\Condition) {
			$gen = new \Query\Generator_Sql($this);
			$sql = $gen->condition($conditions);
		} elseif ($conditions) {
			$sql = $conditions;
		}
		
		$sql = empty($sql) ? 1 : "( $sql )";
		if (!is_null($limit)) {
			if (is_array($limit)) $sql .= " LIMIT $limit[0], $limit[1]";
			else $sql .= " LIMIT $limit ";
		}
		return $sql;
	}
	public function errorMsg() {
		$errorInfo = $this->errorInfo();
		return "<b>Error : </b> [$errorInfo[1]] $errorInfo[2]";
	}
	public function getErrorReport($mode = self::ERROR_REPORT_QUERY_ALL) {
		$errorInfo = $this->errorInfo();
		$report = '';
		if (!empty($errorInfo[1])) {
			$report = "<b>Error : </b> [$errorInfo[1]] $errorInfo[2]";
		}
		if ($mode === self::ERROR_REPORT_QUERY_ALL) {
			$report .= "<br/><b>Query : </b> <pre>" . htmlspecialchars(print_r($this->arQuery, true)) . '</pre>';
		} elseif ($mode === self::ERROR_REPORT_QUERY_LAST) {
			$report .= "<br/><b>Query : </b> <pre>" . htmlspecialchars($this->getQuery()) . '</pre>';
		}
		return $report;
		
	}
	public function triggerError($e = FALSE) {
		if (function_exists('report')) {
			$arErrorReport = [
				'title' => 'Database Error', 
				'message' => ($e ? $e->getMessage() : $this->getErrorReport()), 
				'script' => PHP_SAPI === 'cli' ? __FILE__ : RequestManager::getUrl(), 
				'type' => 'alert'
			];
			\SiteManager::getLogger()->addError('Database Error', $arErrorReport);
			report($arErrorReport);
		}
		if (ENV_DEVELOPMENT || (class_exists('Auth\\Session') && Auth\Session::getInstance()->isAdmin())) {
			trigger_error($e ? "{$e->getMessage()}\n\n{$this->getQuery()}" : $this->getErrorReport(self::ERROR_REPORT_QUERY_LAST), E_USER_WARNING);
		}
		if ($e && $this->exception) {
			throw $e;
		}
	}
}
