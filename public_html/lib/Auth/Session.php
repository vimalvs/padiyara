<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Authenticator Class
 * 
 * Authenticator provides password based and identity based user authentication facility
 * 
 * @package		Access Control
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		2.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

namespace Auth;
class Session implements \ArrayAccess {
	
	protected $data = array (
		'user_id'		=> 0,
		'user_name' 	=> '',
		'user_email'	=> '',
		'user_level'	=> 0,
		'user_is_registered' => 0,
		'auth_provider' => NULL
	);
	
	protected static $instance = NULL;

	protected function __construct($user = NULL, $auth_provider = NULL) {
		if (empty($user) && !isset($_SESSION['userAuth']) && !empty($_COOKIE['usr'])) {
			parse_str($_COOKIE['usr'], $data);
			if (!empty($data['h']) && !empty($data['i'])) {
				$pdbo = \SiteManager::getDatabase();
				$session = $pdbo->getRecord('user_session', array('session_id' => $data['i'], 'session_hash' => $data['h']));
				if ($session && ($user = User::loadById($session['user_id']))) {
					$auth_provider = $session['auth_provider'];
					$pdbo->updateRecord('user_session', array('session_id' => $data['i']), array('session_time_last_seen' => date('Y-m-d H:i:s')));
				}
			}
		}
		if ($user) {
			$this->data['user_id'] = $user->id;
			$this->data['user_name'] = $user->name;
			$this->data['user_email'] = $user->email;
			$this->data['user_level'] = $user->level;
			$this->data['user_is_registered'] = $user->is_registered;
			$this->data['auth_provider'] = $auth_provider;
		}
		$GLOBALS['userAuth'] =& $this;
	}
	
	public function __get($key){
		return isset($this->data['user_' . $key]) ? $this->data['user_' . $key] : NULL;
	}
	
	public function offsetSet($offset, $value) {
        trigger_error('Cannot modify Auth\Session object using array access', E_USER_WARNING);
    }

    public function offsetExists($offset) {
        trigger_error('Cannot query Auth\Session object using array access', E_USER_WARNING);
    }

    public function offsetUnset($offset) {
        trigger_error('Cannot modify Auth\Session object using array access', E_USER_WARNING);
    }

    public function offsetGet($offset) {
		if (isset($this->data["user_$offset"])) {
			return $this->data["user_$offset"];
		} elseif (method_exists($this, $offset)) {
			return $this->$offset();
	 	} else {
		
		}
    }
	
	public function isLoggedIn() {
		return ($this->data['user_id'] > 0);
	}
	
	public function isTrusted() {
		return ($this->data['user_level'] >= 6);
	}
	
	public function isModerator() {
		return ($this->data['user_level'] >= 8);
	}
	
	public function isAdmin() {
		return ($this->data['user_level'] >= 12);
	}
	
	public function isRegistered() {
		return !empty($this->data['user_is_registered']);
	}
	
	// public static function logoutGlobally() {
	// 	if (self::$instance->data['user_id'] > 0) {
	// 		$pdbo->exec('DELETE FROM user_session WHERE user_id = ' . (int)self::$instance->data['user_id']);
	// 	}
	// 	self::forget();
	// }
	
	public function forget() {
		if (!empty($_COOKIE['usr'])) {
			parse_str($_COOKIE['usr'], $data);
			if (!empty($data)) {
				$pdbo = \SiteManager::getDatabase();
				$status = (FALSE !== $pdbo->deleteRecord('user_session', array('session_id' => $data['i'], 'session_hash' => $data['h'])));
			} else {
				$status = FALSE;
			}
		}
		setcookie('usr', '', TIME_NOW - 3600, '/', COOKIE_DOMAIN, FALSE, TRUE);
		// session_regenerate_id();
	}

	public function memorize($ttl = 31536000) {
		if ($this->data['user_id'] > 0) {
			$pdbo = \SiteManager::getDatabase();
			$session_hash = UniqueID(15, 0);
			$data = array(
				'session_hash' => $session_hash, 'user_id' => $this->data['user_id'], 'auth_provider' => $this->data['auth_provider'],
				'session_platform' => self::getUserPlatform(), 'session_time_last_seen' => date('Y-m-d H:i:s')
			);
			if ($pdbo->insertRecord('user_session', $data)) {
				setcookie('usr', "h=$session_hash&i=" . $pdbo->lastInsertId(), TIME_NOW + $ttl, '/', COOKIE_DOMAIN, FALSE, TRUE);
				return TRUE;
			} else {
				trigger_error('[Session] Failed to memorize session');
			}
		}
		return FALSE;
	}
	
	public function remember() {
		
	}
	
	public function getUserData() {
		return array('id' => $this->data['user_id'], 'name' => $this->data['user_name'], 'name_encoded' => htmlspecialchars($this->data['user_name']), 'level' => $this->data['user_level'], 'loggedin' => $this->isLoggedIn(), 'registered' => (bool)$this->data['user_is_registered']);
	}
	
	public function getUser() {
		return User::loadById($this->data['user_id']);
	}
	
	public static function getInstance() {
		if (NULL === self::$instance) {
			if (!isset($_SESSION['userAuth'])) {
				$_SESSION['userAuth'] = new self();
			}
			self::$instance = $_SESSION['userAuth'];
			\SiteManager::invokeHook('user_auth_status_change', self::$instance);
		}
		return self::$instance;
	}
	
	public static function refresh() {
		$user = User::loadById(self::$instance->data['user_id']);
		return self::resetInstance($user);
	}
	
	public static function reload($auth = NULL) {
		if ($auth) {
			\Auth\Session::logout();
			$user = $auth->getUser();
			$auth_provider = $auth->type;
		} else {
			$user = NULL;
			$auth_provider = NULL;
		}
		session_regenerate_id(true);
		return self::resetInstance($user, $auth_provider);
	}
	
	public static function logout() {
		if ((NULL !== self::$instance) && (self::$instance->data['user_id'] > 0)) {
			self::$instance->forget();
			self::reload();
		}
	}
	
	public static function search($condition) {
		$stmt = \SiteManager::getDatabase()->search('user_session', $condition);
		return $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : array();
	}
	
	public static function remove($user_id, $session_id) {
		\SiteManager::getDatabase()->deleteRecord('user_session', compact('session_id', 'user_id'));
	}
	
	private static function resetInstance($user, $auth_provider = NULL) {
		// self::$instance = NULL;
		// if (!isset($_SESSION['userAuth']) || (self::$instance && ($_SESSION['userAuth']->id != self::$instance->data['user_id']))) {
			self::$instance = $_SESSION['userAuth'] = new self($user, $auth_provider);
		// }
		\SiteManager::invokeHook('user_auth_status_change', self::$instance);
		return self::$instance;
	}
	
	private static function getUserPlatform() {
		$ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
		$os = self::getUserOS($ua);
		$browser = self::getUserBrowser($ua);
		if ($browser) {
			return $os ? "$browser on $os" : $browser;
		} else {
			return $os ? $os : 'Unknown Device';
		}
	}
	
	private static function getUserOS($ua) {
		$arOS = array(
			'windows', 'iPad', 'iPod', 'iPhone', 'mac', 
			'android', 'linux', 'Nokia', 'BlackBerry', 
			'FreeBSD', 'OpenBSD', 'NetBSD', 
			'OpenSolaris', 'SunOS', 'OS\\\/2', 'BeOS', 'win'
		);
		if (preg_match('#' . implode('|', $arOS) . '#i', $ua, $match)) {
			$os = $match[0];
			if ($os === 'win') $os = 'Windows';
			return $os[0] === 'i' ? $os : ucfirst($os);
		}
		return false;
	}
	
	private static function getUserBrowser($ua) {
		
		if (preg_match('/Microsoft Internet Explorer|MSIE|Trident/i', $ua) && (stripos($ua, 'Opera') === false)) {
			return 'Internet Explorer';
		} else if (preg_match('/Opera|OPR/i', $ua)) {
			return (stripos($ua, 'Mini') !== false) ? 'Opera Mini' : ((stripos($ua, 'Mobile') !== false) ? 'Opera Mobile' : 'Opera');
		} else if (preg_match('/Firefox|Chrome|UCBrowser|UCWEB|Dolphin/i', $ua, $match)) {
			return ($match[0] === 'U') ? 'UC Browser' : $match[0];
		} else if (stripos($ua, 'Safari')) {
			return 'Safari';
		}
		return false;
	}
}
session_start();