<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * User Class
 * 
 * User details
 * 
 * @package		Access Control
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		2.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

namespace Auth;
class User {
	
	protected $data = array (
		'user_id'		 => 0,
		'user_name' 	 => 'Guest',
		'user_email'	 => '',
		'group_id' 		 => 'guest',
		'user_meta_info' => array(),
		'user_groups'	 => array(),
		'user_level'	 => 0,
		'user_verified'  => 0
	);
	
	protected function __construct($data = array()) {
		if ($data) {
			if (isset($data['user_id'])) {
				$this->hash = $data['user_hash'];
				unset($data['user_hash']);
				$this->data = array_merge($this->data, $data);
			} else {
				$this->updateData = $data;
			}
		}
	}

	public function __get($key){
		return isset($this->data['user_' . $key]) ? $this->data['user_' . $key] : (isset($this->data[$key]) ? $this->data[$key] : NULL);
	}
	
	public function getData($key = NULL) {
		return is_null($key) ? $this->data : $this->data[$key];
	}
	
	public function hash($data, $extra_salt = '') {
		return md5($data . $this->hash . $extra_salt);
	}
	
	public function getConfig($group, $key = NULL, $default = null) {
		$pdbo = \SiteManager::getDatabase();
		if (is_null($key)) {
			$stmt = $pdbo->query("SELECT value, config FROM user_config WHERE user_id = {$this->data['user_id']} AND config LIKE " . $pdbo->quote("$group.%"));
			if ($stmt) {
				$config = $stmt->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_GROUP, 1);
				$return = array();
				foreach ($config as $key => $value) {
					$tmp = explode('.', $key);
					$r =& $return;
					foreach ($tmp as $k) {
						if (!isset($r[$k])) $r[$k] = null;
						$r =& $r[$k];
					}
					$r = count($value) === 1 ? $value[0] : $value;
				}
				return $return;
			}
		} else {
			$stmt = $pdbo->query("SELECT config, value FROM user_config WHERE user_id = {$this->data['user_id']} AND config LIKE " . $pdbo->quote("$group.$key"));
			return $stmt->fetch() ?: $default;
		}
		return $default;
	}
	
	/**
	 * Static Methods
	 */
	private static function load($condition, $joinTable = FALSE, $cache = FALSE) {
		if ($joinTable) {
			$pdbo = \SiteManager::getDatabase();
			$user = $pdbo->getRecord('user_list user LEFT OUTER JOIN user_auth auth USING(user_id)', $condition, 1, 'user.*');
		} else {
			if ($cache && isset($condition['user_id'])) {
				$user = \Cache::load($condition['user_id'], 'user', 10800, function () use ($condition) {
					$pdbo = \SiteManager::getDatabase();
					return $pdbo ? $pdbo->getRecord('user_list', array('user_id' => $condition['user_id'])) : FALSE;
				});
			} else {
				$pdbo = \SiteManager::getDatabase();
				$user = $pdbo ? $pdbo->getRecord('user_list', $condition) : FALSE;
			}
		}
		if ($user) {
			// Any updations here should be performed in UserManager::save too
			if (is_string($user['user_meta_info'])) {
				$user['user_meta_info'] = unserialize($user['user_meta_info']);
			}
			return new static($user);
		}
		return FALSE;	
	}
	
	public static function loadById($user_id, $cache = TRUE) {
		return static::load(array('user_id' => $user_id), FALSE, $cache);
	}
	
	public static function loadByEmail($email) {
		return static::load(array('auth_identifier' => $email, 'auth_type' => UserAuth::AUTH_EMAIL), TRUE);
	}
	
	public static function loadByIdentity($identity, $type) {
		return static::load(array('auth_identifier' => $identity, 'auth_type' => $type), TRUE);
	}
	
	public static function loadGuest() {
		return new self();
	}
	
	public static function search($condition, $order = NULL, $limit = NULL, &$count = FALSE, $fields = null) {
		$pdbo = \SiteManager::getDatabase();
		if ($limit !== 0) {
			if (!$fields) $fields = 'user_id,user_name,user_gender,user_birthday,group_id,user_created,user_is_registered,user_level,user_verified';
			$stmt = $pdbo->search('user_list', $condition, $order, $limit, $fields);
			$users = $stmt ? $stmt->fetchAll() : array();
		} else {
			$users = array();
		}
		if ($count !== FALSE) {
			$count = $pdbo->getCount('user_list', $condition);
		}
		return $users;
	}
}
