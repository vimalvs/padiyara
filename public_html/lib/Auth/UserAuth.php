<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * AuthManager Class
 * 
 * Provides methods for managing user authentication method
 * 
 * @package		Access Control
 * @copyright	Ennexa Technologies (P) Ltd <info@ennexa.com>
 * @version		2.0.0
 * @date		$Date: 2012-04-26 16:09:00 +0530 (Thu, 26 Apr 2012	) $
 */

namespace Auth;

class UserAuth {
	
	const TYPE_UNREGISTERED = 1;
	const TYPE_REGISTERED = 2;
	const TYPE_CONNECTED = 3;
	
	const AUTH_UNREGISTERED = 0;
	const AUTH_EMAIL = 1;
	const AUTH_FACEBOOK = 2;
	const AUTH_GOOGLE = 3;
	const AUTH_YAHOO = 4;
	
	const ERR_AUTH_BAN_TEMP = 1;
	const ERR_PASSWORD_INCORRECT = 2;
	const ERR_USER_INVALID = 3;
	
	/**
	 * Instance Methods / Properties
	 */
	private $data = array();
	private $hash = NULL;
	private $updateData = array();
	
	public function __construct($data = NULL) {
		if ($data) {
			if (isset($data['auth_id'])) {
				$this->data = $data;
			} else {
				$this->updateData = $data;
			}
		}
	}
	
	public function __get($key){
		return isset($this->data['auth_' . $key]) ? $this->data['auth_' . $key] : NULL;
	}
	
	public function update($data, &$errorInfo = NULL) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($errorInfo);
	}
	
	public function save(&$errorInfo = NULL) {
		$isNew = empty($this->data['auth_id']);
		$pdbo = \SiteManager::getDatabase();
		if (is_null($errorInfo)) $errorInfo = array();
		
		unset($this->updateData['auth_id']);
		
		extract($this->data);
		array_walk_recursive($this->updateData, function (&$item, $key) {$item = trim($item);});
		
		extract($this->updateData, EXTR_REFS);
		if ($auth_type == self::AUTH_EMAIL) {
			$this->updateData['auth_comment'] = NULL;
		}
		if ($isNew) {
			$status = ($pdbo->insertRecord('user_auth', $this->updateData) !== FALSE);
		} else {
			$status = ($pdbo->updateRecord('user_auth', array('auth_id' => $this->data['auth_id']), $this->updateData) !== FALSE);
		}
		if ($status) {
			$this->data = $this->updateData + $this->data;
		} else {
			$errorInfo = '[UserAuth] Failed to create auth method';
			trigger_error($errorInfo, E_USER_WARNING);
		}
		return $status;
	}
	
	public function delete(&$errorInfo = NULL) {
		$pdbo = \SiteManager::getDatabase();
		$status = $pdbo->deleteRecord('user_auth', array('auth_id' => $this->data['auth_id']));
		if ($status) {
			\Logger::log("Deleted User Auth Method : {$this->data['auth_type']} [{$this->data['user_id']}]", 'delete', 'user_auth', $this->data['auth_id']);
			return TRUE;
		} else {
			$errorInfo = 'Failed to remove authentication method';
		}
		return FALSE;
	}
	
	public function getUser() {
		return User::loadById($this->data['user_id']);
	}
	
	/**
	 * Static Methods
	 */
	private static function load($arCondition, $joinTable = FALSE) {
		$pdbo = \SiteManager::getDatabase();
		$table = $joinTable ? 'user_auth_password LEFT OUTER JOIN user_auth USING(user_id)' : 'user_auth';
		if ($rec = $pdbo->getRecord($table, $arCondition)) {
			return new UserAuth($rec);
		}
		return FALSE;
	}
	
	public static function loadById($auth_id, $user_id) {
		return self::load(array('auth_id' => $auth_id, 'user_id' => $user_id));
	}
	
	public static function loadByIdentifier($auth_type, $auth_identifier) {
		return self::load(compact('auth_type', 'auth_identifier'));
	}
	
	public static function loadUnregistered($user_id) {
		
	}
	
	public static function check($account_type, $identifier, $auth_type = NULL, &$errCode = NULL) {
		$auth = false;
		
		switch ($account_type) {
			case self::TYPE_UNREGISTERED:
				$user = User::loadById($identifier);
				if (!$user->is_registered) {
					$auth = new UserAuth(array('auth_id' => 0, 'user_id' => $identifier));
				}
				break;
			case self::TYPE_REGISTERED:
				$tmpAuth = self::load(array('auth_type' => UserAuth::AUTH_EMAIL, 'auth_identifier' => $identifier[0]), TRUE);
				$user = $tmpAuth ? User::loadById($tmpAuth->data['user_id']) : false;

				if ($tmpAuth) {
					$locked = $tmpAuth->isLocked($auth_type, $identifier[0]);
					if (!$locked) {
						if ($user) {
							if (password_verify($identifier[1], $tmpAuth->data['auth_password_hash'])) {
								$auth = $tmpAuth;
							} else {
								$errCode = self::ERR_PASSWORD_INCORRECT;
								$cache = \Cache::getInstance();
								$count = (int)$cache->get("strike_{$account_type}_{$identifier[0]}", 'user');
								if (++$count % 5 === 0) {
									static::lock($auth_type, $identifier[0], min(6, $count / 5) * 300);
								}
								$cache->set("strike_{$account_type}_{$identifier[0]}", $count, 3600, 'user');
							}
						} else {
							$errCode = self::ERR_USER_INVALID;
						}
					} else {
						$errCode = self::ERR_AUTH_BAN_TEMP;
					}
				} else {
					$errCode = self::ERR_PASSWORD_INCORRECT;
				}
				break;
			case self::TYPE_CONNECTED:
				$auth = self::load(array('auth_type' => $auth_type, 'auth_identifier' => $identifier));
				break;
			default:
				trigger_error('[UserAuth] Invalid authentication method');
		}
		// $this->status = $status;
		// if ($status) {
		// 	$this->user = $user;
		// 	$this->provider = $provider;
		// 	return TRUE;
		// }
		// $this->user = User::loadGuest();
		// $this->provider = NULL;

		return $auth;
	}
	
	
	public static function isLocked($auth_type, $identifier) {
		$ts = (int)\Cache::getInstance()->get("locked_{$auth_type}_{$identifier}", 'user');
		$expired = $ts < TIME_NOW;
		if ($ts && $expired) {
			static::unlock($auth_type, $identifier);
		}
		return $expired ? false : ($ts - TIME_NOW);
	}
	
	public static function lock($auth_type, $identifier, $ttl = 900) {
		\Cache::getInstance()->set("locked_{$auth_type}_{$identifier}", TIME_NOW + $ttl, $ttl, 'user');
	}
	
	public static function unlock($auth_type, $identifier) {
		\Cache::getInstance()->delete("locked_{$auth_type}_{$identifier}", 'user');
	}

	public static function add () {
		
	}
	
	public static function setUserPassword($user_id, $auth_password_hash) {
		$status = \SiteManager::getDatabase()->replaceRecord('user_auth_password', compact('user_id', 'auth_password_hash'));
		return (FALSE !== $status);
	}
	
	public static function checkEmail($auth_email, &$verified = NULL) {
		$record = \SiteManager::getDatabase()->getRecord('user_auth_email', array('auth_email' => $auth_email));
		if ($record) {
			$verified = $record['auth_verified'];
			return TRUE;
		}
		return FALSE;
	}
	
	public static function search($condition) {
		$pdbo = \SiteManager::getDatabase();
		$stmt = $pdbo->search('user_auth', $condition);
		return $stmt ? $stmt->fetchAll() : array();
	}
}
