<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com    					      # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * News Class
 * 
 * Class for cms page related actions
 * @package		CMS
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2017-11-23 12:10:00 +0530
 */

namespace News;

class News {
	protected $data = array('event_pathname' => '', 'event_title' => '', 'event_category' => '', 'event_description' => '', 'event_summery' => '');
	private static function load($event_id) {
		$event = \SiteManager::getDatabase()->getRecord('event', compact('event_id'));
		if ($event) {
			return $event ? new static($event, false) : false;
		}
		return false;
	}
	
	public static function loadById($id) {
		$event = \SiteManager::getDatabase()->getRecord('event', ['event_id' => $id]);
		if ($event) {
			return $event ? new static($event, false) : false;
		}
		return false;
	}
	
	public function __construct($data) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	public function __get($key){
		return isset($this->data['event_' . $key]) ? $this->data['event_' . $key] : null;
	}
	
	public function getData($key = null, $prefix = 'event_') {
		return is_null($key) ? $this->data : $this->data[$prefix . $key];
	}
	
	public static function search($condition = null, $order = null, $limit = null, &$count = false) {
		$arItem = array();
		$pdbo = \SiteManager::getDatabase();
		if ($order && !is_array($order)) $order = array($order, 'ASC');
		if ($limit && !is_array($limit)) $limit = array(0, $limit);

		$whereSql = empty($condition) ? 1 : $pdbo->genQuery($condition);
		if ($count !== false) {
			$count = $pdbo->getCount('event', $whereSql);
		}
		$stmt = $pdbo->search('event', $whereSql, $order, $limit);
		$arItem = $stmt ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : false;
		return $arItem;
	}
}
