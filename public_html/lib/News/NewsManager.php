<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            				  # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * PageManager Class
 * 
 * @package		Page
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 * @date		$Date: 2017-11-23 12:16:00
 */

namespace News;

class NewsManager extends News {
	
	private $updateData = array();
	private $isNew = false;
	private static $tidyConfig = array('clean' => 'yes', "indent" => true, 'output-html' => true, "indent" => 'auto', "indent-spaces" => 4, "wrap" => 250, 'show-body-only' => true);
	protected static $db = null;
	
	public function __construct($data = null, $isNew = true) {
		$this->isNew = $isNew;
		
		if ($data) {
			if ($isNew) {
				$this->updateData = $data;
			} else {
				$this->data = $data;
			}
		}
	}

	public function delete(&$error = null) {
		$status = static::$db->deleteRecord('event', array('event_id' => $this->data['event_id']));
		if ($status) {
			\Logger::log("Deleted Event: " . $this->data['event_title'], 'delete', 'event', $this->data['event_id']);
		}
		return $status;
	}
	
	public function update($data, &$arError = null) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($arError);
	}
	
	public function save(&$errorInfo = null) {
		// $isNew = empty($this->data['page_key']);
		$pdbo = \SiteManager::getDatabase();
		if (empty($this->updateData)) {
			return true;
		} elseif (!$this->isNew) {
			unset($event_id);
		}
		if($this->data){
			extract($this->data);
		}
		if($this->updateData){
			extract($this->updateData, EXTR_REFS);
		}
		if (is_null($errorInfo)) $errorInfo = array();
		// $page_allow_comments = (bool)$page_allow_comments;
		if(isset($imageDetails)){
			$file_type = $imageDetails['type']; 
			$allowed = array("image/jpeg", "image/png");
			if(!in_array($file_type, $allowed)) {
			  $errorInfo['event_image'] = 'Only jpg and png files are allowed.';
			}
		}
		
		if(empty($event_title)){
			$errorInfo['event_title'] = 'Title cannot be empty';
		}
		if(empty($event_description)){
			$errorInfo['event_description'] = 'Description cannot be empty';
		}
		$status = empty($errorInfo);
		if ($status) {
			/**
			 * Data Validated successfully
			 */
			if ($this->isNew) {
				$event_created_time = TIME_NOW;
				// printr($this->updateData, $this->data);
				
				// $imageDetails = $this->updateData['imageDetails'];
				// unset($this->updateData['imageDetails']);
				// $event_image_pathname = $imageDetails['name'];
				$this->updateData['event_created_time'] = $event_created_time;
				// $this->updateData['event_image_pathname'] = $event_image_pathname;
				// printr($this->updateData, $this->data);
				// exit;
				$status = $pdbo->insertRecord('event', $this->updateData);
				if ($status) {
					$this->data['event_id'] = $pdbo->lastInsertId();
					\Logger::log("Added New Event : $event_title", 'add' , 'event', $event_title);
				}
			} else {
				
				$status = $pdbo->updateRecord('event', array('event_id' => $event_id), $this->updateData);
				// Log event only if the entry was modified
				if ($status !== false) {
					\Logger::log("Updated event : $event_id", 'update' , 'event', $event_id);
				}
				// Return success even if data was not modified
				$status = ($status !== false);
			}
		}
		return $status;
	}

	public static function initialize() {
		static::$db = \SiteManager::getDatabase();
	}
}

NewsManager::initialize();