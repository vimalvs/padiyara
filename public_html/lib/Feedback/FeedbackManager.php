<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Error Report Class
 * 
 * Class for actions related to error reporting
 * @package		Report
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Feedback;

class FeedbackManager extends Feedback {

	protected $updateData = array();

	public static function getSectionInfo($report_section) {
		return \SiteManager::getConfig("feedback_report_section_{$report_section}");
	}
	
	public function __construct($data = null) {
		if ($data) {
			if (isset($data['report_id'])) {
				$this->data = $data;
			} else {
				$this->updateData = $data;
			}
		}
	}


	public function delete(&$errorInfo = null) {
	
		$pdbo = \SiteManager::getDatabase();
		$status = $pdbo->deleteRecord('feedback_report', array('report_id' => $this->data['report_id']));
		if ($status) {
			\Logger::log("Deleted error report : " . $this->data['report_section'] .', '. $this->data['report_item'], 'delete', 'feedback_report', $this->data['report_id']);
			return true;
		} else {
			$errorInfo = 'Failed to delete report from database';
		}		
	}
	
	public function update($data, &$errorInfo = null) {
		$this->updateData = array_merge($this->updateData, $data);
		return $this->save($errorInfo);
	}
	
	public function save(&$errorInfo = null) {
		$isNew = empty($this->data['report_id']);

		unset($this->updateData['report_id']);

		if (empty($this->updateData)) return true;

		extract($this->data); 
		extract($this->updateData, EXTR_REFS);
		parent::checkItem($report_section, $report_item, $errorInfo);
		if (empty($report_section)) $errorInfo['report_section'] = 'Section for feedback is must';
		if (empty($report_item)) $errorInfo['report_item'] = 'Item for feedback is must';
		$report_comment = !empty(trim($report_comment)) ? trim($report_comment) : NULL;

		$status = empty($errorInfo);
		if ($status) {
			$pdbo = \SiteManager::getDatabase();
			$updateData = $this->updateData;
			if ($isNew) {
				$updateData['report_time'] = TIME_NOW;
				$status = $pdbo->insertRecord('feedback_report', $updateData);
				if ($status) {
					$report_id = $pdbo->lastInsertId();
					$this->data['report_id'] = $report_id;
					\Logger::log("Feedback Added For {$report_section}, {$report_item}", 'add' , 'feedback_report', $report_id);
				}
			} else {
				$status = $pdbo->updateRecord('feedback_report', compact('report_id'), $updateData);
				if ($status) {
					\Logger::log("Feedback Updated For {$report_section}, {$report_item}", 'update' , 'feedback_report', $report_id);
				}
			}
			if ($status) {
				$this->data = array_merge($this->data, $this->updateData);
				$this->updateData = [];
			}
		}

		return $status;
	}
}