<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Stats Feedback
 * 
 * Class for actions related to error stats
 * @package		Feedback
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Feedback;

use iFixit\StatsD;

class Stats extends StatsD {
	
	protected static $host = SERVER_ADDR_STATS;
	// protected static $port = '';
	// const MAX_PACKET_SIZE = 512;
	
	public static function _autoInitialize() {
		
		self::pauseStatsOutput();
		register_shutdown_function(function() {
		   self::flushStatsOutput();
		});
		
	}
	/**
	* Log timing information
	*
	* @param string $stat The metric to in log timing info for.
	* @param float $time The ellapsed time (ms) to log
	* @param float $sampleRate the rate (0-1) for sampling.
	**/
	public static function timing($stat, $time, $sampleRate=1.0) {
		parent::timing(SITE_CODE . ".$stat", $time, $sampleRate);
	}

	/**
	* Report the current value of some gauged value.
	*
	* @param string|array $stat The metric to report on
	* @param float $value The value for this gauge
	*/
	public static function gauge($stat, $value) {
		parent::gauge(SITE_CODE . ".$stat", $value);
	}

	/**
	* Increments one stats counter
	*
	* @param string $stat The metric to increment.
	* @param float $sampleRate the rate (0-1) for sampling.
	**/
	public static function increment($stat, $sampleRate=1.0) {
		parent::increment(SITE_CODE . ".$stat", $sampleRate);
	}

	/**
	* Decrements one counter.
	*
	* @param string $stat The metric to decrement.
	* @param float $sampleRate the rate (0-1) for sampling.
	**/
	public static function decrement($stat, $sampleRate=1.0) {
		parent::decrement(SITE_CODE . ".$stat", $sampleRate);
	}

}

Stats::_autoInitialize();
