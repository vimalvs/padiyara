<?php
/*======================================================================*\
|| # ---------------------------------------------------------------- # ||
|| #                                                                  # ||
|| #             Copyright (c) VimalVS.com            # ||
|| #                                                                  # ||
|| # ---------------------------------------------------------------- # ||
\*======================================================================*/

/**
 * Error Report Class
 * 
 * Class for actions related to error reporting
 * @package		Report
 * @copyright	VimalVS <vimal444@hotmail.com>
 * @version		1.0.0
 */

namespace Feedback;
class Feedback {
	protected $data = array();
	protected static $arFeedBackItems = [
									'astrology' => [
														'panchang-chrome-extension',
									]

	];

	public function __construct($data = null) {
		if ($data) {
			$this->data = $data;
		}
	}
	
	protected static function load($condition) {
		$report = \SiteManager::getDatabase()->getRecord('feedback_report', $condition);
		if ($report) {
			// Any updations here should be performed in ReportManager::save too
			$report['report_item_details'] = unserialize($report['report_item_details']);
			return $report;
		}
		return false;	
	}
	
	public static function loadById($report_id) {
		$data = static::load(array('report_id' => $report_id));
		return $data ? new static($data) : false;
	}

	
	public static function search($condition, $order = null, $limit = null, &$count = null) {
		$pdbo = \SiteManager::getDatabase();
		if ($limit !== 0) {
			$stmt = $pdbo->search('feedback_report', $condition, $order, $limit);
			$reports = $stmt ? $stmt->fetchAll() : array();
		} else {
			$reports = array();
		}
		if ($count !== false) {
			$count = $pdbo->getCount('feedback_report', $condition);
		}
		return $reports;
	}

	public function __get($key) {
		return isset($this->data['report_' . $key]) ? $this->data['report_' . $key] : null;
	}
	
	public function getData($key = NULL) {
		return is_null($key) ? $this->data : $this->data[$key];
	}


	public static function checkItem($report_section, $report_item, &$errorInfo = null) {
		if (isset(self::$arFeedBackItems[$report_section])) {
			if (in_array($report_item, self::$arFeedBackItems[$report_section])) {
				return true;
			} else {
				$errorInfo['report_item'] = 'Feedback Item Is Invalid';	
			}
		} else {
			$errorInfo['report_section'] = 'Feedback Section Is Invalid';
		}
		return false;
	}

}