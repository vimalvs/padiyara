<!DOCTYPE html>
<html lang="en">
<head>
<title>History of Padiyara Vallikattu Family Site</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu Family kottayam, Padiyara Vallikattu kudumbayogam, Padiyara Vallikattu, Padiyara Vallikattu genealogy" />
<meta name="description" content="Complete information about the Padiyara Vallikattu Family of Kottayam. Profile of Padiyara Vallikattu Family Members, Padiyara Vallikattu Family Genealogy and Family tree, details about Padiyara Vallikattu Kudumbayogam..." />
<?php $this->render('theme::headContent');?>
<style>
	.has-error > .form-control, .has-danger > .form-control {
		border:1px solid #dc3545;
	}
</style>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
			<div class="py-4">
				<div class="container p-3">
					<div class="text-center">
						<span class="t-xlarge">Contact us</span>	
						<h1>Drop a Line</h1>
						<hr class="h-underline">
					</div>
					<div class="container">
		                <div>
		                    <div class="icon-block pb-3">
		                        <span class="icon-block__icon">
		                            <span class="mbri-letter mbr-iconfont" media-simple="true"></span>
		                        </span>
		                        <h4 class="icon-block__title align-left mbr-fonts-style display-5">
		                            Feedback / Suggestion
		                        </h4>
		                    </div>
		                    <div class="icon-contacts pb-3">
		                        <div class="mbr-text align-left mbr-fonts-style display-7">
		                            <span class="fa fa-envelope "></<span> Drop your email <span class="fa fa-arrow-right">  contact@padiyaravallikattu-kudumbayogam.com
		                        </div>
		                    </div>
		                </div>
		                <div data-form-type="formoid">
		                    <div data-form-alert="" hidden="">
		                        Thanks for filling out the form!
		                    </div>
		                    <form id="contact-form" method="post" action="contact.php" role="form">

		                        <div class="messages"></div>

		                        <div class="controls">

		                            <div class="row">
		                                <div class="col-md-6">
		                                    <div class="form-group">
		                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
		                                        <div class="help-block with-errors"></div>
		                                    </div>
		                                </div>
		                                <div class="col-md-6">
		                                    <div class="form-group">
		                                        <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
		                                        <div class="help-block with-errors"></div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <div class="form-group">
		                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
		                                        <div class="help-block with-errors"></div>
		                                    </div>
		                                </div>
		                                <div class="col-md-6">
		                                    <div class="form-group">
		                                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
		                                        <div class="help-block with-errors"></div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="row">
		                                <div class="col-md-12">
		                                    <div class="form-group">
		                                        <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
		                                        <div class="help-block with-errors"></div>
		                                    </div>
		                                </div>
		                                <div class="col-md-12">
		                                    <input type="submit" class="btn btn-success btn-send" value="Send message">
		                                </div>
		                            </div>
		                        </div>

		                    </form>
		                </div>
				    </div>
				</div>
			</div>
		</div>
	</div>


	<?php $this->render('theme::footer')?>
<script src="/assets/js/validator.js"></script>
<script src="/assets/js/contact.js"></script>

</div>
</body>
</html>