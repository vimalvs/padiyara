<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="gNR8fjaFTguF-fVDgvWH8XEJNoSMzLAO3i_XYRhoxuQ" />
<title>Padiyara Vallikattu Family Site</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Padiyara Vallikattu Family kottayam, Padiyara Vallikattu kudumbayogam, Padiyara Vallikattu, Padiyara Vallikattu genealogy" />
<meta name="description" content="Complete information about the Padiyara Vallikattu Family of Kottayam. Profile of Padiyara Vallikattu Family Members, Padiyara Vallikattu Family Genealogy and Family tree, details about Padiyara Vallikattu Kudumbayogam..." />
<?php $this->render('theme::headContent');?>
</head>
<body>
<div id="wrapper">
	<header id="header">
		<?php $this->render('theme::header');?>
	</header>
	<div id="body">
		<div id="mainContent">
	      	<div id="home_carousel" class="carousel slide" data-ride="carousel">
			    <div class="carousel-inner">
			    	<?php foreach($arImage as $key => $image):?>
				        <div class="carousel-item <?=($key === 0) ? 'active':''?>">
				            <img class="d-block w-100" src="<?=$image['image_path']?>">
				        </div>
			        <?php endforeach;?>
			    </div>
			    <a class="carousel-control-prev" href="#home_carousel" role="button" data-slide="prev">
			        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			        <span class="sr-only">Previous</span>
			    </a>
			    <a class="carousel-control-next" href="#home_carousel" role="button" data-slide="next">
			        <span class="carousel-control-next-icon" aria-hidden="true"></span>
			        <span class="sr-only">Next</span>
			    </a>
			</div>
			<div class="bg-blue">
				<div class="container bg-blue p-3 fg-white">
					<div class="text-center">
						<span class="t-xlarge">Introduction To</span>	
						<h1>PADIYARA VALLIKATTU FAMILY</h1>
						<hr class="h-underline">
					</div>
					<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
					<p class="text-right"><a class="fg-white" href="#">read more.. &raquo;</a></p>
				</div>
			</div>

			<div class="container p-2">
				<div class="p-2">
					<h2 class="text-center">WHO IS WHO ?</h2>
					<hr class="h-underline">
					<p>
						BABY NAMES
						Baby Names &amp; Name Meanings
						EDIT
						Searching for Baby Names? Find the best name for your baby. Choose from 1000's of Boy baby names and Girl baby names. Alphabetic listing of names along with their meanings makes baby name search easy and simple for you.
					</p>
					<ul class="nav-rounded-list grid-col grid-col-xs-12 grid-col-sm-6 grid-col-lg-3 text-center">
						<li class="list-item">
							<a href="#">
					  			<img src="/assets/imgs/man.jpg" alt="Man" class="img-fluid rounded-circle">
					  			<span  class="caption"><strong>Test</strong><br>Description</span>
					  		</a>
						</li>
						<li class="list-item">
					  		<a href="#">
					  			<img src="/assets/imgs/man.jpg" alt="Man" class="img-fluid rounded-circle">
					  			<span  class="caption"><strong>Test</strong><br>Description</span>
					  		</a>
						</li>
						<li class="list-item">
							<a href="#">
					  			<img src="/assets/imgs/man.jpg" alt="Man" class="img-fluid rounded-circle">
					  			<span  class="caption"><strong>Test</strong><br>Description</span>
					  		</a>
						</li>
						<li class="list-item">
					 		<a href="#">
					  			<img src="/assets/imgs/man.jpg" alt="Man" class="img-fluid rounded-circle">
					  			<span  class="caption"><strong>Test</strong><br>Description</span>
					  		</a>
						</li>
					</ul>
					<span class="d-block p-3 text-center"><a class="btn btn-info" href="#">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
				</div>
			</div>
			<div class="bg-blue">
				<div class="container bg-blue p-3 fg-white">
					<div class="text-center">
						<h2>GALLERY</h2>
						<hr class="h-underline">
					</div>
					<p>
						BABY NAMES
						Baby Names &amp; Name Meanings
						EDIT
						Searching for Baby Names? Find the best name for your baby. Choose from 1000's of Boy baby names and Girl baby names. Alphabetic listing of names along with their meanings makes baby name search easy and simple for you.
					</p>
					<div class="container-small">
						
						<ul class="nav-list grid-col grid-col-xs-6 grid-col-sm-4 grid-col-lg-4 text-center">
							<li>
								<a href="#">
						  			<img src="/assets/imgs/thumb.jpg" alt="Man" class="img-fluid	">
						  		</a>
							</li>
							<li>
						  		<a href="#">
						  			<img src="/assets/imgs/thumb.jpg" alt="Man" class="img-fluid">
						  		</a>
							</li>
							<li>
								<a href="#">
						  			<img src="/assets/imgs/thumb.jpg" alt="Man" class="img-fluid">
						  		</a>
							</li>
							<li>
						 		<a href="#">
						  			<img src="/assets/imgs/thumb.jpg" alt="Man" class="img-fluid">
						  		</a>
							</li>
							<li>
						 		<a href="#">
						  			<img src="/assets/imgs/thumb.jpg" alt="Man" class="img-fluid">
						  		</a>
							</li>
							<li>
						 		<a href="#">
						  			<img src="/assets/imgs/thumb.jpg" alt="Man" class="img-fluid">
						  		</a>
							</li>
						</ul>
					</div>
					<span class="d-block p-3 text-center"><a class="btn btn-info" href="#">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
				</div>
				<div class="p-3 text-center">
					<a class="fg-white" href="#">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a>
				</div>
			</div>
			<div class="container p-2">
				<div class="p-2">
					
					<h2 class="text-center">NEWS AND ANNOUNSMENT </h2>
					<hr class="h-underline">

					<div class="grid-col grid-col-xs-12 grid-col-sm-6 grid-col-lg-4 text-center">
						<div class="p-2">
							<div class="card rounded p-2">
								<p class="card-text">
									Searching for Baby Names? Find the best name for your baby. Choose from 1000's of Boy baby names and Girl baby names. Alphabetic listing of names along with their meanings makes baby name search easy and simple for you.
								</p>
							</div>
						</div>
						<div class="p-2">
							<div class="card rounded p-2">
								<p class="card-text">
									Searching for Baby Names? Find the best name for your baby. Choose from 1000's of Boy baby names and Girl baby names. Alphabetic listing of names along with their meanings makes baby name search easy and simple for you.
								</p>
							</div>
						</div>
						<div class="p-2">
							<div class="card rounded p-2">
								<p class="card-text">
									Searching for Baby Names? Find the best name for your baby. Choose from 1000's of Boy baby names and Girl baby names. Alphabetic listing of names along with their meanings makes baby name search easy and simple for you.
								</p>
							</div>
						</div>
					</div>
					<span class="d-block p-3 text-center"><a class="btn btn-info" href="#">&nbsp;&nbsp;&nbsp;&nbsp;view all&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
				</div>
			</div>
		</div>
	</div>
	<?php $this->render('theme::footer')?>
</div>
</body>
</html>