<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Mallu Bar:: Free Glitter Graphics, Glitter Generators, Text Convertors</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="Glitter Graphics, Glitter Scraps, free glitter texts, orkut glitter graphics, glitter images" />
<meta name="description" content="An orkut resource site providing free glitter graphics, glitter text generator, glitter scraps" />
<?php \SiteManager::invokeHook(['template_head', $this]);?>
<link rel="stylesheet" type="text/css" href="/assets/css/style.css" \>
<style type="text/css">
.frmStatus table{margin:10px auto;border:1px #CC6666 solid;background:#FFF;}
.frmStatus table td{padding:5px 15px;}
.frmStatus td.title{background:#FFCCCC;}
.frmStatus .button{margin:0 auto;width:120px;display:block;border:1px solid #999;}
.frmStatus .meta{text-align:center;}
#loggedInContinue{margin:10px 20px;border:1px #CCC solid;}
#loggedInContinue .title{background:#CCC;}
label .flabel,div.label .flabel{width:32%;}
label .field,div.label .field{width:65%;}
.inputField{width:200px;}
label,div.label{display:block;width:100%;min-height:25px;margin-top:2px;padding:5px 4px 0px;clear:both;}
label .field,div.label .field{display:block;width:64%;float:right;clear:right;}
label .flabel,div.label .flabel{display:block;width:34%;float:left;clear:left;color:inherit;font-weight:normal;}

</style>
</head>

<body>
<div id="wrapper">
<div id="innerWrapper">
<div id="header">
<?php $this->render('header');?>
<div id="headContent">
<h1>Glitters from Mallubar.com - Malayalam Scraps, Malayalam Glitter Graphics and More...</h1>
<p>Welcome to Mallubar.com. Your search for quality glitter, glitter texts and Myspace, Orkut resources ends here. Giltter graphics for all occasion. Valentine's Day Glitter, Valentine and Graphics, Love Glitter, Happy Birthday Glitter Text and Glitter Image.</p>
</div>
</div>
<div id="body">
<div id="mainContent">
<!-- Middle Content Begins-->
<div id="middleContent">
<div class="box">
<div class="cb">
<span class="tb green">&nbsp;</span>
<form class="frmStatus" method="post" action="/members/member.php">
<?php if($this->data['mode'] === 'welcome'):?>
    <div class="msgBox successMsg">
        <div class="msgTitle"><span>Success</span></div>
        <div class="msgContent">
                <p>You are now logged in as <?=$this->data['name']?><br/>
		Username : <span class="hilite"><?=$this->data['username']?></span><br/>
		<a class="meta" href="/members/member.php?mode=login&amp;mode=logout&amp;redirect=<?=$this->data['redirect']?>">Are you not <?=$this->data['name']?>? Click here to re-login.</a></p>
            </div>
        <div class="msgButton"><span><a href="<?=$this->data['redirect']?>" class="button">Back</a></span></div>
    </div>
<?php elseif($this->data['mode'] === 'register'):
	$this->printMessage();
	if($this->messages->status !== 'exit'):
?>
    <table cellspacing="0">
    <tr><td class="title" colspan="2">Quick Signup</td></tr>
    <tr><td colspan="2" class="nError"><?php ?></td></tr>
    <tr><td>Your Name</td><td><input class="if" type="text" name="name" value="<?=$this->data['form']['name']?>"/></td></tr>
    <tr><td>Username</td><td><input class="if" type="text" name="username" value="<?=$this->data['form']['username']?>"/></td></tr>
    <tr><td>Email</td><td><input class="if" type="text" name="email" value="<?=$this->data['form']['email']?>"/></td></tr>
    <tr><td>Password</td><td><input class="if" type="password" name="password" /></td></tr>
    <tr><td>Confirm</td><td><input class="if" type="password" name="confirm" /></td></tr>
    <tr><td colspan="2"><input class="button" type="submit" value="Register"/>
    <input type="hidden" name="p" value="1"/><input type="hidden" name="mode" value="register"/>
    </td></tr>
    <tr><td colspan="2" class="meta"><a href="/members/member.php?mode=login">Already registered? Login</a> | <a href="/members/member.php?mode=sendpw">Resend Password</a></td></tr>
    </table>
    <?php endif;?>
<?php elseif($this->data['mode'] === 'register_success'):?>
    <?php $this->printMessage()?>
<?php elseif($this->data['mode'] === 'sendpw'):?>
    <table cellspacing="0">
    <tr><td class="title">Email Password</td></tr>
        <tr><td colspan="2" class="nError"><?php $this->printMessage();?></td></tr>
    <tr><td>Enter your email address<br/><input class="if" type="text" name="email" value="<?=$this->data['form']['email']?>"/></td></tr>
    <tr><td><input class="button" type="submit" value="Continue"/>
    <input type="hidden" name="p" value="1"/><input type="hidden" name="mode" value="sendpw"/>
    </td></tr>
    <tr><td class="meta"><a href="/members/member.php?mode=login">Login</a> | <a href="/members/member.php?mode=register">Register</a></td></tr>
    </table>
<?php elseif($this->data['mode'] === 'activation'):
	$this->printMessage();
  if($this->messages->status !== 'exit'):
	if($this->data['action'] == 'resend'):
	?>
    
    <table cellspacing="0">
    <tr><td class="title">Resend Activation</td></tr>
    <tr><td>
        <p style="text-align:center;">Enter your Username <u><b>OR</b></u> Email Address and click '<i>Go</i>'</p>
        <label><span class="flabel">Username</span><span class="field"><input class="if" type="text" name="username" value=""/>&nbsp;&nbsp;<input type="submit" name="findByUser" value="Go"/></span></label>
        <label><span class="flabel">Email</span><span class="field"><input class="if" type="text" name="email"/>&nbsp;&nbsp;<input class="inputButtonSmall" type="submit" name="findByEmail" value="Go"/></span></label>
    </td></tr>
    <tr><td class="meta"><input type="hidden" name="p" value="1"/><input type="hidden" name="action" value="resend"/><a href="/members/activation.php?action=activate">Enter Activation Code</a></div>
        </td></tr>
        </table>
    <?php else :?>
    <table cellspacing="0">
    <tr><td class="title">Account Activation</td></tr>
    <tr><td>
        <p style="text-align:center;">Enter your Username and Activation code below</p>
        <label><span class="flabel">Username</span><span class="field"><input class="if" type="text" name="username"/></span></label>
        <label><span class="flabel">Activation Code</span><span class="field"><input class="if" type="text" name="code"/></span></label>
        <div class="center"><input type="submit" class="button" value="Activate" /><input type="hidden" name="p" value="1"/><input type="hidden" name="action" value="activate"/><input type="hidden" name="mode" value="activation"/></div></td></tr>
        <tr><td class="meta"><a href="/members/activation.php?action=resend">Resend Activation Code</td></tr>
        </table>
    <?php endif;?>
  <?php endif;?>
<?php else:?>
        <table cellspacing="0">
        <tr><td class="title" colspan="2">Login</td></tr>
        <tr><td colspan="2" class="nError"><?php $this->printMessage();?></td></tr>
        <tr><td>Username</td><td><input class="if" type="text" name="username" value="<?=$this->data['form']['username']?>"/></td></tr>
        <tr><td>Password</td><td><input class="if" type="password" name="password" /></td></tr>
        <tr><td colspan="2"><label><input class="chkInput" name="autologin" type="checkbox" value="1" <?=(!empty($this->data['form']['autologin']) ? 'checked="checked"' : '')?>/>
        Remember me on this computer</label></td></tr>
        <tr><td colspan="2"><input class="button" type="submit" value="Login"/>
        <input type="hidden" name="p" value="1"/>
        <input type="hidden" name="mode" value="login"/>
        </td></tr>
        <tr><td colspan="2" class="meta"><a href="/members/member.php?mode=register">Quick Signup</a> | <a href="/members/member.php?mode=sendpw">Resend Password</a></td></tr>
        </table>
<?php endif;?>
<div><input type="hidden" name="redirect" value="<?=$this->data['redirect']?>"/></div>
</form>
</div></div>

</div>
<!-- Middle Content Ends-->
<!-- Right Content Begins-->
<div id="rightContent">
	<div class="box">
		<div class="cb">
			<span class="tb blue">&nbsp;</span>
			<div class="gAd1"><script type="text/javascript"><!--
			google_ad_client = "ca-pub-2107538958244175";
			/* [MB] Right Content 300x250 */
			google_ad_slot = "1865891229";
			google_ad_width = 300;
			google_ad_height = 250;
			//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script></div>
		</div>
	</div>
	<div class="box">
		<div class="cb bluebox">
		<span class="tb blue">&nbsp;</span>
		<span class="caption">Malayalam Scrap, Glitters</span>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</div>
	</div>
</div>
<div class="clearer"></div>
</div>
<!-- Right Content Ends-->
<!-- Left Content Begins-->
<?php $this->render('leftcontent')?>
<!-- Left Content Ends-->
<div class="clearer"></div>
</div>
</div></div>
</body>
</html>