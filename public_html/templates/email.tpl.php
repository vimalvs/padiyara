<?php
$data = $this->data;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>MalluBar :: Tell a Friend</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<?php $this->printHeader();?>
<script  type="text/javascript" src="/js/jquery.js"></script>
<style type="text/css">
.lnks{margin:10px;padding:3px;border:1px #CCC solid;text-align:center;}
form{margin:0 10px;}
.button{width:125px;border:1px #333 solid;}
#mailWrapper .cb{margin:2px;padding:5px;overflow:auto;}
.insertContactsLink{color:#18397C;font:bold 12px Verdana, Arial, Sans-Serif;}
.error{text-align:center;color:red;font:normal 12px Arial, Verdana, Sans-Serif;}
.info{color:#999;font-style:italic;}
.info a{text-decoration:underline;font-style:italic;color:#999;}
input{display:block;margin:0px auto;}
.tblLogin{margin:10px auto;border:1px #666 solid;font:normal 12px Verdana, sans-serif;}
.tblLogin td{padding:10px 15px;}
.tblLogin .inputField{width:160px;}
.tblLogin .button{width:125px;}
#btnAddEmail{margin:10px 0 5px auto;}
.tblForm{width:100%;margin:0 auto;background:#FEE9FD;border:1px #E5C3E4 solid;font:normal 12px Verdana,Arial,Helvetica,sans-serif;color:#cc33cc;}
.tblForm td{padding:5px 7px;text-align:center;text-align:left;}
.tblForm tr.title,.tblForm tr.title td{font-weight:bold;text-align:center;background:#FFCCEE;}
.tblForm tr.even{background:#FEE9FD;}
.tblForm tr.odd{background:#FFF2FD;}
.tblForm .inputField{margin:1px;width:95%;height:19px;line-height:19px;border:1px #CCC solid;font-style:italic;color:333;}
.tblForm .errorField{border-color:#F00;}
.tblForm .name{font-weight:bold;}
.tblForm .email{font-style:italic;font-size:90%;}
#checkAll{padding:5px 0;width:36px;}
#frmSend{font-size:12px;}
#frmSend label{display:block;height:25px;}
#frmSend label b{float:left;width:75px;clear:left;}
#frmSend .inputField{float:left;clear:right;width:220px;}
#frmSend #recaptcha_area{clear:both;margin:0 auto;}
#frmSend .pad{margin:0px 20px;}
</style>
<script type="text/javascript">
$(document).ready(function(){
$('.tblForm :input[type="checkbox"]').attr("checked", true);
$('#checkAll :input[type=checkbox]').click(function() {$('.tblForm :input[type="checkbox"]').attr("checked", $(this).attr('checked'));});
var trs = $('.tblForm tr').filter(":not(:first,:last,:nth-child(2))");
$('#addrFilter').keyup(function(e){
	var t = $(this);
	if(e.keyCode == 27) t.val('');
	var v = t.val();
	if(v == '') trs.css('display', '');
	else{
		trs.css('display', '').filter(':not(:contains('+v+'))').css('display', 'none');
	}
	if(trs.filter(':visible').length > 0)t.removeClass('errorField');
	else t.addClass('errorField');
});
});
</script>
</head>

<body>
<div id="wrapper">
<div id="innerWrapper">
<div id="header">
<?php $this->render('header');?>
<div id="headContent">
<h1>Glitter Text and Glitter animators - Create custom glitter animations</h1>
<p>Create your own custom glitter graphics. Upload your photos, add glitter effects to it and send them to your friends or post them on your blog, orkut, myspace, face book profiles. Malayalam scraps for all occasions and glitter animation generators.</p>
</div>
</div>
<div id="body">
<div id="mainContent">
<!-- Middle Content Begins-->
<div id="middleContent">
<div class="box">
<div class="cb">
<div class="pad">We respect your privacy and will not save your or your friend's email address.</div>
</div></div>

<div class="frmCalculator">
<div id="mailWrapper">
<div class="lnks"><a href="/email.php?mode=import&amp;ref=<?= $this->data['ref']; ?>" class="insertContactsLink">Import Address Book</a> | <a href="/email.php?mode=manual&amp;ref=<?= $this->data['ref']; ?>" class="insertContactsLink">Enter e-mails manually</a></div>
<?php $this->printMessage();?>
<?php if($this->messages->status === 'exit'):?>
	<!-- Do Nothing -->
<?php elseif($this->data['mode'] == 'manual'): ?>
	<form method="post" action="/email.php">
        <table class="tblForm" cellspacing="0">
        <tr class="title"><td>&nbsp;</td><td>Friends Name</td><td>Friends Email</td></tr>
        <?php foreach($this->data['friendName'] as $idx => $name):?>
            <tr><td>#<?=($idx+1)?></td><td><input type="text" class="inputField" name="friendName[<?=$idx?>]" value="<?=$name?>"/></td><td><input class="inputField" type="text" name="friendEmail[<?=$idx?>]" value="<?=$this->data['friendEmail'][$idx]?>" /></td></tr>
        <?php endforeach;?>
        <tr><td colspan="3">
        	<input type="hidden" name="ref" value="<?=$this->data['ref']?>" />
            <input type="hidden" name="mode" value="manual" />
            <input type="hidden" name="p" value="1" />
            <input class="button" type="submit" value="Continue" /></td></tr>
        </table>
        <p><a id="btnAddEmail" class="addQuestion" href="#">Add Another Friend</a></p>
    </form>
<?php elseif($this->data['mode'] == 'import'): ?>
	<form method="post" action="/email.php">
    	<p>Enter your username and password to import contacts from your address book.</p>
    	<table class="tblForm" style="width:265px;" cellspacing="0">
        <tr class="title"><td colspan="2">Import Address Book</td></tr>
        <tr><td>Email</td><td><input type="text" name="userEmail"  value="<?= $this->data['userEmail'];  ?>" class="inputField" /></td></tr>
        <tr><td>Password</td><td><input type="password" name="userPassword" class="inputField" /></td></tr>
        <tr><td>Provider</td><td><select name="provider" class="inputField">
        <?php
		foreach ($this->data['oi_services'] as $type => $providers):
			foreach ($providers as $service=>$details):
				echo "<option value='$service' ", (($service === $this->data['provider']) ? 'selected="selected"' : ''), ">{$details['name']}</option>";
			endforeach;
		endforeach;
		?>
        </select></td></tr>
        <tr><td colspan="2"><input type="hidden" name="ref" value="<?= $this->data['ref']; ?>" />
        	<input type="hidden" name="mode" value="import" /><input type="hidden" name="p" value="1" />
            <input type="submit" value="Get Contacts" class="button" /></td></tr>
        </table>
    	<p class="info">We will not store your username and/or password on our server for any purpose. The provided details will only be used for importing your contacts. Click <a href="/privacy.htm">here</a> to view our <a href="/privacy.htm">privacy policy</a>.</p>
    </form>
<?php elseif($this->data['mode'] == 'select'): ?>
	<form method="post" action="/email.php">
        <table class="tblForm" cellspacing="0">
    	<tr class="title"><td id="checkAll"><input type='checkbox' /></td><td>Imported Contacts</td></tr>
    	<tr class="odd"><td colspan="2"><input class="inputField" id="addrFilter" value="" /></td></tr>
		<?php 
		$i = 0;
		foreach($this->data['contacts'] as $mailId => $name):?>
            <tr class="<?=(($i++ % 2 === 0) ? 'odd' : 'even')?>"><td><input type='checkbox' id='arSelContacts[<?=$mailId?>]' name='arSelContacts[<?=$mailId?>]' value="<?=$name?>" /></td><td><label for="arSelContacts[<?=$mailId?>]"><span class="name"><?=$name?></span>, &lt; <span class="email"><?=$mailId?></span> &gt;</label></td></tr>
        <?php endforeach; ?>
        <tr class="<?=(($i++ % 2 === 0) ? 'odd' : 'even')?>"><td colspan="2"><input type="hidden" name="ref" value="<?=$this->data['ref']; ?>" /><input type="hidden" name="data" value="<?=$this->data['data']?>" /><input type="hidden" name="mode" value="import" /><input type="hidden" name="p" value="2" /><input class="button" type="submit" value="Add Selected Contacts" /></td></tr>
        </table>
    </form>
<?php elseif($this->data['mode'] == 'send'): 
	$verified = isset($this->data['verified']) ? $this->data['verified'] : '';
?>
	<form id="frmSend" method="post" action="/email.php">
        <div class="cb" style="height:50px;"><b>To : </b><p class="pad"><?= htmlentities($this->data['toEmailString']); ?></p></div>
        <?php if($this->data['edit'] == 'allow'): ?>
            <div style="font:normal 12px Arial, Helvetica, sans-serif;margin:5px;">Enter Your Message Below<br/><textarea name="message" rows="5" cols="45"><?= $this->data['message']; ?></textarea></div>
        <?php else: ?>
            <div class="cb"><b>Message : </b><div class="pad"><?=$this->data['message']?></div></div>
        <?php endif; ?>
        <div class="cb">
        <div class="pad">
    		<label><b>Your Name</b><input type="text" name="fromName" value="<?= $this->data['fromName']; ?>" class="inputField" /></label>
        	<label><b>Your Email</b><input type="text" name="fromEmail" value="<?= $this->data['fromEmail']; ?>" class="inputField" /></label>
			<?php if(!$verified): ?><?= recaptcha_get_html(RECAPTCHA_PUBLIC_KEY, ($verified === '') ? '' : 'incorrect-captcha-sol' ); ?>
            <?php else: ?><input type="hidden" name="recaptcha_challenge_field" value="<?=$this->data['recaptcha_challenge_field']?>" /><input type="hidden" name="recaptcha_response_field" value="<?=$this->data['recaptcha_response_field']?>" />
            <?php endif; ?>
        </div></div>
        <div style="text-align:center;margin-top:5px;"><input type="hidden" name="data" value="<?=$this->data['data']?>" /><input type="hidden" name="mode" value="<?=$this->data['mode']?>" /><input type="hidden" name="p" value="1" /><input type="submit" value="Send Mail" /></div>
    </form>
<?php endif; ?>
</div>
</div>


</div>
<!-- Middle Content Ends-->
<!-- Right Content Begins-->
<div id="rightContent">
<div class="box">
<div class="cb">
<span class="tb blue">&nbsp;</span>
<div class="gAd1"><script type="text/javascript"><!--
google_ad_client = "ca-pub-2107538958244175";
/* [MB] Right Content 300x250 */
google_ad_slot = "1865891229";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>
</div>
</div>
<div class="box">
<div class="cb bluebox">
<span class="tb blue">&nbsp;</span>
<span class="caption">Malayalam Scrap, Glitters</span>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
</div>
</div>
<div class="clearer"></div>
</div>
<!-- Right Content Ends-->
<!-- Left Content Begins-->
<?php $this->render('leftcontent')?>
<!-- Left Content Ends-->
<?php $this->render('footer')?>
<?php exit;?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Email your friend</title>
<?php
$this->printHeader();
?>
</head>
<body>
<div id="wrapper">
<?php $this->render('tpl_header');?>
<div id="body">
<div id="mainContainer">
<!-- Middle Content Begins-->
<div id="middleContainer">
<div class="cb">
<div id="b1" class="pad minHeight">
<h1>Email to friends</h1>
<div class="frmCalculator">
<div id="mailWrapper">
<div class="lnks"><a href="/email.php?mode=import&amp;ref=<?= $this->data['ref']; ?>" class="insertContactsLink">Import Address Book</a> | <a href="/email.php?mode=manual&amp;ref=<?= $this->data['ref']; ?>" class="insertContactsLink">Enter e-mails manually</a></div>
<?php $this->printMessage();?>
<?php if($this->messages->status === 'exit'):?>
	<!-- Do Nothing -->
<?php elseif($this->data['mode'] == 'manual'): ?>
	<form method="post" action="/email.php">
        <table class="tblForm" cellspacing="0">
        <tr class="title"><td>&nbsp;</td><td>Friends Name</td><td>Friends Email</td></tr>
        <?php foreach($this->data['friendName'] as $idx => $name):?>
            <tr><td>#<?=($idx+1)?></td><td><input type="text" class="inputField" name="friendName[<?=$idx?>]" value="<?=$name?>"/></td><td><input class="inputField" type="text" name="friendEmail[<?=$idx?>]" value="<?=$this->data['friendEmail'][$idx]?>" /></td></tr>
        <?php endforeach;?>
        <tr><td colspan="3">
        	<input type="hidden" name="ref" value="<?=$this->data['ref']?>" />
            <input type="hidden" name="mode" value="manual" />
            <input type="hidden" name="p" value="1" />
            <input class="button" type="submit" value="Continue" /></td></tr>
        </table>
        <a id="btnAddEmail" class="addQuestion" href="#">Add Another Friend</a>
    </form>
<?php elseif($this->data['mode'] == 'import'): ?>
	<form method="post" action="/email.php">
    	<p>Enter your username and password to import contacts from your address book.</p>
    	<table class="tblForm" style="width:265px;" cellspacing="0">
        <tr class="title"><td colspan="2">Import Address Book</td></tr>
        <tr><td>Email</td><td><input type="text" name="userEmail"  value="<?= $this->data['userEmail'];  ?>" class="inputField" /></td></tr>
        <tr><td>Password</td><td><input type="password" name="userPassword" class="inputField" /></td></tr>
        <tr><td>Provider</td><td><select name="provider" class="inputField">
        <?php
		foreach ($this->data['oi_services'] as $type => $providers):
			foreach ($providers as $service=>$details):
				echo "<option value='$service' ", (($service === $this->data['provider']) ? 'selected="selected"' : ''), ">{$details['name']}</option>";
			endforeach;
		endforeach;
		?>
        </select></td></tr>
        <tr><td colspan="2"><input type="hidden" name="ref" value="<?= $this->data['ref']; ?>" />
        	<input type="hidden" name="mode" value="import" /><input type="hidden" name="p" value="1" />
            <input type="submit" value="Get Contacts" class="button" /></td></tr>
        </table>
    	<p class="info">We will not store your username and/or password on our server for any purpose. The provided details will only be used for importing your contacts. Click <a href="/privacy.htm">here</a> to view our <a href="/privacy.htm">privacy policy</a>.</p>
    </form>
<?php elseif($this->data['mode'] == 'select'): ?>
	<form method="post" action="/email.php">
        <table class="tblForm" cellspacing="0">
    	<tr class="title"><td id="checkAll"><input type='checkbox' /></td><td>Imported Contacts</td></tr>
    	<tr class="odd"><td colspan="2"><input class="inputField" id="addrFilter" value="" /></td></tr>
		<?php 
		$i = 0;
		foreach($this->data['contacts'] as $mailId => $name):?>
            <tr class="<?=(($i++ % 2 === 0) ? 'odd' : 'even')?>"><td><input type='checkbox' id='arSelContacts[<?=$mailId?>]' name='arSelContacts[<?=$mailId?>]' value="<?=$name?>" /></td><td><label for="arSelContacts[<?=$mailId?>]"><span class="name"><?=$name?></span>, &lt; <span class="email"><?=$mailId?></span> &gt;</label></td></tr>
        <?php endforeach; ?>
        <tr class="<?=(($i++ % 2 === 0) ? 'odd' : 'even')?>"><td colspan="2"><input type="hidden" name="ref" value="<?=$this->data['ref']; ?>" /><input type="hidden" name="data" value="<?=$this->data['data']?>" /><input type="hidden" name="mode" value="import" /><input type="hidden" name="p" value="2" /><input type="submit" value="Add Selected Contacts" /></td></tr>
        </table>
    </form>
<?php elseif($this->data['mode'] == 'send'): 
	$verified = isset($this->data['verified']) ? $this->data['verified'] : '';
?>
	<form id="frmSend" method="post" action="/email.php">
        <div class="cb" style="height:50px;"><b>To : </b><p class="pad"><?= htmlentities($this->data['toEmailString']); ?></p></div>
        <?php if($this->data['edit'] == 'allow'): ?>
            <div style="font:normal 12px Arial, Helvetica, sans-serif;margin:5px;">Enter Your Message Below<br/><textarea name="message" rows="5" cols="45"><?= $this->data['message']; ?></textarea></div>
        <?php else: ?>
            <div class="cb"><b>Message : </b><div class="pad"><?=$this->data['message']?></div></div>
        <?php endif; ?>
        <div class="cb">
        <div class="pad">
    		<label><b>Your Name</b><input type="text" name="fromName" value="<?= $this->data['fromName']; ?>" class="inputField" /></label>
        	<label><b>Your Email</b><input type="text" name="fromEmail" value="<?= $this->data['fromEmail']; ?>" class="inputField" /></label>
			<?php if(!$verified): ?><?= recaptcha_get_html(RECAPTCHA_PUBLIC_KEY, ($verified === '') ? '' : 'incorrect-captcha-sol' ); ?>
            <?php else: ?><input type="hidden" name="recaptcha_challenge_field" value="<?=$this->data['recaptcha_challenge_field']?>" /><input type="hidden" name="recaptcha_response_field" value="<?=$this->data['recaptcha_response_field']?>" />
            <?php endif; ?>
        </div></div>
        <div style="text-align:center;margin-top:5px;"><input type="hidden" name="data" value="<?=$this->data['data']?>" /><input type="hidden" name="mode" value="<?=$this->data['mode']?>" /><input type="hidden" name="p" value="1" /><input type="submit" value="Send Mail" /></div>
    </form>
<?php endif; ?>
</div>
</div>
</div></div>

</div>
<!-- Middle Content Ends-->
<?php $this->render('tpl_meter_rightcontent');?>
<div class="clearer">&nbsp;</div>
</div>
<?php $this->render('tpl_leftcontent');?>
<?php $this->render('tpl_footer');?>