Hello <?=$name?>,
 
We received a password reset request at MalluBar. If you did not initiate the request, please ignore this mail. This request will auto expire in 72 hours.

You can set a new password for your account at the following url
http://www.mallubar.com/members/password.php?code=<?=$code?>&username=<?=$username?>&action=reset
 
Alternatively, you may also visit http://www.mallubar.com/members/password.php?action=reset and enter the following code
-------------------------------
Username : <?=$username?>

Verification Code : <?=$code?>

-------------------------------

If you need any assistance, feel free to contact us at contact@mallubar.com.

Yours,
MalluBar Team