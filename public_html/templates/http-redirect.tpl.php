<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Redirecting...</title>
<style type="text/css">
body {padding-top: 40px;background-color: #E6E1C1;color: #333333;font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 14px;line-height: 22px;margin: 0;}
.container {width: 500px;margin:auto;}
.container > .content {background-color: #fff;padding: 20px;margin: 0 -20px;-webkit-border-radius: 10px 10px 10px 10px;-moz-border-radius: 10px 10px 10px 10px;border-radius: 10px 10px 10px 10px;-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);-moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);box-shadow: 0 1px 2px rgba(0,0,0,.15);}
.btn, .button {-moz-border-bottom-colors: none;-moz-border-left-colors: none;-moz-border-right-colors: none;-moz-border-top-colors: none;background-color: #F5F5F5;background-image: linear-gradient(to bottom, #FFF, #E6E6E6);background-repeat: repeat-x;border-color: #BBB #BBB #A2A2A2;border-image: none;border-radius: 2px 2px 2px 2px;border-style: solid;border-width: 1px;color: #333;cursor: pointer;display: inline-block;font-size: 14px;line-height: 22px;margin-bottom: 0;padding: 4px 10px;text-align: center;text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);vertical-align: middle;background-color: #0022CC;color: #fff;}
.btn[disabled]{background-image: none;box-shadow: none;cursor: default;opacity: 0.65;}
.tc {text-align:center;}
code {background-color: #F7F7F9;border: 1px solid #E1E1E8;color: #DD1144;padding: 2px 4px;white-space: nowrap;border-radius: 3px 3px 3px 3px;font-family: Monaco,Menlo,Consolas,"Courier New",monospace;font-size: 12px;}
</style>
</head>
<body>
<div class="container">
<div class="content">
<div class="section">
	<div class="page-header">
		<h2>Redirecting...</h2>
	</div>
	<p>You will be taken back to the previous page. Click &quot;<code>Continue</code>&quot;, if you are not automatically redirected.</p>
	<form id="frmRedirect" method="post" action="<?=$action?>">
		<div class="tc">
			<?php foreach ($params as $key => $val):?>
				<?php if (is_array($val)):?>
					<?php foreach ($val as $k => $v):?>
						<input type="hidden" name="<?=htmlspecialchars("{$key}[$k]")?>" value="<?=htmlspecialchars($v)?>">
					<?php endforeach;?>
				<?php else:?>
					<input type="hidden" name="<?=htmlspecialchars($key)?>" value="<?=htmlspecialchars($val)?>">
				<?php endif;?>
			<?php endforeach;?>
			<input id="btnContinue" class="btn btn-primary" type="submit" value="Continue">
		</div>
	</form>
	<script>
	document.forms[0].submit();
	document.getElementById('btnContinue').disabled = true;
	</script>
</div>
</div>
</div>
</body>
</html>